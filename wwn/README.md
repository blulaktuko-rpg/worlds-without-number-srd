[Go back to index](../index.html)

# Worlds Without Number SRD

This is a cleaned-up version of the WWN SRD. You can find the original in PDF here: [https://www.drivethrurpg.com/product/473939/Worlds-Without-Number-System-Reference-Document](https://www.drivethrurpg.com/product/473939/Worlds-Without-Number-System-Reference-Document).

**This document is based on version 1.0**.

The document has the same license as the SRD (as per itself written in the document) and includes adapted text, to text/Markdown, from the original SRD by Sine Nomine Publishing. Any mistake in the document is probably mine and not Kevin's or anyone else's.

# Links

- [Markdown version](srd.md)
- [HTML (one page)](srd.html)
- [HTML (several pages)](srd/index.html)

# Notable Changes

- The tables for Necromancer Arts follow the numbers in the base PDF book, instead of the SRD (which are different).

# Updates

- 2025-01-09: Style updates and markdown changed to [semantic linefeeds](https://rhodesmill.org/brandon/2012/one-sentence-per-line/) style

# Corrections

Feel free to [create issues](https://gitlab.com/blulaktuko-rpg/rpg-blulaktuko-net/-/issues) (you'll need a free GitLab account), new Pull Requests, or contact me in case you find errors. You can also contact me writing an email to `rpg at blulaktuko.net`.

The text was automatically transformed from the original PDF to text, and then cleaned-up using some automations. It's very probable there's errors here and there.

# Resources and Support

The first two entries in the [WWN subreddit](https://www..reddit.com/r/WWN/) include sources and a link to the most popular Discord. During gameplay or setting things up, in particular, I recommend to check the [Latter Earth](http://latter.earth/) page.

I'd encourage you to support the game and the community. A way of doing so is buying products from the publisher, Sine Nomine Publishing:

- [Directly](https://sine-nomine-publishing.myshopify.com/)
- At [DrivethruRPG](https://www.drivethrurpg.com/browse/pub/3482/Sine-Nomine-Publishing)
- Follow them at [Kickstarter](https://www.kickstarter.com/profile/sinenomineinc), where sometimes projects are funded
