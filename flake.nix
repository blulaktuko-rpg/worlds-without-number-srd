{
  description = "RPG Blulaktuko";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShell =
          let
            inherit (nixpkgs.lib) optional optionals;

          in
          pkgs.mkShell {
            buildInputs = with pkgs; [
              entr
              nodejs # bash-ls needs it
              nodePackages.bash-language-server
              nodePackages.prettier
              shellcheck
              nixfmt-rfc-style
              nix-prefetch-git
              nil
            ];
          };
      }
    );
}
