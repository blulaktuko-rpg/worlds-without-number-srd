# An Introduction

The document that follows is intended as a reference for game designers who wish to produce role-playing games using the mechanics of the Cities Without Number cyberpunk RPG.
It is not intended to substitute for the free PDF version of the game as a player resource or other reference, so the text included here is largely devoid of examples or other instructional text and it is not written to be convenient as a table reference.

Instead, it is meant to give other RPG designers an explicit document showing exactly what I consider to be freely-usable game mechanics or trivially implemented descriptive text.

Regarding this document, its copyright and related rights are waived via CC0, with a summary of the waiver at https://creativecommons.org/publicdomain/zero/1.0/ and the legal text itself at https://creativecommons.org/publicdomain/zero/1.0/ legalcode.

While I cannot offer legal advice to the reader, I am providing this introduction as a plain English description of my position.

## The Reason For The Document

To my understanding, the only right that the following document confers that a user did not already have is the right to use its text verbatim in a commercial product.
As game mechanics cannot be copyrighted, I have always felt it fair and reasonable for other designers to duplicate the mechanics of my games for their own ends, and the following document simply specifies clearly what I consider to be those unprotected mechanics.

Along with these mechanics, trivially-derived ideas such as the damage dice of weapons, the particular layout of tables, and other details of implementation are laid out so as to make clear that I claim no legal or moral right to restrict their use.

Unlike many other designers, I have been made prosperous by indie RPG publishing.
I feel a certain degree of obligation derived from that success, and so I wish to provide the following document to assist others in producing profitable new creations for their own benefit. The reader is invited to copy, derive, change, or expand whatever they wish from the following document, and may rest assured of my encouragement and good wishes for their commercial success.

## A Note On Approvals

As the following document is affirmed under CC0 terms, I have absolutely no right to edit, approve, police, or restrict materials created with it.
While I might have personal opinions regarding products derived from the document, their existence may in no way be represented as official or approved Sine Nomine Publishing products, nor should my endorsement be assumed for anything created. Designers may create what they wish, and I neither have nor desire any ability to control that.

## What You Can Do

While certain uses are implicit in a CC0 affirmation, I list some of them out here in plain English for the sake of clarity.
These uses apply to both personal and commercial uses of the following document, whether for tabletop RPGs, VTTs, online utilities, computer games, or any other work.

- You may copy verbatim any text in the following document for your own products, including tables and their contents.
- You may copy, derive, modify, or expand any content in the following document in your own products.
- You may publish products derived from the following document without crediting either me or this document, as I waive all moral rights to it to the extent permissible by law.
- You do not need to release your products under a CC0 affirmation or any other license.
- You may explicitly advertise your products as being compatible with **Cities Without Number** or other Sine Nomine RPGs if, in your opinion, the product is compatible.
- You may not represent your products as “official” Sine Nomine offerings or as otherwise licensed, produced, associated with, or approved by Sine Nomine Publishing.
- You may not replicate text or setting material included in the **Cities Without Number** free or deluxe game but not included in the following document.
Setting details such as the default setting, the the specific megacorps, or particular NPCs, places, or events are reserved as intellectual property and have been intentionally omitted from the following text, as have the bulk of the GM tools and other non-trivial creative content. These elements are actual creative works, and so I do not consider them free for common public use. Note that this does not apply to the basic concepts involved in the GM tools; while I keep my world tag text to myself, for example, I have no objection to someone else writing their own tags with the same enemy/friend/complication/thing/place structure, or creating one-roll tables with their own text.

_Kevin Crawford_<br>
_9.8.23_

# 1.0.0 Character Creation

## 1.1.0 Attributes
A character has six attributes ranging from 3 to 18, reflecting a range from the minimum viable capacity for a playable character to the maximum normal human level.

Three of these attributes are physical.

- **Strength**, reflecting physical prowess, melee combat, carrying gear, and brute force.
- **Dexterity**, reflecting speed, evasion, manual dexterity, reaction time, and combat initiative.
- **Constitution**, reflecting hardiness, enduring injury, and tolerating large amounts of cyberware.

Three are mental attributes.

- **Intelligence**, reflecting memory, reasoning, technical skills, and general education.
- **Wisdom**, reflecting noticing things, making judgments, reading situations, and intuition.
- **Charisma**, reflecting commanding, charming, attracting attention, and being taken seriously.

NPCs do not normally have attributes.
If necessary, the GM can choose them as appropriate, but usually they are assumed to have average scores if it's ever relevant.

### 1.1.1 Generating Attributes

To generate their six attributes, the player rolls 3d6 in order, once for each attribute.
At any point before section 1.5.0 in character creation the player may substitute a score of 14 for one rolled score.

Optionally, a player may choose to assign their stats from the following array of numbers: 14, 12, 11, 10, 9, and 7, divided up as desired among the attributes.
If an array is used a score may not be substituted with a 14 later.

### 1.1.2 Attribute Modifiers

Each attribute has a modifier, usually ranging from -2 to +2.
This modifier is added to skill checks, attack rolls, damage rolls, Shock damage, and the relevant saving throw targets.

An attribute score of 3 has a modifier of -2.
A score of 4-7 has a modifier of -1. A score of 8-13 has a modifier of +0. A score of 14-17 has a modifier of +1. A score of 18 has a modifier of +2.

Some Foci and cyberware may add bonuses or penalties to an attribute's base modifier.
Such bonuses or penalties cannot increase the modifier above +2 or below -2 unless explicitly indicated. Some injuries or character advancements may alter an attribute score; this new score may change the attribute's modifier.

## 1.2.0 Skills

A character's skills are the PC's learned abilities.
A newly-created character starts with a few relevant skills and may acquire more as they advance in level. NPCs do not have individual skills, instead relying on their combat stat line's skill bonus when relevant. See section 2.3.0 for the rules for making skill checks.

### 1.2.1 Skill Levels

Skills are rated on a scale between level-0 and level-4.
A character must reach a certain minimum experience level to develop a skill to level-2 or beyond.

| Skill Level |                                                                            |
|-------------|----------------------------------------------------------------------------|
| Level-0     | Basic competence in the skill, such as an ordinary practitioner would have |
| Level-1     | An experienced professional in the skill, clearly better than most         |
| Level-2     | Veteran expert, one respected even by those with considerable experience   |
| Level-3     | Master of the skill, likely one of the best in the city                    |
| Level-4     | Superlative expertise, one of the best in the world                        |

### 1.2.2 Gaining Skills in Character Creation

Characters gain skills from their Backgrounds as described below and from a single free pick as noted in section 1.7.4.
Some Foci also grant particular skills. The first time a skill is picked or given, a character obtains it at level-0. The second time it is picked or given, the skill becomes level-1. The third and further times a skill is picked or given during character creation, the player instead picks any other skill that is not already level-1. No character can begin play with skills above level-1.

### 1.2.3 The Skill List

The following skills are standard to most cyberpunk campaigns.
GMs may add or subtract from this list for specialized settings. Some skills may overlap at points in their application; the character may use either skill at their discretion.

- **Administer**: Manage an organization, handle paperwork, analyze records, and keep an institution functioning on a daily basis.
Roll it for bureaucratic expertise, organizational management, legal knowledge, dealing with government agencies, and understanding how corps really work.
- **Connect**: Find people who can be helpful to your purposes and get them to cooperate with you.
Roll it to make useful connections with others, find people you know, know where to get illicit goods and services, and be familiar with foreign cultures and languages. You can use it in place of Talk for persuading people you find via this skill. Note that the people you meet via Connect are not necessarily inclined to work with you without a good reason; if you want reliably cooperative allies, you need Contacts as described on page 11.
- **Drive**: Drive vehicles, sail ships, fly planes, pilot drones, and perform maintenance and basic repairs on such devices.
A PC’s background may incline them to a particular kind of driving, but with some practice this skill can be applied generally.
- **Exert**: Apply trained speed, strength, or stamina in some feat of physical exertion.
Roll it to run, jump, lift, swim, climb, throw, and so forth. You can use it as a combat skill when throwing things, though it doesn’t qualify as a combat skill for other uses.
- **Fix**: Create and repair devices both simple and complex.
Your PC may be specialized in some field depending on their background, but this skill can be applied generally by most operators. Roll it to fix things, build things, and identify what something is supposed to do.
- **Heal**: Employ medical and psychological treatment for the injured or disturbed.
Roll it to cure diseases, stabilize the critically injured, treat psychological disorders, or diagnose illnesses. It’s also an important skill for implanting and maintaining cyberware.
- **Know**: Know facts about academic or scientific fields.
Roll it to understand academic topics, remember relevant history, solve science mysteries, and know the basic facts about rare or esoteric topics.
- **Lead**: Convince others to also do whatever it is you’re trying to do.
Talk might persuade them that following you is smart, but Lead can make them do it even when they think it’s a bad idea. Roll it to lead troops in combat, convince others to follow you, inspire employee loyalty, or maintain morale and discipline.
- **Notice**: Spot anomalies or interesting facts about your environment.
Roll it for searching places, detecting ambushes, spotting things, and reading the emotional state of other people.
- **Perform**: Exhibit some performance skill.
Roll it to dance, sing, orate, act, or otherwise put on a convincing or emotionally moving performance.
- **Program**: Operating or hacking computing and communications hardware.
Roll it to program or hack computers, control computer-operated hardware, operate communications tech, or decrypt things. Hacker PCs rely heavily on this skill.
- **Punch**: Use it as a combat skill when fighting unarmed.
If your PC means to make a habit of this rather than as a recourse of desperation, you should take the Unarmed Combatant Focus described later.
- **Shoot**: Use it as a combat skill when using ranged weaponry, whether thrown weapons, bows, gunlinked pistols, combat rifles, or heavy artillery.
- **Sneak**: Move without drawing notice.
Roll it for stealth, disguise, infiltration, manual legerdemain, pickpocketing, and the physical defeating of security measures such as electronic locks.
- **Stab**: Use it as a combat skill when wielding melee weapons, whether primitive or complex.
It can also be used when throwing weapons.
- **Survive**: Depending on the character’s background, this might be more a matter of street smarts and urban survival or it might be directed towards outlands bushcraft and wilderness living.
Regardless of the original focus, some time and practice can allow it to be generalized by a PC.
- **Talk**: Convince other people of the facts you want them to believe.
What they do with that conviction may not be completely predictable. Roll it to persuade, charm, or deceive others in conversation.
- **Trade**: Find what you need on the market and sell what you have.
Roll it to sell or buy things, figure out where to purchase hard-to-get or illicit goods, deal with customs agents, or run a business.
- **Work**: This is a catch-all skill for professions not represented by other skills.
When you take it, pick a particular profession, such as lawyering, stevedore work, painting, or some other career. The skill then applies to performing that work or making contacts with people in its sphere.

Two further skills exist for campaigns that involve magic.
Any character may develop these skills as an intellectual exercise, but only those with the Spellcaster Edge or Summoner Edge may actually use them to create magical effects.

- **Cast**: Cast spells, know about magic, know about important magical events.
- **Summon**: Summon spirits, know about magic, know about spirits and supernatural beings.

## 1.3.0 Backgrounds

Every character has a Background, a past reflecting their career before they took up adventuring.
A background may be chosen from the list below or a new one made up with GM permission.

### 1.3.1 Background Skills

When a Background is chosen, a PC immediately gets its free skill at level-0.
At that point, the player decides whether to gain further skills randomly or to pick specific choices.

If they choose randomly, they may make three rolls divided between the Growth and Learning tables of their background in any way they wish, including taking all three from just one table.
If they pick specific choices they can pick any two skills from the Learning table, including picking the same one twice to raise it to level-1. They may not pick the "Any Skill" option, if it exists.

A skill pick of "Any Combat" means the player can pick Shoot, Stab, or Punch.
A skill roll of "Any Skill" means they can pick any skill from the list above. Some campaigns may involve special magical or psychic skills; these cannot be chose with the Any Skill pick.

If an attribute bonus is rolled, such as "+2 Physical", the player may apply it to any physical attribute or split the bonus between two physical attributes.
The same principle applies to Mental attribute increases. "Any Stat" increases may be applied to either physical or mental attributes. No attribute can be raised above 18.

### 1.3.2 New Backgrounds

To create a new background, the player describes it to the GM and picks an existing background table that best fits the concept.
Assuming the GM approves it, they may then roll or pick as usual. If no existing background table fits, they may make a new one with the GM's permission.

### 1.3.3 Background List

The following backgrounds are examples of those possible for a player.
The free granted skill is listed after each background's name.


#### Bum
Survive

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Any Combat |
| 2  | +2 Physical | 2  | Survive    |
| 3  | +2 Physical | 3  | Connect    |
| 4  | +2 Mental   | 4  | Sneak      |
| 5  | Survive     | 5  | Notice     |
| 6  | Any Skill   | 6  | Talk       |
|    |             | 7  | Fix        |
|    |             | 8  | Trade      |


#### Bureaucrat

Administer

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Administer |
| 2  | +2 Mental   | 2  | Know       |
| 3  | +2 Mental   | 3  | Talk       |
| 4  | +2 Mental   | 4  | Program    |
| 5  | Administer  | 5  | Lead       |
| 6  | Any Skill   | 6  | Trade      |
|    |             | 7  | Notice     |
|    |             | 8  | Any Skill  |


#### Clergy

Lead

| d6 | Growth      | d8 | Learning  |
|----|-------------|----|-----------|
| 1  | +1 Any Stat | 1  | Lead      |
| 2  | +2 Mental   | 2  | Talk      |
| 3  | +2 Mental   | 3  | Connect   |
| 4  | +2 Mental   | 4  | Perform   |
| 5  | Connect     | 5  | Know      |
| 6  | Any Skill   | 6  | Heal      |
|    |             | 7  | Notice    |
|    |             | 8  | Any Skill |


#### Coder

Program

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Program    |
| 2  | +2 Mental   | 2  | Administer |
| 3  | +2 Mental   | 3  | Fix        |
| 4  | +2 Mental   | 4  | Talk       |
| 5  | Program     | 5  | Connect    |
| 6  | Any Skill   | 6  | Know       |
|    |             | 7  | Notice     |
|    |             | 8  | Any Skill  |


#### Corp Security

Any Combat

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Any Combat |
| 2  | +2 Physical | 2  | Administer |
| 3  | +2 Physical | 3  | Connect    |
| 4  | +2 Physical | 4  | Notice     |
| 5  | Any Combat  | 5  | Exert      |
| 6  | Any Skill   | 6  | Heal       |
|    |             | 7  | Sneak      |
|    |             | 8  | Lead       |


#### Corper

Work

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Work       |
| 2  | +1 Any Stat | 2  | Talk       |
| 3  | +2 Physical | 3  | Connect    |
| 4  | +2 Mental   | 4  | Any Combat |
| 5  | Any Skill   | 5  | Notice     |
| 6  | Any Skill   | 6  | Sneak      |
|    |             | 7  | Fix        |
|    |             | 8  | Any Skill  |


#### Criminal

Sneak

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Any Combat |
| 2  | +1 Any Stat | 2  | Sneak      |
| 3  | +2 Physical | 3  | Notice     |
| 4  | +2 Mental   | 4  | Connect    |
| 5  | Sneak       | 5  | Talk       |
| 6  | Any Skill   | 6  | Survive    |
|    |             | 7  | Exert      |
|    |             | 8  | Lead       |


#### Doctor

Heal

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Heal       |
| 2  | +2 Physical | 2  | Notice     |
| 3  | +2 Mental   | 3  | Know       |
| 4  | +2 Mental   | 4  | Talk       |
| 5  | Heal        | 5  | Administer |
| 6  | Any Skill   | 6  | Connect    |
|    |             | 7  | Fix        |
|    |             | 8  | Any Skill  |


#### Drone Jockey

Drive

| d6 | Growth      | d8 | Learning  |
|----|-------------|----|-----------|
| 1  | +1 Any Stat | 1  | Drive     |
| 2  | +2 Physical | 2  | Fix       |
| 3  | +2 Physical | 3  | Shoot     |
| 4  | +2 Mental   | 4  | Notice    |
| 5  | Drive       | 5  | Sneak     |
| 6  | Any Skill   | 6  | Exert     |
|    |             | 7  | Program   |
|    |             | 8  | Any Skill |


#### Ganger

Any Combat

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Any Combat |
| 2  | +2 Physical | 2  | Connect    |
| 3  | +2 Physical | 3  | Survive    |
| 4  | +2 Mental   | 4  | Notice     |
| 5  | Any Combat  | 5  | Sneak      |
| 6  | Any Skill   | 6  | Talk       |
|    |             | 7  | Fix        |
|    |             | 8  | Any Skill  |


#### Laborer

Exert

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Exert      |
| 2  | +2 Physical | 2  | Work       |
| 3  | +2 Physical | 3  | Sneak      |
| 4  | +2 Physical | 4  | Trade      |
| 5  | Exert       | 5  | Survive    |
| 6  | Any Skill   | 6  | Fix        |
|    |             | 7  | Any Combat |
|    |             | 8  | Any Skill  |


#### Law Enforcement

Administer

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Administer |
| 2  | +2 Physical | 2  | Any Combat |
| 3  | +2 Physical | 3  | Notice     |
| 4  | +2 Mental   | 4  | Sneak      |
| 5  | Administer  | 5  | Talk       |
| 6  | Any Skill   | 6  | Connect    |
|    |             | 7  | Heal       |
|    |             | 8  | Any Skill  |


#### Manager

Administer

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Administer |
| 2  | +2 Mental   | 2  | Talk       |
| 3  | +2 Mental   | 3  | Trade      |
| 4  | +2 Mental   | 4  | Connect    |
| 5  | Administer  | 5  | Notice     |
| 6  | Any Skill   | 6  | Lead       |
|    |             | 7  | Program    |
|    |             | 8  | Any Skill  |


#### Outlander

Survive

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Survive    |
| 2  | +2 Mental   | 2  | Drive      |
| 3  | +2 Physical | 3  | Fix        |
| 4  | +2 Physical | 4  | Trade      |
| 5  | Survive     | 5  | Sneak      |
| 6  | Any Skill   | 6  | Connect    |
|    |             | 7  | Any Combat |
|    |             | 8  | Any Skill  |


#### Performer

Perform

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Perform    |
| 2  | +1 Any Stat | 2  | Lead       |
| 3  | +2 Mental   | 3  | Connect    |
| 4  | +2 Physical | 4  | Talk       |
| 5  | Perform     | 5  | Trade      |
| 6  | Any Skill   | 6  | Sneak      |
|    |             | 7  | Any Combat |
|    |             | 8  | Any Skill  |


#### Soldier

Any Combat

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Any Combat |
| 2  | +2 Mental   | 2  | Exert      |
| 3  | +2 Physical | 3  | Lead       |
| 4  | +2 Physical | 4  | Notice     |
| 5  | Any Combat  | 5  | Survive    |
| 6  | Any Skill   | 6  | Heal       |
|    |             | 7  | Administer |
|    |             | 8  | Any Skill  |


#### Spy

Sneak

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Sneak      |
| 2  | +1 Any Stat | 2  | Notice     |
| 3  | +2 Mental   | 3  | Talk       |
| 4  | +2 Physical | 4  | Connect    |
| 5  | Sneak       | 5  | Program    |
| 6  | Any Skill   | 6  | Exert      |
|    |             | 7  | Any Combat |
|    |             | 8  | Any Skill  |


#### Trader

Trade

| d6 | Growth      | d8 | Learning   |
|----|-------------|----|------------|
| 1  | +1 Any Stat | 1  | Trade      |
| 2  | +2 Mental   | 2  | Notice     |
| 3  | +2 Mental   | 3  | Connect    |
| 4  | +2 Physical | 4  | Fix        |
| 5  | Trade       | 5  | Talk       |
| 6  | Any Skill   | 6  | Administer |
|    |             | 7  | Any Combat |
|    |             | 8  | Any Skill  |


#### Streetwalker

Talk

| d6 | Growth      | d8 | Learning  |
|----|-------------|----|-----------|
| 1  | +1 Any Stat | 1  | Talk      |
| 2  | +1 Any Stat | 2  | Perform   |
| 3  | +2 Physical | 3  | Connect   |
| 4  | +2 Mental   | 4  | Trade     |
| 5  | Talk        | 5  | Sneak     |
| 6  | Any Skill   | 6  | Notice    |
|    |             | 7  | Heal      |
|    |             | 8  | Any Skill |


#### Technician

Fix

| d6 | Growth      | d8 | Learning  |
|----|-------------|----|-----------|
| 1  | +1 Any Stat | 1  | Fix       |
| 2  | +2 Physical | 2  | Program   |
| 3  | +2 Mental   | 3  | Exert     |
| 4  | +2 Mental   | 4  | Trade     |
| 5  | Fix         | 5  | Notice    |
| 6  | Any Skill   | 6  | Connect   |
|    |             | 7  | Drive     |
|    |             | 8  | Any Skill |

## 1.4.0 Contacts

Contacts are associates the PC has acquired in their past life or potential new acquaintances they make during play.
Contacts come in two levels: acquaintances and friends. Acquaintances will deal honestly with the PC, do favors for them that don't put them at risk or significant expense, and perform their professional skills on the PC's behalf. Friends will also do as much, but will undertake favors that cause them considerable risk or significant expense. Contacts will not normally go on missions with the PCs.

### 1.4.1 Initial Contacts

A new PC begins play with either two acquaintances or one friend of their choice.
The player should give a name to each, a brief description of their profession, and a sentence or two about how they developed the relationship. Starting Contacts should not be elite figures or major players in the setting, but capable mid-level NPCs who can provide services and favors. Some examples would include a local cyberdoc, a black-market arms dealer, a popular club owner, an HR officer at a local megacorp branch, or similar sorts.

### 1.4.2 Gaining and Losing Contacts

Contacts can be gained and lost during play.
When a PC acts in such a way as would reasonably earn a particular NPC's friendship and trust, the GM may tell the player to note that NPC down as an acquaintance or friend. In the same fashion, if the PC burns or abuses a relationship, it may downgrade a friend to an acquaintance or cost them the connection entirely. Contacts exposed to danger by a request or favor may also be killed if events turn out poorly.

## 1.5.0 Edges

Edges define the main talents that make a PC a viable operator.
They are major capabilities, aptitudes, or professional functions. An Edge can only be selected once unless indicated otherwise. A beginning PC chooses two, and may choose a third once they reach the fifth level of experience.

### 1.5.1 The Underdog Rule

A PC who ends character generation with a attribute modifier total of -1 or less may pick a bonus Edge to reflect the fact that they must have some sort of special talent to have become an operator despite their manifest shortcomings.
Their total attribute modifiers must still sum to -1 or less by the end of character creation to claim this bonus; that means that the Prodigy Edge is unlikely to be permissible, as it would probably increase their modifier total above -1.

In campaigns that involve magical Edges or other special powers as Edges, the Underdog Rule bonus may not be used to pick such Edges.

Characters who take advantage of the Underdog Rule may not increase their attribute scores later by spending skill points during character advancement as indicated in section 2.7.1.5.
Attribute or modifier enhancements from other sources, such as cyberware or newly-picked Foci, do not disqualify a PC.

### 1.5.2 Common Edge List

The Edges below are normal for most cyberpunk campaigns.
GMs may add additional options to support certain character concepts that are unique or specific to their setting.

- **Educated**: You may pick a bonus skill of your choice.
Whenever you gain skill points from character level advancement, you get a bonus skill point.
- **Face**: Gain Connect as a bonus skill.
Once per game week, whenever it’s convenient, gain one temporary Acquaintance Contact of your choice, describing them in any way the GM finds acceptable. You lose touch with this Contact after you use this Edge again, but you can use this Edge to connect with them again at a different time. You may also provide favors or payment sufficient to cement the relationship at the GM’s discretion.
- **Focused**: You begin play with an extra Focus pick.
You may choose this Edge more than once.
- **Ghost**: You are uncannily elusive.
Gain Sneak as a bonus skill and the Fighting Withdrawal combat action is an On Turn action for you. Once per scene, reroll a failed Sneak check related to sneaking or going unseen. Once per game day, as a Move action, move up to 10 meters without anyone around you seeing you move. If you use this movement to get behind cover, you might seem to vanish outright to onlookers.
- **Hacker**: Gain Program as a bonus skill.
You may begin play with an installed Cranial Jack cybersystem, a scrap deck from page 75, and eight program elements of your choice among available Verbs and Subjects. Each round, you gain a bonus Main Action that can only be used to perform hacking or cyberspace-related mental actions. This does not include drone piloting or vehicle driving.
- **Hard To Kill**: Instead of rolling 1d6 per level for your hit points, you roll 1d6+2.
Your base Trauma Target increases by +1, turning the base 6 into 7.
- **Killing Blow**: Gain a combat skill as a bonus skill.
Whenever you inflict hit point damage on something, whether by weapon, Shock, special ability, or any other source, the damage is increased by 1 point per two character levels, rounded up. Any Trauma Die you roll gains a +1 bonus.
- **Masterful Expertise**: Once per scene, as an Instant action, reroll a failed check for a non-combat skill.
- **On Target**: Gain a combat skill as a bonus skill.
Your basic attack bonus is equal to your character level, instead of the usual half level, rounded down.
- **Prodigy**: Pick an attribute other than Constitution; its score becomes 18 and it grants a +3 modifier instead of +2.
Characters benefiting from the Underdog Rule can’t take this Edge.
- **Operator’s Fortune**: You may or may not be good, but you are undeniably lucky.
Once per game session as an Instant action, when something bad happens to you such as an injury, a failed save, or a botched skill check, test your luck and roll 1d6. On a 1, the bad event is unaffected. On a 2-5, you somehow avert the consequences by blind chance. On a 6, it actually lands on an enemy or rival of the GM’s choice, if that’s possible. Only events that happened in this same round can be averted.
- **Veteran’s Luck**: Once per scene, as an Instant action, trigger this ability to gain one of two effects: an attack roll that just hit you is instead treated as a miss, or an attack roll you just made that missed is instead treated as a hit.
This ability can be applied to vehicle weapons used by the operator, but it cannot protect against environmental damage, vehicle crashes, falls, or other harm that doesn’t involve an attack roll.
- **Voice of the People**: You are a rocker, graffiti artist, poet, demagogue, or other rabble-rouser with a considerable reputation.
You gain both levels of the Pop Idol Focus and an additional Friend Contact related to your art.
- **Wired**: You may begin play with up to $200,000 worth of new or secondhand cyber.
If secondhand, you roll once per system for its defect and may end up with unusable junk. You don’t need to pay for installation and its maintenance is covered for the next two months. You can redeem this Edge later in your career by paying double the cost of the cyber you selected; if so, you can trade this Edge in for a different one with the GM’s approval. See page 27 for example packages of cyber you might pick to suit your character’s concept.

### 1.5.3 Magical Edge List

The Edges below are appropriate only to those campaigns that involve magic or supernatural abilities.
They are meant for use with the optional magic rules in section 6.0.0. Characters who are taking advantage of the Underdog Rule cannot use their bonus Edge to pick one of these.

- **Graced**: You have innate magical abilities that enhance your physical or mental prowess.
You gain the Graced powers and hindrances described in section 6.3.0.
- **Spellcaster**: You are a mage, capable of casting spells as described in section 6.1.0and suffering a mage's limits in using cyberware.
- **Summoner**: You are a summoner, capable of calling up spirit servants as described in page 6.2.0, though you also have limits in using cyberware.

## 1.6.0 Foci

Foci are special talents that a PC can possess.
They aren't as powerful as Edges, but a PC will develop more of them over time. Any PC can select any Focus, barring a few with specific requirements; they do not need to fit the Edges they select. So long as the player can explain how or why they acquired the talent they can have it.

Foci usually come in two levels, though some have only one.
The first time a Focus is chosen, the benefits of the first level are gained. The second time it's chosen, the benefits of the second level are added to those of the first.

A new character may choose one Focus, plus one more if they have the Focused Edge.

### 1.6.1 Focus List

#### Ace Driver

If it’s got wheels or wings, you can drive it.
Your background may lend itself to a particular type of transport, but your natural talent lets you operate any vehicle with an almost instinctive aptitude. These Focus benefits do not apply to drone piloting, however.

**Level 1**: Gain Drive as a bonus skill.
You have “acquired” vehicles worth no more than the budget given in section 1.6.2, and can replace lost or destroyed gear at a rate of $10,000 per week. Once per scene, as an Instant action, reroll a failed skill check related to driving or vehicle maintenance and repair.

**Level 2**: Gain Fix as a bonus skill.
The Speed of a vehicle you drive is increased by 1 point. Once per vehicle, you can add a mod to it for free, ignoring money and experimental component costs. Only you can effectively operate this mod, but it requires no Maintenance. You can change this free mod with a week of downtime.

#### Alert
You are keenly aware of your surroundings and virtually impossible to take unaware.
You have an instinctive alacrity of response that helps you act before less wary persons can think to move.

**Level 1**: Gain Notice as a bonus skill.
You cannot be surprised, nor can others use the Execution Attack option on you. When your group rolls initiative, your vigilance allows them to roll twice and take the higher roll.

**Level 2**: In addition to the benefits of level 1, you always act first in a combat round unless someone else involved is also this Alert.

#### All Natural

Your mind and body are superbly gifted, but this very excellence leaves you profoundly incompatible with most cyber.
You cannot accept implants except for minor cosmetic ones with an unmodified System Strain cost of zero. If a GM is mixing rules from multiple Without Number games, this Focus should not be allowed unless cyberware is expected equipment for all PCs. If you’re using the deluxe version rules for magic, PCs with magical or psychic Edges such as Spellcaster, Summoner, or Graced cannot take this Focus.

**Level 1**: Gain any skill as a bonus skill.
Pick an attribute; its modifier increases by +1, up to a maximum of +3. You can make another such attribute pick at levels 3, 5, 7, and 10, choosing the same attribute or a different one. While you can still suffer Traumatic Hits, you never suffer major injuries as described on page 41.

#### Armsmaster

You have an unusual competence with thrown weapons and melee attacks.
This Focus’ benefits do not apply to unarmed attacks or non-thrown projectile weapons. This Focus’ bonuses also don’t stack with Deadeye or other Foci that add a skill’s level to your damage or Shock.

**Level 1**: Gain Stab as a bonus skill.
You can Ready a Stowed melee or thrown weapon as an Instant action. You may add your Stab skill level to a melee or thrown weapon’s damage roll or Shock damage, assuming it has any to begin with.

**Level 2**: The Shock from your melee attacks always treats the target as if they have AC 10.
Gain a +1 bonus to hit with all thrown or melee attacks.

#### Assassin

You are practiced at sudden murder, and have certain advantages in carrying out an Execution Attack as described in the rules in section 2.4.7.3.

**Level 1**: Gain Sneak as a bonus skill.
You can conceal an object no larger than a knife or pistol from anything less invasive than a strip search, including normal weapon detection devices. You can draw or produce this object as an On Turn action, and your point-blank ranged attacks made from surprise with it cannot miss the target.

**Level 2**: You can take a Move action on the same round as you make an Execution Attack, closing rapidly with a target before you attack.
You may split this Move action when making an Execution Attack, taking part of it before you murder your target and part of it afterwards. This movement happens too quickly to alert a victim or to be hindered by bodyguards not directly in your path.

#### Authority

You have an uncanny kind of charisma about you, one that makes others instinctively follow your instructions and further your causes.
Where this Focus refers to followers, it means NPCs who have voluntarily chosen to be in your service. PCs never count as followers.

**Level 1**: Gain Lead as a bonus skill.
Once per day, you can make a request from an NPC who is not openly hostile to you, rolling a Cha/Lead skill check at a difficulty of the NPC’s Morale score. If you succeed, they will comply with the request, provided it is not harmful or very uncharacteristic.

**Level 2**: Those who follow you are fired with confidence.
Any NPC being directly led by you gains a Morale and hit roll bonus equal to your Lead skill and a +1 bonus on all skill checks. Your followers will not act against your interests unless under extreme pressure.

#### Close Combatant

You’ve had all too much practice at close-in fighting and desperate struggles with pistol or blade.
You’re extremely skilled at avoiding injury in melee combat, and at level 2 you can dodge through a melee scrum without fear of being knifed in passing.

**Level 1**: Gain any combat skill as a bonus skill.
You can use pistol-sized ranged weapons in melee without suffering penalties for the proximity of melee attackers. You ignore Shock damage from melee assailants, even if you’re unarmored at the time.

**Level 2**: The Shock damage from your melee attacks treats all targets as if they were AC 10.
The Fighting Withdrawal combat action is treated as an On Turn action for you and can be performed freely.

#### Cyberdoc

Any skilled medic can implant and maintain cyber systems, but you have a special aptitude for the work.

**Level 1**: Gain Fix and Heal as bonus skills.
You start play with a cyberdoc kit, and you can implant cyberware even if your Heal skill is level-0. You gain a +2 bonus on all cyber implant surgery skill checks. If you perform cyber maintenance for a person, the delicacy of your adjustments decreases the total System Strain cost of their implants by one point until their next maintenance interval.

**Level 2**: The quality of your cyber maintenance improves; the System Strain decrease is equal to two points now instead of one.
You never fail to install cyberware correctly. Once per patient, you can build and install a cyber modification to a system as described in section 2.8.2 without any cost in money or experimental components, assuming you have the requisite skill levels to build it.

#### Deadeye

You have a gift with ranged weapons.
While this talent most commonly applies to guns, it is also applicable to thrown weapons or other ranged weapons that can be used with the Shoot skill. For thrown weapons, you can’t use the benefits of the Armsmaster Focus at the same time as Deadeye.

**Level 1**: Gain Shoot as a bonus skill.
You can Ready a Stowed ranged weapon as an Instant action. You may use a rifle or two-handed ranged weapon even when an enemy is within melee range, albeit at a -4 hit penalty. You may add your Shoot skill level to a ranged weapon’s damage roll.

**Level 2**: You can reload guns, crossbows, or other slow-loading weapons as an On Turn action, provided they don’t take more than a round to reload.
You can use ranged weapons of any size in melee without penalty. Once per scene, as an On Turn action when target shooting at an inanimate, non-creature target, you automatically hit unless you roll a 2 on your Shoot skill check or the shot is physically impossible.

#### Diplomat

You know how to get your way in personal negotiations, and can manipulate the attitudes of those around you.
Even so, while smooth words are versatile, they’ll only work if your interlocutor is actually willing to listen to you.

**Level 1**: Gain Talk as a bonus skill.
You speak all the languages common to the city and can learn new ones to a workable level in a week, becoming fluent in a month. Reroll 1s on any skill check dice related to negotiation or diplomacy.

**Level 2**: Once per game session, shift an intelligent NPC’s reaction roll one step closer to friendly if you can talk to them for at least thirty seconds.

#### Drone Pilot

While anyone can drive a drone under casual circumstances, your knack for it is something unusual.

**Level 1**: Gain Drive and Fix as bonus skills.
You acquire or start play with a Remote Control Unit cybersystem and its installation. Through connections, scavenging, and parts repurpose, you have free drones and their weapons and fittings worth no more than the budget given in section 1.6.2. You can repair these drones without needing spare parts, and destroyed drones can be replaced with a week’s work. You always have the equivalent of a drone repair kit on you at no Encumbrance cost.

**Level 2**: You can use the Assume Command drone action once per round as an On Turn action.
Once per scene, gain a bonus Main Action to command a drone. Any drones you control gain a +2 bonus to their hit rolls, whether or not you’re personally firing their weaponry.

#### Expert Programmer

A skilled hacker can program their own utilities, but you take this expertise far beyond the norm.

**Level 1**: Gain Program as a bonus skill.
You can create and maintain an additional number of program elements equal to your character level+2, split among Verbs and Subjects as you see fit as explained in section 4.3.0. You can change your choices with a week’s work. Once per day, as an On Turn action, you can make an on-the-fly edit to a Subject program element to turn it into any other Subject program element you need. The element remains altered until you change it again.

**Level 2**: Your programs are exceedingly efficient.
Any program elements you write take up only half the usual Memory. A cyberdeck you use gains a CPU bonus equal to your Program skill level.

#### Healer

Healing comes naturally to you, and you’re particularly gifted at preventing the quick bleed-out of wounded allies and comrades.

**Level 1**: Gain Heal as a bonus skill.
You may attempt to stabilize one mortally-wounded adjacent person per round as an On Turn action. When rolling Heal skill checks, roll 3d6 and drop the lowest die.

**Level 2**: Pharmaceuticals or other technological healing devices applied by you heal twice as many hit points as normal.
Using only basic medical supplies, you can heal 1d6+Heal skill hit points of damage to every injured or wounded person in your group with ten minutes of first aid spread among them. Such healing adds no System Strain, but can be applied to a given target only once per day.

#### Henchkeeper

You have a distinct knack for picking up lost souls who willingly do your bidding.
You might induce them with promises of money, power, excitement, sex, or some other prize that you may or may not eventually grant. A henchman obtained with this Focus will serve in loyal fashion until clearly betrayed or placed in unacceptable danger. Henchmen are not “important” people, and are usually marginal sorts, criminals, the desperate, or other persons with few options.

You can use more conventional pay or inducements to acquire additional henchmen, but these extra hirelings are no more loyal or competent than your pay and treatment can purchase.

**Level 1**: Gain Lead as a bonus skill.
You can acquire henchmen within 24 hours of arriving in a community, assuming anyone is suitable hench material. These henchmen will not fight except to save their own lives, but will go with you on missions and risk great danger to help you. Most corper henchmen will have 5 HP, a +0 attack bonus, and a Morale of 7, plus whatever gear they’re given. Slum-dwellers and other natives of harsh societies will fight as Street Thugs per page 188 if pressed. You can have one henchmen at a time for every three character levels you have, rounded up. You can release henchmen with no hard feelings at any plausible time and pick them back up later should you be without a current henchman.

**Level 2**: Your henchmen are remarkably loyal and determined, and will fight for you against anything but clearly overwhelming odds.
Whether through natural competence or their devotion to you, they’re treated as a Basic Corp Security from page 186. You can make faithful henchmen out of skilled and highly-capable NPCs, but this requires that you actually have done them some favor or help that would reasonably earn such fierce loyalty.

#### Many Faces

You have multiple usable identities registered with corporate and governmental databases.
These identities are so deeply embedded in the systems that they’re almost impossible to pry out unless you do something to compromise them.

**Level 1**: Gain Sneak as a bonus skill.
You can maintain one alternate identity at a time per three character levels, rounded up. These identities have their own names, backgrounds, criminal records, financial dealings, and bank accounts, and will register as authentic to all normal corporate and governmental checks. If an identity is compromised or you want a different one, you can replace it with a week’s work. False identities cannot be important people or involve corporations you don’t have a Contact in already.

#### Pop Idol

Whether a street musician, graffiti artist, underground journalist, cam girl, folk singer, or Robin Hood-esque thief, you have a devoted following of enthusiasts who are willing to help you when you need them.

**Level 1**: Gain Perform as a bonus skill.
Once per game week, with an hour or so of messaging, you can mobilize about a hundred of your fans to perform some act of your choice, provided it’s no more than mildly criminal or slightly dangerous. Flash mobs, getaway drivers, scouting reports, tailing people, or instant parties might all qualify as services. Your fans don’t have any special skills, but they’ll do anything ordinary workers or civilians could do. If you mobilize them for donations or merch purchases, you get $1,000 per character level, doubled at fifth level and quadrupled at tenth. You can’t mobilize them to buy your content more than once per month.

**Level 2**: You can mobilize up to a hundred fans per character level, though major mobs are likely to draw a law enforcement response.
You’ve cultivated fan leaders who can pass along your wishes deniably, concealing your involvement in the crowd. Your donation and merch earning amounts double. Your Charisma modifier increases by +1, to a maximum of +2.

#### Roamer

You might be a footloose bum with a knack for stowing aboard cargo shipments, a hard-bitten outlander smuggler, or a restless seeker of the horizon.
Either way, you’ve seen more of the world with your own two eyes than any common corper ever will.

**Level 1**: Gain Survive and Drive as bonus skills.
You have conversational skill in all common languages spoken in the region or city, and you never get lost. You have “acquired” one or more vehicles worth no more than the budget given in section 1.6.2. You can replace lost or damaged vehicles at a rate of $10,000 per week.

**Level 2**: Once per scene, as an Instant action, you can reroll a failed skill check related to safe traveling or vehicle operation, whether to fix a blown engine or talk down a ganger who doesn’t like strangers crossing his turf.

#### Safe Haven

You have the contacts and expertise to find safehouses and bolt holes that no one else would think to find.
You know how to persuade landlords into helping you for nebulous future advantages.

**Level 1**: Gain Sneak as a bonus skill.
If you spend a week in a particular neighborhood, you can find or arrange a secure safe house and the on-call assistance of a local cyberdoc or medic willing to perform emergency care for no more than you can afford to pay. This safe house will always go unnoticed unless you are at Heat 8+ or specifically compromise it; even in that case, it will remain undiscovered for at least 24 hours if you can get to it without being followed. If a safe house is burnt, you can find a new one with another week’s work. A PC can’t have more safe houses active at once than their character level.

**Level 2**: Your safe houses are actively protected by the local authorities, be they gang members, paid-off cops, or cooperative corp security.
Provided you don’t make them angry, they’ll defend you from most ordinary degrees of pursuit. You can find safe havens geared with the equivalent of tech workshops or level one cyberclinics.

#### Shocking Assault

You’re extremely dangerous to enemies around you.
The ferocity of your melee attacks stresses and distracts enemies even when your blows don’t draw blood.

**Level 1**: Gain Punch or Stab as a bonus skill.
The Shock damage of your weapon treats all targets as if they were AC 10, assuming your weapon is capable of harming the target in the first place and the target is not immune to Shock.

**Level 2**: In addition, you gain a +2 bonus to the Shock damage rating of all melee weapons and unarmed attacks that do Shock.
As usual, regular hits never do less damage than this Shock would do.

#### Sniper’s Eye

You are an expert at placing a bullet or arrow on an unsuspecting target.
These special benefits only apply when making an Execution Attack with a gun, bow, or thrown weapon, as described in section 2.4.7.3.

**Level 1**: Gain Shoot as a bonus skill.
When making a skill check for a ranged Execution Attack or target shooting, roll 3d6 and drop the lowest die.

**Level 2**: You don’t miss ranged Execution Attacks.
A target hit by one takes a -4 penalty on the Physical saving throw to avoid immediate mortal injury. Even if the save is successful, the target takes double the normal damage inflicted by the attack.

#### Specialist

You are remarkably talented at a particular skill.
Whether a marvelous cat burglar, a natural athlete, a brilliant engineer, or some other savant, your expertise is extremely reliable. You may take this Focus more than once for different skills.

**Level 1**: Gain a non-combat skill as a bonus.
Roll 3d6 and drop the lowest die for all skill checks in this skill.

**Level 2**: Roll 4d6 and drop the two lowest dice for all skill checks in this skill.

#### Tinker

You have a natural talent for modifying and improving equipment, as given in the rules in section 2.8.2.

**Level 1**: Gain Fix as a bonus skill.
Your Maintenance score is doubled, allowing you to maintain twice as many mods. Vehicle, cyber, and gear mods cost only half their usual price in dollars, though experimental component requirements remain the same.

**Level 2**: Your Fix skill is treated as one level higher for purposes of building and maintaining mods and calculating your Maintenance score, up to a maximum of Fix-5.
Advanced mods require one fewer experimental components to make, down to a minimum of zero.

#### Unarmed Combatant

Your empty hands are more dangerous than knives in the grip of the less gifted.
Your unarmed attacks are counted as melee weapons when it comes to binding up opponents wielding pistols, rifles and similar ranged arms, though you need at least one hand free to do so.

**Level 1**: Gain Punch as a bonus skill.
Your unarmed attacks become more dangerous as your Punch skill increases. At level-0, they do 1d6 damage. At level-1, they do 1d8 damage. At level-2 they do 1d10, level-3 does 1d12, and level-4 does 1d12+1. At Punch-1 or better, they have the Shock quality equal to your Punch skill against AC 15 or less. While you normally add your Punch skill level to any unarmed damage you inflict, don’t add it twice to Shock damage. If you choose to strike lethally with unarmed attacks, they have a Trauma Die of 1d6 and a Trauma Rating of x2.

**Level 2**: Even on a miss with a Punch attack, you do an unmodified 1d6 damage, plus any Shock that the blow might inflict on the target.
Your Trauma Die becomes 1d8 for lethal attacks.

#### Unique Gift

Your PC has a unique piece of cyberware, an exceptional human ability, or some experimental genetic modification that grants them some benefit that isn’t covered under existing options.
This benefit shouldn’t be a simple bonus to something you already do; it should be a power or ability that gives you options that you just wouldn’t have otherwise.
It also shouldn’t be used to simply optimize an existing character concept, but instead should allow you to make a character who wouldn’t make sense without this special ability.

It’s up to the GM to decide what’s reasonable and fair to be covered under this gift, and whatever they allow should be roughly equivalent to the existing Focus options in overall power If an ability is particularly powerful or the cybernetics are especially draining, it might require the user to take System Strain to use it, as described on page 40.

As a general rule this ability should be better than a piece of gear the PC could buy.
The player is spending a very limited resource when they make this Focus pick, so what they get should be good enough that they can’t just duplicate it with a fat bank account.


#### Unregistered

Whether by unrecorded birth, database corruption, or sheer luck, you simply do not exist in any government or corporate database.
If taken with the Many Faces Focus, your own identity is lost, but you can create others for your own uses. If this Focus is taken after character creation, it means your existing records have become hopelessly corrupted and lost.

**Level 1**: You have no government or corporate database records associated with you, and it is almost impossible to add any such records without them ending up corrupted or deleted within a week.
Human beings can remember you, but they can’t rely on computerized records to keep track of you or your activities. You can keep money on credit chips or in cash, but banking or formal property ownership is almost impossible for you.

#### Whirlwind Assault

You are a frenzy of bloody havoc in melee combat, and can hack down numerous lesser foes in close combat… assuming you survive being surrounded.

**Level 1**: Gain Stab or Punch as a bonus skill.
Once per scene, as an On Turn action, apply your Shock damage to all foes within melee range, assuming they’re susceptible to your Shock.

**Level 2**: The first time you kill someone in a round with a normal attack, either with its rolled damage on a hit or with the Shock damage it inflicts, instantly gain a second attack on any target within range using any Ready weapon you have.

### 1.6.2 Focus Gear Budgets

Foci such as Ace Driver, Roamer, and Drone Pilot allow a PC to always have a certain amount of vehicle or drone gear available regardless of their financial means, with this budget increasing as they advance in experience level.
Characters with Ace Driver or Roamer can use this budget to buy vehicles, vehicle fittings, and vehicle weapons, along with any necessary ammo. Those with Drone Pilot can do the same, except with drones. They are assumed to have stolen, scavenged, or built this gear as needed, and while they can operate it normally the gear is too idiosyncratic to sell or have others operate it long-term. Other PCs can borrow the gear for particular missions, however.

If the gear is destroyed or lost, vehicles can be rebuilt at a rate of $10,000 worth per week.
Any number of destroyed drones can be entirely rebuilt in a week. This assumes the PC has ordinary access to an urban area or salvage zone from which to acquire parts.

Focus gear budgets cannot be used to buy vehicles, fittings, or weapons that the PCs could not readily acquire in another way.
For example, if they don't have someone willing to sell them a tank, the PC with Ace Driver cannot use their budget to buy a tank.

If the PC wants more than their budget allows they can spend their own money to supplement the difference.
This additional expense is not covered by their ability to freely rebuild lost or damaged gear, however.


| PC Level | Vehicle  | Drone   |
|----------|---------:|--------:|
| 1        | $5,000   | $1,000  |
| 2        | $10,000  | $5,000  |
| 3        | $15,000  | $10,000 |
| 4        | $20,000  | $15,000 |
| 5        | $30,000  | $20,000 |
| 6        | $40,000  | $25,000 |
| 7        | $60,000  | $35,000 |
| 8        | $80,000  | $45,000 |
| 9        | $100,000 | $60,000 |
| 10       | $200,000 | $80,000 |

## 1.7.0 Final Character Creation Steps

The player now records their character's final statistics and chooses their name and current goal.

### 1.7.1 Record Maximum Hit Points

Your character's hit points measure their distance from defeat or death.
If your character is reduced to zero hit points, they are either dying or incapacitated based on the nature of the injury.

A new character rolls 1d6 for their maximum hit points, adding their Constitution modifier to the roll.
Even a penalty cannot reduce this roll below 1 point. If they have chosen the Hard to Kill Edge they may add +2 to the roll.

A character gains hit points as they advance in character level, rerolling their prior levels and taking the new score if it's higher, as explained in section 2.7.1.1.

### 1.7.2 Record Attack Bonus

Your character has a certain degree of basic combat competence based on their character level and Edges.
This bonus increases as you advance in character levels and is added to your attack roll.
A new character's attack bonus is usually +0.
If they have the On Target Edge it is +1.

### 1.7.3 Record Saving Throws

When faced with unusual dangers such as grenade explosions, toxic darts, pit traps, or cyberware hacks, the character may need to make a saving throw to resist or mitigate the peril.
Saving throws are rolled on a d20 and are explained in section 2.2.0.

**Physical** saving throws are used to resist exhaustion, disease, poison, or other biological harms.
A new character's Physical save target is equal to 15 minus the better of their Strength or Constitution modifiers.

**Evasion** saving throws are used to avoid explosions, traps, or other dangers requiring fast reactions.
A new character's Evasion save target is equal to 15 minus the better of their Intelligence or Dexterity modifiers.

**Mental** saving throws are used to resist cyberware hacks, mind-altering drugs, or other tests of willpower or self-control.
A new character's Mental save target is equal to 15 minus the better of their Wisdom or Charisma modifiers.

**Luck** saving throws are rolled when facing a danger that only blind chance can spare them from, such as an artillery strike, bridge collapse, or a sniper's random choice of victims.
A new character's Luck save target is always 15.

A character's save targets all decrease by 1 point each time they advance an experience level.

### 1.7.4 Pick a Free Skill

Your character has developed some side interest that may be unrelated to your background or Edges.
You can pick any one skill of your choice, excepting magical or supernatural skills if your campaign includes them. This skill pick is gained at level-0, or level-1 if it's already level-0. You cannot pick a skill that is already at level-1.

### 1.7.5 Choose Starting Languages

Your PC begins play speaking the lingua franca of the campaign's current city along with their native tongue if it happens to be different.
They also have fluency in additional languages based on their Know or Connect skills. Either skill at level-0 grants one extra language, or two extra if it's at level-1. Thus, a PC with both Know-1 and Connect-1 skills could pick four additional languages.

PCs can learn additional languages to a conversational level by spending a few months immersed in it or studying it diligently during downtime.
Obtaining native fluency is at the GM's discretion.

### 1.7.5 Choose Starting Gear

Your PC begins play with $500 worth of gear, plus one item of their choice that doesn't cost more than $1,000, with price lists given in section 3.0.0.
They also have one month of a middle-class lifestyle prepaid. Some GMs may choose to make pre-made gear packages that the players can choose; if this is done, the packages should be worth somewhat more than the usual starting funds to encourage players to avoid lengthy gear-shopping at the start of the game.

### 1.7.6 Choose a Name and Goal

As final step, the player should pick a name and initial goal for the PC.
This goal can be anything so long as it gives a compelling reason for the PC to be doing dangerous missions and associating with suspicious fellows. The player must make up a good reason for the PC to be associating with the other players; it is not the GM's job to justify the party's existence, and if the player decides that their PC can't reasonably run with the other party members it's up to them to create a new character who can.

# 2.0.0 The Rules of the Game

This section summarizes the rules of the game.
They are intended to be functional for the average play group with typical needs; individual GMs may find it useful to alter them based on the specific interests or makeup of their own player group.

## 2.1.0 Scenes, Rounds, and Mission Time

During play, three special measures of time are used: scenes, rounds, and mission time.

### 2.1.1 Scenes

A scene is a time measurement used to determine how often certain abilities or actions can be taken.
Some cyber can be triggered only so many times per scene, while some special abilities only work once per scene.
A scene is one particular fight, event, activity, or effort that usually doesn’t take more than ten or fifteen minutes.
A fight is a scene. A chase is a scene. A tense backroom negotiation is a scene. So long as the PCs are doing the same general activity in the same general location, it’s probably one scene. Most scenes don’t last more than fifteen minutes, though a GM can stretch this if it seems logical.

During mission time, as described below, a scene automatically refreshes after every fight or after every ten minutes of mission time.

### 2.1.2 Rounds

Combat is made up of rounds, each one lasting approximately six seconds.
A single combat may involve multiple rounds of action. A round begins with the actions of the side that wins initiative and ends after the actions of the side that lost initiative.

### 2.1.3 Mission Time

During infiltrations, chases, or other periods of extended action where timekeeping is very important, time is measured in mission time.
Mission time begins at minute zero and then ticks over every minute as the PCs infiltrate a facility, flee the corp security, or escape the night club riot. Most activities during mission time are assumed to take one minute, such as searching a desk, picking a lock at normal speed, hotwiring a car, engaging in a fight, or so forth. Some tasks may take longer, such as five minutes to apply first aid to anyone who needs it, or ten minutes to thoroughly search a room. The GM decides how long an activity takes when it's ambiguous.

The minute-by-minute tracking of mission time is important because there is a good chance of a team's discovery by patrolling security, or a steadily-escalating response by an alerted facility's guards.
The longer the PCs take to do their job, the more likely it is that something will go wrong.

## 2.2.0 Saving Throws

Saving throws are rolled to resist some unusual danger or chance hazard.
To make a saving throw, a person rolls 1d20 and tries to get equal or higher than their saving throw target. Sometimes a save might have bonuses or penalties applied to the roll, but a natural roll of 1 on the die always fails the save, and a natural roll of 20 is always a success.

There are four types of saving throws.
Usually it will be obvious which type is most appropriate for a threat, but the GM can decide in marginal situations.

**Physical** saves resist exhaustion, poisons, diseases, or other bodily afflictions.
A PC’s Physical saving throw target is equal to 16 minus their character level and the highest of their Strength or Constitution modifiers.

**Evasion** saves apply when dodging explosions, avoiding traps, reacting to sudden peril, or other occasions where speed is of the essence.
A PC’s Evasion saving throw target is equal to 16 minus their character level and the highest of their Dexterity or Intelligence modifiers.

**Mental** saves apply when resisting mental cyberware hacks, psychotropic influences, psychological trauma, and other mental hazards.
A PC’s Mental saving throw target is equal to 16 minus their character level and the highest of their Wisdom or Charisma modifiers.

**Luck** saves are used when only blind chance can save a PC, regardless of their native abilities.
A PC’s Luck saving throw target is equal to 16 minus their character level, unmodified by their attributes.

### 2.2.1 NPC Saving Throws

NPCs have a single saving throw target equal to 15 minus half their rounded-down hit dice.
Thus, an NPC with 3 HD would have a saving throw target of 14+ for any particular hazard. The GM may modify this in special circumstances, but it's usually not worth tracking more closely.

## 2.3.0 Skill Checks

Most characters are skilled, competent men and women who are perfectly capable of carrying out the ordinary duties of their role.
Sometimes, however, they are faced with a situation or challenge beyond the usual scope of their role and the GM calls for a skill check.

To make a skill check, roll 2d6 and add the most relevant skill level and attribute modifier.
If the total is equal or higher than the check’s difficulty, the check is a success. On a failure, the PC either can’t accomplish the feat at all, bad luck cheats them, or they achieve it at the cost of some further complication. The GM determines the specific consequence of a failure.

If the character doesn’t even have level-0 in the pertinent skill, they suffer a -1 penalty to the roll.
In the case of particularly technical or esoteric skills they might not even be able to attempt the skill check at all.

The GM is always the one who calls for a skill check, and they do so at their discretion.
The player simply describes what their PC is attempting to do, and the GM will tell them what skill and attribute combination to roll. If multiple skills or attributes might plausibly fit the action, the player can pick the one most favorable to them. If the combination is only marginally relevant, but still reasonably plausible, it might suffer a -1 or -2 penalty at the GM's discretion.

### 2.3.1 Skill Check Difficulties

The following difficulties ratings reflect common challenges.


| Difficulty | Skill Check                                                                                                                                                                   |
|-----------:|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|          6 | A relatively simple task that is still more than the PC would usually be expected to manage in their regular background. Anything easier than this isn’t worth a skill check. |
|          8 | A significant challenge to a competent professional that they’d still succeed at more often than not.                                                                         |
|         10 | Something too difficult to be expected of anyone but a skilled expert, and even they might fail.                                                                              |
|         12 | Only a true master could expect to carry this off with any degree of reliability.                                                                                             |
|        14+ | Only a true master has any chance of achieving this at all, and even they will probably fail.                                                                                 |

Helpful or hostile circumstances can modify a skill check by -2 to +2.
Usually, no combination of situational modifiers should alter the roll by more than this, or else it becomes a near-foregone conclusion. This does not include modifiers applied by gear mods, cyberware, or PC aid.

### 2.3.2 NPC Skill Checks

When an NPC needs to make a skill check, they roll 2d6 and add their listed skill modifier if their action is something they ought reasonably to be good at.
If it isn't, they roll at +0, or even at -1 if it seems like something they'd be particularly bad at doing. If the NPC is special enough to have actual attribute scores and skill levels, they use those instead.

### 2.3.3 Aiding a Skill Check

To aid a comrade's skill check, a player explains what their PC is doing to help.
If the GM agrees that it's plausible, they may roll a relevant skill and attribute modifier against the same difficulty as the check they are aiding. If they succeed, their ally gains a +1 on their skill check. If they fail, no harm is done. Multiple PCs can try to aid if their actions are plausible, but the total bonus can't exceed +1.

Aiding a comrade is usually done in ways that let the aiding PC leverage their own special talents or skills.
A PC may not have the skills to attempt to Sneak past a vigilant guard, for example, but they might have a good Perform skill they can use to create a distraction that helps their comrade skulk past.

### 2.3.4 Opposed Skill Checks

When skills oppose each other, each participant makes a skill check and the winner is the one who rolls higher.
In cases of ties, the PC wins. Thus, a PC trying to sneak past a guard might roll 2d6 plus their Dex/Sneak against the guard's 2d6 plus their skill modifier. If the guard was significant enough to actually have attributes and skill levels, it might be a Dex/Sneak challenge versus their Wis/Notice.

## 2.4.0 Combat

Violence is inevitable in most cyberpunk campaigns.
The rules below handle its most common manifestations.

### 2.4.1 The Combat Sequence

When combat begins, the fight progresses in the following sequence.
The sections below explain each step in the process.

First, each participating side rolls for initiative.
The side that rolled highest acts first.

Second, each member of a side gets to take their actions.
Members of a side act in whatever order they wish. NPC sides act in whatever order the GM wishes.

Third, once every member of a side has acted, the side that rolled next-highest gets to act.
If NPCs have taken losses or are facing defeat, they may need to roll a Morale check as explained in section 5.2.0. PCs never check Morale.

Fourth, once every side has acted the process repeats from the top in the same order.
Initiative is not re-rolled.

### 2.4.2 Combat Initiative

When combat begins, each side involved in it rolls initiative, rolling 1d8 and adding their group's best Dexterity modifier.
NPCs usually add nothing. The groups then act in order from highest to lowest rolls, with PC sides winning ties. When the slowest group has acted, the round ends and a new round starts in the same initiative order. Members of a side can act in any order the group agrees upon when it is that side's turn to act, performing their allowed actions as explained in the section below.

#### 2.4.2.1 Individual Initiative

As an optional rule, the GM may use individual initiative.
In this case, each combatant rolls 1d8 individually, adding their Dexterity modifier, and acting in order from highest to lowest with PCs winning ties. This leaves a group less likely to be caught flat-footed by enemies, but makes it harder for a group to coordinate actions.

#### 2.4.2.2 Surprise

If a group is caught entirely unawares they may suffer surprise, automatically granting their enemies a full free round of action before initiative can be rolled.
The GM decides when surprise applies, possibly calling for an opposed Dex/Sneak skill check versus the target's Wis/Notice. Groups cannot be surprised if they are actively anticipating the possibility of combat; at most, they might suffer an initiative penalty at the GM's discretion.

#### 2.4.2.3 Automatic Initiative Powers

A PC with certain Foci or cyberware may be immune to surprise or gain automatic initiative.
In such cases they automatically act first during a combat round, even if the rest of their side is slower. If multiple combatants have these powers, they roll initiative normally amongst themselves to see which of them acts first.

### 2.4.3 Combat Action Types

Attacks, movement, cyberware triggering, and other combat activities all require one of the following four types of actions.

**Main actions** are a character's primary action during a combat round, such as shooting a target, applying first aid to a downed ally, running a cyberware hack, frantically evading gunfire, or something else that takes less than six seconds to do.
A combatant gets one Main action per round.

**Move actions** involve moving the character's normal movement rate of 10 meters or performing some other relatively brief bodily action, such as getting up from prone.
A combatant gets one Move action per round, but can spend their main action to get a second.

**On Turn actions** are brief, simple acts that require only a moment's concentration.
Activating most cyberware or speaking a few words might constitute an On Turn action. A character can take as many On Turn actions on their round as the GM finds plausible.

**Instant actions** are special, most being provided only by certain powers or certain special actions.
Instant actions can be performed even when it's not your turn in the round, even after the dice have already been rolled. The Veteran's Luck Edge provides one such Instant action, allowing the PC to treat a missed attack roll as an automatic hit. A PC can use as many Instant actions in a round as the GM finds plausible. Instant actions performed at the same time are resolved simultaneously, with the GM adjudicating any ambiguities.

### 2.4.4 Common Combat Actions

The actions listed below are merely some of the most common taken in combat.

**Make a Melee Attack (Main Action)**: Attack a target in melee range with an unarmed attack or melee weapon.
Such weapons use either the Punch or the Stab skill, depending on the type of attack.

**Make a Ranged Attack (Main Action)**: Attack a target with a gun, bow, or thrown weapon.
The Shoot skill is used for these attacks, though Stab or Exert can optionally be used for thrown weapons. If there is an enemy attacker in melee range, one-handed guns and thrown weapons suffer a -4 penalty to hit, while rifles and two-handed ranged weapons cannot be fired at all.

**Make a Snap Attack (Instant Action)**: As an Instant action, give up your Main Action and either Make a Melee Attack or Make a Ranged Attack at a -4 penalty to hit.
As an Instant action, you can Make a Snap Attack even when it's not your turn, but you must not have taken your Main Action this round yet. Only well-trained and disciplined NPCs have enough focus to Make a Snap Attack.

**Make a Swarm Attack (Main Action)**: Target an enemy within range of your weapon and take this action until up to four allies have Made a Swarm Attack on that target this round.
At that point or any point beforehand, one of these assailants can Make a Melee Attack or Make a Ranged Attack on the target with a +2 bonus to hit and +1 bonus to damage for every other assailant, up to a maximum bonus of +6 to hit and +3 damage. This bonus damage does not add to the attack's Shock and cannot make it do more than its usual maximum damage. Any Shock inflicted by this attack is always applicable, however, even if the target's AC is too high, they're using a shield, or have some power that makes them immune to Shock; the damage a Swarm Attack does isn't really Shock, but a reflection of the inevitable hazards of being swarmed by numerous armed foes.

**Charge (Special Action)**: Spend both your Main Action and your Move action to move up to twice your normal movement rate in a straight line, making a melee or thrown ranged attack at the end of it with a +2 bonus to hit.
You must be able to charge at least 3 meters to build up sufficient momentum and you suffer a -2 penalty to your Armor Classes until the end of the round.

**Screen an Ally (Move Action)**: Move up to your normal movement rate to get adjacent to an ally.
You then physically block attacks against them until the start of your next turn, provided they remain within 3 meters of you. Enemies who attack your ward must make a successful opposed combat skill check against you using either Str or Dex and the most applicable combat skill. If the enemy succeeds, their attack targets your ward normally. If you succeed, their attack instead targets you. You can screen against a number of attackers each round equal to your highest combat skill; thus, you need at least level-1 in a combat skill to successfully screen. Multiple defenders can screen the same target, in which case the opposed skill check is compared to all defenders and targets the worst-rolling successful defender. You can only screen against attacks you could feasibly physically parry or body-block; gunfire cannot be blocked without integral cyber armor.

**Total Defense (Instant Action)**: Give up your Main Action to focus entirely on dodging and evading incoming perils.
Your Melee and Ranged Armor Classes increase by +2 and you become immune to Shock until the start of your next turn, including the otherwise-unavoidable damage from a Swarm Attack. You cannot take this action if you have already spent your Main Action for the round.

**Run (Move Action)**: Move your normal movement rate in combat, which is 10 meters for an ordinary human.
If you start your movement adjacent to an armed melee combatant, they get a free melee attack against you as you flee. To avoid this, you must make a Fighting Withdrawal first. Movement taken as part of a hyper-accelerated bonus action such as an Enhanced Reflexes cyberware activation does not require a Fighting Withdrawal.

**Make a Fighting Withdrawal (Main Action)**: Disengage from an adjacent melee attacker, allowing you to move away from them without incurring a free attack as you retreat.
You do not actually leave melee range with this action alone, and your enemy can simply re-engage you next round if you don't actually take a move action to retreat.

**Use a Skill (Main Action)**: Perform first aid on a downed comrade, trigger a cyber hack, or otherwise use a skill that wouldn't normally take more than six seconds.

**Ready or Stow an Item (Main Action)**: A character can Ready an item for use from their pack or stowage or Stow it, as per the encumbrance rules in section 2.9.0.
Sheathing or holstering a Readied weapon without actually Stowing it does not require this action, though the GM may disallow rapid weapon swaps if they start to become implausible.

**Reload a Weapon (Main Action)**: Reload a firearm with a Readied magazine.
Modern bows and crossbows may be reloaded as an On Turn action if the shooter has at least Shoot-1 skill; otherwise it's a Move action to nock a new arrow.

**Drop an Item (Instant Action)**: Drop an item you are holding.
This may be done at any time to free up a hand.

**Pick up an Item (Move Action)**: Scoop up a dropped item within melee range, leaving it Readied in your hand.

**Stand Up (Move Action)**: Rise from a prone position, picking up any dropped items as you do so.

**Go Prone (On Turn Action)**: Fall prone, applying a -2 penalty to ranged attacks against you and a +2 bonus to melee-range attacks against you.
Your normal movement rate is halved while you remain prone.

**Hold An Action (Move Action)**: Spend your Move action to delay acting on your side's turn.
You may trigger the rest of your turn's actions as an Instant action at any point until the end of the round, after which they are lost. If your held action is taken in response to someone else's action, yours resolves first.

### 2.4.5 Combat Attack Rolls

When an assailant makes an attack, they roll 1d20 and add their base attack bonus, the weapon's relevant attribute modifier, and their relevant combat skill level.
If they lack even level-0 in the appropriate combat skill, they apply a -2 penalty to the roll. If the total is equal or greater than the target's relevant Melee or Ranged Armor Class, they hit. If less, they miss.

Every weapon listed in section 3.4.0 is listed as using one or more attributes, such as either Str or Dex for a knife.
The attacker may choose either attribute for modifying the weapon's attack and damage rolls.

#### 2.4.5.1 NPC Attack Rolls

NPCs usually do not have attribute modifiers or skill levels.
Instead, the attack bonus of a trained NPC combatant is usually equal to their hit dice, often with an additional bonus to reflect particularly good training or talent.

#### 2.4.5.2 Attack Roll Modifiers

Some common situations can modify an attack roll, granting a bonus or penalty.
GMs may add others depending on the situation. Attackers with the Gunlink cybersystem ignore 2 points of penalty applied by cover or concealment, and do not suffer penalties for long range firing.

| Situation                                                                       | Mod |
|---------------------------------------------------------------------------------|----:|
| Shooting at a distant prone foe                                                 |  -2 |
| Attacking an adjacent prone foe                                                 |  +2 |
| Melee attacking while prone                                                     |  -4 |
| Your target is past your gun's range, up to its maximum long range.             |  -2 |
| The target is at least half behind cover                                        |  -2 |
| The target is almost completely in cover                                        |  -4 |
| Making a thrown attack while in melee                                           |  -4 |
| Shooting a one-handed gun while in melee                                        |  -4 |
| Shooting a two-handed rifle while in melee                                      | N/A |
| You are shooting at a target you can’t see but you know where they are.         |  -4 |
| You are shooting at a target you can’t see and don’t know their exact position. | N/A |

### 2.4.6 Damage, Trauma, and Shock

If an attack hits, it inflicts hit point damage equal to the weapon's damage die plus the weapon's relevant attribute modifier.
Special weapon mods or cyberware may increase this damage.

Some armor provides Damage Soak.
Incoming weapon damage is subtracted from this Damage Soak first before it can reach the wearer. Once this Damage Soak is depleted it can give no further protection for the rest of the fight; it refreshes after combat is over to reflect a new chance for the wearer's luck to save them.

#### 2.4.6.1 Non-Lethal Damage

You may attack non-lethally with an appropriate weapon or unarmed attack.
If so, do not roll the Trauma Die as explained below. Your attacks will only incapacitate the target if you reduce them to zero hit points.

#### 2.4.6.2 Punch Weapon Damage

If you are making a purely unarmed attack you may add your Punch skill to the damage.
You may not add the skill to the damage done by Body Blade cyber or other artificial weaponry that uses the Punch skill.

#### 2.4.6.3 Trauma

When you hit with a weapon or lethally-intended unarmed attack, roll the weapon's associated Trauma Die.
If it equals or exceeds the victim's Trauma Target, which is usually 6 for a normal unarmored human, you have inflicted a Traumatic Hit.

Traumatic Hits multiply the total damage of the hit by the weapon's listed Trauma Rating.
Thus, if a shotgun with a x3 Trauma Rating would normally have done 9 damage in total, it instead does 27. If this damage or any later damage in the same fight reduces the victim to zero hit points, they risk a Major Injury as described in section 2.5.6.

Some Edges, armor, or cyberware may increase a subject's Trauma Target.
Some other abilities might grant a bonus to the Trauma Die roll. To speed the process, it's generally best to roll the Trauma Die at the same time as the attack or damage roll.

Drones, vehicles, and other inanimate objects are immune to Traumatic Hits from weapons that could not reasonably inflict catastrophic structural damage on them.
A shotgun may be sufficient to wreck a backpack-portable drone, while it is unlikely to inflict ruin on a car engine. The GM decides on applicability in ambiguous cases.

#### 2.4.6.4 Shock

Some melee weapons inflict Shock on a missed attack roll.
This damage reflects the inevitable harm a poorly-armored combatant suffers when engaging in armed combat. Shock for a weapon is recorded as a point value and target Armor Class, such as "Shock 2/15". If the wielder misses a target with this weapon that has a Melee Armor Class equal or less than the weapon's Shock rating, they suffer the listed amount of damage anyway. Thus, if that weapon were to miss a victim with Melee AC 13, it would still do 2 points of damage.

Some attacks apply Shock on a miss regardless of the target's Armor Class.
This benefit may be granted by certain abilities, or it may be part of a dangerous NPC's talents. Such Shock ratings are recorded with "-" as the affected AC, such as "Shock 5/-". This automatic Shock is still negated by cyber or abilities that grant a subject immunity to Shock.

The only modifiers that add to Shock damage are the wielder's relevant attribute modifier for the weapon and any damage bonuses that explicitly add to Shock.
Thus, the Killing Blow Edge adds to Shock because it specifically says so, while a weapon mod that merely says it adds +2 damage would not.

A person using a riot shield or other barrier can ignore the first source of Shock they would normally suffer in a round.
Some other Foci or special actions such as Total Defense can also render a subject immune to Shock.

Damage inflicted by Shock cannot cause a Traumatic Hit.
An attack that hits can never do less damage than the Shock that would have been inflicted on a miss.

### 2.4.7 Special Combat Maneuvers

There are certain special maneuvers or activities that commonly arise in combat.

#### 2.4.7.1 Shoving and Grappling

To shove a target the attacker must make a successful melee attack.
This attack does no damage, but forces an opposed Str/Exert or Str/Punch skill check. If the attacker wins, the target is shoved back up to 3 meters or knocked prone at the attacker's discretion.

To grapple, the attacker must make a successful unarmed melee attack while having both hands free.
This attack does no damage but forces an opposed Str/Punch skill check. If the attacker wins, the victim is grappled. A grappled victim remains so until they take a Main Action to perform a successful opposed Str/Punch skill check against their assailant.

While grappled, neither the assailant or the target can move from their location, nor can they fight with anything but unarmed attacks, including Body Blades cyber or similar body-mounted weaponry.
At the end of each round, a grappled victim automatically suffers damage as if hit by their assailant's unarmed attack.

If the attacker wishes to move the grappled target, they must spend a Main Action and make an opposed Str/Punch skill check.
On a success, they move the target up to 3 meters along with them or throw them 2 meters and leave them prone. On a loss or tie, the target escapes.

An attacker can grapple only one target at a time, but a defender can be grappled by multiple assailants, within reason.
Any skill checks forced on a multiply-grappled target are compared against all assailants, and win only if all assailant rolls are beaten.

These rules assume both assailant and target are relatively human-sized.
Grappling or shoving humanoid but substantially larger targets is done with a -2 penalty on all skill checks, while trying to handle quadrupeds or those only barely plausible to wrestle is done at a -4 penalty.

#### 2.4.7.2 Dual-Wielding Weapons

Some attackers prefer to use two weapons at once.
PCs who wish to do so must have at least level-1 in the relevant weapon skills, such as Stab-1 and Shoot-1 for dual-wielding a knife and pistol.

When making an attack while dual-wielding, the attacker chooses which weapon they wish to use, rolling the attack roll accordingly.
On a hit, the weapon does +2 damage so long as the target is within range of both wielded weapons. This bonus does not add to Shock.

Managing two weapons at once is difficult, and applies a -1 penalty to all hit rolls.

#### 2.4.7.3 Execution Attacks

A target that is entirely unsuspecting of damage is subject to execution attacks.
A subject that is expecting danger or alert to potential harm cannot be targeted by an execution attack.

A ranged execution attack requires one full minute of aiming, waiting, and adjusting on the part of the would-be sniper.
Any disturbance during this time will spoil the shot. After spending this time, the assassin may make a Dex/Shoot skill check. The difficulty is 6 for an attack within two meters, 8 for an attack within the weapon's normal range, or 10 for one at the weapon's long range. On a success, the attack hits; the victim's Armor Class is ignored.

A melee execution attack requires one full minute of near proximity to the target, watching for just the right opening and getting to within melee range of the victim.
If this time is granted, the assassin may make a melee attack, automatically hitting.

When a target is hit with an execution attack they must make a Physical saving throw at a penalty equal to the assailant's combat skill.
On a failure, they are immediately reduced to zero hit points and Mortally Wounded, or knocked unconscious if the weapon was non-lethal.

If they succeed on the save, they still take damage normally.
Successful execution attacks always count as Traumatic Hits, so the ensuing multiplied damage or Major Injury may be enough to kill the victim anyway.

### 2.4.8 Vehicle Combat

Combat between vehicles works much as combat between people does.
Each vehicle counts as its own side for rolling initiative and all passengers act on its turn. The driver must spend a Main Action each round maintaining control of the vehicle. If they can't and no one can grab the wheel, the driver must make a Luck save or crash. A successful save results in a safe halt.

Each vehicle requires a gunner for each mounted weapon, and firing them requires a Main Action from the gunner.
Firing personal weapons out the window is done at a -4 hit penalty while the vehicle is moving.

Vehicles have their own Armor Class applied to both melee and ranged attacks against it.
A stationary vehicle takes a -4 penalty, while a moving one adds its driver's Drive skill to its AC. Vehicles may also have an Armor rating, which is subtracted from all damage done to it. A GM may rule that some weapons have no feasible way of harming some vehicles.

Vehicles reduced to zero hit points are destroyed, and any passengers take crash damage.

Vehicle movement is abstracted, and it's assumed that all vehicles participating in a fray are close enough to shoot at each other.
If one or more wishes to flee and the others try to follow, the pursuit rules from section 2.6.2 are used.

#### 2.4.8.1 Vehicle Destruction

A vehicle that crashes or is reduced to zero hit points is destroyed.
Any occupants take damage as if they were also hit by whatever took out the vehicle, with a Luck save for half. Thus, if a rocket launcher inflicted 18 points of damage on a car and blew it to pieces, the passengers would also take 18 points of damage, with a Luck save to halve it.

#### 2.4.8.2 Vehicle Crashes

If the vehicle was moving at combat speeds at the time it was destroyed or goes out of control, the passengers also take crash damage.
Each passenger rolls both a Physical save and a Luck save. If they make both, they survive largely unscathed. If they fail one, they take half their maximum hit points in damage, which may leave them Mortally Wounded. If they fail both, they are Mortally Wounded and will suffer a Major Injury if they survive.

#### 2.4.8.3 Vehicles and Traumatic Hits

Some weapons can reasonably inflict Traumatic Hits on a given vehicle, at the GM's discretion.
Each vehicle type has its own Trauma Target, never less than 6 and often much higher.

A vehicle that takes a Traumatic Hit from such weapons takes the multiplied damage as usual.
If the damage is enough to destroy the vehicle, the unlucky passengers are also subject to this multiplied damage when making their Luck saves.

#### 2.4.8.4 Vehicle Maneuvers and Ramming

A driver who wants to accomplish a stunt can do so as part of the Main Action they spend controlling the vehicle for the round.
These maneuvers may be an attempt to open up a route of escape, to get cover from enemy fire, to cut off a specific enemy vehicle, or to jump Snake River Canyon. This is normally resolved with a skill check at a difficulty set by the GM. Checks made against a driver, such as an attempt to skid out of a vehicle’s line of fire, may be made as opposed checks.

Ramming a human-sized target is generally only possible in confined areas such as narrow streets.
The driver and target make opposed Dex/Drive and Dex/Exert skill checks. If the driver wins, the target must make an Evasion save or take the vehicle’s maximum HP in damage, with a Trauma Die of 1d12/x3. Ramming a vehicle also takes an opposed check, albeit with no Evasion save allowed. The ramming vehicle also takes damage as if the target vehicle rammed it in turn.

## 2.5.0 Injury, Healing, and System Strain

Injury is almost inevitable in an operator's career.
Some forms of it can be longer-lasting than others.

### 2.5.1 Mortal Injury and Stabilization

When a PC is reduced to zero hit points by a lethal attack, they are Mortally Injured.
They will die at the end of the sixth round after their incapacitation unless stabilized by an ally or some special ability or cyber. A Mortally Wounded character is helpless, and can take no actions and do nothing useful.

Stabilizing an ally is usually a Main Action that requires a Dex/Heal or Int/Heal skill check.
The difficulty is 8 plus the number of full rounds since the target fell. If the medic lacks a medkit or other tools, this difficulty is increased by 2. Only one ally can try to stabilize a victim per round, though others can attempt to aid their check, but attempts may be retried each round for as long as hope lasts.

Once stabilized the victim remains incapacitated for ten minutes before recovering with 1 hit point and the Frail condition.
They may act normally after they recover, but if they are reduced to zero hit points again while still Frail, they die instantly. Frailty is removed by a week of bed rest and medical care. A physician can also make one attempt to remove Frailty with a medkit and an hour of work, rolling a Dex/Heal or Int/Heal skill check against difficulty 10.

#### 2.5.1.1 NPCs and Mortal Injury

NPCs who aren't important enough to merit a name usually die instantly when reduced to zero hit points.

#### 2.5.1.2 Catastrophic Damage

Targets reduced to zero hit points by some injury or cause that could not be reasonably survivable are instantly killed.
A gunshot might be patched; a direct hit with a rocket-propelled grenade is less survivable. What counts as "not be reasonably survivable" may vary if the target is heavily cyber-armored.

#### 2.5.1.3 Non-Lethal Incapacitation

If a target is brought to zero hit points by a non-lethal attack, they are incapacitated for ten minutes before regaining 1 hit point.
They do not become Frail.

### 2.5.2 System Strain

Non-natural forms of healing or intense cyberware activation takes a toll on a user's physiology.
Their System Strain total reflects the total amount of stress their body has undergone.

A healthy character normally starts at zero System Strain and has their Constitution score as their allowed maximum.
A character cannot accumulate more than this maximum in System Strain.

Cyberware installation generally adds a certain amount of permanent System Strain to the target.
This permanent Strain may be fractional, but a character's body cannot ever support more than their maximum System Strain. Removing the cyber allows this permanent System Strain to be lost.

Pharmaceutical healing and cyberware activation often add to a subject's System Strain.
If this addition would put them over their maximum they cannot activate the cyber, benefit from the drugs, or otherwise gain any use from the ability. If they are forced over the maximum by some unavoidable effect, they are instead knocked unconscious for at least an hour.

Characters lose one point of accumulated System Strain after each night's rest, assuming they are warm, fed, and comfortable and can get at least eight uninterrupted hours of sleep.

### 2.5.3 Natural Healing

A wounded creature can recover hit points by getting a good night's rest and adequate food.
Provided they are warm, fed, and comfortable, they regain hit points each morning equal to their experience level, or equal to their hit dice if they are NPCs.

Frail creatures do not recover hit points through natural healing.
They must cure their Frail condition first or rely on medical pharmaceuticals. Removing the Frail condition requires a full week of bed rest and the medical attention of someone with at least Heal-0 skill and a medkit. Frail victims without this level of medical care must make a Physical save after a week; on a failure they die sometime in the next week, while success means they lose their Frailty after another month of rest.

### 2.5.4 First Aid

Modern combat medicine can patch up victims in a hurry, albeit at a cost to their physical resilience.
By spending one minute patching up an ally with a medkit, a healer can heal 1d6 points of damage plus their Heal skill. If they lack any Heal skill at all, they restore 1d6-1 points. Each such application of first aid adds one System Strain to the target. First aid can restore hit points to a Frail target, but it cannot remove their Frailty.

Five minutes is enough time for a medic to apply as much first aid as is wanted to the rest of their team.

### 2.5.5 Poisons and Diseases

Most toxins force a victim to make a Physical saving throw to resist their effects or mitigate their harm.
Weak perils might grant as much as a +4 to the saving throw, while dire threats might apply a -4 penalty.

If the save is failed, the poison or disease takes hold.
Most poisons act quickly, inflicting hit point damage, adding System Strain to the target, or applying long-lasting penalties. Diseases can have a slower onset but often apply the same sort of harms.

A medic who gets to a poisoned person within a minute of the poisoning can use a medkit to give them a better chance to resist.
They may add twice their Heal skill level to the victim’s saving throw roll, or +1 if they have only Heal-0 skill. Specialized antitoxins may be able to neutralize such poisons entirely.

### 2.5.6 Major Injuries

If a subject is reduced to zero hit points by lethal damage during the same scene in which they suffered a Traumatic Hit, they run the risk of suffering a Major Injury.
The damage that brings them down doesn't need to have come from the Traumatic Hit; they remain vulnerable until the end of the scene.

On reaching zero hit points they must make a Physical save.
If they pass, they are still Mortally Wounded but may be stabilized and recover normally. If they fail the save, they suffer a Major Injury. The victim rolls 1d12 and compares it to the table below. Some special effects or situations may modify the roll, but a roll less than 1 still counts as 1, and one more than 12 still counts as 12.

A victim with a Major Injury may still be stabilized by ordinary medical help to prevent them from bleeding out, but dealing with the actual injury will generally require serious work by a properly-equipped street doc.


|   d12 | Major Injury                                                                                                                                                                                                            |
|------:|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|     1 | **Instant Death**: Wrong place, wrong time. You're not coming back from this.                                                                                                                                           |
|     2 | **Internal Damage**: You're dead in an hour if you don’t get to a street doc's clinic. You're in a coma until you install a Prosthetic Cyber implant. Any Body or Nerves location cyber you have is critically damaged. |
|     3 | **Brain Damage**: You're in a coma until you install a Prosthetic Cyber implant. Any Head or Nerves location cyber you have is critically damaged.                                                                      |
|     4 | **Eye Damage**: You're blind until you get a pair of cybereyes. If you already have cybereyes, they and any implants in them are critically damaged.                                                                    |
|     5 | **Gut Wound**: If you don't get to a street doc's clinic in an hour, you suffer a -1 penalty to your Con modifier until you get a Prosthetic Cyber implant.                                                             |
|     6 | **Right Leg Ruined***: You can crawl at best once stabilized.                                                                                                                                                           |
|     7 | **Left Leg Ruined\***: As above, but for your left leg.                                                                                                                                                                 |
|     8 | **Right Arm Ruined\***: You've only got one functional hand.                                                                                                                                                            |
|     9 | **Left Arm Ruined\***: As above, but for your left arm.                                                                                                                                                                 |
|    10 | **System Damage**: Your most System Strain-expensive implant has been critically damaged. If several tie, the GM picks one.                                                                                             |
| 11-12 | **Just a Flesh Wound**: It's not that bad of a hit after all.                                                                                                                                                           |


\*: Any Limb cyber exclusively and entirely located in the ruined limb, such as a Cyberarm system, is also damaged by the injury.
Limb systems split over multiple limbs are not disabled.

#### 2.5.6.1 Prosthetic Cyber

Some Major Injuries force the installation of Prosthetic Cyber systems as listed in section 3.6.7.6.
If a limb is destroyed, an appropriate Cyberarm or Cyberleg is needed, while brain damage, internal damage, and gut wounds require Prosthetic Cyber.

Wealthy characters can instead have custom-grown clone organs and limbs used to replace damaged tissue, but such measures require extensive medical contacts with top-end facilities and at least $100,000 in costs.

Characters in magically-active campaigns who are unable to safely use cyberware, such as PCs with the magical Edges, may use makeshift enchantments in place of cyberlimbs, cybereyes, or Prosthetic Cyber.
These enchantments do not damage their maximum Effort scores, but they add 2 System Strain each and grant no benefits beyond minimal functionality. They have the same cost as the cyber.

#### 2.5.6.2 Cyberware Damage

Many major injuries damage cyberware located in the body part that was crippled by the blow.
Such damaged cyberware is useless until it has been repaired by someone with at least Fix-0 and Heal-0 skills. If the cyberware is necessary to preserve the user’s life somehow, it functions just enough to avoid death, but not well enough to do any good.

Repairing damaged cyberware takes one day per damaged system and costs 20% of the systems’ original cost in maintenance parts.
A cyberdoc kit at a minimum is necessary to perform the repairs. Paired cyber implants such as cybereyes or synthears are treated as a single system and repaired as a single system.

## 2.6.0 Chases and Pursuit

Operators have a habit of chasing after others and being chased in turn.
The specific rules used vary depending on whether it's a foot chase or vehicle pursuit.

### 2.6.1 Foot Chases

The group member in the fleeing party with the best Dex/Exert or Con/Exert total rolls a skill check.
Their result is the fleeing group's pace, as they help and encourage the slower members.

Other fleeing group members then hinder pursuit in whatever ways they think are helpful.
Sometimes a skill check is needed, while other times a GM will simply decide it works. Each successful effort adds a +1 bonus to the pace, up to +3 maximum. Botched efforts are either unhelpful or apply a -1 penalty if they’re actively harmful. If the fleeing group is made up of NPCs, it’s the GM’s judgment as to whether any of them try to do something clever to stall the PCs.

The pursuing group then makes a single Dex/Exert or Con/Exert skill check, modified by the table below.
If they beat the fleeing group's total they catch up to them, and if they tie or roll less the fleeing party has escaped immediate pursuit.

| Situation                                 |       Mod |
|-------------------------------------------|----------:|
| There are more pursuers than pursued      |        -1 |
| The pursued have no head start at all     |        +2 |
| 〃 have one round’s head start            |        +1 |
| 〃 have less than a minute’s head start   |        +0 |
| 〃 have more than a minute’s head start   |        -2 |
| Who knows the local terrain better?       | -2 to  +2 |
| The pursuit is half-hearted or obligatory |        -1 |
| The pursuers are enraged or vengeful      |        +1 |

### 2.6.2 Vehicle Chases

For vehicle chases, the fleeing driver makes a Drive skill check, usually modified by Dexterity.
This is the fleeing vehicle's pace. The vehicle's passengers may think of something useful to do to hinder pursuit, just as with a foot chase.

Each pursuing vehicle then makes its own Dex/Drive skill check to catch up with the quarry, modified by the table below.
Any of them who don't equal or exceed the fleeing vehicle's pace fall behind and are lost from the pursuit. Any of them who do make the roll catch up to the vehicle, and it usually becomes a matter of vehicle combat until the quarry can make another escape attempt or win the ensuing battle.


| Situation                                   | Modifier |
|---------------------------------------------|---------:|
| The pursuer can’t directly see the pursued  |       -2 |
| The pursuer is flying but the pursued isn’t |       +3 |
| The pursued is flying but the pursuer isn’t |       -3 |
| A spotter is relaying the target’s position |       +1 |
| Who knows the local terrain better?         | -2 to +2 |
| The pursuit is half-hearted or obligatory   |       -1 |
| The pursuers are enraged or vengeful        |       +1 |

## 2.7.0 Character Advancement

Characters accumulate experience points through successful mission completion or other activities appropriate to the campaign's focus.
By default, PCs gain three experience points for an average successful mission. When enough experience points have been accumulated, they advance an experience level. New characters begin at first level and can rise to a maximum of tenth under the default rules.

The requirements listed below are for "fast" campaigns, where PCs advance in level relatively rapidly, and "slow" campaigns, where the advancement is more measured.
Individual GMs may alter these rates to suit their table's needs. Experience points do not reset on leveling up; the totals listed are total points accumulated.

### Experience Point Requirements

| Level | Fast | Slow |
|------:|-----:|-----:|
|     1 |    0 |    0 |
|     2 |    3 |    6 |
|     3 |    6 |   15 |
|     4 |   12 |   24 |
|     5 |   18 |   36 |
|     6 |   27 |   51 |
|     7 |   39 |   69 |
|     8 |   54 |   87 |
|     9 |   72 |  105 |
|    10 |   93 |  139 |

### 2.7.1 Advancement Benefits

Whenever a character advances a level, they obtain certain benefits.

#### 2.7.1.1 Additional Hit Points

To determine their new maximum hit points, they roll 1d6 for each character level they possess.
If they have the Hard to Kill Edge, they roll 1d6+2 for each. To each of these dice, they add their Constitution modifier, whether a bonus or a penalty. No individual die can be reduced below 1 point, even with a Constitution penalty. If the total roll is greater than their current maximum hit points, they take the roll. If less or equal, their maximum hit points increase by one.

#### 2.7.1.2 Improved Saving Throw

Their saving throw scores decrease by one, making it easier to succeed on saving throws by rolling equal or over it.
As a first level character has saving throw scores of 15, reaching second level would lower them to 14, modified by their appropriate attributes.

#### 2.7.1.3 Improved Attack Bonus

A PC’s base attack bonus improves according to their level.
Most characters have a basic attack bonus equal to half their character level, rounded down. If they have the On Target Edge, their bonus is equal to their full character level.

#### 2.7.1.4 Gaining and Spending Skill Points

A PC who advances a level gains three skill points they can spend on improving their skills or save to spend later.
Operators with the Educated Edge gain an extra bonus skill point to spend, giving them four points each time they advance.

Skills that are gained or improved immediately on gaining a level are assumed to have been perfected over the past level and require no training time or teaching.
If they save their skill points to spend them later then they’ll need to find some teacher or other explanation for developing them in the meanwhile.

The cost for improving a skill is listed below.
Every skill level must be purchased in order; to gain level-1 in a skill you need to pay one point for level-0 and then two points for level-1. A PC must be the requisite minimum level to increase a skill to certain levels. Less hardened operators simply don’t have the focus and real-life experience to attain such a pitch of mastery.

A PC cannot “partially buy” a skill level.
If they don’t have enough skill points to buy a new level, they need to save them up until they can. A PC cannot develop skills beyond level-4.

| Skill Level | Point Cost | Min.  Character Level |
|------------:|-----------:|----------------------:|
|           0 |          1 |                     1 |
|           1 |          2 |                     1 |
|           2 |          3 |                     3 |
|           3 |          4 |                     6 |
|           4 |          5 |                     9 |

#### 2.7.1.5 Improving Attributes

A PC may optionally choose to use their new skill points to improve their attribute scores, assuming they haven't taken the Underdog Rule option in character creation.

The first time a PC improves an attribute, it costs 1 skill point and adds +1 to an attribute of their choice.
The second improvement to their attributes costs 2 skill points, the third 3, and so forth. Each improvement adds +1 to the attribute, potentially improving its modifier. A PC can only ever make five such improvements total; not five per attribute.

PCs must be third level before buying their third boost, sixth level before buying their fourth boost, and ninth level before buying their fifth boost.
No more than five attribute boosts can ever be purchased by a PC.

#### 2.7.1.6 Choosing a new Focus or Edge

Finally, the PC might be eligible to pick an additional level in a Focus.
At levels 2, 5, 7, and 10 a PC can add a level to an existing Focus or pick up the first level in a new Focus.

If this is the first level they’ve taken in the Focus, they might be granted a skill as a free bonus pick, depending on the Focus’ benefits.
During character creation, this bonus skill pick is treated like any other skill pick. If the Focus is taken as part of advancement, however, it instead counts as three skill points spent toward increasing the skill. This is enough to raise a nonexistent skill to level-1, or boost a level-1 skill to level-2. They may do this even if they aren’t high-enough level to normally qualify for a skill level that high.

If the skill points aren’t quite enough to raise the skill to a new level, they remain as credit toward future advances.
If applied to a skill that is already at level-4, the PC can spend the three skill points on any other skill of their choice.

In addition, upon reaching fifth level, an operator can pick a third Edge to add to their existing two.
Any benefits this Edge grants are retroactive to first level, such as the bonus skill points from Educated or the hit point bonus of Hard to Kill. For campaigns that involve magical Edges, some may not be allowed for purchase after first level. Spellcaster, Summoner, and Graced can only be taken by new characters.

## 2.8.0 Crafting and Modifying Gear

PCs with the right skills can build or modify their own gear or that of their teammates.
The specific mods available for weapons, armor, vehicles, and cyberware are listed in the sections dedicated to those forms of gear.

### 2.8.1 Crafting Gear

A tech requires a workshop that could plausibly build the gear in question.
This may be nothing more than a toolkit for a simple device, or a full-fledged pharmaceutical lab for others. They also require a plausible source of parts for the device. This is usually a given if in a city or other salvage-rich area, but it may not be practical in a wilderness. Prudent techs can choose to buy a certain dollar amount of spare parts in the city and pack them along in a vehicle.

Building gear takes a month for a vehicle or a week for a weapon, suit of armor, or other portable complex device.
Very simple devices may be built faster at the GM's discretion.

Crafted tech is made at three levels of quality.
Jury-rigged tech takes one-half the time to build and costs one-quarter the market cost in parts. If scrap salvage is available it can be built at no cost but normal build times. As an improvised device, it counts as a mod requiring Fix-0 to keep functional, as explained below in the mod maintenance rules. If it goes 24 hours without maintenance, it stops functioning. Jury-rigged devices cannot be further modded.

Normal devices cost the same amount in parts as the market cost and take the normal amount of time to build.
They cannot be built with salvaged parts unless the GM decides the salvage is perfectly suited for it. Drugs, explosives, and other consumables must be crafted as normal devices rather than jury-rigged or mastercrafted ones.

Mastercrafted devices cost ten times as much in parts as the usual market cost and take twice as long to build.
They are ideal platforms for a tech's mods, however, and the first mod their creator installs in them requires no maintenance.

### 2.8.2 Modifying Gear

Crafted or purchased gear can be modified by a skilled tech.
The individual gear sections describe example mods that are appropriate.

To install mods in gear, a tech needs a suitable workshop capable of fine craftsmanship.
Such a shop can be rented for $500 a day in the slums or purchased outright for around $20,000.

Crafting mods also requires a minimum Fix skill, or both Fix and Heal in the case of some cyberware mods.
Without this skill level the tech cannot install the mod or maintain it afterwards.

Crafting and installing mods has a cost in dollars and in special tech.
The latter involves prototype devices, restricted materials, and other advanced components that are not normally available on the open market. Techs must usually either acquire these things through missions, obtain them as mission rewards, or steal them from manufacturers. If they want more than they get through their normal employment, they may need to plan specific missions around obtaining them. These components are generic in nature, so a given special component can be used in any mod, and a salvaged mod can be stripped for special components.

It takes one week per minimum skill level of the mod to build and install it.
Thus, if the mod requires Fix-1 skill, it takes one week, while one requiring Fix-2 and Heal-1 requires three. If the tech has an assistant with at least Fix-0 skill, this time is halved. If they do nothing but eat, sleep, and work, this time is further halved.

Pre-built existing mods can be removed or installed with a day's work.
Mods must be built for specific items or cyberware systems; a tech cannot pull a Customized mod off one gun and plug it into another.

### 2.8.3 Maintaining Mods

Mods normally require maintenance to keep functioning correctly, and a given tech can maintain only so many mods at once.
A tech can only maintain mods they have the requisite skill levels to build; if a tech has Fix-1 and the mod requires Fix-2 to build, they can't maintain it.

A tech's maximum maintenance score is equal to the total of their Intelligence and Constitution modifiers plus three times their Fix skill level.
Thus, a tech with a +1 Intelligence mod, a -1 Constitution mod and Fix-1 could maintain up to three mods at any one time.

Maintenance is assumed to take place during downtime and does not require any significantly expensive components.
If a tech does nothing but maintain mods, they can double their allowed number. Such work assumes sixteen-hour workdays.

If a mod goes without maintenance for 24 hours, it stops working.
If it goes without maintenance for a week, the item it's attached to stops working. A maintenance backlog on an item can be cleared by an hour's work by a tech capable of maintaining it.

### 2.8.4 Factory Mods

Some corporate brands, product lines, or special prototypes may come with certain mods pre-engineered into the device.
Sometimes these mods are flat upgrades, while other times they come with certain disadvantages or weaknesses in the gear to compensate.

Such gear usually sells at a premium.
The device's cost is usually increased by at least five times the mod's build price, and each unit of special components that are required for its mods doubles the device's total price.

These factory mods do not require maintenance.
If a tech further mods the gear, however, the delicate balance of engineering in it is disrupted, and the factory mods begin to require maintenance as normal.
The availability of factory-modded gear depends on the campaign and the connections of the PCs.
Some might be relatively available for those with the money, while other items might require special contacts or private information to obtain.

## 2.9.0 Encumbrance

While gear load is seldom as important to cyberpunk operatives as it is to dungeon-crawling adventurers, some limits on the number of guns and amount of ammo any given operator can carry are required.

Gear has encumbrance, measured in points, as exampled in the table below.
The more awkward or bulky the object, the greater the encumbrance. The GM adjudicates ambiguous objects.


| Gear                                    |                              Encumbrance |
|:----------------------------------------|-----------------------------------------:|
| Portable in a small pocket              | 0 (Any reasonable number can be carried) |
| Portable in one hand                    |                                        1 |
| Requires two hands to carry or use it   |                                        2 |
| Requires a whole-body effort to haul it |                                       5+ |
| Dragging an unconscious teammate        |                                       12 |


Gear is either Stowed or Readied.
Stowed gear is packed away carefully in pockets, packs, and harnesses. It's easier to carry but harder to quickly access. Using Stowed gear requires that the bearer take a Main Action to pull it out before using it. Readied gear is carried in hands, holsters, quick-access pockets, or other easily-accessible places. It can be used as part of an action without any further preparation.

A character can carry a total number of Stowed encumbrance points equal to their Strength score.
They can carry a number of Readied points equal to half their Strength, rounded down.  Thus, a PC with a Strength score of 11 could carry 11 points of Stowed gear and 5 points of Readied.

PCs can haul much heavier objects if necessary.
If they push their limits for longer terms, they can carry an additional two Ready and four Stowed items. The first time they do this, their Move speed is cut by 30%, from 10m to 7m. The second time, it’s cut by 50%, from 10m to 5m. More weight than this can’t be practically hauled over significant distances.

### 2.9.1 Bundled Gear

Small, regularly-shaped objects such as grenades, pharmaceuticals, rations, and firearm magazines can be wrapped into bundles for easier portability.
Three such items can be tied into a bundle that only counts as one item of encumbrance. Breaking into this bundle takes an extra Main Action, however.

### 2.9.2 Bulk Weights

Sometimes the PCs need to transport bulk amounts of goods that are measured in kilograms.
When it's necessary to convert these weights into encumbrance points, a GM can just assume that twenty-five kilograms is worth about ten points of encumbrance to a PC hauling a pack out on their back.

When judging the ability of a vehicle to carry encumbrance points of cargo, it can be assumed that a car or truck can carry as much as the PCs need it to carry, within reason.

### 2.9.3 Games Without Encumbrance

Because most cyberpunk missions involve rapid strikes and have access to vehicles, long-term tracking of weights and supplies is less important than it is in a game that involves weeks-long expeditions into the unknown with nothing more than a few pack animals to carry gear.
Some GMs may prefer to omit encumbrance rules in this case.

If the GM so elects, then PCs can carry and Ready whatever amount of gear the GM thinks is reasonable.
In such cases the GM should check over character sheets before each mission to make sure reason is not outraged.

# 3.0.0 Gear, Vehicles, and Cyberware

This section provides a selection of example gear appropriate to most cyberpunk campaigns.
The GM is naturally expected to add other items that might suit their particular world.

## 3.1.0 Mission Gear

The following items are often useful to operatives.
Their prices can be used to extrapolate other costs for other common goods. The prices given assume that the gear is legally available where the PCs are.
Some gear is meant to be worn.
Worn equipment that is being used, such as IR goggles or a gas mask, must be Readied to be employed.

| Gear                          | Cost | Encumbrance |
|:------------------------------|-----:|------------:|
| Active hearing protection     | $250 |           1 |
| Ammunition, empty magazine    |  $10 |           1 |
| Ammunition, per round         |   $1 |           * |
| Backpack or gear harness      |  $25 |          1~ |
| Binoculars                    | $100 |           1 |
| Bus pass, monthly             |  $50 |           * |
| Climbing kit                  | $150 |           2 |
| Clothing, fashionable         | $500 |          1~ |
| Clothing, haute couture       | $10K |          1~ |
| Clothing, ordinary            |  $25 |          1~ |
| Gas mask                      |  $1K |           1 |
| Goggles, Anti-Flash           | $100 |           1 |
| Goggles, IR                   |  $1K |           1 |
| Kit, Basic Tools              | $100 |           2 |
| Kit, Cyberdoc                 | $500 |           2 |
| Kit, Medkit                   | $100 |           1 |
| Kit, Survival                 | $100 |           2 |
| Lockpicks                     | $100 |           1 |
| Military ration, per day      |  $20 |           1 |
| Portable video camera         | $300 |           1 |
| Radio, handheld               |  $50 |           1 |
| Radio, ultralight tab         | $500 |           0 |
| Smartphone service plan/month |  $10 |           - |
| Smartphone, basic             |  $50 |           * |
| Smartphone, fashionable       |  $2K |           * |
| VR crown, cheap               |  $50 |           1 |
| Wearable light                |  $25 |          1~ |

\~: These items have no Encumbrance while worn.<br>
\*: No Encumbrance for any reasonable amount


- **Active Hearing Protection**: This headset protects the wearer from disorientation due to loud noises.
It passes along conversational-level speech without difficulty.
- **Ammunition**: Individual rounds are loaded into empty magazines that are specific to individual firearms.
- **Backpack**: Backpacks, gear harnesses, and other such kit may add some bulk to an operator’s burden, but without them the GM is perfectly justified in asking how a PC is fitting their entire loadout in their jacket pockets.
- **Binoculars**: Most modern binocs have low-light functionality and magnification sufficient to bring targets as much as a kilometer distant into view.
- **Bus pass, monthly**: A critical mobility resource for the first mission of many operators.
- **Climbing kit**: Ultralight cord, a grapnel, some grip handholds, and a fast-acting, ultra-strong wall adhesive make up this kit.
- **Clothing**: Clothing has no Encumbrance when worn, but it’s sometimes prudent to have a spare outfit in your pack.
- **Gas mask**: Bulky and uncomfortable, but once Readied it leaves the user immune to most inhaled gases, including tear gas grenades.
- **Goggles**: Basic anti-flash goggles defuse the visual threat of flashbangs and similar dazzlers.
IR goggles include anti-flash tech, but also give the user effective IR vision out to 50 meters in most environments.
- **Kit**: These bundles of gear are carefully packed for minimal weight.
Toolkits give a tech the gear they need for basic repairs and construction, while medical kits provide first aid implements. Cyberdoc kits contain everything medical kits do, as well as gear for cyber maintenance or even emergency implantation. Survival kits are for badlands operations, with water filtration, firemaking tools, and other such gear for extended wilderness stays.
- **Lockpicks**: Both manual lockpicks and a selection of useful electronic shims sufficient to threaten most modern locks.
- **Military rations**: This food brick is unappetizing but contains the nutrition necessary for a day of heavy activity.
Water can be included for an extra point of Encumbrance per day.
- **Portable video camera**: Palm-sized and discreet, this camera can record up to twelve hours of high-def video before its internal memory is filled.
- **Radio**: Both conventional handheld radios and ultra-convenient headset or collar-tab radios can be had.
Ranges are one kilometer in the city and six in an open, flat area.
- **Smartphone**: Corp-approved media devices are cheap and ubiquitous, but few operators trust them with sensitive information.
- **VR Crown**: A cheap headset for a VR addict or a hacker too poor for a cranial jack.
- **Wearable Light**: These clip-on lights can illuminate up to 30 meters ahead of the wearer.

## 3.2.0 Lifestyles, Wages, and Bribes

Certain operating expenses are common among operators, including the ordinary cost of living, the price of employing subcontractors, and bribes paid to local officials.

### 3.2.1 Lifestyle Costs

Living costs money.
Every operator needs to make room in their budget for rent, soyburgers, and gang protection payments, and failing to find the money for these things will add a load of stress to the PC.

The table below describes common lifestyle costs and the maximum System Strain modifiers that apply while a PC is living such a life.
These modifiers come into effect as soon as a PC starts paying – or not paying – for a given lifestyle level.

Operator teams who obtain their own property or headquarters and make their own deals for local support may be able to lower or eliminate these lifestyle costs at the GM’s discretion.


| Cost/ Month | Lifestyle                                                              |
|------------:|------------------------------------------------------------------------|
|          $0 | Squatter life in an abandoned building<br>-2 maximum System Strain     |
|        $300 | Slum living in a dirty shared apartment<br>-1 maximum System Strain    |
|      $1,000 | Middle-class corper life in a nice rental<br>No System Strain modifier |
|      $5,000 | Fine living in a clean, secure building<br>+1 maximum System Strain    |
|     $20,000 | Luxury life with all its little pleasures<br>+2 maximum System Strain  |

### 3.2.2 Common Wages

Sometimes the PCs need to hire some extra help, or the GM needs to know what a reasonable wage would be for some NPC.
The table below gives the average daily earnings for various types of workers and elites.

Hirelings will carry out their duties with reasonable levels of loyalty, but most won’t risk their lives or face opposition that threatens their destruction.
Bodyguards will act to defend their employers, but only the best will stick around in the face of overwhelming opposition. Ordinary street thugs and hired gangers will be willing to commit acts of extreme violence against enemy gangs or helpless civilians, but few of them have any interest in fair fights under other circumstances.

| Worker                            | Daily Earnings |
|-----------------------------------|---------------:|
| Unskilled street laborer          |            $10 |
| Low-level ganger or street thug   |            $15 |
| Common streetwalker               |            $20 |
| Petty street stall merchant       |            $25 |
| Capable street tech or cyberdoc   |            $50 |
| Competent street bodyguard        |            $50 |
| Entry-level corp worker           |            $30 |
| Low-level corp lifer              |            $40 |
| Street cop or skilled corp worker |           $100 |
| Professional companion            |           $200 |
| Corp-employed hacker              |           $200 |
| Corp middle manager               |           $250 |
| Corp-sponsored cyberdoc           |           $250 |
| Elite or well-known bodyguard     |           $500 |
| Successful gang boss              |           $750 |
| Corp branch office manager        |         $1,000 |
| High-ranking corp exec            |         $5,000 |
| Oligarchic elite of a city        |        $10,000 |

### 3.2.3 Bribe Costs

Bribery is a commonplace in any organization of consequence, and it’s widely accepted that anyone with authority will use it to make a certain amount of side profit.
While technically forbidden, everyone expects that cash will obtain faster service, forgiveness for minor offenses, or access to restricted areas.

Serious bribery is for favors beyond that line.
Favors that could cause problems for a superior, cost the official their job, or buy them trouble with someone else are going to cost far more, if they’ll do it at all.

The bribe costs on the table below are expressed in multiples of the target’s daily wage, as it costs more to bribe a manager than a janitor.
Particular like or dislike for the PCs can move the price accordingly.

| Favor                                       | Bribe Multiple |
|---------------------------------------------|---------------:|
| Look the other way at a minor infraction    |             x2 |
| Pass out information they shouldn’t         |             x2 |
| Do a small favor within their authority     |             x5 |
| Turn a blind eye when it might make trouble |           x14* |
| Turn a blind eye when it will make trouble  |           x60* |
| Do a favor that could have minor backlash   |           x30* |
| Do a favor that might get them punished     |           x90* |
| Do a favor that might get them fired        |          x180* |

\* This is a serious risk, and many NPCs will need reasons beyond simple cash to agree to it

## 3.3.0 Armor

The table below gives the statistics for various common types of armor.

| Civilian Armor      | Ranged AC | Melee AC | Damage Soak | Enc. | Trauma Target Mod | Subtle? |   Cost | Note |
|---------------------|----------:|---------:|------------:|-----:|------------------:|---------|-------:|------|
| Ordinary Clothing   |        10 |       10 |           0 |    0 |                 0 | Subtle  |    $25 |      |
| Reinforced Clothing |        13 |       10 |           2 |    0 |                 0 | Subtle  |   $100 |      |
| War Harness         |        13 |       14 |           5 |    1 |                 0 | Obvious |   $200 |      |
| Street Leathers     |        13 |       12 |           3 |    0 |                 0 | Subtle  |   $250 |      |
| Reinforced Longcoat |        15 |       13 |           5 |    1 |                +1 | Subtle  |   $500 |      |
| Armored Clothing    |        16 |       14 |           5 |    2 |                +1 | Subtle  | $1,000 |      |
| Plated Longcoat     |        17 |       15 |           5 |    3 |                +1 | Obvious | $2,000 | H    |
| Impact Jacket       |        12 |       14 |           8 |    1 |                +1 | Obvious | $1,000 |      |


| Suit Armor          | Ranged AC | Melee AC | Damage Soak | Enc. | Trauma Target Mod | Subtle? |     Cost | Note |
|---------------------|----------:|---------:|------------:|-----:|------------------:|---------|---------:|------|
| Light Armored Suit  |        16 |       13 |           5 |    2 |                +2 | Obvious |  $5,000@ |      |
| Medium Armored Suit |        18 |       14 |          10 |    3 |                +2 | Obvious | $10,000@ |      |
| Heavy Armored Suit  |        20 |       18 |          15 |    3 |                +3 | Obvious | $20,000@ | H    |


| Armor Accessories   | Ranged AC | Melee AC | Damage Soak | Enc. | Trauma Target Mod | Subtle? |   Cost | Note  |
|---------------------|----------:|---------:|------------:|-----:|------------------:|---------|-------:|-------|
| Riot Shield         |        +2 |       +4 |           0 |    2 |                 0 | Obvious | $1,000 |       |
| Absorption Plates   |        +2 |       +2 |           3 |    1 |                 0 | Obvious |   $500 | H, NS |
| Joint Reinforcement |        +1 |       +1 |           0 |    0 |                 0 | Subtle  |   $250 | H, NS |
| Obsolete Tech       |      -1d2 |     -1d2 |        -1d4 |    0 |                 0 | -       |   x50% |       |

@: This armor requires a relevant Contact to purchase, or some other special opportunity<br>
H: The armor or accessory applies a -1 penalty to all Sneak or Exert skill checks.
Multiple heavy items stack.<br>
NS: This accessory cannot be added to suit armor.

- **Armored Clothing**: The bulky plates and heavy ballistic weaves of this outfit are disguised as drapes and fashion elements.
- **Heavy Armored Suit**: A head-to-toe suit of interlocking plates and impact absorption units, these suits are reserved for high-end response teams and military operations.
- **Impact Jacket**: Built to absorb the impact of various contact sports, extreme athletic pursuits, and unguided vehicle races, these jackets usually come as part of a full-body protective outfit.
- **Light Armored Suit**: Ordinary law enforcement and security guards will be wearing suits like this, usually brightly emblazoned with their corporate affiliation.
- **Medium Armored Suit**: Heavy security usually sports armor on this level, including police rapid response teams.
- **Ordinary Clothing**: Some bold, impoverished, or highly fashionable people still insist on wearing clothing that has no functional use as armor.
- **Plated Longcoat**: An ankle-length coat, robe, or mantle is used as a substrate for numerous reinforced plates and swaths of ballistic weave.
It’s heavy, bulky, and blatant, but it can absorb a lot of punishment.
- **Reinforced Clothing**: Subtle polymer sheets and reinforced weaves are used to provide a modicum of protection in this clothing without sacrificing taste.
- **Reinforced Longcoat**: Long, draping clothing is bolstered with advanced fabrics and discreetly-placed absorption plates, the whole remaining deniable as armor.
- **Street Leathers**: Fashionable, aggressive, and often mismatched, “street leathers” seldom have any non-synthetic leather in their makeup, but are light and comfortable enough to wear as a habit.
- **War Harness**: An upgraded form of street leathers, these outfits have sacrificed all discretion for whatever spare ballistic plates and advanced polymer weaves its owner can sew or bolt on.
It’s usually the best armor that local gangs or common thugs can afford.

### 3.3.1 Armor Terms and Definitions

- **Ranged AC**: The Armor Class target for ranged attacks against the wearer.
Both ranged and melee AC is always modified by the target's Dexterity mod.
- **Melee AC**:  The Armor Class target for melee attacks against the wearer.
- **Damage Soak**: The amount of hit point damage the armor can absorb before allowing damage through to the wearer.
Damage Soak cannot protect against injury that armor could not normally lessen. Damage Soak ablates during combat and refreshes to full after each fight.
- **Encumbrance**: The number of encumbrance points the armor adds.
Armor must be Readied to be useful.
- **Trauma Target Mod**: This modifier is added to the wearer's Trauma Target for purposes of determining Traumatic Hits.
A normal human's base unarmored Trauma Target is 6.
- **Obviousness**: Subtle armor can pass for ordinary streetwear and can be worn in polite social circumstances without drawing remark.
Obvious armor is clearly intended for combat and will make onlookers think the wearer is expecting imminent trouble.
- **Cost**: The usual price for the armor through its normally-accessible channels.
Some armor is restricted for public sale and will require an appropriate Contact to buy or some mission-generated opportunity.

### 3.3.2 Armor Accessories

Some armor can be enhanced with additional protection such as absorption plates or joint reinforcement.
These additions may increase the armor's bulk or obviousness, but they also improve the armor's AC or damage soak. A given accessory can only be applied once to any set of armor. Armor accessories cannot be modded with armor mods.

The three types of suit armor listed on the table above are built as cohesive systems, and cannot accept these accessories.
A riot shield can still be used while wearing a suit, however.

- **Riot Shield**: These bulky, transparent shields are most effective in fending off melee attackers.
They must be Readied in one hand; users with multiple cyberarms can benefit from only one shield at a time. A shield negates the first melee Shock damage a bearer takes in a round.
- **Absorption Plates**: Additional impact-absorbing buffer plates are added to the armor.
Their bulk inflicts a -1 penalty to the user’s Sneak and Exert checks.
- **Joint Reinforcement**: The weak points around limb joints are reinforced with extra layers of weave.
The extra weave hinders movement, and applies a -1 penalty to the wearer’s Sneak and Exert checks.

### 3.3.3 Obsolete Armor Tech

Bargains can be had for last year’s fashions, whether in armor or accessories.
Such items cost only half as much as usual, but suffer a 1d2 penalty to both their ranged and Melee ACs, down to a minimum of AC 10. They also absorb 1d4 fewer Damage Soak, to a minimum of 0. These dice are only rolled after the obsolete armor has been purchased, so the buyer won't know exactly what they have until they try to fit it to an outfit.

### 3.3.4 Armor Mods

The mods listed here can be applied to armor by a tech with the listed skill level at the given costs, as explained in section 2.8.2.
Some require a certain number of units of special technology as well as ordinary parts. Unless specified otherwise, a given mod can be applied only once to an item.


| Mod             | Skill |    Cost | Special Tech |
|-----------------|:------|--------:|-------------:|
| Absorption Pads | Fix-2 |  $2,000 |            0 |
| Active Response | Fix-3 | $20,000 |            2 |
| Biostabilizing  | Fix-1 |  $2,500 |            0 |
| Customized      | Fix-1 |  $1,000 |            0 |
| Discreet Design | Fix-2 |  $5,000 |            1 |
| Flexible        | Fix-2 | $10,000 |            1 |
| Quickchange     | Fix-1 |  $1,000 |            0 |
| Sealed          | Fix-1 |  $2,500 |            0 |
| Tailored Rig    | Fix-1 |  $2,000 |            0 |
| Trauma Dampers  | Fix-3 | $10,000 |            1 |
| Whisperlight    | Fix-2 | $10,000 |            1 |


- **Absorption Pads**: Kinetic padding is added to the armor, granting it 5 more hit points of damage soak per fight.
- **Active Response**: The armor has hyper-advanced reactive defenses that deflect and mitigate otherwise-critical injuries.
Increase the wearer’s Trauma Target by 1. This stacks with any existing Trauma Target bonus the armor may already confer on the wearer.
- **Biostabilizing**: An integral biostatus monitor triggers emergency trauma drugs when the wearer is Mortally Wounded.
As an Instant action, they roll 2d6+2 versus difficulty 8 to stabilize. Only one such attempt can be made before the suit needs an hour to recalibrate.
- **Customized**: The armor is tailored to a specific wearer, granting them a +1 bonus to both ranged and melee AC while it’s worn.
- **Discreet Design**: The armor is carefully tailored and adjusted to be less imposing.
Obvious armor is made Subtle by this mod, though it suffers a -2 penalty to its ranged and melee AC. The armor retains its original bulk, but its lines, drapes, and fabrics now appear to be particularly voluminous fashion couture rather than protective equipment.
- **Flexible**: Bulky armor is carefully cut and tailored to allow a wearer the optimal range of movement.
The Heavy quality of a suit of armor is removed.
- **Quickchange**: The armor or clothing is designed with chroma-shifting fabrics, bi-facial texture panels, and removable accessories.
As a Main Action, the wearer can shift its appearance to that of a different type of clothing or armor with the same level of obviousness. The details of this alternate appearance can be set with an hour of adjustment.
- **Sealed**: The armor is equipped with a concealed filter hood and temporary pressure gaskets.
As a Main Action, the user can seal the clothing against the external atmosphere, rendering them immune to gases and contact toxins. The seal and its integral air supply last 30 minutes, after which an hour of cleaning and purging will be needed to refresh the system.
- **Tailored Rig**: Pouches, attachment points, holsters, and other rigging are designed for a particular wearer’s body contours.
They gain 1 point of Readied Encumbrance and 2 points of Stowed Encumbrance added to their maximum. These items are clearly visible to others, however, and cannot be concealed.
- **Trauma Dampers**: This mod can only be installed after Absorption Pads are in place.
Once fully engineered, they add 5 additional hit points of damage soak per fight.
- **Whisperlight**: The armor’s bulk is cut down and conventional materials are replaced with more advanced composites.
The armor’s Encumbrance decreases by 1 point.

## 3.4.0 Weapons

Operators favor a wide variety of implements of destruction.
The ones listed below are only some of the most common.

### 3.4.1 Weapon Stats and Definitions

**Dmg** is the die it rolls for hit point damage on a successful hit.
This damage is always increased by the weapon’s relevant attribute modifier, and it may be further boosted by certain Foci or mods.

**Range** is expressed in meters for guns and thrown weapons, with a normal range and an extreme range.
Attacks beyond normal range take a -2 hit penalty.

**Cost** is the average street cost for the weapon.

**Shock** applies only to melee weapons.
If the target’s melee AC is equal or less than the weapon’s Shock rating, the target takes the listed Shock damage even if the attack misses. Shock damage is modified by the weapon’s attribute modifier and any damage bonuses that explicitly add to Shock.

**Mag** is the number of rounds in a loaded firearm.
Each attack costs one round, while using a weapon in burst fire mode costs three. Reloading a weapon is usually a Main Action.

**Attr** is the attribute that modifies the weapon’s hit and damage roll.
If two attributes are listed, you can use whichever one has the better modifier.

**Encumbrance** is the number of items of encumbrance the weapon takes up.
Weapons must be Readied to use them; if not, the attacker must spend a Main Action getting them ready for employment.

**Trauma Die** is the die rolled when a lethal weapon hits.
If it’s equal or higher than the target’s Trauma Target, which is usually 6, the victim takes a Traumatic Hit. Non-lethal weapons and attacks do not roll the Trauma Die.

**Trauma Rating** is the multiplier applied to a weapon's total damage when it makes a Traumatic Hit.

#### 3.4.1.1 Burst Firing

Some firearms can fire in bursts.
Such attacks use up three rounds of ammunition and add +2 to the weapon's hit roll and damage die.

#### 3.4.1.2 Suppressive Fire

If a weapon can fire to suppress, the wielder may do so as a Main Action.
Each round of suppressive fire costs twice the usual ammo.

The attacker picks a 90-degree cone and fills it with lead.
Any targets within the weapon’s normal range who are not behind hard cover must make an Evasion save or take half the weapon’s damage, rounded up. The attacker rolls the weapon’s Trauma Die individually against each victim struck, and may inflict Traumatic Hits with this damage.

#### 3.4.1.3 Grenade Weapons

Grenades may be thrown at a point within range and always target AC 10.
On a miss, the grenade bounces 1d10 meters in a random direction before exploding. Most grenades have a 5-meter radius of effect.

### 3.4.2 Firearms

| Firearm           |    Dmg |       Range |     Cost | Mag | Attr. | Enc. | Trauma Die | Trauma Rating |
|-------------------|-------:|------------:|---------:|----:|-------|-----:|-----------:|--------------:|
| Light Pistol      |    1d6 |       10/80 |     $200 |  15 | Dex   |    1 |        1d8 |            x2 |
| Heavy Pistol      |    1d8 |      10/100 |     $200 |   8 | Dex   |    1 |        1d6 |            x3 |
| Advanced Bow      |    1d8 |      30/200 |     $500 |  1§ | Dex   |    2 |      1d8+1 |            x3 |
| Rifle             | 1d10+2 |     200/400 |   $1,000 |   6 | Dex   |    2 |        1d8 |            x3 |
| Combat Rifle      | 1d12\* |     100/300 |  $2,500@ |  30 | Dex   |    2 |        1d8 |            x3 |
| Submachine Gun    |  1d8\* |      30/100 |  $2,000@ |  20 | Dex   |    1 |        1d6 |            x2 |
| Shotgun           |    3d4 |       10/30 |     $200 |   2 | Dex   |    2 |       1d10 |            x3 |
| Semi-Auto Shotgun |    3d4 |       10/30 |   $1,000 |   6 | Dex   |    2 |       1d10 |            x3 |
| Combat Shotgun    |  3d4\* |       10/30 |  $3,000@ |  12 | Dex   |    2 |       1d10 |            x3 |
| Sniper Rifle¶     |    2d8 | 1,000/2,000 |   $3,000 |  1§ | Dex   |    2 |       1d10 |            x4 |
| Taser Pistol      |    1d8 |       10/15 |     $500 |   2 | Dex   |    1 |          - |             - |
| Automatic Rifle   |   2d8# |     200/400 | $10,000@ |  10 | Dex   |    4 |       1d10 |            x3 |

\*: This weapon may use burst fire when attacking, firing three rounds to gain +2 to hit and damage rolls.<br>
§: This weapon can be reloaded with a Move action, or an On Turn if the user has at least Shoot-1 skill<br>
¶: If not fired from a stationary rest, this weapon’s statistics are the same as a regular rifle<br>
@: This weapon requires an applicable Contact to buy, being generally illegal for open sale<br>
#: This weapon can fire to suppress if braced against a solid support or the gunner is prone


**Light and heavy pistols** are categorized by the caliber of the bullet they fire.
Some operators prefer the low recoil and high mag capacity possible with a low-caliber round, while others like to have a little more certainty in dropping the targets they hit.

**Advanced bows** are used by a few specialists who prefer the near-silence that modern composites provide or the massive blood loss induced by a modern fractal broadhead carried on a fifty-gram arrow.

**Rifles and combat rifles** are similar semi-automatic tech, though the latter is equipped with a burst-fire selector and usually trades the range of a heavier caliber for the ability to carry more ammunition.

**Submachine guns** are popular for their small form factor and convenient burst fire, though like other burst-fire weaponry the corps frown heavily on civilian possession.

**Shotguns** of various stripes can be found under the counter of practically every bar and shop in a slum.
Simple double-barrel models can be made by any home gunsmith with a pulse, but the finer tolerances and more elaborate machining of a burst-fire capable combat shotgun leaves them as a more selective product. Ranges and damages given are for shot shells; slugs double their normal and extreme ranges but apply a -1 penalty to hit rolls.

**Sniper rifles** are often nothing more than bolt-action rifles with expensive tuning and optics.
If not fired from a rest in a prepared position, a sniper rifle’s statistics are the same as a normal rifle.

**Taser pistols** cover a family of ranged non-lethal weapons small and light enough to carry concealed.
Most rely on tried-and-true electrical shocks to drop a target; they inflict non-lethal hit point damage and cannot make Traumatic Hits.

**Automatic rifles** include a large range of barely man-portable firearms designed to hurl vast amounts of lead through the air, and unlike smaller firearms, they can use suppressive fire if properly braced.
Ammunition for these guns comes in belts or boxes. The mag rating reflects how many rounds the weapon can fire before needing a refill, with each “round” costing $50 worth of ammunition. The compactness of their loaded magazines still only counts as one item, however.

### 3.4.3 Melee and Thrown Weapons

Virtually every ganger or thug has at least a knife on their person.
The quality of such weapons can vary from bits of curb-sharpened metal to the latest monomolecular-edge fighting knives.


| Weapon             |   Dmg | Range |   Cost |   Shock | Attr.   | Enc. | Trauma Die | Trauma Rating |
|--------------------|------:|------:|-------:|--------:|---------|-----:|-----------:|--------------:|
| Unarmed Attack     |  1d2^ |     - |      - |       - | Str/Dex |    - |        1d6 |            x1 |
| Knife              |   1d4 | 10/20 |    $20 | 1/AC 15 | Str/Dex |    1 |        1d6 |            x3 |
| Club               |  1d4^ | 10/20 |    N/A | 1/AC 18 | Str     |    1 |        1d6 |            x2 |
| Spear              |   1d6 | 10/20 |    $50 | 2/AC 13 | Str/Dex |    1 |        1d8 |            x3 |
| Sword              |   1d8 |     - |   $200 | 2/AC 13 | Str/Dex |    1 |        1d8 |            x2 |
| Big Sword          |   2d6 |     - |   $500 | 2/AC 15 | Str     |   2* |        1d8 |            x3 |
| Big Club           | 1d10^ |     - |   $100 | 2/AC 18 | Str     |   2* |        1d8 |            x3 |
| Advanced Knife     |   1d6 | 10/20 |   $200 | 2/AC 15 | Str/Dex |    1 |        1d8 |            x3 |
| Advanced Sword     |  1d10 |     - | $1,000 | 3/AC 15 | Str/Dex |    1 |        1d8 |            x3 |
| Advanced Big Sword |   2d8 |     - | $2,500 | 4/AC 15 | Str     |   2* |        1d8 |            x3 |
| Advanced Club      |  1d8^ |     - |   $500 | 2/AC 18 | Str     |    1 |        1d8 |            x3 |
| Grenade, Flash     | Spec. | 10/30 |    $50 |    None | Dex     |    1 |          - |             - |
| Grenade, Frag      |   2d6 | 10/30 |  $100@ |    None | Dex     |    1 |        1d8 |            x2 |
| Grenade, Gas       | 1d10^ | 10/30 |    $50 |    None | Dex     |    1 |          - |             - |
| Grenade, Smoke     |     - | 10/30 |    $25 |    None | Dex     |    1 |          - |             - |

^: This weapon’s damage is always non-lethal, unless desired otherwise.
Non-lethal hits don’t roll the Trauma Die.<br>
\*: This is a two-handed weapon, making it impossible to have a Readied item in the user’s off-han<dbr>
@: This weapon requires an applicable Contact to buy, being generally illegal for open sale

**Knives, clubs, swords**, and other simple melee weapons are available almost anywhere on the street.
Particularly impoverished gangers sometimes fashion makeshift machetes and improvised shivs when better can’t be obtained. Such weapons are effectively free, but have a -1 penalty to hit and damage rolls due to their ungainly make.

**Advanced weapons** employ modern polymers, sophisticated metallurgy, fractal edges, electro-stun tech, and a small legion of marketers to sell products that are distinctly superior to the plain steel edges of a prior age.

**Flash grenades** force victims without eye and ear protection to make a Physical save; on a failure, the victim loses their next Main Action and takes a -2 penalty to hit rolls and AC for the next 1d6 rounds.
Flashbang effects don’t stack, and a target who makes their save cannot be flashbanged again that same round.

**Frag grenades** inflict 2d6 damage on all victims in range, plus the chance for a Traumatic Hit.
An Evasion save can halve a frag grenade’s damage, and each point of melee AC the target has above 14 reduces the final damage total by one point, possibly down to zero.

**Gas grenades** create a 10-meter-diameter cloud of opaque, choking, irritating gas that disperses in one round.
They inflict non-lethal damage, halved on a successful Physical save, and are useless against targets with breathing protection. They provide full concealment for those within the gas.

**Smoke grenades** are harmless, but provide the same 10-meter dome of concealment where it’s necessary.
Gas and smoke aren’t enough to defeat IR or augmented visuals.

### 3.4.4 Heavy Weapons

While uncommon, some operators may be unfortunate enough to run into military opposition or antagonists who have no concerns about drawing intolerable heat from the local authorities.
These combatants are likely to pack weaponry much heavier than a normal street operative would sport. For operators, using weapons like the ones below on most missions would risk perilously intense reprisals by worried corps and local police.


| Heavy Weapon           |     Dmg |   Range | Cost    | Mag | Attr. | Enc. | Trauma Die | Trauma Rating |
|------------------------|--------:|--------:|---------|----:|-------|-----:|-----------:|--------------:|
| Anti-Materiel Rifle ¶  |    3d8! |   1K/2K | $8,000@ |   5 | Dex   |    3 |       1d12 |            x3 |
| Grenade Launcher       | Grenade | 150/350 | $3,000@ |   3 | Dex   |    1 |          - |             - |
| Demo Charge            |   3d10! |   20/20 | $1,000@ |   - | -     |    1 |       1d10 |            x3 |
| Heavy Machine Gun ¶    |   3d6#! |  500/2K | $10K@   |  10 | Dex   |    3 |       1d12 |            x3 |
| Mortar                 |     3d6 |   1K/2K | $5,000@ |   1 | Wis   |    3 |       1d12 |            x3 |
| Rocket Launcher        |   3d10! |   2K/4K | $5,000@ |   - | Dex   |    2 |       1d10 |            x3 |
| Land Mine, Anti-Pers.  |     1d8 |       - | $150@   |   - | -     |    1 |       1d10 |            x3 |
| Land Mine, Anti-Vehic. |    3d8! |       - | $1,000@ |   - | -     |    2 |       1d20 |            x3 |

@: This weapon requires an applicable Contact to buy, being generally illegal for open sale<br>
¶: If not fired from a stationary rest, this weapon’s statistics are the same as a regular rifle<br>
#: This weapon can fire to suppress if fixed to a vehicle or stationary firing position<br>
!: This weapon’s Trauma Die can inflict Traumatic Hits on drones, vehicles, and other machines.<br>


**Anti-materiel rifles** have encountered something of a renaissance with the ubiquity of armored drones and heavily cybered combatants.
While not suitable for downing tanks or other heavily armored vehicles, their 20mm rounds are more than sufficient to punch through lighter armor plating, concrete walls, and full body conversion cyborgs. Use of a secure firing rest is recommended for best results.

**Grenade launchers** are accessories that are mounted on rifle-sized firearms, adding 1 Encumbrance to their bulk.
These weapons can launch specially-designed grenades to the listed range, targeting them as if they were thrown normally. These special grenades cost and function the same as their regular equivalents but cannot be thrown by hand. When attacking, the user can choose whether to fire the grenade launcher or the weapon it’s mounted to, but both cannot be fired in the same round.

Stand-alone grenade launcher models are available for vehicle or drone mounting, counting as 2 Encumbrance but otherwise having the same cost and statistics.

**Demo charges** are shaped explosive charges usually meant to knock human-sized holes in anything short of fortified defensive walls.
While ineffective as thrown weapons, when used as part of a trap or ambush they inflict their listed damage in a cone out to normal range, with an Evasion save for half damage. If applied carefully to a vehicle target, the following explosion will inevitably destroy it. Like frag grenades, use of demo charges tends to be very exciting to security forces and can provoke more heat than many operators like to draw.

**Heavy machine guns** are old tech, but reliable.
Without a fixed firing position or vehicle mount, however, their recoil makes them almost uncontrollable. Due to the mass of lead involved in firing an HMG, each “round” of its ammunition costs $100 and counts as a full item of encumbrance.

**Mortars** are rare, but have their uses amid the tall buildings of an urban sprawl.
They have a minimum range of 20 meters, but can lob indirect fire over buildings and obstacles. Mortars always target AC 20 at first, but each round of fire at the same target grants a +1 hit bonus if there’s a forward spotter to adjust the aim. Mortar rounds act as frag grenades on the target point, but do 3d6 damage at base with a 10-meter radius. Armor doesn’t lessen harm from mortar rounds. Relatively simple as they are, a mortar round costs $50 and counts as one item of encumbrance.

**Rocket launchers** include a wide variety of single-shot disposable launchers.
Their general imprecision forces a -4 hit penalty against human-sized targets, but they inflict frag grenade damage on any target within 5 meters of the rocket’s landing point. Specialized anti-armor warheads lose this frag grenade bonus, but do double damage to vehicles or obstacles. Operators who intend to take down enemy vehicles favor them for their effectiveness in that role.

**Land mines** come in anti-personnel and anti-vehicle flavors.
The first activates on any human-sized approach within 1 meter, inflicting damage in a 2-meter radius with an Evasion save for half. Anti-vehicle mines can only be activated by vehicular levels of ground pressure or vehicle-sized proximate metal masses, but do their damage in a 5-meter radius. Humans get an Evasion save for half damage in either case.

### 3.4.5 Weapon Mods

The mods listed here can be applied to weapons by a tech with the listed skill level at the given costs, as explained in section 2.8.2.
Some require a certain number of units of special technology as well as ordinary parts. Unless specified otherwise, a given mod can be applied only once to an item.

| Mod                 | Skill |    Cost | Special Components |
|---------------------|:------|--------:|-------------------:|
| Autotargeting       | Fix-1 |  $5,000 |                  0 |
| Concealed           | Fix-2 |  $5,000 |                  1 |
| Customized          | Fix-1 |  $1,000 |                  0 |
| Extended Mag        | Fix-1 |  $1,000 |                  0 |
| Heavy Sabot         | Fix-1 |  $2,000 |                  0 |
| Integral Toxins     | Fix-2 | $10,000 |                  1 |
| Onboard Gunlink     | Fix-2 | $10,000 |                  1 |
| Predictive Guidance | Fix-3 | $15,000 |                  2 |
| Reel Wires          | Fix-1 |  $2,500 |                  0 |
| Savage Impact       | Fix-1 |  $5,000 |                  0 |
| Shock Burst         | Fix-2 |  $5,000 |                  0 |
| Stun Rounds         | Fix-2 |  $5,000 |                  0 |
| Thermal Charge      | Fix-2 |  $7,500 |                  0 |


- **Autotargeting**: The weapon has kinetic sensors embedded in it that synchronize with onboard targeting chips.
The user gains a +1 bonus to all hit rolls with the weapon, but needs to connect with it via either a Gunlink or a Cranial Jack.
- **Concealed**: Materials are replaced, outlines are altered, and appearances are adjusted.
Pistol-sized and smaller weapons now cannot be detected without a full minute of patting down the bearer, while anything no larger than a rifle can be concealed as if it were a pistol, unfolding to full size upon use.
- **Customized**: The weapon’s balance and form are tailored to a specific user.
That user gains a +1 bonus to all hit rolls with the weapon.
- **Extended Magazine**: Caliber adjustments, feed assists, and magazine dimension alterations double a gun’s reliable magazine capacity.
- **Heavy Sabot**: Applicable only to projectile weapons, the gun is modified to fire heavy AP rounds that can plausibly land Traumatic Hits even on armored vehicles or heavy drones.
- **Integral Toxins**: A melee or thrown weapon is impregnated with numerous microcells of an advanced hemotoxin, the cells rupturing on impact with a target.
The weapon does +2 damage and Shock to subjects not immune to poison.
- **Onboard Gunlink**: Usable only on firearms, the weapon’s onboard targeting hardware emulates the effects of the Gunlink cyber-system.
The user must have a Cranial Jack system to plug it in.
- **Predictive Guidance**: An entire onboard computing core is augmented by advanced materials and cutting-edge kinetic analysis to increase the odds of landing a lethal blow.
The weapon gains a +1 bonus to hit rolls, damage, and Shock. The user must be connected via a Gunlink or a Cranial Jack cybersystem.
- **Reel Wires**: A thrown weapon is linked to a wrist spool with retractable wires that allow its retrieval as an On Turn action.
The wires can be cut by a properly-timed attack with a sharp weapon, but are too thin and sharp to be easily grabbed. The wires can drag up to 20 kilos of mass when retracting. Replacing broken wires takes fifteen minutes.
- **Savage Impact**: Usable only on melee weapons, powered impact plates, vibrating serrations, or other after-market adjustments increase its damage and Shock by +1.
- **Shock Burst**: Once per fight, as an On Turn action, the weapon can imbue itself or a round of ammunition with an electrical charge, inflicting an additional 2d6 damage on a hit, or +2 damage to melee Shock on the next attack.
This damage is not affected by the +3 cap to mod hit and damage.
- **Stun Rounds**: Usable only on firearms, the weapon’s barrel and firing mechanism is altered to accept soft, electrically-charged stun rounds.
The firearm’s range is halved, it takes a -2 penalty to damage, and it loses its Trauma Die, but all damage it inflicts is treated as non-lethal. Stun rounds do no significant harm to inanimate objects.
- **Thermal Charge**: Usable only on melee or thrown weapons, the mod adds a bank of heat cells to the weapon.
The weapon then inflicts +2 damage and Shock to all creatures not impervious to heat. The cells are activated as an On Turn action, but last for only two fights before requiring an hour to swap batteries and let the elements cool.

## 3.5.0 Pharmaceuticals and Street Drugs

Injecting or ingesting a drug requires a Main Action.
Some drugs require medical expertise to administer safely. The table gives the minimum Heal skill required to apply a drug, or “None” for those so user-friendly that anyone could shoot it up.

Some drugs inflict System Strain on use.
If the total would exceed the user's maximum System Strain they get no benefit from the chem but suffer any negative effects normally.

Some drugs grant bonuses or combat benefits.
In the case that multiple drugs are taken at once, only the highest bonus applies


| Chem                 |    Cost | Min. Heal |
|----------------------|--------:|----------:|
| Avalanche            |    $100 |         0 |
| Boneshaker           |     $10 |      None |
| Chokeout             |     $50 |      None |
| Control-Delete       |     $25 |      None |
| Hellbender           |    $100 |      None |
| Lurch                |     $25 |         0 |
| Madeleine            |    $100 |      None |
| Medical prescription |     $20 |      None |
| Olympus              |    $100 |         0 |
| Panacea              |    $200 |         1 |
| Pillow               |     $10 |         0 |
| Psycho               |     $10 |      None |
| Reset                | $1,000@ |      None |
| Sand                 |      $2 |      None |
| Trauma Patch         |     $50 |      None |
| Window               |    $200 |         1 |

@: This chem is rare enough to require a Contact or other connection to obtain it on the street.

- **Avalanche**: A somewhat pricey combat drug favored by shock troops and those first out of the trenches, Avalanche numbs sensations of pain or exhaustion for one hour, granting the user an effective +10 to their current hit points, even if it takes them above their maximum.
This numbness tends to make it harder to notice serious harms, however, and the d12 roll for any Major Injury suffered while under its effects takes a -1 penalty.
- **Boneshaker**: A filthy mix of jailbroken corp industrial chems and amphetamine scrapings, Boneshaker is a popular combat drug among the most impoverished or savage street gangs.
On application, the subject is filled with blind aggression, gaining a +2 Morale bonus and a +2 bonus to hit rolls, damage, and Shock. The recklessness induced by Boneshaker leaves them wide open, however, and all attacks against them add +2 to any Trauma Die rolls. Boneshaker lasts for one scene and adds 2 System Strain at the end of it.
- **Chokeout**: A chem that briefly induces severe swelling of the mucous membranes, Chokeout is odorless and tasteless, but must be consumed in food or drink and naturally degrades after twelve hours.
A subject who consumes a dose of Chokeout must make a Physical save; on a failure, they are strangled to unconsciousness for an hour, waking up with one hit point at the end. On a success, they take 1d10 non-lethal damage as they struggle to breathe. On a natural saving throw of 1, the victim chokes to death.
- **Control-Delete**: A chemical that temporarily destroys a person’s ability to form long-term memories, Control-Delete must be ingested in food or drink.
Toxin-filtering cyber can fend it off, but it otherwise grants no saving throw. Once affected, the target will continue to act normally, but anything that happens to them between then and their next sleep or unconsciousness will be completely forgotten by the time they awake.
- **Hellbender**: A watery black toxin that must be administered through food or direct injection, Hellbender causes the victim to convulse with bone-cracking force shortly after consumption.
A dose of Hellbender has a distinct acrid taste, but if the victim consumes it they must make a Physical saving throw or begin convulsing helplessly for 1d6 rounds, suffering 1d10 damage each round unless restrained by at least two associates. This damage can kill an unfortunate victim.
- **Lurch**: An emergency stimulant meant to shock a downed ally back into mobility, Lurch heals 1d10 damage plus the physician’s Heal skill on application, adding one System Strain to the target.
Each dose of Lurch after the first each day increases the required Heal skill by one, so the third dose requires Heal-2 to successfully administer. Lurch only works on conscious and stabilized targets; the shock will kill a Mortally Wounded subject that hasn’t been made stable.
- **Madeleine**: A chem that intensely stimulates a subject’s memory centers, Madeleine has its dangers.
Users normally focus on a particular memory before injecting the drug; for the next 1d6 hours, they relive some portion of that memory with perfect fidelity while remaining oblivious to their physical surroundings. Psychological addiction is a considerable risk for long-term users, as well as choosing a less pleasant memory to recall.
- **Medical Prescription**: One of a thousand different medical drugs, a prescription like this is often necessary to treat some chronic condition or keep a medical problem from growing worse.
Corp employees get their drugs for a pittance, but less fortunate souls need to source their drugs from extortionate corp pharmacies or street docs. Lucky users only need a dose a few times a week, but others must have a daily supply or suffer potentially lethal complications.
- **Olympus**: A “respectable” combat chem used by certain units of corporate security, Olympus numbs pain, sharpens mental focus, and slows down instinctive panic or rush instincts, allowing a calm observation of the battlefield.
Users can reroll their first failed Morale check in a fight and gain a +2 bonus to hit rolls. One dose of Olympus lasts for one scene, and adds one System Strain at its end.
- **Panacea**: Not so much a single drug as a pharmaceutical suite in a single injection system, this chem requires a skilled physician to adjust its mix.
When used as part of a first aid attempt, this drug doubles the amount of hit points recovered by the attempt, with a minimum healing rate of 6 HP. Rumors of a drastically increased rate of bone cancers among regular users are, of course, mere slander against its manufacturer.
- **Pillow**: A powerful sedative that requires medical expertise to administer properly, Pillow must be injected into a restrained or helpless subject, as vigorous physical action can disrupt the effects.
Once injected, the subject falls into a torpor indistinguishable from death that lasts for 24 hours. They require only trace amounts of oxygen while in their trance, and can be identified as alive only through careful medical examination. If Pillow is administered with less than a 24-hour gap between doses, the victim must make a Physical save or die sometime before the dose ends.
- **Psycho**: Ingesting one of these small pills rapidly flattens a user’s capacity for emotional distress while heavily increasing their aggressiveness.
For roughly one hour, users gain a +2 bonus to Morale checks and experience no immediate emotional trauma from performing even the most heinous and violent actions. The drug is a favorite pre-fight dose among gangers, and used as self-medication among many of them who would otherwise be incapacitated by the emotional scars of what they’ve done.
- **Reset**: A rare drug of desperation, Reset is a combination injector patch and wireless cyber override key.
When applied to a willing subject with at least one Body or Nerve cyber system, it deluges the user’s system with safety override commands and neurotransmitter stimulants. They lose all accumulated System Strain in excess of their permanent minimum. Five minutes later, their System Strain is maximized and they must make a Physical saving throw at a penalty equal to the number of System Strain they’ve gained since the drug was taken. On a success, they drop to 1 hit point. On a failure, they become Mortally Wounded. If their adjusted roll is 1 or less, they must make a second modified Physical save or instantly and unavoidably die. Using Reset more than once a week is invariably fatal.
- **Sand**: A gritty powder trafficked to those addicts too poor to afford anything better, a snorted dose of Sand will put its user into a euphoric haze for as long as an hour, depending on tolerance.
Beautiful visions and dreamy contentment give way to shooting pains and intense light sensitivity afterwards, with most heavy users dying within six to twelve months of picking up the habit.
- **Trauma Patch**: A theoretically foolproof cocktail of coagulants, stimulants, and painkillers, a trauma patch can be applied to a Mortally Wounded ally to stabilize them on a successful Int or Dex/Heal skill check against difficulty 6, with a +1 difficulty penalty for each full round since the subject went down.
The patch is useless after six rounds.<br>
  **Trauma patches cannot revive victims downed by poisons or diseases, or those dismembered by explosives or heavy weapons.**
- **Window**: A neurobooster favored among hackers and knowledge workers, Window augments the user’s focus and mental reaction speed at the cost of distracting them from their terrestrial surroundings.
A Window user can trade their Move action to gain a bonus Main Action that can only be used for cyberspace actions. Each time this is done in a scene, however, the user gains one System Strain. One dose lasts for one scene.

## 3.6.0 Cyberware

Implanted cybernetic hardware is a staple of the cyberpunk genre.
This section discusses the rules for its implantation, maintenance, and use.

### 3.6.1 Installing Cyberware

Cyberware must be implanted by a physician with at least Heal-1 skill or an NPC with at least a +1 skill bonus at medical matters.
The average street doc will have a +2 skill total, with the better having +3 and the elite having +4 or even +5. Access to such high-grade medical care often requires a Contact or mission.

Paired systems such as cybereyes or cyberears are implanted together as a single operation.
Add-ons to such tech, such as an eye flechette system, require a separate operation.

The implantation surgery itself takes a short time but comes at a cost and skill check difficulty as given in the table below.
If the surgeon donates their work, the installation cost is halved.

| Cyberware System Strain |    Cost | Recovery Time | Difficulty |
|------------------------:|--------:|--------------:|-----------:|
|                       0 |    $100 |        1 Hour |          7 |
|               0.1 — 0.5 |    $500 |         1 Day |          8 |
|                 0.6 — 1 |  $2,500 |        4 Days |         10 |
|                 1.1 — 3 | $12,500 |       2 Weeks |         11 |
|             3.1 or More | $50,000 |       1 Month |         12 |

Once the surgery is complete, the doc makes an Int/Heal or Dex/Heal skill check against the installation difficulty, modified by the table below.

| Mod | Circumstance                                |
|----:|---------------------------------------------|
|  -1 | Only has a cyberdoc kit                     |
|  +0 | Has level 1 surgical theater equipment      |
|  +1 | Has level 2 surgical theater equipment      |
|  +2 | Has level 3 surgical theater equipment      |
|  -1 | Lacks a dedicated surgical room             |
|  +0 | Has a dedicated, clean room to work in      |
|  +1 | Has a sterile medical operating room        |
|  +2 | Has an entire medical clinic worth of space |
|  +1 | The cyber is new and unused                 |


If they fail by more than two points, the surgery is unsuccessful, the target’s System Strain is maximized, and the target is reduced to 1 hit point.
The cyber survives, but now counts as used cyber for future implant purposes.

If they fail by one or two points, the surgery is successful, but the user suffers a random implant complication from the table in section 3.6.1.1.
This glitch isn’t obvious until after the surgery is complete. A user who wants a second try at installing a particular system will need to remove the original cyber, spend a month undergoing therapeutic medical treatment at a cost equal to the offending cyber’s original installation cost, and then try again. Such additional attempts have a cumulative -1 penalty on all installation attempts after the original, including any penalty for re-implanting used cyber.

If it succeeds, the cyber is implanted successfully.
The system immediately adds its System Strain value as permanent System Strain to the user. A subject cannot accept cyber if it would put their System Strain above their maximum score.

Whether successful or not, the subject needs a certain amount of time for recovery before they can return to action, whether an hour for the most superficial cyber or a full month for full-body invasive surgery.
During this time they must rest and follow the appropriate anti-rejection medication regime. Failure to do so risks implant complications at best and fatal system rejection at the worst.

#### 3.6.1.1 Implant Complications

If a surgery results in a complication or cyberware removal damages the tech, the following table is used.
If the resulting complication somehow does not apply to the cyber, it counts as a result of 12.

| d12 | Complication                                                                                                                                                                                       |
|----:|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1 | *Unreliable*: The first time each day that it's important that the system function, roll 1d6; on a 1, it's nonfunctional until it receives maintenance.                                            |
|   2 | *Inefficient*: The ware incurs an additional 0.5 System Strain cost.                                                                                                                               |
|   3 | *Loud*: When the system is operating, it's loud enough to be heard clearly five meters away, making stealth impossible. Systems that are always on make this noise constantly.                     |
|   4 | *Fussy*: The required interval between maintenance is halved.                                                                                                                                      |
|   5 | *Bad Connection*: Each time the cyberware is triggered, there's a 1 in 6 chance it fails to activate and the action is wasted. Cyberware that is always on is not hindered by this.                |
|   6 | *Blatant*: Cyberware that normally requires Medical or Touch detection now becomes obvious on sight, perhaps due to excessive scarring or required surface-mount support.                          |
|   7 | *Debilitating*: Pick an attribute in some way relevant to the cyber. The attribute suffers a 1d2 point penalty to its base score that persists as long as the system remains implanted.            |
|   8 | *Uncooperative*: Add 1 System Strain if this isn’t the only implant of the same category, such as Limb/Nerve/Body.                                                                                 |
|   9 | *Complicated*: The complexity of the installation increases the minimum skill level required to maintain it to either Fix-1 or Heal-1.                                                             |
|  10 | *Exhausting*: Maintenance on the system is exceptionally taxing. After it's done, the user needs to spend 24 hours of downtime resting; if they omit this, they gain 2 System Strain immediately.  |
|  11 | *Power Hungry*: The user needs to supplement the system by plugging into an urban power grid or vehicle power port for a half-hour each day. Without this, the system goes inert until re-powered. |
|  12 | *Petty Annoyance*: It has some small quirk or deficiency of fit that causes no real problem in its functionality.                                                                                  |

#### 3.6.1.2 Cyberdoc Tools

The following gear is often useful in installing cyberware.

| Tool                             | Enc. |    Cost |
|----------------------------------|-----:|--------:|
| Cyberdoc Kit                     |    2 |    $500 |
| Maintenance Supplies             |    1 |  Varies |
| Manufacturer’s Installation Kit  |    1 | $10,000 |
| Surgical Theater Tools / Level 1 |   10 | $10,000 |
| Surgical Theater Tools / Level 2 |  N/A |   $500K |
| Surgical Theater Tools / Level 3 |  N/A |     $5M |

- **Cyberdoc Kit**: A portable kit with all the tools needed for maintaining cyberware, albeit the maintenance supplies cost extra.
In a pinch, this kit can be used to implant or remove cyberware, though few would want to risk using such rudimentary tools. The kit can serve as a medkit at need.
- **Maintenance Supplies**: A collection of consumable parts, lubricants, medications, faked subscription keys, and pirated firmware updates.
Any reasonable dollar value of supplies can be carried as 1 point of Encumbrance.
- **Manufacturer’s Installation Kit**: These tools and technical resources are usually restricted to licensed installation vendors of a given cyberware company.
They grant a +1 skill check bonus when installing or removing cyberware made by that manufacturer. They are closely tracked, and it can be difficult to find a corp doc willing to sell them under the table.
- **Surgical Theater/Level 1**: A basic collection of medical tools, pharmaceuticals, scanners, technical manuals, and software subscriptions.
Most ordinary street docs command this level of hardware. Laying it out for use requires at least a semi-clean room’s worth of working space.
- **Surgical Theater/Level 2**: A more sophisticated, extensive, and advanced operating theater than the level 1 version.
High-grade street docs and standard corporate employee clinics would use this tech, which requires the same amount of space as the level 1 array.
- **Surgical Theater/Level 3**: Some of the most advanced medical tech commonly available on the market, this grade of surgical assistance is usually found only in corporate clinics for high-level employees or in the private clinics of top-end street docs.
It requires a half-dozen rooms to support all its functionality.

### 3.6.2 Removing Cyberware

Street docs can attempt to safely remove cyber at the same difficulty rating as it took to implant it, with the surgery taking one hour.
On a success, the implant is removed intact and can be re-used. On a failure, the implant is ruined, but the patient remains healthy. In both cases, the permanent System Strain inflicted by the implant is lost immediately.

If the doc has no interest in the health or survival of the subject, the system removal is automatically successful and takes only ten minutes.
The subject of this ungentle disassembly almost always dies on the table unless the system removed was minimally invasive.

Removing cyber is safest for the implant when the subject is alive during the removal, as death runs the risk of ruining delicate elements of the hardware.
If the subject is dead when the doc goes to salvage their implants, an Int or Dex/Heal skill check is needed against difficulty 10. On a success, the implant is recovered intact, while failure means that it’s ruined. Subjects dead for more than 24 hours are unsalvageable.

Once the cyber’s out, the doc makes an Int or Dex/Heal skill check against difficulty 10.
On a success, the cyber is used, but still perfectly functional. On a failure, it now counts as “secondhand cyber”, with a randomly-determined implantation complication that can’t be avoided.

### 3.6.3 Concealing and Detecting Cyber

Most conventional cyber has one of three different levels of blatancy.
Its signatures are usually restricted to the body part augmented, but obvious neural or internal organ work is usually plain on the user’s skin.

Medical-rated cyber is completely concealed from surface-level inspection, and cannot be detected without a cyber scanner or a medical examination.

Touch-rated cyber is inobvious to mere visual examination, but a person who touches the augmented limb, skin, or other organ will immediately recognize the signs of cybernetic modification.

Sight-rated cyber is so blatant that anyone who looks at the affected body part can see that some form of augmentation has been done.
Baggy clothing may suffice to conceal it, but even the most perfunctory patdown will detect it.

Ordinary investigators may not recognize the specific cyber, but experienced operators and other cyber-versed examiners can usually get a good idea of what’s under a person’s skin, often recognizing the specific model of cyber by its signature telltales.

Cyber that produces obvious bodily changes is likewise obvious when it is being used.

### 3.6.4 Cyberware Maintenance

A cyberware system's maintenance and subscription expenses add up to 5% of the cyberware’s base cost due monthly, not including any cyberware mods or implant surgery expenses.
The maintenance work itself takes a number of hours equal to the System Strain of the system, to a minimum of fifteen minutes, and must be performed by someone with at least Fix-0 or Heal-0. Maintenance supplies can be purchased almost anywhere cyberware is available, and any reasonable dollar amount of them can be carried as 1 Encumbrance item.

If a system goes more than a month without upkeep, it starts to malfunction.
The GM rolls or picks an implant complication and applies it to the system until it receives maintenance. Very extended lack of maintenance can provoke chronic illnesses or death. Some ultra-high-end bespoke ware may absolutely require timely maintenance if their complex systems are not to stop operating entirely.

### 3.6.5 Skillplugs

Commonly-available skillplugs can be found for any skill at level-0 or level-1.
A basic Skillplug Jack I system can only interface with intellectual skills that require only modest physical expertise; Know, Fix, or Heal could qualify for this, but Shoot, Exert, or most forms of Perform would not. Such physical skills require an upgraded Skillplug Jack II unit.

Someone with a plug jack and the Skillplug Wires implant can make use of rare, costly level-2 skillplugs, or the vanishingly rare level-3 plugs.
Level-3 plugs are normally only available through special missions to obtain them.

Whenever the user rolls a natural 2 on a skillplug-augmented skill check, or a natural 1 on a skillplug-augmented attack roll, the check or roll is an automatic failure that no reroll ability can save.
The skillplug jack itself then locks up uselessly for the scene.

While it is possible to implant multiple plug jacks and use multiple skillplugs at once, the increased neural crosstalk invariably increases the chance of severe error.
Each additional skillplug run after the first increases the automatic failure roll range by one point.


|    Cost | Skillplug                                               |
|--------:|---------------------------------------------------------|
|  $1,000 | Level-0 mental skill or conversational language mastery |
| $10,000 | Level-1 mental skill or fluent language                 |
| $50,000 | Level-2 mental skill                                    |
|     N/A | Level-3 mental skill                                    |
|      X2 | Physical-based skill                                    |

### 3.6.6 Cyberware Mods

As with weapons, armor, drones, and many other varieties of gear, cyberware can also be modded by a sufficiently talented medic.
Creating and installing these mods usually requires both Fix and Heal skill. No implant surgery is required as part of installation.

Unless specified otherwise, a given cyberware mod can only be installed once on any given system.
The same mod may be installed on multiple different cyber systems, however, if a tech is willing to manage the Maintenance they will require.

Cyberware mod costs are expressed as a percentage cost of the system they’re installed on


| Mod                 | Fix/Heal | Cost | Special Components |
|---------------------|---------:|-----:|-------------------:|
| Biocapacitors       |      1/2 |  30% |                  1 |
| Durable System      |      1/1 |  20% |                  0 |
| Firewalled          |      1/1 |  20% |                  0 |
| Hardened Weave      |      2/1 |  30% |                  0 |
| Low Maintenance     |      2/2 |  10% |                  1 |
| Monoblade           |      2/1 |  20% |                  0 |
| Profile Adjustment  |      1/2 |  20% |                  0 |
| Quick Detach        |      2/2 |  30% |                  0 |
| Tailored Interface  |      1/3 |  30% |                  1 |
| Targeting Processor |      2/1 |  30% |                  0 |


- **Biocapacitors**: The first System Strain cost that the cyber would normally exact in a day is ignored.
Thus, the first time an Enhanced Reflexes system is triggered, it would not add System Strain to the modded user.
- **Durable System**: A limb or eye system is up-armored to resist injury.
The next Major Injury directed at the modded limb is treated as if it were just a flesh wound roll of 12. It destroys this mod instead.
- **Firewalled**: The modded system is much harder to hack, inflicting a -2 penalty on any related skill checks.
- **Hardened Weave**: Skin cyber that grants an improved base armor class such as Dermal Armor has any AC it grants improved by +2.
The additional armor makes the system Obvious, regardless of its original subtlety, and counts as one item of Readied encumbrance that cannot be dropped.
- **Low Maintenance**: The modded system no longer requires any significant maintenance.
This mod does not work on systems with special maintenance costs or consequences, such as a Full Body Conversion or a Regulated Anagathic Substrate.
- **Monoblade**: A bladed cyber system has advanced monomolecular cutting elements installed.
The edge is difficult to maintain but inflicts horrific injuries when it strikes cleanly. The weapon’s Trauma Die gets a +1 bonus, but its base damage die and Shock is decreased by -2.
- **Profile Adjustment**: The system’s obviousness is lowered by one step, from Sight to Touch, or Touch to Medical.
It has no benefit for a system that is already at a Medical grade of concealment.
- **Quick Detach**: Usable only on limb cyber, eyes, or other parts that could conceivably be removed, this mod allows the user to attach or detach the system with five minutes of work, and replace it with any other Quick Detach-modded system that would fit in the same place.
- **Tailored Interface**: A demanding cyber system is adjusted to specifically match the biochemistry of the user rather than using a factory-set best approximation.
This mod only functions on cyber that inflicts 2+ points of permanent System Strain, but lowers the strain cost by 1 point.
- **Targeting Processor**: This mod must be installed in Gunlink, or in a weapon system such as Body Blades or Eye Mod/Flechette.
Improved targeting calculations grant a +1 bonus to hit with the cyber-weapon or with guns aimed with the Gunlink system. An attack can only ever benefit from one instance of this mod.

### 3.6.7 Cyberware Systems

The systems below are some of the more common ones available.
Each one is listed with their price, type, concealment level, and the permanent System Strain they add.

Cyber systems are divided by type, indicating what general part of the body they affect.
This may be relevant if a Major Injury destroys all cyber of a particular type in a limb of torso.

#### 3.6.7.1 Body Cyberware

| Body Ware                          |  Cost | Type | Conc.   | SysStr | Effect                          |
|------------------------------------|------:|------|---------|-------:|---------------------------------|
| Aesthetic Augmentation Suite       |  $50K | Body | Sight   |      2 | Body sculpt and Cha bonus       |
| Assisted Glide System              |  $50K | Body | Touch   |      2 | Glide from high launch points   |
| Banshee Module                     |  $30K | Body | Medical |      1 | Mimic voices and stun enemies   |
| Cybernetic Infrastructure Baseline |  $20K | Body | Medical |      0 | Gain Con 12 for cyber purposes  |
| Deadman Circuit                    |  $10K | Body | Sight   |   0.25 | Fry cyber without access codes  |
| Dermal Armor/Trauma Shielding      | $100K | Body | Medical |      1 | Add +1 to user’s Trauma Target  |
| Emergency Stabilization Factor     |  $30K | Body | Medical |      1 | Automatically stabilize         |
| Fleshmod                           |  $20K | Body | Medical |      1 | Completely rework your body     |
| Full Body Conversion               |   $6M | Body | Sight   |      0 | Become a full body cyborg       |
| Hemosynthetic Filter System        |  $25K | Body | Medical |      1 | Immune to normal disease/toxin  |
| Holdout Cavity                     |  $10K | Body | Medical |      1 | 2 Enc. of hidden body space     |
| Medical Support Readout            |  $10K | Body | Medical |   0.25 | Gain +2 to Heal checks on you   |
| Recovery Support Unit              |  $30K | Body | Medical |      1 | Gain 4 System Strain for heals  |
| Redundant Systems                  |  $15K | Body | Medical |      1 | Sacrifice to avoid Major Injury |
| Retribution Shield                 |  $50K | Body | Touch   |      1 | Burst to harm melee targets     |
| Therapeutic Control Dampers        |  $25K | Body | Medical |      1 | Suppress an implant side-effect |
| Titan Gun System                   | $100K | Body | Sight   |      1 | Mount a Heavy weapon            |
| Viper Sting                        |  $25K | Body | Medical |    0.5 | Hidden drug injection system    |


- **Aesthetic Augmentation Suite**: Any standard fleshmod can supply beauty, but an aesthetic augmentation suite provides not only a fleshmod’s versatility, but tailored pheromones, vocal harmonizers, social cue triggers, and an absolutely inhuman visual perfection.
Subjects gain a Charisma score of 14, or a +2 score bonus if already 14 or greater, up to a maximum of 18.
- **Assisted Glide System**: Retractable nanofiber wings and integral boost jets allow a limited degree of flight for the user of this implant.
When leaping from a high place, they gain a flight Move speed of 30m for up to two kilometers before being forced to land. Gaining altitude requires a Dex/Exert skill check against difficulty 10, however, with failure forcing a landing, and the wings can’t carry them more than twice as high as the height they jumped from. By deploying the boost jets and wings as an Instant action, the user can ignore up to a hundred meters of falling damage.
- **Banshee Module**: Resonant cavities are designed into the user’s torso, along with an advanced amplification system.
They can reproduce any sound or voice they have heard before, and at maximum volume can be heard clearly up to five hundred meters away. Once per day, as a Main Action, this sound can be used to shatter normal glass in a ten-meter line in front of the user; creatures without ear protection in this area must make a Physical save or take 2d6 non-lethal damage.
- **Cybernetic Infrastructure Baseline**: A set of standardized support implants provide a baseline degree of cyber-compatibility to a subject.
They can ignore System Strain from cyber implants equal to 12 minus their Constitution score- so a PC with a Constitution of 9 could ignore three points worth of System Strain from cyber implants. PCs with a Con of 12 or more have no use for this system.
- **Deadman Circuit**: A common corporate implant to discourage kidnapping and warelegging.
Any attempt to remove any of the user’s cyber without the right access codes will fry that hardware unless a difficulty 13 Int/Heal skill check is made.
- **Dermal Armor/Trauma Shielding**: A system that can only be installed on a user with at least one level of Dermal Armor, the trauma shielding addition adds +1 to the user’s Trauma Target.
This benefit stacks with any existing Dermal Armor bonus to the roll.
- **Emergency Stabilization Factor**: When brought to zero hit points by an injury that does not instantly kill them, the user automatically stabilizes, gaining one System Strain, and will regain consciousness at the end of the scene with 1 hit point.
If they are already at maximum strain, this cyberware does not function.
- **Fleshmod**: Careful surgery and support implants allow for a complete physical remolding within generally humanoid lines, including alterations of sex, height, cosmetic limbs such as tails or decorative ears, and overall weight.
Reproductive ability is not conferred, but rumors persist of experimental mods that can grant even that.
- **Full Body Conversion**: The user’s brain and central nervous system are transplanted into a cutting-edge synthetic shell at a cost and difficulty as if this cyber took 4 System Strain.
Once converted, the subject has an effective Constitution of 20 for System Strain purpose, but cannot be healed by conventional first aid or medical drugs, requiring ten minutes of repair work with a toolkit to repair any amount of damage, and $250 worth of parts for each hit point to be restored. A full conversion borg no longer requires food, water, air, or sleep, but each day without the latter adds one System Strain due to the mental stress. The conversion requires twelve hours of maintenance every two weeks, and missing it will result in the user’s death in 2d6 days unless the omitted maintenance is performed. Current full-body conversion tech is unstable; after 1d4 years, the subject has a cumulative 5% death chance each year. Borgs do not take Major Injuries and gain a +6 bonus to their Trauma Target that stacks with existing bonuses.
- **Hemosynthetic Filter System**: A wide-spectrum array of blood scrubbers, intake filters, and microbial barriers grants the user effective immunity to most natural diseases and non-synthetic toxins.
Weaponized toxins and certain exotic natural afflictions can still affect the user, but they get a +2 bonus on all saving throws against them. The system automatically alerts the user if they have been exposed to a recognized disease or toxin, whether or not it affects them.
- **Holdout Cavity**: A biosculpted torso pocket allows for the air-tight, climate-controlled concealment of 2 Encumbrance worth of items inside the user’s body and their easy access with a Main Action.
Any given object stored can’t be larger than a pistol or a tightly-folded suit of clothing, and the pocket’s contents don’t count against the user’s Encumbrance limit. The pocket’s signature is disguised against conventional scanning technology, and it cannot be detected without a manual strip search by a trained examiner.
- **Medical Support Readout**: A diagnostic system is interfaced with the user’s biological processes, allowing a realtime analysis of their current health and any significant injuries.
Aside from immediately alerting the user when they are poisoned, diseased, or injured, it grants any healing or medical support skill check a +2 bonus thanks to the diagnostic information and increases first aid or drug healing by +2 HP per application.
- **Recovery Support Unit**: A number of support systems and blood purification filters are installed.
The user can absorb up to four points of System Strain incurred from healing or wound stabilization effects; this is a separate pool that recovers one point every night at the same time as the user’s natural System Strain recovers.
- **Redundant Systems**: Several pieces of prosthetic support cyber are preemptively installed to mitigate future trauma.
When the user takes a Major Injury, they may instead choose to have the Redundant Systems critically damaged instead of accepting the major injury roll result. Redundant Systems can be installed more than once, but the System Strain inflicted increases by one point each time.
- **Retribution Shield**: Numerous small projectile launchers are embedded under synthetic skin.
When triggered as an On Turn action, the user gains one System Strain to launch a cloud of shrapnel around them, acting as ground zero of a frag grenade explosion. The user has enough targeting control to avoid hitting allies in range. If grappling or grappled at the time, their opponent automatically takes maximum damage. Reloading the Retribution Shield takes five minutes, and the system will not function if the user is wearing suit-grade armor. Lesser armor can usually be tailored to expose adequate amounts of skin.
- **Therapeutic Control Dampers**: A set of customized medical and therapeutic implants are used to control the side-effects of a partially-successful cyberware implant.
For each implantation of this system, the user can choose to ignore one implant side-effect they currently suffer. The skill check to implant this system can fail, but it never induces any side effects on its own.
- **Titan Gun System**: Usable only by full body conversion cyborgs or subjects with a Strength score of 18, this mount allows the installation of any man-portable Heavy weapon such as those given in the Weapons section.
The user counts as a stable firing rest, and the weapon has two integral magazines of ammo, though it can be loaded by hand as well. The specific weapon mounted can be swapped during maintenance.
- **Viper Sting**: A tool of assassins, a Viper Sting is a fang, fingernail needle, palm injector, or more exotic implement for injecting an unsuspecting target with a drug of the user’s choice.
A Viper Sting can be loaded with up to four doses of pharmaceuticals, dispensed as the user wishes. If loaded with a toxin, it does 1d12 damage with a 1d10/x3 Trauma Die. If a tranquilizer is used, the damage is the same, but unconsciousness will result instead of death. A Viper Sting can only be effectively used as part of an Execution Attack, as an alert opponent can avoid it easily. This cyber inflicts a -2 penalty on any check to detect its presence.

#### 3.6.7.2 Head Cyberware

| Head Ware                  |  Cost | Type | Conc.   | SysStr | Effect                        |
|----------------------------|------:|------|---------|-------:|-------------------------------|
| Courier Memory             |  $10K | Head | Medical |   0.25 | Carry locked Memory data      |
| Cranial Jack               |   $1K | Head | Touch   |   0.25 | Link to jack-equipped gear    |
| Discretion Insurance Unit  |  $10K | Head | Medical |    0.5 | Cranial bomb with remote key  |
| Eye Mod/Dazzler            |  $15K | Head | Medical |    0.5 | Dazzle enemies within 5m      |
| Eye Mod/Flechette Launcher |  $20K | Head | Medical |    0.5 | Surprise light pistol attack  |
| Funes Complex              |  $40K | Head | Medical |      1 | Gain eidetic memory           |
| Medusa Implant             |  $20K | Head | Obvious |     .5 | Prehensile hair implants      |
| Neural Buffer              |  $40K | Head | Medical |      1 | Gain 3 HP/level vs hacker dmg |
| Skull Citadel              | $100K | Head | Medical |      2 | Armor the brain against harm  |


- **Courier Memory**: One unit of cranial Memory is installed with a subdermal upload pad.
The person uploading the data can set a passphrase; while the implant user can erase the data, they can’t download it again without the passphrase, which is usually told only to the data’s intended recipient. Courier memory is exceptionally well-hidden and even medical scans take a -2 penalty to find it.
- **Cranial Jack**: A discreetly-placed plug socket in the user’s head or neck allows interfacing with cyberdecks and gear equipped with a jack line.
Cranial jacks can be modded with cyberdeck mods.
- **Discretion Insurance Unit**: A small cranial bomb that can be keyed go off when it receives- or does not receive- certain codes or transmissions.
Its unfortunate tendency to misfire its anti-removal protocols makes it an unpopular choice for use in anyone but the most disposable subjects. Removing this cyber is done at a +2 difficulty, and failure causes the death of the subject and a Physical save to avoid the death of the physician, with 5d6 damage taken on success.
- **Eye Mod/Dazzler**: This mod can emit a dazzling strobe of intense light, dazing and disorienting those standing up to five meters in front of the user.
The strobe is triggered up to once per round as an On Turn action and adds one System Strain. Victims without eye protection must make a Physical saving throw or take a -4 penalty to their AC and hit rolls for the next 1d6 rounds. A victim can be targeted by this dazzling only once per scene.
- **Eye Mod/Flechette Launcher**: A holdout weapon designed for surprise short-ranged attacks, this eye has been loaded with a small explosive flechette.
It has the statistics of a light pistol, has a range of 10 meters, and requires a Main Action to reload its single-round magazine. If used against a target in melee range who does not suspect its existence, a normal hit roll is made; on a miss, it does full normal damage anyway, and on a hit, the damage is doubled. It can be used for Execution Attacks.
- **Funes Complex**: A synthetic eidetic memory is granted by this implant, with formalized bodies of knowledge easily cross-referenced and recollected.
Once per day, as an Instant action, reroll any failed Int-based skill check. This memory can be difficult to control, however, and unwanted allusions can overwhelm the desired datum. If the user rolls a natural 2 on any Int skill check, it automatically fails and cannot be recovered by any rerolling ability.
- **Medusa Implant**: These prehensile cranial tendrils take multiple forms, from thick armored cables to bundles of brightly-colored articulated polymer fibers not unlike normal hair.
The tendrils are shoulder-length under most circumstances, but can extend to manipulate objects up to three meters distant with the agility of a human hand. The bundles are not very strong, however, and cannot lift more than ten kilograms. Like other extra limbs, they do not grant extra actions, but can hold and use items as any other limb could.
- **Neural Buffer**: A system favored by hackers, a Neural Buffer rearranges the user’s brain topography, making it more difficult to harm the user with neural feedback.
The user gains an additional 3 hit points per level that can only absorb cyberspace damage from Stun or Kill Verbs. These phantom hit points refresh completely each hour.
- **Skull Citadel**: The user’s head is augmented with independent blood oxygenation pumps, armor plating, and trauma buffers designed to keep the brain intact even after the rest of the body is critically damaged.
Barring dismemberment or the use of high-powered ammunition directly on the target’s head, the subject ignores head-affecting Major Injuries and can be stabilized even as long as ten minutes after their technical “death”. Each such delayed revivification permanently reduces a randomly-chosen attribute by 2 points, however. This ability damage cannot be undone.

#### 3.6.7.3 Skin Cyberware

| Skin Ware              |  Cost | Type | Conc.   | SysStr | Effect                           |
|------------------------|------:|------|---------|-------:|----------------------------------|
| Dermal Armor I         |  $40K | Skin | Medical |      1 | AC 16, +1 to Trauma Target       |
| Dermal Armor II        |  $80K | Skin | Touch   |      2 | As I, but AC 18 and Shock resist |
| Dermal Armor III       | $200K | Skin | Sight   |      3 | As II, but AC 20 and +2 TT       |
| Poseidon Implants      |  $30K | Skin | Touch   |      1 | Aquatic adaptation mods          |
| Sealed Systems Implant |  $15K | Skin | Medical |      1 | Trigger a temp space suit        |
| Sharkskin Electrodes   |  $20K | Skin | Touch   |      1 | Shock grapplers                  |
| Skinmod                |  $250 | Skin | Sight   |      0 | Make cosmetic-level body mods    |
| Skyborn Shielding      |  $40K | Skin | Sight   |      2 | Orbital hab lifestyle mods       |


- **Dermal Armor I**: Subtle dermal weave reinforcement grants a base ranged and melee AC of 16 and a +1 bonus to the user’s Trauma Target.
- **Dermal Armor II**: Heavier subcutaneous plating acts as Dermal Armor I with a base ranged and melee AC of 18.
In addition, the user can ignore the first instance of Shock in a round.
- **Dermal Armor III**: Overt ceramic body plating and double-mesh joint reinforcement acts as Dermal Armor II, but with a base ranged and melee AC of 20 and +2 to the user’s Trauma Target.
- **Poseidon Implants**: These implants allow the user to function normally while submerged for an indefinite period, drawing any necessary oxygen or drinking water from the surrounding salt or freshwater.
Normal temperature extremes are managed without difficulty, and pressure can be handled down to depths of one kilometer. Visual augments and pressure gradient sensors allow the equivalent of normal sight up to 30 meters even in lightless water, and miniaturized support jets allow three-dimensional movement at twice the user’s normal Move rating.
- **Sealed Systems Implant**: When triggered as an Instant action, implanted shields deploy and skinweaves energize to seal the user off from hostile external environments.
For the next six hours, the user can operate as if wearing a vacuum suit, ignoring vacuum, contact poisons, non-immediately-lethal levels of radiation, toxic gases, low or high-pressure atmospheres, and environmental temperature hazards short of open flame. After deployment, the implant needs an equal time of recovery before it can be deployed again.
- **Sharkskin Electrodes**: An implanted mesh of electrodes beneath the user’s skin allows them to deliver debilitating shocks while grappling a target, even if both are heavily clothed.
At the end of each round of grappling, the user inflicts 2d6 non-lethal electrical damage on anyone they grapple or who is grappling them. The shielding built into the cyber automatically negates non-lethal electrical damage for the user and halves lethal shocks.
- **Skinmod**: Various neon tattoos, skin texture and color changes, and other superficial physical mods can be performed with no real systemic burden beyond the usual need for regular maintenance.
- **Skyborn Shielding**: A set of implants and skin treatments meant for those who spend extended periods in hard vacuum, this implant can be triggered as an Instant action.
When deployed, the user gains the benefits of a Sealed Systems Implant and low-G microjets that allow 30m/round flight in microgravity conditions. Skyborn Shielding can operate for up to 72 hours before needing an hour of downtime to regenerate.

#### 3.6.7.4 Limb Cyberware

| Limb Ware                   |  Cost | Type | Conc.   | SysStr | Effect                           |
|-----------------------------|------:|------|---------|-------:|----------------------------------|
| Body Blades I               |  $10K | Limb | Medical |      1 | Integral body weaponry           |
| Body Blades II              |  $25K | Limb | Sight   |      2 | Larger body weaponry             |
| Cyberlimb                   |  $10K | Limb | Touch   |    0.5 | Prosthetic with storage space    |
| Iron Hand Aegis             |  $40K | Limb | Touch   |      1 | Deflect one ranged hit per scene |
| Limbgun                     |  $30K | Limb | Touch   |      1 | Implanted gun in a limb          |
| Muscle Fiber Replacement I  |  $50K | Limb | Touch   |      1 | Str 14, or +2 if higher          |
| Muscle Fiber Replacement II | $200K | Limb | Sight   |      2 | Str 18 and extreme feats         |
| Neolimb                     |  $25K | Limb | Sight   |      1 | Add a new additional limb        |
| Omnihand                    |  $10K | Limb | Touch   |   0.25 | Toolkit hand, +1 check 1/day     |
| Shock Fists                 |  $10K | Limb | Touch   |      1 | Do electric fist damage          |
| Stick Pads                  |  $15K | Limb | Touch   |    0.5 | Climb sheer or vertical surfaces |
| Synthlimb                   |  $25K | Limb | Medical |    0.5 | Lifelike artificial limb         |


- **Body Blades I**: Assorted retractable blades and spikes are implanted in the user’s limbs.
Their unarmed attacks do 1d8 damage, Shock of 2/15, Trauma Die 1d8/x3 and can be rolled with either Stab or Punch as the relevant combat skill. The Punch skill does not add damage to Body Blades attacks. When not in use, the blades are perceptible only to a medical scan.
- **Body Blades II**: As level one, but the user’s body armament includes not only conventional blades, but assorted improvements such as monomolecular edges, cutting vibrations, or thermal augmentation.
Their unarmed attacks do 2d6 damage with a Shock of 4/15, and a Trauma Die 1d10/x3. Anyone who sees or touches their arms can detect signs of the cyber, however.
- **Cyberlimb**: A standard medical-grade cyberlimb, albeit one Encumbrance worth of non-weapon equipment or storage can be implanted in each limb and remain Readied without counting against the character’s Encumbrance.
Add the equipment cost to the limb. The limb provides power to any electronic equipment implanted.
- **Iron Hand Aegis**: Absorption plates and kinetic ablation units are implanted in the user’s arms and linked with a reactive ballistic calculator.
Once per scene, as an Instant action, the user can deflect a successful ranged bullet, arrow, or thrown weapon attack, including burst or suppressive fire. Surprise attacks cannot be deflected this way, nor can melee attacks or explosives.
- **Limbgun**: A ranged weapon is implanted into an arm, tail, or other significant limb, along with space for two magazines worth of ammunition.
A full reload of this space can be accomplished as a Main Action. Limbguns can be used with Gunlink cyber, count as Readied at all times, and can be used even while both hands are occupied, but can be bound up in melee as normal guns can. Limbguns have an intrinsic +1 hit bonus, in addition to whatever bonus the implanted weapon may have. The maximum size of the gun is Encumbrance 1 for legs or neolimb tails, or Encumbrance 2 for arms. The buyer must supply the gun. Different guns can be swapped in as desired during the system’s monthly maintenance, but once implanted, a gun is useless for later conventional use.
- **Muscle Fiber Replacement I**: Artificial muscle fibers are implanted as replacements for the user’s own flesh.
The subject gains a Strength score of 14, or +2 if already 14 or higher.
- **Muscle Fiber Replacement II**: The subject gains a Strength score of 18, and can smash through standard interior doors or equivalent barriers as a Move action.
- **Neolimb**: The user gains an additional limb that normal humans lack.
Usual choices involve an additional pair of arms or manipulatory mechano-tentacles, though functional prehensile tails are popular too. One installation of the cyber covers paired limbs or a single tail or body-mounted tendril up to 3 meters long. Neolimbs can support up to one Encumbrance point of non-weaponry built-in tech and add two items to the user’s Readied encumbrance allowance. Neolimbs can hold or grip objects, but cannot employ additional shields or grant extra actions.
- **Omnihand**: A full array of small mechanical and electronic tools are folded into this cybernetic hand.
The user is never without the tools for relevant skill checks, and once per day as an Instant action can gain +1 on any skill check involving the omnihand’s tool use. As an improvised weapon, the tools do damage as a knife.
- **Shock Fists**: The user’s hands are implanted with subdermal electrical webbing.
As an Instant action, accept one System Strain; for the rest of the scene, the user’s unarmed attacks do an additional 1d8 electrical damage on a successful hit. Unarmed attacks augmented with this mod are non-lethal if desired. This system does not stack with Body Blades.
- **Stick Pads**: Macroscale van der Waals generators allow the user’s hands and feet to stick to vertical or overhanging surfaces, their full weight supported by one arm.
They can climb such surfaces at their full movement rate with one free hand.
- **Synthlimb**: A high-grade cyberlimb that’s indistinguishable from an organic body part by anything short of a medical examination.
It functions as a Cyberlimb for purposes of implanted equipment.

#### 3.6.7.5 Sensory Cyberware

| Cyberware                    | Cost | Type    | Conc.   | SysStr | Effect                          |
|------------------------------|-----:|---------|---------|-------:|---------------------------------|
| Active Sense Processor       | $50K | Sensory | Medical |      1 | Gain +1 Wis mod for senses      |
| Cyberears (pair)             | $10K | Sensory | Touch   |   0.25 | Noise-filtering synthetic ears  |
| Cybereyes (pair)             | $10K | Sensory | Sight   |   0.25 | Flash-protected synthetic eyes  |
| Ear Mod/Filter               | $10K | Sensory | Medical |    0.5 | Get +2 on hearing skill checks  |
| Ear Mod/Positional Detection | $15K | Sensory | Medical |    0.5 | Map all sound positions nearby  |
| Ear Mod/Sonar                | $20K | Sensory | Medical |    0.5 | Sense surrounds as if visually  |
| Ear Mod/Tracer               | $10K | Sensory | Medical |    0.5 | Eavesdrop on specific targets   |
| Eye Mod/Impostor             | $10K | Sensory | Medical |      1 | Mimic retinal patterns          |
| Eye Mod/Infrared Vision      |  $5K | Sensory | Medical |    0.5 | See heat patterns in the area   |
| Eye Mod/Low Light Vision     |  $5K | Sensory | Medical |    0.5 | See in low-light conditions     |
| Eye Mod/Tactical View        | $10K | Sensory | Medical |    0.5 | Get transmission, search better |
| Eye Mod/Zoom                 | $10K | Sensory | Medical |    0.5 | 500m telescopic vision          |
| Gunlink                      | $25K | Sensory | Touch   |      1 | Bonus when using firearms       |
| Headcomm                     |  $1K | Sensory | Medical |   0.25 | Silent radio or phone comms     |
| Sensory Recorder             | $10K | Sensory | Medical |    0.5 | Record 3 hours of sense input   |
| Synthears (pair)             | $25K | Sensory | Medical |   0.25 | Lifelike artificial ears        |
| Syntheyes (pair)             | $25K | Sensory | Medical |   0.25 | Lifelike artificial eyes        |


- **Active Sense Processor**: Augmented sensory processing grants a +1 Wisdom modifier for all skill checks involving sensing or noticing things, up to a +2 modifier maximum.
- **Cyberears**: While normal in appearance, touch reveals the synthetic nature of these ears.
Some owners prefer to style them in fanciful or unnatural ways. While the base model provides only normal human hearing, all are equipped with ear protection against loud noises. A pair of cyberears or synthears halves the System Strain cost of ear implants.
- **Cybereyes**: While a single cybernetic eye can be implanted, it is recommended that both be replaced at the same time to improve neural adaptation.
These cybereyes are clearly artificial, though most are styled in attractive or striking fashions. They all have perfect 20/20 vision and integral flash protectors, but additional functionality must be implanted separately. A user with a pair of cybereyes or syntheyes halves the System Strain cost of cybernetic eye implants.
- **Ear Mod/Filter**: The user’s sense of hearing is sharpened remarkably, allowing them to follow individual conversations in a noisy crowd, eavesdrop on whispers up to twenty meters distant, and gain a +2 bonus on all hearing-related Notice checks.
- **Ear Mod/Positional Detection**: This ear mod constructs a spatial map of significant surrounding sounds, allowing the user to know the exact location of anything making audible noise.
The precise location of distant shooters, the location of targets in visual concealment, and the positions of moving creatures in darkness are all clear.
- **Ear Mod/Sonar**: The user can navigate even in darkness or while blinded by means of ultrasonic sensors, being able to construct a crude map of people and obstacles in front of them out to 20 meters.
This sense is the equivalent of vision, though it can’t discern details more subtle than faces.
- **Ear Mod/Tracer**: As an On Turn action, the user can nominate a target within 20 meters.
Until they get more than 100 meters away from the user or pass behind a solid barrier, the implant’s hyper-specific hearing can track their motions perfectly and overhear anything they say or that is said to them. The user can listen to one such target at a time.
- **Eye Mod/Impostor**: Aside from allowing free alteration of eye color, this mod allows the user to copy another person’s retinal patterns.
Provided they are within two meters, they need only meet the user’s gaze for a few moments to get a good imprint. The eye can store up to a dozen imprints, but it takes a Main Action to shift from one to another. Note that it is a relatively simple procedure to alter a person’s retinal pattern, so an important target alerted to the imposture may end up changing their imprint.
- **Eye Mod/Infrared Vision**: The infrared vision this mod allows permits basic navigation in dark areas and makes the presence of heat sources such as humans, engines, and infrared tripwires very obvious.
The user gains a +1 bonus on any Notice checks to detect the presence of thermally-unshielded living creatures.
- **Eye Mod/Low Light Vision**: This mod grants low-light vision, allowing clear sight to normal distances in any light condition better than pitch blackness.
- **Eye Mod/Tactical View**: These eyes can receive text or video transmissions and play them on an inset in the user’s normal vision.
Outline and pattern discrimination is enhanced; when specifically looking for things or keeping watch, gain a +1 bonus to any relevant Notice check.
- **Eye Mod/Zoom**: The user’s vision can zoom in on distant targets.
The user can see objects within 500 meters as if they were standing next to them. The hit penalty for a ranged weapon’s long range is eliminated.
- **Gunlink**: Hardwired control points interface with the onboard targeting systems of most modern firearms.
When using a modern firearm, the PC ignores range penalties and negates up to 2 points of hit penalties for cover, concealment, or prone targets. Once per scene, as an Instant action, they can reroll a missed attack roll with a gun.
- **Headcomm**: An implanted phone/short-range radio that can directly communicate within 100 meters or use the phone grid, if it’s available.
For cranial security reasons, only text and voice can be transmitted, but conversations can be held without audible vocalization.
- **Sensory Recorder**: The user can record up to 180 minutes of sensory input.
If they have a jack line into a transmitter or cyberware capable of transmitting data, they can share these recordings in real time or afterwards.
- **Synthears**: This model of artificial ear is designed to be indistinguishable from normal ears, though they have all the properties of a normal pair of cyberears.
- **Syntheyes**: These high-end cybereyes are designed to be indistinguishable from normal eyes under anything short of a medical examination.
Any additional mods made to the eyes share this subtlety

#### 3.6.7.6 Medical Cyberware

| Medical Ware                  |  Cost | Type | Conc.   | SysStr | Effect                   |
|-------------------------------|------:|------|---------|-------:|--------------------------|
| Prosthetic Cyber I            |   $5K | None | Sight   |      1 | Mitigates a Major Injury |
| Prosthetic Cyber II           |  $15K | None | Medical |   0.25 | Mitigates a Major Injury |
| Regulated Anagathic Substrate | $200K | None | Medical |      1 | Prolongs human lifespan  |


- **Prosthetic Cyber I**: One of an array of minimally-adequate and very obvious organ replacements, eye prosthetics, neural patches, and other medical cyber.
Prosthetic cyber grants no benefits over ordinary flesh, but can mitigate a Major Injury as explained on page 41. Patients who’ve lost an arm or leg don’t need this cyber; they need a serviceable new limb system such as a Cyberlimb or Synthlimb.
- **Prosthetic Cyber II**: A more sophisticated and expensive version of basic prosthetic cyber, inobvious to anything but a medical scan and far less burdensome on the user’s system.
- **Regulated Anagathic Substrate**: This whole-body network of chem regulators can prolong human lifespans by up to 1d4+2 decades.
Maintenance on this cyber costs $50,000 a month, however, and interruptions have catastrophic consequences. Rumors persist of even more effective models existing among the megacorp ultra-elite.

#### 3.6.7.7 Nerve Cyberware

| Cyberware                    |  Cost | Type  | Conc.   | SysStr | Effect                           |
|------------------------------|------:|-------|---------|-------:|----------------------------------|
| Coordination Augment I       |  $50K | Nerve | Medical |      2 | Dex 14, or +2 if higher          |
| Coordination Augment II      | $200K | Nerve | Touch   |      3 | Dex 18 and +10m Move             |
| Enhanced Reflexes I          | $100K | Nerve | Medical |      2 | 1/scene, bonus Main Action       |
| Enhanced Reflexes II         | $250K | Nerve | Medical |      3 | 1/scene, bonus Main and Move     |
| Enhanced Reflexes III        | $750K | Nerve | Touch   |      4 | 2/scene, bonus Main and Move     |
| Reaction Booster I           |  $50K | Nerve | Medical |      1 | +2 Init and Snap Attack benefits |
| Reaction Booster II          | $100K | Nerve | Medical |      2 | Automatically win initiative     |
| Remote Control Unit          |  $10K | Nerve | Touch   |      2 | Remote control drones/vehicles   |
| Skillplug Jack I             |  $10K | Head  | Touch   |   0.25 | Use level-1 intellectual plugs   |
| Skillplug Jack II            |  $25K | Head  | Touch   |    0.5 | Use level-1 plugs of any kind    |
| Skillplug Wiring             |  $50K | Nerve | Medical |      1 | Boost skillplug max to level-3   |
| Trajectory Optimization Node |  $50K | Nerve | Medical |      1 | 1/scene turn a miss into a hit   |
| Zombie Wires                 |  $60K | Nerve | Medical |      2 | Keep acting at zero HP           |


- **Coordination Augment I**: The user’s natural mind-body interface is tightened.
The subject gains a Dexterity score of 14, or +2 if already 14 or higher.
- **Coordination Augment II**: As Coordination Augment I, but the subject gains a Dexterity score of 18, and their base Move rate is increased by 10 meters.
- **Enhanced Reflexes I**: Neural acceleration matrices can be deployed to briefly boost the user’s reaction speed.
Once per scene, as an On Turn action, the user gets a bonus non-cyberspace Main Action.
- **Enhanced Reflexes II**: Once per scene, as an On Turn action, the user gains a bonus non-cyberspace Main Action and Move action.
By accepting one System Strain, this ability can be used as an Instant action, potentially interrupting incoming attacks if the user moves out of reach.
- **Enhanced Reflexes III**: Twice per scene, as an On Turn action up to once per round, the user gains a bonus non-cyberspace Main Action and bonus Move Action.
By accepting one System Strain, this ability can be used as an Instant action.
- **Reaction Booster I**: These implants grant a +2 bonus to your Initiative, perhaps making you act before the rest of your team.
Once per scene, as an Instant action, ignore Snap Attack hit penalties.
- **Reaction Booster II**: As level one, but the user automatically wins initiative against targets that don’t also automatically win initiative; if both have this ability, initiative is rolled without modifications.
The user can act normally during surprise rounds.
- **Remote Control Unit**: A neurally-integrated control link allows for near-range control of drones or remote-rigged vehicles, as per the drone control rules.
- **Skillplug Jack I**: A discreet head-mounted plug port allows use of an intellectual skillplug for a skill requiring minimal physical ability, up to level-1.
Once slotted, a skillplug takes fifteen minutes to boot up and integrate properly.
- **Skillplug Jack II**: This improved plug jack allows use of either an intellectual skillplug or one requiring physical expertise, up to level-1.
- **Skillplug Wiring**: A neural wiring connection improves a skillplug jack’s limits to allow up to level-3 skill usage.
One set of wires can support any number of jacks, but at least one is needed.
- **Trajectory Optimization Node**: This cyber can calculate the optimal path for bypassing defenses and striking a target.
Once per scene, as an Instant action, turn a missed hit roll into a success. Melee and unarmed attacks can be used as-is, but using this unit with a ranged attack requires an active Gunlink with the weapon.
- **Zombie Wires**: Functioning only for a user with at least one Body cybersystem to serve as a motor substrate, “zombie wires” allow a user to keep functioning even at zero hit points.
As an Instant action at zero hit points, the user can give their body a one-sentence command involving physical action. Their insensate flesh will blindly carry out that action for up to six rounds until it is complete or they take up to one quarter of their maximum hit points in further damage, which will kill them instantly. Time spent under Zombie Wires counts against a user’s stabilization time limit. Triggering this cyber adds 2 System Strain.

### 3.6.8 Cyberware Alienation

In the default cyber rules, chrome does not cause dramatic psychological changes in of itself.
The new senses, capabilities, and needs of the cyber may have consequences on a user’s thinking, but these changes aren’t reified in game mechanics. This works for many cyberpunk settings, but some GMs prefer to use worlds where replacing half your brain with wiring makes you considerably less human in your thinking.

In these settings, cyber causes a progressive distancing of the user from the normal human condition.
The constant barrage of synthetic nerve impulses, hormonal modifications, and neural compromises necessary to integrate cold metal with living flesh forces an adaptation process not unlike the creation of psychological scar tissue. The user must adapt to these alterations in ways that are not always rational, productive, or non-homicidal.

This adaptation is called Alienation, and it’s counted in points much like System Strain.
A user’s total begins at zero and they gain points as they add cybernetics. If these points ever exceed the user’s Wisdom score, they break down entirely and become an NPC, incapable of functioning in a human society.

#### 3.6.8.1 Gaining and Losing Alienation

Campaigns that involve cybernetic alienation do so under three particular rules.

**When a user adds a cybernetic system**, they gain permanent Alienation equal to its System Strain cost, rounded up to the nearest whole number.
If it’s Nerve cyberware, this total is increased by +2 per system. Mods or Focus abilities that lower the System Strain cost of a mod can also lower its Alienation cost, as the system is made less intrusive or more finely-tuned to the user. Eye or ear mods discounted by cybereyes or cyberears still round up to 1 Alienation each, however.

**Users can also gain Alienation if they fail an Alienation check** as described below.
Their failure has aggravated their psychological condition, and they’re forced to deal with the fallout and contain the influence of their chrome on their thinking. This added Alienation might push the total above their maximum, causing an incapacitating cyber-induced psychosis.

When a user fights their chrome syndromes and tries to behave in ways contrary to them, they may be forced to check for Alienation.
To do so, the user rolls 1d20; if the result is equal or less than their current Alienation, they gain one point. If this pushes them over their maximum allowed Alienation, they make a Mental save; on success, they pick a new chrome syndrome, and on a failure they immediately suffer CIP.

To lose Alienation, the user must spend a week in therapy with a skilled cyberpsychologist or another PC with Talk-1 and Heal-1 skills.
NPCs usually charge about $2,000 a week for this service. At the end, the user’s Alienation score drops by one point. It cannot drop below the permanent Alienation induced by their cyberware. Removing cyberware will lower this minimum.

#### 3.6.8.2 Cyber-Induced Psychosis

A subject whose Alienation score rises above their Wisdom attribute score breaks down entirely, becoming enslaved to their existing adaptations.
They become a GM-controlled NPC who acts solely to fulfill the impulses of any chrome syndromes they may have with no regard for law, morality, or long-term prudence.

The only way to rescue a CIP victim is to subdue them and bring them in for cyberpsychological therapy as described above.
All cyber must be removed save for medically-necessary prostheses and the therapist must get their Alienation below their Wisdom attribute. If they can do so, the victim makes a Mental saving throw with a bonus equal to the therapist’s best Heal or Talk skill level. On a success, they regain their reason. On a failure, they are hopelessly and permanently compromised.

#### 3.6.8.3 Chrome Syndromes

Very few cyber addicts leap directly to CIP.
Most make progressively-larger accommodation to the demands of the cyber in their bodies, dealing with its quirks or demands in various maladaptive ways. These adaptations are referred to as “chrome syndromes”, and have some common typologies.

A PC can pick one or more chrome syndromes to accept any time they get new cyber.
The various syndromes are described in their most common forms, but the GM can allow a PC to pick any syndrome they can reasonably explain as being related to their new cyber; it may be that someone’s new reflex aug feels so good to use that they become Ravenous for street racing, or their dermal armor leaves them feeling half-monstrous and Distant from normal humans.

Each chrome syndrome increases the user’s effective Alienation maximum by a certain number of points.
Thus, a PC who becomes Distant can add 3 points to their allowed maximum Alienation. The same syndrome cannot be picked twice.

The only way to get rid of a chrome syndrome is to uninstall the cyber that induced it and get therapy to eliminate any Alienation its installation caused.
The user can never re-install that specific cyber system.

Several example chrome syndromes are described below.


| Chrome Syndrome                     | +Max Alienation |
|-------------------------------------|----------------:|
| Brutal, unable to hold back         |              +4 |
| Candid, unable to keep silent       |              +3 |
| Cold, unable to feel love           |              +4 |
| Distant, unable to tolerate people  |              +3 |
| Exultant, unable to imagine failure |              +3 |
| Fearless, unable to feel dread      |              +4 |
| Hypervigilant, unable to rest       |              +3 |
| Murderous, unable to stop killing   |              +6 |
| Ravenous, unable to stop feeling    |              +4 |
| Savage, unable to retreat           |              +5 |
| Secretive, unable to admit it       |              +3 |
| Stressed, unable to endure it       |              +5 |


- **Brutal**: Your hardware isn’t meant to be restrained.
It hurts to leave the limiters on; the confirmation prompts keep blaring in your brain and it feels like you’re suffocating when you don’t let them work according to spec. Sub-optimal performance feels like something sharp digging in your head. Effect: You cannot engage in non-lethal combat, though you don’t necessarily need to coup de grace downed foes. Check for Alienation each round you attack someone with non-lethal intent.
- **Candid**: The processors feed you information so quickly and so smoothly that your meat brain barely has time to process it.
If you try to slow it down, it hurts, so it’s easier to just let it run. Effect: You instinctively answer any question with at least one sentence of truthful reply. Check for Alienation if you try to remain silent or avert this instinct for a scene.
- **Cold**: The hormonal adjustments are fraying your ability to form emotional bonds with others.
You intellectually may know they love you and you may have every reason to love them, but it’s a distant abstraction to you. It’s not a real feeling, like the hate, or lust, or longing that still sometimes sparks in you. You stick with your friends out of habit or rational calculation. Effect: You are unable to feel love or affection for others. Lose your closest Contact.
- **Distant**: Everything’s so much clearer with the augs.
You can see what you need to do, to say, to be, while those around you wallow in weak, fleshly delusion. Dealing with them is so very tiresome. Effect: Take a -1 penalty to all social skill checks.
- **Exultant**: The chrome is perfect.
If it breaks, you can just fix it. If you break, you can just get more chrome. You can’t be beaten, not for long, because the metal can do anything. Effect: Your overconfidence causes an automatic, unavoidable skill failure on a natural skill roll of 12.
- **Fearless**: The combat augs are great for suppressing fear.
Honestly, it feels so good that you just leave them on all the time. Being able to live your life without worry or anxiety is such a tremendous relief, even if it sometimes makes you overlook some things. Effect: Your maximum HP is decreased by 20%, rounded down, because your survival instincts are dulled. If an NPC, your Morale is 12.
- **Hypervigilant**: Can’t sleep.
Wireless push updates will eat me. Effect: Gain one permanent System Strain point due to persistent insomnia.
- **Murderous**: You were wired to kill, and you can’t turn it off.
The longer between kills, the more everything starts looking like a weapon, and everyone starts looking like a throat to cut. When it gets really bad, the wires won’t let you think of anything but your real purpose, what you made yourself to be. Effect: Must personally kill at least one person a week, or check for Alienation for each day beyond without a murder.
- **Ravenous**: These senses are incredible; you’re seeing, or hearing, or feeling things human meat was never meant to experience.
You want more, you need more, and you’ll do anything to get it. Effect: Pick a sensory vice. Check for Alienation each time you resist an easy opportunity to indulge it. Pay $100 per total permanent Alienation point per week in feeding it or check for Alienation.
- **Savage**: Your cyber is telling you to kill.
It’s constantly reminding you of active enemies or weak points on targets, and you have to keep rejecting confirmation prompts for killing blows. It’s only letting you think about trajectories and kill counts, about threats and unfinished terminations. You can’t think with a live enemy around. Effect: You can’t run from combat after you’ve suffered hit point damage in it. If you choose to flee, check for Alienation.
- **Secretive**: Your social protocols know best.
They’re telling you not to say that, warning you that you’ll ruin everything if you admit it. You’re supposed to say something better, something more appropriate, and the prompts keep screaming at you if you try to defy them. Effect: You cannot entirely truthfully answer any question that doesn’t relate to common knowledge or your wishes in a business transaction. If you do so, check for Alienation for each topic that’s truthfully discussed in a scene.
- **Stressed**: The hardware is getting in the way of the wetware.
The demands are aggravating an already weakened system, and it can’t take the strain. Effect: Your lowest attribute suffers a -1 modifier penalty, to a minimum of -3. If multiple attributes tie, pick one.

## 3.7.0 Cyberdecks

These laptop-sized computers range from scrap-built makeshifts to cutting edge hardware stolen from the very best corps.
All of them have certain important qualities.

Bonus Access reflects the onboard intrusion hardware integral to the deck.
It augments the user’s base Access score, which is equal to their Intelligence modifier plus their Program skill plus this bonus Access.

Memory is the number of memory units standard to the deck.
Most programs and paydata files take up one unit of memory. Erasing a program from memory is an Instant action, but loading it off a program chip takes fifteen minutes to complete all its linkages.

Shielding is the amount of buffer circuitry and hardwired signal damping the deck can use to lessen incoming damage to the user.
Hit point losses inflicted by the Stun or Kill program Verbs deplete the deck’s Shielding before affecting the user. Shielding regenerates after fifteen minutes of calm.

CPU indicates the number of programs the deck can run at once.
Some programs are Immediate, and self-terminate right after executing, while others are Ongoing, and take up a CPU slot until terminated as an Instant action.


| Cyberdeck         |     Cost | Bonus Access | Memory | Shielding | CPU | Enc |
|-------------------|---------:|-------------:|-------:|----------:|----:|----:|
| Cranial Jack Only | As cyber |            0 |      0 |         0 |   1 | N/A |
| Scrap Deck        |     $500 |            1 |      8 |         5 |   2 |   1 |
| Tanto             |   $5,000 |            1 |     10 |        10 |   3 |   1 |
| Icepick           |  $15,000 |            2 |     10 |        10 |   3 |   1 |
| Synapse           |  $30,000 |            2 |     11 |         5 |   4 |   1 |
| Beowulf           |  $60,000 |            2 |     13 |        10 |   4 |   1 |
| Tizona            | $100,000 |            3 |     11 |        10 |   5 |   1 |
| Taifu             | $250,000 |            3 |     13 |        15 |   6 |   1 |

### 3.7.1 Cyberdeck Mods

Cyberdeck mods use the same modding rules as given in section 2.8.2, with each deck mod counting against the tech’s Maintenance score.

Prices are given as percentages of the original deck’s unmodified cost; the more sophisticated the deck, the more expensive it is to improve it.
Unless specified otherwise, a deck mod can be applied only once to a given deck.


- **Bespoke Code Optimization (Fix-1)**: Pick one Verb and one Subject when installing this mod; those utilities occupy no Memory and one instance of that Verb-Subject program doesn’t count against its CPU burden.
Costs 20% of the base deck cost.
- **Buffer Circuits (Fix-1)**: Add 5 points to the deck’s Shielding rating.
Costs 20% of the base deck cost.
- **CPU Overclocking (Fix-2)**: Add one to the deck’s CPU rating.
Costs 30% of the base deck cost.
- **Memory Expansion (Fix-1)**: Add four units of Memory to the deck.
Costs 20% of the base deck cost.
- **Polymorphic Intrusion Algorithms (Fix-1)**: Add one to the unit’s Bonus Access but subtract one from its CPU rating.
Costs 10% of the base deck cost.
- **Skeletal Case (Fix-1)**: The deck’s Encumbrance value is decreased by 1, but its Memory suffers a -2 penalty.
Costs 10% of the base deck cost.
- **Thermal Exhaust Case (Fix-2)**: The deck’s Encumbrance value is increased by 1, but its CPU value increases by 1 as well.
Costs 30% of the base deck cost.

## 3.8.0 Drones

Small wheeled, rotored, or even aquajet-driven drones are common tools for modern operatives.

Most operators rely on man-portable drones that can be carried in packs or specially-designed harnesses.
These portable drones require a Main Action to unpack and deploy before they can be commanded, in addition to any action necessary to get at a Stowed item.

Once a drone has been deployed, the user must bring it online and ready for command using a control board or a Remote Control Unit cybersystem.
This takes both a Main and a Move action on the user’s part. Once it’s been brought up, the drone can be commanded until it’s packed again, which requires a Main Action.

A controlled drone automatically relays audiovisual sensor data back to the control board or Remote Control Unit cyber being used to control it.
A pilot can see and hear what their drones see and hear. Default sensors are as acute as normal human senses.

Flying drones operate by means of rotors and directed thrust and can hover in place.
Ground-based drones can navigate stairs and other obstacles that human legs could manage. The noise made during operation is no louder than speech.

Flying drones have an operational altitude maximum of 1,000 meters, and all drones have a safe default control range of 1,000 meters.
Beyond this range, control signal latency leaves them critically vulnerable to modern signal hijacking tech; all wireless hack or jamming attempts against them are automatically successful. Their theoretical maximum control range is 8,000 meters.

Drones operate for one hour on a full battery charge.
They do not have swappable batteries, and require one hour of charging per Encumbrance, or six hours at most.

### 3.8.1 Common Drones

Most drone statistics are self-explanatory; they have an Armor Class against both ranged and melee attacks, a Trauma Target against weapons capable of inflicting Traumatic Hits on it, a Move rate that is either ground-based, flying, or swimming, and a hit point total that can be depleted to destroy them.

Fittings indicates the maximum number of drone fittings that can be mounted on the drone.

Hardpoints are the number of ranged weapons that can be mounted on the drone.
Drones with a base, unmodified Encumbrance of 3 or less can mount only pistol-sized weapons, while larger man-portable drones can mount rifle-sized ones. Non-portable drones can mount heavy weapons that the GM finds plausible. Melee weapons cannot be practically mounted on a drone, and swapping weapons takes a day in the shop.

Weapons mounted on a hardpoint include one magazine of ammo.
If more is needed, the Ammo Supply fitting is required.


| Drone       |    Cost | AC | TT | HP | Fittings | Move       | Hardpoints | Enc |
|-------------|--------:|---:|---:|---:|---------:|------------|-----------:|----:|
| Mouse       |    $500 | 13 |  6 |  1 |        0 | 5m ground  |          0 |   1 |
| Roach       |  $1,000 | 13 |  6 |  8 |        3 | 10m ground |          0 |   3 |
| Hummingbird |  $2,000 | 15 |  6 |  5 |        2 | 10m fly    |          0 |   3 |
| Sunfish     |  $1,000 | 15 |  6 |  8 |        3 | 10m swim   |          0 |   3 |
| Pitbull     |  $5,000 | 15 |  8 | 15 |        5 | 20m ground |          1 |   5 |
| Javelin     | $10,000 | 16 |  6 | 12 |        5 | 20m fly    |          1 |   6 |
| Kerberos    | $15,000 | 18 |  8 | 25 |        6 | 20m ground |          3 |   - |
| Kraken      | $10,000 | 16 |  8 | 20 |        5 | 15m swim   |          2 |   - |
| Shrike      | $25,000 | 18 |  8 | 20 |        6 | 30m fly    |          2 |   - |

### 3.8.2 Drone Mods

These mods require that the tinkerer have both Fix and Drive skill, as the adjustments require a keen understanding of their practical consequences on handling and operation.

Mods require upkeep and count against the tech’s Maintenance total.
Unless specified otherwise, a given mod can only be applied once. Drone mods don’t need special components.


- **Additional Fitting (Fix-1 & Drive-1)**: Careful optimization of power lines and chassis reinforcement allows for adding an additional fitting to the drone.
Man-portable drones add one point of Encumbrance, though this doesn’t affect their maximum hardpoint weapon size. The cost is 50% of the drone’s base cost.
- **Additional Hardpoint (Fix-1 & Drive-1)**: By rearranging wiring, adding mass, and overriding safety protocols, an additional hardpoint can be added to a drone.
This mod adds one point of Encumbrance to portable drones, though this doesn’t affect their maximum weapon size. The cost is 50% of the drone’s base cost plus the weapon.
- **Battery Swapping (Fix-1 & Drive-1)**: Replacing proprietary battery systems with off-the-shelf models allows man-portable drones to have their battery swapped with a minute of work.
Backup batteries cost 10% of the drone’s base cost and count as Enc 1 items, and this mod itself costs 10% of the drone’s base cost.
- **Boosted Engine (Fix-1 & Drive-2)**: The drone’s engines are tuned to optimize speed, increasing its Move rate by 10m.
It costs 25% of the drone’s base cost.
- **Heavy Plating (Fix-1 & Drive-2)**: Strategic armor reinforcement improves its base AC by 2, but decreases its Move rate by 5m.
It also increases its effective Encumbrance by 1 if it’s man-portable. The cost is 25% of the drone’s base cost.
- **Quick Launch (Fix-1 & Drive-2)**: A packed man-portable drone is modified to deploy itself as an On Turn action by its controller instead of a Main Action, even if it’s currently Stowed.
The cost is 10% of the drone’s base cost.
- **Redundant Systems (Fix-2 & Drive-2)**: Careful duplication of critical systems increases the drone’s hit points by 25% of its unmodified base score, rounded up.
The extra mass increases the Encumbrance of man-portable drones by 1. The cost is 50% of the drone’s base cost.
- **Stripped Fittings (Fix-2 & Drive-1)**: Mass and power allocated to a man-portable drone’s fittings are removed.
The drone’s maximum fitting total is decreased by 1 and its Encumbrance rating is reduced by 1, to a minimum of 1. The cost is 10% of the drone’s base cost.

### 3.8.3 Drone Fittings

Each of the fittings below counts as one choice against a given drone model’s maximum fittings.
Prices for adding a fitting are given as a percentage of the drone's base cost. Most of these fittings can be installed only once on any given drone, though a few specifically allow multiple installations.

| Drone Fitting            | Cost |
|--------------------------|-----:|
| Ablative Code Buffer     |  10% |
| Altitude Boost Unit      |  10% |
| Ammo Caddy               |  10% |
| Ammo Supply              |  10% |
| Assisted Boost Burner    |  10% |
| Cargo Space              |  20% |
| Command Deck/Follow      |  10% |
| Command Deck/Kill        |  25% |
| Command Deck/Patrol      |  10% |
| Command Deck/Watch       |  10% |
| Emergency Evac Litter    |  25% |
| Emissions Shielding      |  20% |
| Enhanced Structure       |  25% |
| Extended Range           |  25% |
| Glider Grips             |  20% |
| Improved Armor           |  25% |
| Improved Targeting Logic |  20% |
| Laser Comms              |  10% |
| Manipulator Tendrils     |  20% |
| Memory Banks             |  10% |
| No Touch Web             |  10% |
| Sleep Mode               |  10% |
| Stealth Package          |  25% |
| Suicide Charge           |  25% |
| Telescopic Optics        |  20% |
| Thermal Optics           |  20% |
| Trauma Response Suite    |  25% |
| Voice Broadcast          |  10% |
| Wallcrawler              |  20% |
| Weapon Hardpoint         |  25% |


- **Ablative Code Buffer**: Numerous self-correcting backup circuits are wired in to delay any hostile hacking intrusions.
The drone automatically succeeds on the first opposed skill check made against a hostile hacker in a given scene.
- **Altitude Boost Unit**: Most flying drones have an effective ceiling of 1,000 meters if there are no scramblers to lessen the effective control range.
This unit increases a flying drone’s maximum altitude to 5,000 meters, making most effectively invisible to ordinary sight. Such height does, however, make it extremely conspicuous to aerial scanners used by city law enforcement and high-security corp buildings. The speed of their response will depend on the drone’s apparent threat.
- **Ammo Caddy**: A specialized form of cargo space, this drone is able to carry and dispense substantial amounts of ammunition to adjacent allies.
It can hold up to 6 fully-loaded magazines in quick-release clips that can be grabbed as On Turn actions by an ally. The clips remain locked for strangers. This fitting can be taken up to once per two points of the drone’s Encumbrance, rounded up.
- **Ammo Supply**: An automated ammo feeder is added to the drone.
Two full magazines are added to one mounted weapon’s allotment, or one magazine to two weapons. This fitting can be added more than once.
- **Assisted Boost Burner**: The drone is equipped with reactive chemical booster that can be ignited to give it a very brief burst of tremendous speed.
When activated as a Move action, the burner launches the drone up to 500 meters in a straight line. The boost is not precise enough to use as an aimed weapon, but solid obstacles in front of the drone will suffer half the drone’s HP in damage and inflict all of theirs on the drone. The burner functions only once before it require a scene to regenerate its charge.
- **Cargo Space**: Most drones are designed to carry only their own weight.
This fitting upgrades engines and adds cargo mounts to the drone’s exterior; it can carry up to 3 points of Encumbrance now, but the drone’s own Encumbrance value is increased by one. Any additional cargo it carries is added to this burden, of course. Carried cargo is sealed within the drone and requires a Main Action to dig out.
- **Command Deck/Follow**: The drone is equipped with minimal autonomous pursuit capabilities.
As a Main Action, the drone can be ordered to follow a particular visible target at a given distance at either half or full speed. If half speed, it will use its Move action to follow, and full speed will use both Move and Main Actions. Following can be ended as an On Turn command.
- **Command Deck/Kill**: The drone has autonomous attack routines wired into it.
As a Main Action, it can be ordered to kill one specific visible target. If the target is in range of its weapons, it will use its Main Action to attack; if no additional fittings are added, its autonomous attacks have a total hit bonus of +2. The drone will not automatically move to follow or engage its target or otherwise position itself tactically unless directly piloted to do so or operating under the Follow command.
- **Command Deck/Patrol**: As a Main Action, the drone can be ordered to autonomously move to one or more waypoints on whatever schedule the pilot wishes.
The drone can also be ordered to loiter at a particular location until it gets an On Turn command to proceed to the next location.
- **Command Deck/Watch**: The drone is equipped to monitor its surroundings autonomously and alert its pilot when something of interest is noticed.
Its onboard intelligence is limited, however, and it can only handle about a sentence’s worth of specific orders or subjects to be watchful for. Putting a drone in Watch mode is a command that takes a Main Action.
- **Emergency Evac Litter**: Functioning only for ground-based drones of Enc 3 or greater, the drone is equipped with a retractable litter that can fit one human patient, along with manipulators sufficient to get a prone victim onto the litter with a Main Action.
Onboard meds allow for remote Heal skill checks to stabilize a Mortally Wounded target, with one attempt allowed as part of the loading action. A loaded patient can’t be harmed until the drone is destroyed or opens the litter, but if the drone dies, the patient does too. As a Main Action, the drone can be ordered to autonomously take the patient to a particular location. As an On Turn action, an onboard transmitter can also place a call for medical assistance to a pre-arranged number.
- **Emissions Shielding**: The drone is designed to minimize its electromagnetic emissions and thermal signature.
Skill checks to spot it with such sensors suffer a -2 penalty.
- **Enhanced Structure**: The drone’s maximum hit points increase by 25% of its unmodified base, rounded up.
As with Improved Armor, this fitting is ineffective on drones that aren’t man-portable.
- **Extended Range**: The drone can now operate for two hours on a full battery charge.
- **Glider Grips**: Usable only by flying drones of Enc 3 size or greater, this fitting allows a human-sized mass to use the drone as a form of parachute-glider.
As a Main Action, a user can leap off a high place after grabbing the drone and descend safely to the ground at a distance as far from the initial point as that point was above the ground. Similar weights can be sent safely to the ground in the same way.
- **Improved Armor**: The drone’s AC improves by 2, to a maximum of 18, but its Encumbrance rating increases by one.
This fitting is ineffective on drones too big to have an Encumbrance rating; they need vehicle-grade armor upgrades.
- **Improved Targeting Logic**: This unit allows a skilled drone pilot to tweak the combat logic of its onboard weaponry.
The drone’s attack gains a hit bonus equal to the pilot’s Drive or Program skill level when firing autonomously; no bonus is given if the pilot is in direct control.
- **Laser Comms**: This fitting provides both a set of universal transceiver plug-ins and a laser comm hub for the drone.
Provided the PCs are in line of sight of the drone, normal radio-wave based comms can be switched to lasers, providing uncrackable, electromagnetically-inaudible communications within the group. Radio comms can still be used as needed.
- **Manipulator Tendrils**: One set of manipulators is added to the drone, whether a mechanical claw, an articulated metal tendril, or some other device.
With a Main Action command from the pilot, it can function as a single human hand with an effective Strength of 12, but it cannot effectively be used for an attack or to lift anything heavier than the drone itself. The fitting can carry things for short distances, but to haul weight any real distance the Cargo Space fitting is required. This fitting can be added more than once if a pair of “hands” are needed.
- **Memory Banks**: The drone has two units of cyberdeck Memory added to it.
While this Memory can be used to store hacker programs or datafiles, it also has room to record up to a week of audiovisual data per unit used. This fitting can be added more than once.
- **No Touch Web**: Most commonly installed on small ground drones, this web can be armed as a Main Action by the pilot.
Until disarmed, any attempt to grab or pick up the drone automatically inflicts 2d6 non-lethal electrical damage on the grabber. This shock continues each round as long as the drone is held, with up to 5 discharges total before the battery must be recharged. The firm contact required makes it impractical to use the web as an offensive weapon.
- **Sleep Mode**: Sleep mode can be enabled with a Main Action command or ended with an On Turn command.
While in sleep mode, a drone cannot fly, move, or perform actions, but it will continue to communicate with the pilot and observe its surroundings. A day of sleep mode counts as ten minutes of ordinary operation for battery drain purposes.
- **Stealth Package**: Usable only on drones of Enc 3 or smaller, this fitting adds color-changing drone sheathing, noise damping, and thermal sealing to the unit.
It gains an effective Sneak-1 skill when operating autonomously, or a +2 bonus to Sneak checks when being directly piloted.
- **Suicide Charge**: With a Main Action command to detonate, the drone will explode, doing 3d6 damage to anyone within ten meters, with an Evasion save for half.
Subjects behind solid cover further halve the damage. Drones reduced to zero hit points are too badly damaged to respond to detonation commands.
- **Telescopic Optics**: The drone can focus on objects up to one kilometer away with a visual acuity sufficient to read a newspaper.
Integral low-light sensors allow it to see in all but total darkness.
- **Thermal Optics**: The drone is equipped with IR optics that can see heat patterns.
The drone can function adequately even in complete darkness and grants the pilot a +1 bonus to Notice skill checks where thermal imaging might be useful.
- **Trauma Response Suite**: An array of injectors, scalpels, sutures, and trauma chems are built into the drone, allowing it to function as a medkit and make Heal skill checks for a medically-competent pilot or an on-site doc in need of a kit.
Such medical efforts require a Main Action from the pilot as usual, and flying drones apply a -2 penalty to the skill check due to their innate instability. Up to six pharmaceutical doses of the pilot’s choice can be loaded into the drone.
- **Voice Broadcast**: The pilot can speak through the drone or cause it to emit other noises at a volume loud enough to be heard clearly a hundred meters away.
- **Wallcrawler**: Usable only on ground-based drones of Enc 3 or less, this fitting allows the drone to climb sheer surfaces at its normal movement rate, assuming the surface is sturdy enough to bear its weight.
Most high-security buildings are constructed with veneers that defeat this fitting, but ordinary structures seldom have such measures.
- **Weapon Hardpoint**: Drones that are not factory-equipped with weapon hardpoints can have one worked in as an aftermarket addition.
The usual hardpoint limits on maximum allowed weapon size still apply, however, and this fitting counts as two for purposes of the drone’s maximum fittings.

### 3.8.4 Drone Operation

Operating a drone can be done either with the control board that comes with the drone or with a specialized Remote Control Unit cyber system.
In both cases, at least Drive-0 skill is needed to operate drones effectively.

A control board can command one drone at a time.
The user suffers a -2 penalty on all hit rolls and skill checks made by the drone. If the user has a cranial jack and plugs into the board, this penalty is eliminated. A control board takes up 1 Encumbrance and must be Readied to use.

A Remote Control Unit can command a number of drones at once equal to one plus the user’s Drive skill.
It grants the user a bonus Move action each round that can only be used to command drones. If the user pushes the system and accepts a System Strain point, this bonus Move action is upgraded to a Main Action.

Drones can be operated relatively securely at a distance of up to 1,000 meters, with flying drones having a maximum altitude of 1,000 meters as well.
Beyond one kilometer, signal latency makes it trivially easy to hijack the control signal with modern wifi sniffers. Any hack or jamming attempt on a drone beyond this range is automatically successful. If the operator is willing to risk this, drones can function at a maximum range of 8,000 meters.

#### 3.8.4.1 Controlling Drones

Pilots use their own actions to drive their drones.
If the pilot wants the drone to take a Move action, they need to spend a Move action piloting it in the desired direction. If they want it to fire its guns, they need to spend their own Main Action performing the attack.

A drone under autonomous command, such as with the Command Deck/Kill fitting, can perform a maximum of one Main and one Move action per round, and will use those actions to carry out its last command.
In case of conflicting autonomous imperatives, the latest one overrides earlier ones.

Any attacks the drone makes are performed with the operator’s hit bonus plus the better of their Drive or Program skills, modified by Int or Dex.
If the drone is autonomous, it usually has a native +2 bonus.

Any skill checks the drone makes are performed with the operator’s skill level, up to a maximum of the pilot’s Drive skill.
They may be a master infiltrator with Sneak-4, but if they’ve only got Drive-0 skill they can exert only Sneak-0 through the drone.

Drones cut off from control by jamming or operator abandonment or incapacitation do nothing, not even performing autonomous commands.
Flying drones automatically land where they are.

#### 3.8.4.2 Common Drone Actions

When in doubt, a GM can simply assign a drone action the same cost as they would the same action performed by a PC.
However, some common actions have special cases for drones and are listed as such below.

- **Attack (Main Action)**: The drone fires all of its onboard weapons, selecting targets for each individually.
The pilot may direct one attack, adding their base attack bonus modified by Int or Dex and the better of their Drive or Program skills; all others fire at the drone’s usual attack bonus of +2.
- **Move (Move Action)**: The drone moves its normal movement rate.
Flying drones can hover, and ground drones can navigate rough terrain just as a human would, moving at half their usual rate. Prudent placement may grant a drone the benefits of cover or concealment, but they can’t go Prone.
- **Assume Command (Main and Move Actions)**: A pilot who needs to assume command of one of their newly-deployed drones can do so by taking both a Main and Move action to shift the signal.
Uncommanded drones remain inert and will not act autonomously. Dropping control to free up the pilot’s attention is an On Turn action.
- **Halt Autonomous Mode (On Turn)**: An active autonomous command such as Kill or Follow can be stopped with this action, the drone doing nothing until a new command is issued.
- **Grab a Drone (Main Action)**: Man-portable drones can be grabbed by assailants in melee range.
This counts as a grappling attempt with a hit roll required, but the drone automatically fails any Exert skill checks to resist. A grappled drone is helpless while the grip is maintained.

#### 3.8.4.3 Drone Damage and Repair

A drone reduced to zero hit points has been destroyed, and cannot be repaired.
Any cargo it might have been carrying has almost certainly been smashed as well.

Repairing a portable drone requires a suitable toolkit, Fix-0 skill, and a supply of spare parts.
Non-portable drones require a full workshop. Repairs take one hour of work.

Spare parts for a drone cost 25% of the drone’s base cost.
As manufacturers make a point of not building cross-compatible components, each model of drone requires its own supply of parts. These parts are sufficient to repair any reasonable amount of damage, though more than one or two full rebuilds may cause the GM to rule that they’ve run out.

Spare parts for a portable drone take up 3 Encumbrance.
Parts for a non-portable drone are too bulky to be reasonably man-portable.

## 3.9.0 Vehicles

A fast set of wheels is necessary for many missions.
The vehicles below are a selection of some of the more common options in a city.

A vehicle’s qualities are measured by certain statistics.
Some are self-explanatory, such as the Hit Points of damage it can take before being rendered inoperable or the Armor Class that must be hit with a melee or ranged weapon in order to damage it.

**Speed** is a combination of the vehicle’s overall speed, maneuverability, and capability for handling bad terrain.
It’s added to any vehicle-related skill checks the driver might make with it.

**Armor** indicates the vehicle’s general toughness and resistance to damage, as explained in the vehicle combat rules.

**Trauma Target** is the vehicle’s target number for Trauma Die rolls by weapons capable of inflicting Traumatic Hits on such a vehicle.

**Crew** is the maximum number of people the vehicle is intended to carry, including the driver.
More might be wedged into a truck bed or strapped to a roof rack, but they are very likely to be thrown free during sharp maneuvers.

**Power** and **Mass** measure the amount of additional fittings that can be mounted on the vehicle.
Added hardware takes up electrical and computing Power, and it can’t be fitted without enough free Mass.

**Size** is the general size class of the vehicle, whether small-sized like a motorcycle, medium like a car, small helicopter, or SUV, or large like a flatbed truck or tank.
Some vehicle fittings can only be installed on vehicles of a certain size or larger.

**Hardpoints** are the number of Heavy weapons the vehicle can mount in its factory configuration.
Vehicle combat is detailed in its own section, but every gun requires a gunner, and even a jacked-in driver is hard-pressed to shoot and drive at the same time.


| Vehicle     |   Cost | Spd | Armor | TT | AC | HP | Crew | Power | Mass | Size | Hrdpt. |
|-------------|-------:|----:|------:|---:|---:|---:|-----:|------:|-----:|------|-------:|
| Motorcycle  |    $1K |   1 |     4 | 10 | 13 | 10 |    1 |     1 |    3 | S    |      0 |
| Micro Flyer |    $3K |   0 |     0 |  6 | 13 | 10 |    1 |     1 |    4 | S    |      0 |
| Car         |    $5K |   0 |     6 | 12 | 11 | 30 |    5 |     3 |    7 | M    |      1 |
| Truck       |  $7.5K |   0 |     6 | 12 | 11 | 35 |    2 |     3 |   14 | L    |      1 |
| Helicopter  |   $50K |   3 |     6 | 10 | 14 | 20 |    6 |     4 |    9 | M    |      1 |
| Tank        | $500K@ |   0 |    ** | 12 | 18 | 40 |    3 |     8 |   15 | L    |      3 |
| APC         |  $60K@ |  -1 |     * | 10 | 16 | 30 |   16 |     5 |   14 | L    |      1 |
| GEV         | $100K@ |   1 |     * | 10 | 16 | 30 |    3 |     6 |   10 | L    |      2 |
| CASRA       | $200K@ |   2 |    10 | 10 | 18 | 35 |    2 |     7 |   10 | L    |      2 |
| Dropcraft   |   $1M@ |   3 |    12 | 12 | 16 | 40 |   13 |     8 |   12 | L    |      2 |

@: This vehicle cannot be obtained without a suitable Contact or related adventure<br>
\*: This vehicle is immune to anything short of a Heavy weapon or one that can inflict Traumatic Hits on vehicles.<br>
\*\*: This vehicle is immune to anything the GM thinks could not reasonably harm the tank in question.
It can suffer Traumatic Hits from a weapon if the GM thinks it reasonable for the weapon and circumstances.<br>


- **Motorcycle**: Fast, cheap, and nimble, both gangers and operators find these vehicles handy for making escapes down alleys and over terrain that larger vehicles can’t navigate.
- **Microlight Flyer**: One of a class of featherlight aircraft, a flyer is woven of the lightest, thinnest, strongest materials available at its price point, and can provide cheap aerial recon or transit over rough terrain for its single occupant.
Such flyers are light enough to be broken down into components that can be easily transported by a pickup truck.
- **Car**: An urban luxury, private cars are for the middle class and better, or those rough-edged slum entrepreneurs who are clever enough to keep some salvaged hulk running.
This vehicle class also includes small pickup trucks and SUVs.
- **Truck**: More commonly found among corp employees, these flatbed trucks, delivery vans, panel vans, and other large work vehicles earn their keep in a hundred ways each day.
- **Helicopter**: A typical civilian helicopter, used for elite transit between city districts or the quick dispatch of personnel to some hotspot.
Their ability to weave through the obstacles of urban high-rises is sometimes valuable when evading hostile pursuit.
- **Tank**: A military classic, this model is relatively low-tech but more than enough to deal with any civilian vehicle.
Tanks are seldom found in urban areas without a protective screen of infantry to keep hostiles with demo charges or rocket launchers from getting uncomfortable close.
- **APC**: Armored personnel carriers are often custom-built for city law enforcement, the better to dump a squad of cops where they’re most needed.
The mounted weapon on most of them helps pacify a hot zone before the occupants dismount.
- **GEV**: Ground effect vehicles have evolved considerably from their 20th-century ancestors, and their armored pneumatic skirts and high-efficiency engines can send them over terrain that might defeat a tracked tank.
The sacrifice in armor weight is generally deemed acceptable, given the usual opposition these vehicles face.
- **CASRA**: The “Close Air Support Rotorwing Aircraft” of corporate R&D comes in a half-dozen different configurations, from multirotor helicopters to rotor-in-wing strike craft.
Light and vicious, they’re favored by city SWAT teams who need military-grade firepower capable of maneuvering through city airspace. When a CASRA shows up, minigun rounds are seldom far behind.
- **Dropcraft**: One of a family of armored military air transports, dropcraft are used when a squad needs to be inserted under heavy fire.
Thick armor, a heavy gun, and VTOL engines combine with considerable loitering ability to make an excellent tool for pacifying corporate enemies.

### 3.9.1 Repairing and Running Vehicles

Fixing a vehicle requires a Tool Rack fitting or a suitable workshop.
A bike might be repaired on a sidewalk, but an APC is going to need heavier equipment.

Vehicles that have been reduced to zero hit points have been totaled and cannot be cost-effectively repaired.
Lesser damage can be repaired at a rate of 1 point per day, plus the technician’s Fix skill and Drive skill. A would-be repairman must have at least Fix-0 or Drive-1 skill to repair a damaged vehicle.

An Ace Driver or Roamer pays nothing to repair vehicles, being able to scrounge free parts here and there.
Others need to pay $200 per vehicle hit point repaired.

A vehicle’s fuel and maintenance costs are generally minor, and can usually be ignored.
If the PCs are going to be spending long periods of time away from gas stations, however, a GM is justified in making them take some measures to ensure a fuel supply

### 3.9.2 Vehicle Fittings

To add a fitting, the vehicle must have enough free Power and Mass to support it; a bike with 1 Power and 3 Mass can’t mount more than 1 total Power and 3 total Mass of fittings.
It must also be of an adequate minimum size to support the fitting.

Installing or removing a fitting usually takes a day or two of work in a suitably-equipped shop.
The price for such work is included in the fitting cost. A given fitting can normally be added only once.


| Vehicle Fitting      |     Cost | Power |   Mass | Min. Size | Effect                                 |
|----------------------|---------:|------:|-------:|-----------|----------------------------------------|
| Advanced Sensors     |   $8,000 |     1 |      0 | S         | Adds night vision and more             |
| Afterburners         |   $5,000 |     1 |      2 | S         | Boost Speed briefly in combat          |
| Armor Plating        |   $5,000 |     0 |      3 | S         | Adds Armor to the vehicle              |
| Cargo Space          |     None |     0 |      1 | S         | Adds protected cargo space             |
| Crash Pod            |   $2,500 |     0 |      2 | M         | Protects in case of crash              |
| ECM Emitter          |  $10,000 |     2 |      0 | M         | Jams incoming missiles                 |
| Emissions Cloaking   |  $10,000 |     1 |      2 | S         | Radar and thermal near-invisibility    |
| Extra Durability     |   $5,000 |     0 |      4 | M         | Increases maximum HP by 25%            |
| Extra Passengers     |   $2,500 |     0 |      2 | S         | Add additional Crew                    |
| Field Portable       |   $1,000 |     0 |      2 | S         | Break it down into portable components |
| Ghost Driver         |   $2,500 |     1 |      1 | S         | Limited AI driving capabilities        |
| Hardpoint Support    |   $5,000 |     1 |      1 | M         | Adds another hardpoint                 |
| Jack Control Port    |   $5,000 |     2 |      0 | S         | Can drive the vehicle via cranial jack |
| Limpet Mount         | $5K/$10K |     0 | 3 or 6 | M         | Mount a smaller vehicle on it          |
| Living Quarters      |   $8,000 |     0 |      4 | L         | Cramped but usable living quarters     |
| Medbay               |  $10,000 |     1 |      2 | M         | Emergency bay for one patient          |
| Offroad Package      |   $5,000 |     1 |      3 | L         | Enables deep wilderness operation      |
| Power System, Small  |   $1,000 |    +2 |      2 | S         | Adds Power at a cost in Mass           |
| Power System, Medium |   $5,000 |    +4 |      3 | M         | Adds Power at a cost in Mass           |
| Power System, Large  |  $10,000 |    +8 |      5 | L         | Adds Power at a cost in Mass           |
| Sealed Atmosphere    |   $5,000 |     1 |      1 | M         | Pressurized, temp-controlled interior  |
| Smuggler’s Hold      |   $1,000 |     0 |      1 | S         | Hidden cargo space                     |
| Targeting Board      |   $2,500 |     1 |      1 | M         | One gunner can run up to three guns    |
| Tool Rack            |   $2,500 |     0 |      2 | M         | Can repair vehicle or other things     |


- **Advanced Sensors**: The vehicle is equipped with night vision, infrared sensors, and short-range radar good up to twenty kilometers of airspace.
It can be driven safely even in pitch darkness.
- **Afterburners**: Once per scene, as an On Turn action, boost the vehicle’s Speed by 1 for five rounds.
- **Armor Plating**: Increase the vehicle’s Armor rating by 3 points.
Medium vehicles can apply this fitting twice, and Large ones can apply it three times.
- **Cargo Space**: This fitting can be taken multiple times.
Each time it adds 50 kg of protected cargo space for Small vehicles, 500 kg for Medium ones, and 2 metric tons for Large ones.
- **Crash Pod**: If the vehicle crashes, the occupants can each reroll one failed crash save.
If they make any of their saves, their HP can’t be reduced below 1 point by the crash.
- **ECM Emitter**: On a successful hit roll by a headshot pod, rocket launcher, or other long-range guided projectile, the driver can make an opposed Int or Dex/Drive skill check against the attacker’s combat skill.
On a success, the attacker must reroll their hit roll and take the worst result.
- **Emissions Cloaking**: Once per day, for one hour, the vehicle can become almost invisible to infrared or radar.
This cloak becomes useless if the vehicle is visually spotted by defenders.
- **Extra Durability**: The vehicle’s maximum hit points increase by 25%, rounded up.
- **Extra Passengers**: Small vehicles add 1 Crew, Medium ones add 2, and Large ones add 4.
This fitting can be taken multiple times, and any vehicle amenities such as crash pods or living quarters extend to these additional Crew as well.
- **Field Portable**: Only Small or Medium vehicles can take this option, allowing them to be broken down into easily-portable parts packages.
Small vehicles turn into 30 items of Encumbrance and Medium ones into 150. Disassembly takes 30 minutes for a Small vehicle and 3 hours for a Medium one, with reassembly taking four times as long.
- **Ghost Driver**: The driver can order the vehicle directly or by radio or phone to go to particular places at particular times or perform other simple, direct actions on a given schedule.
The onboard expert system can handle traffic-related challenges, but has no real intelligence to process more complicated problems.
- **Hardpoint Support**: Additional power and structural support enable the addition of a hardpoint.
- **Jack Control Port**: The vehicle is wired to be controlled through a plugged-in cranial jack as well as a conventional wheel.
While jacked in, the driver need only use a Move Action to control the vehicle rather than a Main Action.
- **Limpet Mount**: An entire second vehicle can be mounted with this fitting, Medium vehicles taking Small parasites and Large ones taking Small or Medium.
Adding a mount for a Small vehicle takes up 3 Mass while a Medium one takes 6. Modifying a vehicle to be mounted adds 10% to its base cost, but it can be launched even while the parent vehicle is underway. These parasite vehicles are destroyed if the parent is; otherwise, they take only half the damage the parent takes.
- **Living Quarters**: Basic, cramped quarters are provided for sleeping, sanitation, and cooking for all crew.
Permanent living in a vehicle costs and counts as a slum lifestyle, though occupants with the Ace Driver or Roamer Focus can treat it as middle-class for the same price.
- **Medbay**: Surgical tools and medical support are provided for up to one critically-injured patient at a time.
A medbay counts as an emergency clinic for purposes of treating Major Injuries, if someone with at least Heal-0 is available to tend to the patient.
- **Offroad Package**: While any modern vehicle can handle abandoned roads and mostly-flat terrain, this package includes winches, tire armor, chassis reinforcement, and other upgrades needed to go through all but the most impassible terrain.
- **Power Systems**: These fittings add extra Power in battery capacity and computing support at the cost of an additional Mass burden.
- **Sealed Atmosphere**: For up to two hours per day the vehicle can seal itself entirely from the outside atmosphere, with internal air tanks and positive pressure to keep out external toxins.
At all times it can maintain a comfortable interior temperature in any terrestrial climate. Aircraft do not need this fitting to function normally at high altitudes.
- **Smuggler’s Hold**: This fitting can be taken more than once, and adds 10 kg of concealed cargo space in Small vehicles, 100 kg in Medium ones, and 400 kg in Large ones.
It can be detected only by taking the vehicle apart or by an acute, careful inspection with a difficulty 12 Wis/Notice or Wis/Drive skill check.
- **Targeting Board**: A single unified targeting board allows one gunner to manage up to three vehicle weapons instead of the one gun per gunner that would usually be allowed.
- **Tool Rack**: Any vehicle can carry a toolkit in the trunk, but this rack is equipped with welders, heavy tooling, winches, and room for up 40 HP worth of spare parts.
It can repair any vehicle and can be used as a makeshift workshop for related efforts.

### 3.9.3 Vehicle Weapons

Vehicles with hardpoints can mount weaponry, provided they have the spare Power and Mass to support the guns.

A vehicle can mount a number of weapons equal to its hardpoints.
Each weapon takes up a certain amount of the vehicle’s Power and Mass, just as a fitting does; the heavier and more computationally-demanding the weapon is, the greater its demands.

Some weapons also require vehicles of a certain size in order to support them; no one is mounting an autogun on a motorcycle when the recoil alone would be enough to send the bike skidding.

While most professionals prefer to mount heavy weaponry on their rides, it’s also possible to mount smaller guns, such as rifles or shotguns.
These weapons make minimal demands on a vehicle’s structure, but still require at least a Medium-sized vehicle to provide a stable firing platform.

Each mounted weapon can carry one normal magazine’s worth of ammunition.
A loader can reload a vehicle weapon’s magazine as a Main Action.

Vehicle weapons each require a gunner, and any attacks made by the weapon are made with the gunner’s attack bonus and Shoot skill.

Shooting at enemy vehicles requires targeting the vehicle’s Armor Class and overcoming its Armor with the weapon’s damage.
Armor is subtracted from any damage the weapon inflicts, after multiplying for Traumatic Hits. Shooting at targets on foot requires hitting their usual ranged AC.

Mounted weapons don’t take a hit penalty for firing from a moving vehicle, but passengers who are blazing away out the windows take up to a -4 circumstance penalty to such efforts.

#### 3.9.3.1 Vehicle-Only Weaponry

These weapons are too large and bulky to be mounted on anything but vehicles or non-portable drones


| Vehicle Weapon  |   Dmg |       Range |      Cost | Mag | Attr. | Enc | Trauma<br>Die | Trauma<br>Rating |
|-----------------|------:|------------:|----------:|----:|-------|-----|--------------:|-----------------:|
| Drone Cannon    |  2d8! |   200/1,000 |   $5,000@ |  10 | Dex   | N/A |          1d10 |               x3 |
| Headshot Pod    |  4d6! | 1,000/2,000 |  $20,000@ |   1 | Dex   | N/A |          1d20 |               x4 |
| Main Tank Gun   | 4d12! | 1,000/3,000 | $100,000@ |   1 | Dex   | N/A |          1d20 |               x4 |
| Mounted Autogun | 3d8#! |   500/2,000 |  $15,000@ |  10 | Dex   | N/A |          1d12 |               x2 |
| Shrieker Gun    | 2d6^# |     100/400 |  $10,000@ |   - | -     | N/A |             - |                - |

@: This weapon requires an applicable Contact to buy, being generally illegal for open sale<br>
#: This weapon can fire to suppress if fixed to a vehicle or stationary firing position<br>
^: This weapon’s damage is always non-lethal, unless desired otherwise<br>
!: This weapon can inflict Traumatic Hits on vehicles and drones<br>


**Drone cannons** are heavy semi-automatic weapons too bulky for human portability.
A round of firing costs $100 worth of ammunition.

**Headshot pods** are a family of guided missiles meant for mounted deployment on vehicles or large drones.
They always target AC 10 to hit, but require two full consecutive rounds of aiming at a visible target to get a target lock on them and prime for firing on the third round. Any bystanders within 5 meters of the impact take damage as if from a frag grenade. A headshot pod missile costs $2,000.

**Main tank gun** attacks always target AC 10, and anyone within 10 meters of impact takes 1d20 damage if not behind heavy cover.
They cannot be mounted on aircraft or anything smaller than a tank. Each round costs $1,000.

**Mounted autoguns** are a drone cannon’s big brother, for larger platforms.
Each round of firing costs $100.

**Shrieker guns** are sonic cannons used for crowd control and non-lethal suppression.
While highly effective against unprotected targets, military-grade ear protection or the integral dampers in cyber-ears are sufficient to render the weapon useless.

### 3.9.4 Vehicle Mods

Vehicle mods generally require both Fix and Drive skills to build, install, and maintain.
Vehicle mod costs are expressed as percentages of the vehicle’s base cost, not including any fittings or weapons. Unless specified otherwise, a vehicle mod can be installed only once on any given vehicle.


| Mod                   | Skills        | Cost | Spec. Tech. |
|-----------------------|---------------|-----:|------------:|
| Augmented Armor       | Fix-3/Drive-1 |  25% |           1 |
| Drone Hub             | Fix-1/Drive-0 |  10% |           0 |
| Extra Seating         | Fix-0/Drive-1 |  10% |           0 |
| Integrated Magazines  | Fix-1/Drive-0 |  10% |           0 |
| Personalized Controls | Fix-1/Drive-2 |  20% |           0 |
| Power System Upgrade  | Fix-2/Drive-2 |  25% |           1 |
| Q-Cladding            | Fix-1/Drive-2 |  20% |           0 |
| Reactive Defenses     | Fix-2/Drive-2 |  25% |           1 |
| Reinforced Chassis    | Fix-2/Drive-1 |  25% |           1 |
| Remote Sensors        | Fix-1/Drive-0 |  10% |           0 |
| Supplementary Tanks   | Fix-0/Drive-1 |  10% |           0 |
| Ultralight Components | Fix-2/Drive-1 |  20% |           1 |
| Upgraded Speed        | Fix-2/Drive-3 |  25% |           2 |


- **Augmented Armor**: Increase the vehicle’s Armor rating by 3 points.
- **Drone Hub**: Install a housing that can hold a portable drone on Small vehicles or a non-portable drone on Medium or larger vehicles.
The drone can be deployed while the vehicle is in motion. Onboard signal encryption adds +2 to the difficulty of hacking any drone controlled by someone in or adjacent to this vehicle. This fitting can be installed twice on Large vehicles.
- **Extra Seating**: Increase the maximum Crew size by 1 for Small vehicles, 2 for Medium, and 4 for Large.
- **Integrated Magazines**: Mounted weapons gain one extra built-in magazine for Small vehicles, two for Medium, or four for Large.
Reloading one vehicle magazine takes a Main Action.
- **Personalized Controls**: The vehicle’s controls are designed for the idiosyncrasies of a specific driver.
Once per scene, as an Instant action, they can add +1 to a Drive skill check. Anyone else takes -1 to drive the vehicle.
- **Power System Upgrade**: Increase the Power rating of the vehicle by 2 points for Small vehicles, 3 for Medium, and 4 for Large.
- **Q-Cladding**: Disguise the vehicle as another vehicle of the same general type, and hide its mounted weaponry.
Replacing the disguise after weapons are deployed takes five minutes.
- **Reactive Defenses**: Increase the vehicle’s Armor Class by 2 points, but subtract 2 points from its Armor, if it has any.
- **Reinforced Chassis**: Increase the vehicle’s maximum HP by 25% of its base, but subtract 2 Mass from its maximum.
- **Remote Sensors**: The driver’s smartphone or headware can tap the vehicle’s sensors as an On Turn action, getting audio and visual from it if no special sensors are installed.
This signal can reach up to two kilometers.
- **Supplementary Tanks**: The vehicle can operate for up to three days of normal driving without refueling.
- **Ultralight Components**: Increase the vehicle’s maximum Mass by 2 points for Small vehicles, 3 for Medium, or 4 for Large.
Its maximum HP decreases by 20% of its base, rounded up.
- **Upgraded Speed**: Increase the vehicle’s Speed by 1, but lose 20% of its maximum Mass, rounded up.

# 4.0.0 Hacking

The hacking rules for this game presume a largely hardwired world.
Wifi connections are trivially cracked by modern cryptography and cyberdecks have field projection units that can penetrate computer housings or cyberware to reach the wires within. Important installations are kept on isolated local networks and connected to wide-area networks only at pre-arranged and unpredictable intervals. As a consequence, it is necessary for hackers to physically infiltrate most facilities in order to perform their duties.

## 4.1.0 Cyberspace Basics

Cyberspace exits as a VR-mediated digital world that appears in whatever idiom the system's owner has selected.
While a cyberdeck can reinterpret these idioms into a sensory experience more palatable to the user, most hackers prefer to see things "natively" when hacking a system due to the lower processing requirements.

Cyberspace exists in nodes, each one mirroring a particular network device or cluster of devices.
A single camera might be its own node, or a set of factory machines, or a net-monitored minefield, or any other network “place”. Nodes are where programs and datafiles are kept and where hackers encounter guardian Demons and human watchdogs.

Nodes are linked by connections.
To move from one node to another, a hacker needs to cross these connections, defeating any digital barriers that security may have erected. A hacker who jacks in to a security camera on the outside of a building might then hop its connection to the security panel where its output is being monitored. Without this long-distance travel, the hacker would have to physically reach the security panel in order to manipulate it.

Every human in cyberspace is represented by their avatar.
Hackers tend to go to elaborate lengths to customize their avatars and the visual representations of the programs they use, but most corporate watchdogs are limited to corporation-approved brand icons. Most “civilian” users simply wear whatever digital disguise they’ve bought most recently.

### 4.1.1 Time in Cyberspace

Cyberspace actions take the same amount of time as physical actions; the code may be fast, but the human mind is only so agile.
Hackers must split their available actions between cyberspace and any physical activities they want to perform while jacked in.

### 4.1.2 Connecting to Cyberspace

To connect to a node, a user has two choices.

Physically attach a self-adhering field modulation cable from your cyberdeck or cranial jack to the device housing.
Even without a convenient plug socket, this will let you hack the device’s internals.

Wirelessly connect to the device with your cyberdeck.
The target must be within 30 meters with an unobstructed line of sight. Wireless connections are much more tenuous than physical links; you’ll take a -2 penalty to any cyberspace skill checks you make and your avatar won’t be able to move out of the node.

In both cases, it’s assumed that you’re connecting to your cyberdeck with a cranial jack.
VR crowns can be used as a makeshift link for those without a cranial jack, but they apply a -1 penalty to all cyberspace skill checks made with them.

Connections last until someone yanks your cable off the device housing or you voluntarily jack out of the system.
Even if unconscious, your avatar remains until you’re jacked out. Crash disconnections are disorienting and will leave a hacker stunned, but sometimes an ally needs to cut the link before some watchdog boils a nerve-locked hacker’s brain.

Wireless connections have no cable to yank, but fail if the target moves out of range; once the link is established, the line of sight need not be unobstructed.

Hackers normally remain aware of their physical surroundings while jacked in.
They can act in either the physical or virtual realm as they choose, spending their actions during the round as they wish.

### 4.1.3 Access

Access is the digital currency of control, representing a hacker's array of countermeasures, zero-day hacks, fresh intrusion measures, and unsecured backdoors.
It's spent to run programs  on a secured system. Access is a general pool, though sometimes a hacker may acquire specific data that gives them bonus Access against particular networks.

A hacker’s base Access is equal to their Intelligence modifier plus their Program skill plus their cyberdeck’s Bonus Access rating.
To refresh this pool, they need to spend an hour reprogramming their deck and slotting in new exploits. Such a refresh can only happen once a day as it takes time for new security holes to open in newly-updated software.

Hackers can acquire bonus Access to a network through social engineering: inside information, moles, and backdoors they’ve been informed about through Contacts or field work.
This information is perishable, and usually becomes useless within a few weeks.

### 4.1.4 Running Programs

Once a hacker has jacked into a node, they likely want to do some mischief.
This is accomplished by using programs made up of two different kinds of code fragments stored in their deck’s Memory.

**Verbs** are what the hacker wants to do to the target: Glitch, Sabotage, Blind, Decrypt, Delude, or some other malfeasance.
A full list of the most common Verbs is provided on page 98.

**Subjects** are what the hacker wants to do it to: Camera, Turret, Door, Barrier, Avatar, or something else.
A hacker needs the right code libraries on hand to work their magic, and if they don’t have the data specs for a target in their deck’s Memory they can’t hit it.

Each Verb or Subject takes up one unit of Memory in a cyberdeck.
The hacker can mix and match these fragments as they wish to create programs. A deck with the Glitch, Blind, Datafile, Camera, and Drone fragments on it could Glitch Drone or Blind Camera or any combination of Verb and Subject the hacker wishes, determined at the time they run the program.

Verbs are flexible, but each has a limit on what type of Subjects they can use.
The Glitch Verb is only usable on Device or Cyber types of Subjects. Drone is a Device Subject, so it’s valid, but Datafile is a Data Subject, so Glitch can’t operate on it.

Most Verbs require that the hacker beat the target network’s security difficulty with an Int/Program skill check.
This difficulty varies with the quality of the network tech, usually ranging from 7 to 12, or even more.

Each failed execution against network security increases the network’s alertness.
Each two failed attempts counts as one Alert the Network action, so no more than four botches will light up the facility. Opposed skill checks, such as in combat, don’t trigger this effect. Even networks without a Demon active can thus end up alerted by a sufficiently inept or unfortunate hacker.

### 4.1.5 Cyberspace Combat and Demons

While only well-funded networks will have a human “watchdog” jacked into the system to monitor it, all but the poorest network will have at least one Demon program positioned to deter intruders and raise alarms.

Demons are programs that usually remain stationary in a single node.
When an intruder arrives, the Demon will carry out its programming. This usually entails raising a network alert to on-site security and launching Stun Avatar programs at the hacker. A Demon’s particular program loadout and directives will depend on what the network admins granted it.

Cyberspace combat is usually played out via Stun Avatar programs that inflict non-lethal hit point damage to hackers or Demons hit by them.
These hit points are subtracted from the actual physical hit points of the hacker; if reduced to zero HP by Stun Avatar, a human goes unconscious, waking up ten minutes later with one hit point. Demons reduced to zero HP are fragged, and won’t reboot for another hour.

The specific procedures for cyberspace combat are detailed in a section below.

### 4.1.6 Noticing Hacking

A hacker generally needs to be plugged into their cyberdeck in order to hack, whether with a cranial jack or a VR crown.
This is going to be enough to catch the attention of people around them, who may not know what the hacker is doing, but they’ll know that the hacker is prepared to do something. Decks with 0 Enc might be small enough to hide discreetly, if the user is wearing a head covering.

Hacking attempts on cyberware are always obvious to the target unless specified otherwise; the user’s cyberware will naturally ping the user that something is happening, even if the effect may not be obvious.
Frisking someone for cyberware via hacking is generally considered a hostile act. A target might put up with it if they’re being checked for weapons as part of a security process, but running a Frisk on the street is likely to convince the target that they’re about to be attacked.

Hacking attempts on other hardware are obvious from within the device’s node, and any hacker, watchdog, or Demon in it is capable of detecting tampering automatically.
An outside observer won’t necessarily notice anything wrong unless the device is behaving in some uncharacteristic way.

### 4.1.7 What Can Be Hacked?

Software-controlled devices can be hacked, such as drones, most electronic locks, cyberware, cameras, and other program Subjects listed in this section.

Mechanical or strictly electrical devices cannot be hacked.
In the default setting, this includes most vehicles, dumb locks, household appliances, thermostats, and anything else that can do its job without talking to a computer. Security-minded corps have no reason to make life any easier for an intruder than they must.

## 4.2.0 Hacking Mechanics

The following procedures are used for hacking various types of targets and engaging in cyberspace combat.

### 4.2.1 Hacking a Device or Server

1.
Check your deck’s CPU rating. Do you have a free point of CPU? If not, your deck doesn’t have the horsepower to run another program right now.
2.
As a Move action, take the Jack In action to connect to the device if you aren’t already connected. It’s best to use the self-adhering field manipulation cable of your cyberdeck. Wireless connections are possible within 30 meters of unobstructed line-of-sight, but apply -2 to all cyberspace skill checks while connected that way. If you’re connecting to your deck with a VR crown instead of a cranial jack, take an additional -1 penalty.
3.
As a Main action, run your program. Combine a Verb and a Subject stored in your deck’s Memory, pay any Access the Verb requires, and roll Int/Program versus the target’s Security difficulty, modified by any difficulty modifiers the Verb applies. On a success, the program has its effect. On a failure, you lose any Access you spent for no benefit, the program fails to run, and you potentially alarm the network. Every two failed skill checks against network Security count as one triggering of the Alert the Network action.
4.
Is the program still running? If your program self-terminates after completion, it gives back its CPU slot. If you need to keep it running to maintain its effects or control, it keeps the CPU slot until you terminate it as an Instant action, your connection is cut, or you get knocked unconscious.

### 4.2.2 Hacking Cyberware or Drones

1.
Check your deck’s CPU rating. Do you have a free point of CPU? If not, your deck doesn’t have the horsepower to run another program right now.
2.
Can you reach them? While in theory you can stick your deck’s field cable on a target, in practice you’re usually going to be attacking wirelessly, with a 30 meter line-of-sight range and a -2 penalty on all cyberspace skill checks. You don’t have to connect to them beforehand with the Jack In action, however; you can launch your programs directly against the target. If you’re connecting to your deck with a VR crown instead of a cranial jack, take an additional -1 penalty to all cyberspace skill checks.
3.
Do you know what cyberware you’re targeting? Cyber with a Sight obviousness rating can be recognized on sight. Otherwise, you need to either know of a cyber system’s existence beforehand, run the Frisk Cyber program, or launch your hack blindly and hope it hits a valid target.
4.
As a Main Action, run your program, combining the desired Verb and applicable Subject from your deck’s Memory and spending the Access the Verb requires. If the target has no cyber, your effort is wasted. If the Subject you’re using doesn’t match any of their cyber, your effort is wasted. If the Subject you’re using matches multiple systems, like the generic Cyber Subject, and you didn’t specify which one beforehand, the GM picks a valid target randomly and tells you what you hit. Most Verbs require a skill check to successfully affect the cyber. The base difficulty for your Int/Program skill check is equal to 7 plus one-third of the target’s or the drone operator’s HD or level, rounded up. Drones without a connected operator are difficulty 8. Remember that you’re probably attacking wirelessly for a -2 penalty on your skill check, plus any roll bonus or penalty the Verb you’re using may apply.
5.
Is the program still running? If your program self-terminates after completion, it gives back its CPU slot. If you need to keep it running to maintain its effects or control, it keeps the CPU slot until you terminate it as an Instant action, your target moves out of range, or you get knocked unconscious. Just moving behind cover is not enough to cut the connection; they need to get more than 30 meters away from you.

### 4.2.3 Cyberspace Combat

1.
Be connected. If you lose your connection, you drop out of cyberspace and can’t interact with it.
2.
Be in the same network node. All avatars or Demons involved in the fight have to be in the same network device.
3.
Both sides roll initiative. If you’re already in meatspace combat, keep your original initiative.
4.
Both sides take actions normally. Main or Move actions spent in cyberspace are consumed in meatspace as well; you don’t get two separate sets of actions. Those with the Hacker Edge, however, get a free Main Action each round to spend in cyberspace. Demons get the same Main and Move actions that any others do.
5.
Attacks are usually made with the Stun Verb and the Avatar Subject, though Kill Avatar is used by some particularly vicious defenders. These Verbs inflict normal hit point damage on the target; at zero hit points, they fall unconscious if human or are fragged for an hour if a Demon program, though fragged demons still count for a node’s maximum Demon capacity. Kill Avatar can inflict lethal damage and Traumatic Hits; the victim’s Trauma Target is not modified by armor, but is by cyber or other sources.
6.
Combat continues until one side flees, is disconnected, or otherwise ceases hostilities. Demons will follow their programming, while human watchdogs are more flexible. Both will likely put a high priority on the Alert the Network action, which must be performed twice to activate the network’s intruder alert.

### 4.2.4 Moving in Cyberspace

1.
Be connected. Your avatar initially manifests in the network node corresponding to the device you connected to.
2.
Look for node connections. Most connections to another node are obvious. Some may be hidden, however, and require a Main Action and a Wis/Program skill check to detect them at the security’s difficulty level.
3.
Is there a barrier on the connection? If so, you need to successfully run the Unlock Barrier program on it to pop it open. Once unlocked, it remains so until the Lock Barrier program is run.
4.
If there is no barrier or it has been unlocked, take a Move action to move to the connecting node. Hostile Demons or watchdogs who are present get to run a free program against you as you flee.
### 4.2.5 Common Cyberspace Security Levels

The levels below represent the usual difficulties for hacking skill checks in a given network.
Individual networks may vary based on their importance and the competence of their guardians, however.


| Common Security Levels           | Diff. |
|----------------------------------|------:|
| Private home network             |     7 |
| Small business or minor gang     |     8 |
| Minor government office          |     8 |
| Corp branch office or major gang |     9 |
| Important government facility    |     9 |
| Corp high-security network       |    10 |
| Corp or government black site    |    12 |


| Situation                                | Diff. Mod |
|------------------------------------------|----------:|
| Network has been alerted                 |        +1 |
| The device is very important             |        +1 |
| Their tech or skill is poor or makeshift |  -1 to -2 |
| Their tech or skill is unusually good    |  +1 to +2 |
| Hacker has inside security info          |  -1 to -3 |

### 4.2.6 Cyberspace Actions

Cyberspace actions take the same amount of time as meat-space actions.
Thus, if the PC takes a Main Action to run a program in cyberspace, they can’t then use that same Main Action to fire a weapon in reality, as the hacker needs to split their available action pool over both realms of being. The following actions are among the most common, but others may be allowed at the GM’s discretion.


#### Network Actions

- **Alert the Network (Main)**: An action normally only taken by Demons and human watchdog hackers, this action will alert the whole network that there’s an intruder present in this particular cyberspace node.
This action must be taken twice to trigger an alert; one Demon could use it on two different rounds or two Demons could both use it the same round. Up to an hour can elapse between the two uses of this action; if a defeated Demon still gets it off once, another enemy three nodes later can still complete the alert with their own action. These alerts usually are automatically messaged to at least some physical security staff.
- **Jack In (Move)**: Attach a cyberdeck’s field manipulation cable to a device or initiate a wireless connection to a device within 30 meters with no significant obstacles to the user’s line of sight.
The hacker’s avatar appears in the cyberspace node associated with the target device.
- **Jack Out (Move)**: Safely disconnect a wireless connection or remove a cyberdeck’s field manipulation cable.
Hostile assailants can do this with no real chance of failure if within melee range. If someone forcibly yanks a physical cable loose while a hacker is jacked in, the hacker is stunned and loses their next Main Action.
- **Move Nodes (Move)**: Assuming there’s no barrier between the hacker’s current network node and an adjacent one, they can move one hop on the network.
If they’re being engaged by an enemy Demon or human attacker, however, their foe may choose to run one program against them as a parting shot before they can complete the move.
- **Send Message (On Turn)**: Send a brief message over the network to an avatar or device in a connected node, provided no barriers obstruct the path.
Demons, watchdogs, and others with admin privileges can message any known avatar or device on the network regardless of active barriers.
- **Crash Shutdown (Main)**: This action can only be taken by someone with admin privileges, usually the senior human watchdog hacker on duty for a given network.
Once triggered, the entire network will shut down in 1d4+4 minutes, deactivating every connected device and booting every user out of the system as if forcibly disconnected. It will take 24 hours to bring the system back up and inflict 10% of the network’s total hardware costs in component damage. Corps reserve this action for dire emergencies, when it seems certain that a cyberspace intruder is soon to do something even worse than this. Impending shutdowns are obvious to everyone in the system.


#### Cyberdeck Actions

- **Copy File (Move)**: Make a copy of a datafile located in your current node and store it in your deck’s Memory.
The inevitable wrapper of anti-tamper code and log files makes almost all important datafiles take up one point of Memory, however small the actual contents.
- **Delete Local File (Move)**: Erase one datafile, Verb, or Subject on your own cyberdeck, freeing up the Memory it occupied.
Deleting files in a server node you don’t have admin rights for requires the Erase Verb, as the local network may not permit such tampering.
- **Issue Command (Main)**: Verbs such as Hijack or Delude seize control of devices or provide some lasting influence over a system.
Ordering a controlled target to do something takes a Main Action. A controlled but uncommanded target will continue doing whatever it was last told to do. If that’s impossible or impractical, it will do nothing until it gets a new command.
- **Run a Program (Main)**: Run a program from the hacker’s cyberdeck or the watchdog’s server, combining a Verb with a Target to produce a desired effect.
- **Terminate a Program (Instant)**: Shut down a program the hacker is running, freeing up CPU capacity.
Programs are automatically terminated if the hacker gets dumped from the network, their wireless signal is blocked, or they’re otherwise cut off from their intended targets.

## 4.3.0 Programs

A program is made up of two parts: a Verb and a Subject.
A hacker loads a selection of each into the memory of their cyberdeck, and as they need them, they pair any two to create the effects they need. While programs are flexible, they have some limitations.

Programs can only be run from cyberspace.
If a hacker wants to blind a camera, they’ve got to jack into it first before they can run the Blind Verb with the Camera Subject. Programs with the Cyber, Drone or Transmission Subjects are an exception to this, and can be run directly to attack enemy cyberware or interact with local radio transmissions.

Most Verbs cost a point of Access to run.
Some very subtle Verbs may function without this cost.

Most programs require a skill check against the Subject’s security difficulty.
The more sophisticated and well-hardened the target, the harder it is to hack it, with difficulties given on the table in section 4.2.5. On a failure, the action and Access is wasted. Some Verbs are harder or easier to execute than others, and may apply a modifier to this skill check roll.

Programs take up one CPU slot while running.
A cyberdeck with a CPU rating of 4, for example, can run no more than 4 programs at a time. Some Verbs auto-terminate after performing their task, while others are ongoing, and continue to have their effect as long as the program runs.

Programs terminate once the hacker jacks out or is knocked out.
Once the cable gets pulled or the hacker goes unconscious, any program that the hacker was running automatically ends. If they’re connecting wirelessly to the target, as with programs affecting cyber or radio transmissions, their programs only terminate on unconsciousness, the Terminate Program Instant action, or if the target goes out of range.

Each Verb or Subject takes up one point of deck Memory, and can be combined arbitrarily to make up a program.
Programs can be used as often as the user wishes, so long as they have the CPU and Access needed to run them; the same Blind and Camera elements can be used to blind three different cameras at the same time so long as the deck has at least a CPU of 3 and the hacker takes three separate actions to run the program at different targets. Swapping utility choices requires an hour of recompiling.

### 4.3.1 Acquiring Programs

Getting a new Verb or Subject isn’t as simple as buying it off the rack or copying it from a friend.
The programs that hackers use are profoundly illicit, programmed to sneak under corporate security barriers and ignore corporate spyware requirements. Each one is a work of digital art that relies on secret corporate database taps, spoofed security certificates, and unpatched exploit lists. If such a program was simply copied, the doubled hit on those resources would instantly flag both programs as threats and ruin them for any practical use.

As such, these programs are painstakingly designed one-by-one by expert criminal codesmiths and sold and traded among the hackers of a city.
The prices given on the chart for Verbs and Subjects reflect the usual fees charged for such things; any competent hacker will know where to buy them.

### 4.3.2 Writing Programs

Crafting a constant stream of usable Verbs and Subjects for sale is impractical for most PCs.
The work requires contacts with a host of insider moles in corporate databases, criminal data launderers, and subject-field specialists in hardware security subversion.

While few hackers have the time or resources to write illicit programs as a business, most can manage to keep a few of their most important needs satisfied through their own coding prowess.
A sufficiently talented hacker skill can maintain a few usable Verbs and Subjects, keeping them up-to-date and concealed from ubiquitous corporate DRM.

Writing a Verb or Subject takes at least Program-1 skill and one week, less one day per level of Program skill.
Two such programs can be maintained at once per level of Program skill, so a PC with Program-2 can maintain up to four total Verbs and Subjects at once. They might choose to keep Glitch, Blind, Cyber, and Camera available this way, after a few weeks of work to write the code. Code that is abandoned in favor of a new program must be rewritten from scratch if the hacker needs it once again, as entirely new security bypasses must be researched and implemented.

Characters with the Hacker Edge gain eight Verbs or Subjects at the start of the game.
These do not count against their programming limits and can be re-written if erased at the usual time requirements. They cannot be changed once selected, however.

Hackers can also write specialized or unique Subjects, often in preparation for a mission.
If a hacker can find out the exact model of camera or electronic lock their target is using, they can write a unique Subject aimed at that specific device, gaining a +2 bonus on their skill checks to overcome its security difficulty.

### 4.3.3 Program Subjects

The Subjects listed below are the ones most often relevant to an operator.
Other Subjects do exist, as do more specialized, specific utilities that aim at specific types of devices or particular models of hardware.


| Subject             |   Cost | Subject Type |
|---------------------|-------:|--------------|
| Avatar              |   $500 | Avatar       |
| Barrier             | $1,000 | Data         |
| Camera              |   $500 | Device       |
| Cyber               | $1,000 | Cyber        |
| Datafile            |   $500 | Data         |
| Door                |   $500 | Device       |
| Drone               | $1,000 | Device       |
| Machine             |   $500 | Device       |
| Program             |   $500 | Program      |
| Sensor              |   $500 | Device       |
| Transmission        | $1,000 | Data         |
| Turret              | $1,000 | Device       |
| Specialized Subject |     x2 | -            |
| Unique Subject      |     x4 | -            |


- **Avatar**: A cyberspace avatar, whether one adopted by a hacker who’s jacked into the network or a Demon program standing guard there.
- **Barrier**: A network node barrier that blocks avatars from passing through a network connection.
- **Camera**: Any device that transmits audiovisual information.
Some defensive hardware has its own onboard camera, which is susceptible to this target, while others rely on a separate device’s input.
- **Cyber**: A cybernetic system in a visible subject within 30 meters with no significant obstacles to line of sight.
Unlike most Subjects, it’s not necessary for a hacker to manifest an avatar inside the target node to affect cyber systems; they can launch their program directly at it, and the system itself need not be visible on the target’s body. Given the difficulty of affecting cyber wirelessly, many hackers prefer to use more specific Subjects for a +1 bonus, such as Nerve Cyber or Limb Cyber.
- **Datafile**: A datafile stored within a cyberspace location.
While individual files may have very little actual data in them, the autoencryption functions and anti-tamper code they’re wrapped in almost always take up a full unit of Memory per file .
- **Door**: A physical door, shutter, hatch, or other barrier with an electronic lock or opening mechanism.
- **Drone**: A remote-controlled drone.
The base difficulty is equal to 7 plus the operator’s level or HD/3, rounded up, or 8 if it’s not actively controlled. Like the Cyber Subject, it is not necessary to manifest in a drone’s node to hack it wirelessly.
- **Machine**: Security panels, factory machinery, electronic minefields, or any other hackable device not covered under an existing Subject.
Its generality applies a +1 penalty to the security difficulty.
- **Program**: A hostile program, used most often with the Terminate verb to end the effects of an enemy hacker’s programs prematurely.
- **Sensor**: Similar to the Camera element, this code block handles sensors of any kind, regardless of what they are intended to detect.
The generality of the element decreases its efficiency, however, and it applies a +1 security difficulty penalty.
- **Transmission**: Ambient radio transmissions can be affected with this element, most often for the purpose of tampering with local comms.
A hacker need not be jacked into a cyberspace node to target local radio transmissions, but the usual -2 check penalty for wireless hacking applies.
- **Turret**: A security turret, emplaced gun, or other fixed automated weapon.

#### 4.3.3.1 Specialized Subjects

The Subject elements for sale in most underworld circles are intentionally generic in nature.
They’re packed with basic intrusion profiles for dozens or hundreds of different targets of that type, with a host of generic exploits and cracks stored in their database. Even if the specific target model isn’t on their list, there are enough generic resources to give the hacker a chance.

A specialized Subject is more focused, however.
Instead of being a Cyber Subject, for example, it might be a Nerve Cyber Subject. These specialized Subjects are useless against more generic systems, but against their specific target, they grant a +1 bonus to any skill checks the program may require. Thus, the Nerve Cyber Subject would be worthless against a cyberleg, but could affect an Enhanced Reflexes I system.

A unique Subject is focused on a single system, device model, or specific make of target.
Instead of being a Nerve Cyber Subject, it might be an Enhanced Reflexes I Subject, or instead of a Flying Drone Target, it might be a Xiaowen Phoenix Subject. It’s useless against anything but that specific target, but grants a +2 bonus to any relevant skill checks.

Specialized Subjects are sometimes available on the black market.
Unique Subjects are almost never to be found for casual purchase, and either require hand-development by the hacker who might need them or a specific Contact who can get them.

### 4.3.4 Program Verbs

The Verbs listed here are simply the most common ones available on the black market.
Specialized alternatives do exist to produce non-standard effects in their targets, but these bespoke Verbs are usually custom-coded by those who need them.

Verbs are listed with the valid Subject types they function against.
A Verb cannot be run with an invalid target; you can’t Blind a data file or Delude an avatar


| Verb       |   Cost | Target Types Allowed | Access Cost | Skill Check<br>Modifier | Use                                         |
|------------|-------:|----------------------|------------:|------------------------:|---------------------------------------------|
| Activate   | $1,000 | Device/Cyber         |          1* |                      +1 | Turn on a device or piece of cyberware      |
| Analyze    |   $500 | Device/Data          |          0* |                      +1 | Identify a target device or a file’s topic  |
| Append     |   $500 | Data                 |          1* |                      +0 | Add a new entry or data into a file         |
| Blind      | $2,000 | Device/Cyber         |           1 |                      +0 | Turn off sensory input to a device or cyber |
| Deactivate | $2,000 | Device/Cyber         |          1* |                      +1 | Turn off the target for at least a round    |
| Decrypt    | $1,000 | Data                 |          1* |                      +0 | Decrypt a file or radio transmission        |
| Defend     | $1.000 | Device/Cyber         |           0 |                     N/A | Shield a friendly device or cyber system    |
| Delude     | $5,000 | Device               |           1 |                      -1 | Spoof a device with false sensor input      |
| Erase      |   $500 | Data                 |          1* |                      +1 | Erase a data file and corrupt backups       |
| Frisk      |   $500 | Cyber                |          0* |                      +1 | Get a list of a target person’s cyberware   |
| Ghost      | $2,000 | Avatar               |           1 |                      +0 | Turn “invisible” so long as you only move   |
| Glitch     | $1,000 | Device/Cyber         |           0 |                      +2 | Briefly deactivate a device or cyber        |
| Hijack     | $5,000 | Device               |           1 |                      -1 | Take remote control of a device             |
| Kill       | $5,000 | Avatar               |          1* |                      +0 | Inflict lethal feedback on a human avatar   |
| Lock       | $1,000 | Device/Data          |           1 |                      +1 | Lock an electronic lock or node barrier     |
| Paralyze   | $2,000 | Avatar               |           1 |                      -1 | Paralyze an avatar’s owner via network      |
| Replace    | $1,000 | Data                 |          1* |                      +0 | Change file data to something else          |
| Sabotage   | $2,000 | Device/Cyber         |          1* |                      -1 | Make the target damage itself               |
| Sense      | $1,000 | Device/Cyber         |           0 |                      +0 | Piggyback on a sensor feed or cyber         |
| Siege      | $2,500 | Device               |           1 |                      -2 | Cut off a device’s node from the network    |
| Silence    | $1,000 | Avatar               |           1 |                      +1 | Lock out the Send Message action            |
| Stun       | $1,000 | Avatar               |          0* |                      +1 | Inflict non-lethal damage on an avatar      |
| Terminate  |   $500 | Program              |          1* |                      +0 | Forcibly end an unwanted program            |
| Unlock     |   $500 | Device/Data          |          1* |                      +1 | Unlock an electric lock or node barrier     |

\*: These verbs are self-terminating, and return their CPU slot immediately after the program is run.

- **Activate**: Turn a device or piece of cyberware on.
You can’t control it or direct its function, but if it’s programmed to do something when activated, such as grant a bonus action in exchange for System Strain, it does it for one round or one action. The spasmodic activation of cyber outside of the wearer’s turn is unlikely to do anything useful, but it eats System Strain as usual if relevant.
- **Analyze**: Identify the specific functionality of a device or the general topics of local datafiles.
Hackers get a general idea of what kind of device a cyberspace node represents, but Analyze is needed to give specific details of where a device is in reality.
- **Append**: Add a new entry or new data into a file.
These new entries can’t overwrite or erase any existing data, however.
- **Blind**: Deactivate a device’s sensory input or make cyberware temporarily unable to get any input from anything but the wearer’s nervous system.
Cybereyes are blind, cameras sense nothing, and laser tripwires don’t register the beam. Blind characters can’t effectively attack at range, their movement rate is halved, and they suffer a -4 penalty to both AC and melee hit rolls.
- **Deactivate**: Turn a device or piece of cyberware off.
Most devices and cyber can be rebooted by the network or the user, but it’ll take a Main Action and a round to do so. If a network alert hasn’t been issued, the network may not notice a deactivated device until someone spots the problem.
- **Decrypt**: Decrypt a data file or radio transmission.
This is often automatic for civilian encoding, though some files take Int/Program checks.
- **Defend**: A special Verb that can only be targeted at a friendly device or allied cyberware user within range, and costs no Access to use.
While the program remains running, any hostile hacking attempts aimed at that device or any of the target’s cyber systems must first beat the user in an opposed Int/Program skill check before they can attempt their hack. This Verb cannot negate programs that are already running on the target.
- **Delude**: Spoof a device with false sensor input.
A camera can be made to record nonexistent scenes, a minefield can be made to see phantom intrusions, a drone can feed the operator false visuals, and a turret can be made to mistake friends for foes. This Verb can’t control a device directly, however, and must rely on feeding it false sensor data.
- **Erase**: Erase a data file.
This Verb is extremely thorough, and any on-site backups can be expected to be nuked as well, either immediately or as soon as the automatic backup protocols expose them.
- **Frisk**: A Frisk Cyber program can give a list of all the cyber implanted in a specific human target within 30 meters, assuming the hacker’s skill check beats the cyber user’s security difficulty.
- **Ghost**: Renders the targeted friendly avatar or Demon “invisible” until they take some cyberspace action other than Move Nodes.
Observers in a node get an automatic opposed Int/Program check to detect a ghosted avatar’s presence, ending the program.
- **Glitch**: Temporarily deactivate a device or cyberware system while the program remains running, for no more than one round per Program skill level plus two.
A device can be targeted by a particular hacker’s Glitch only once a day, successful or not.
- **Hijack**: Seize control of a device, operating it as you wish within the limits of its functionality for as long as the program remains running.
One command is free with the program’s successful execution; others take a Main Action to issue.
- **Kill**: Through forced autonomic feedback and out-of-bounds electrical current parameters, inflict lethal damage to an enemy avatar equal to 1d10 per Program skill level, to a minimum of 1d10 on a successful opposed Int/Program skill check.
This damage has a Trauma Die of 1d8 and a x3 Trauma Rating. The victim’s Trauma Target is not modified by armor, but it is by cyber or other modifiers. Unconscious hackers cannot resist this Verb and will be instantly killed by it. This Verb has no effect on Demons.
- **Lock**: Lock a physical device that has some sort of electronic locking mechanism or a currently-unlocked cyberspace node barrier.
- **Paralyze**: On a successful opposed Int/Program skill check against the target, render a Demon or avatar incapable of moving out of a node or using the Alert the Network or Send Message actions while the program is running.
Other cyberspace actions can be taken normally. Human hackers are also physically paralyzed while the program runs, incapable of moving in meatspace until the connection is cut or the program ends.
- **Replace**: Edit a data file, changing one piece of data in it into another of the hacker’s choice.
It cannot erase or add data outright, only alter it.
- **Sabotage**: Cause a physical device or cyberware system to damage itself through violent movement or self-destructive current surges.
Devices smaller than a car will usually be disabled until repaired. Cyberware users do not take damage, but must make two Physical saves to resist the effect; if they fail both, the targeted system is disabled until the next maintenance, and if they fail one, it’s frozen for one round. A device or cyber can be targeted by this Verb only once per scene.
- **Sense**: Piggyback on the sensory feed of a device or piece of cyber.
Anything the device senses or records, the hacker can sense. Swapping focus from cyberspace to reality is an On Turn action.
- **Siege**: If a Device is successfully Sieged, its network connections are severed for everyone but the hacker, blocking transit, data, or drone control.
Defenders can run Terminate Program against it from any adjacent node.
- **Silence**: The affected avatar or Demon is unable to use the Send Message cyberspace action while this program runs.
Alert the Network can still be done.
- **Stun**: Disrupt an avatar or Demon with sensory glitching, non-lethal current surges, and code corruption.
Make an Int/Program skill check; beat an opposed Int/Program check for a human target, or a static 8+skill bonus target for a Demon. The target takes 1d10 non-lethal damage per Program level, to a minimum of 1d10.
- **Terminate**: Prematurely end an undesired program.
Any damage or changes the program already inflicted are unaffected.
- **Unlock**: Unlock a physical device with an electronic lock or a network’s cyberspace node barrier.

## 4.4.0 Demons and Watchdogs

Corporate systems almost always have some sort of active defenses.
Without a vigilant Demon or two in the system to alert security guards, occupants of a facility might never realize that their network is under attack. In the same fashion, human watchdog hackers are often plugged into the more important networks, maintaining a 24/7 human presence within it.

### 4.4.1 Demons

Demons are autonomous programs supported by a network’s server hardware.
Demons have avatars much as human hackers do, but operate based on a list of prioritized command lines, carrying out their duties according to their programmed behavior.

Demons are smart enough to prioritize commands intelligently; they won’t try to perform an obviously-impossible task in preference to an action that actually is possible.
They are not intelligent, however, and can’t make choices unrelated to their program.

Demon programming is expressed in command lines, each line representing one action or purpose.
Demons prioritize their command list from top to bottom, skipping commands that aren’t applicable. Every demon has a limit in the number of command lines they can process. Lists more lengthy than this are too complicated for the program’s logic to handle effectively.

Demon programs can use the Verbs and Subjects loaded into the network server they are operating on.

Demons have hit points which are depleted by the Stun Avatar program or other effects that damage code integrity.
When HP reach zero, the Demon is fragged, and can do nothing until it reboots itself in an hour. It still counts for maximum-demons-per-node, however.

Demons also have a skill bonus.
This bonus is applied to any cyberspace-related skill checks they may need to make, and counts as their skill level for the damage done by Stun or Kill Avatar.

Demons are located in a network based on the owner’s preferences.
Sensitive nodes are often guarded by a Demon, as are network chokepoints. Some Demons are set to patrol the network, either in a regular pattern or as a random walk through connected nodes. Demons can ignore network barriers and do not need to spend Access to run programs, as they are assumed to have admin privileges.

A given network can support only so many Demons, based on the power of the network server.
The network server lists both the maximum number of Demons that the network can support and the maximum number of Demons that can effectively act in the same node. If more than this number of Demons are crowded into the same node, the newest arrivals can do nothing but Move Nodes.


| Demon      |     Cost | Lines | HP | Skill |
|------------|---------:|------:|---:|------:|
| Tripwire   |   $5,000 |     2 |  3 |    +1 |
| Mastiff    |  $10,000 |     4 |  5 |    +2 |
| Siren      |  $15,000 |     2 |  8 |    +3 |
| Cataphract |  $25,000 |     3 | 20 |    +3 |
| Ogre       |  $50,000 |     4 | 25 |    +2 |
| Headsman   | $100,000 |     4 | 30 |    +3 |
| Hydra      | $200,000 |     7 | 40 |    +4 |
| Nemesis    | $500,000 |     5 | 50 |    +5 |

#### 4.4.1.1 Demon Command Lines

The following command lines are examples of some of the instructions that can be given to a Demon.
A given Demon can be programmed with as many of these lines as their capabilities allow, and will follow them in order of priority from top to bottom. Demons are intelligent enough not to waste time trying to do impossible things.


##### Example Command Lines
- Perform the Alert the Network action when an intruder is sighted, if it isn’t already alerted.
- Reboot deactivated devices on the node and message any watchdogs who may be on duty.
- Run Stun Avatar against any intruders until they have been knocked unconscious or offline.
- Run Lock Barrier on an unlocked node barriers you encounter and message any watchdogs who may be on duty.
- Move through the network in a specific patrol or random wander.
- Run Kill Avatar against any unconscious intruders until they are dead.
- Pursue intruders through the network, choosing randomly if multiple intruders are encountered.
- Run Erase Program against an intruder’s Stun Verb if they have used it in your presence before.
- Run Terminate Program against any active Hijack programs currently running in your presence.
- Run Paralyze Avatar against an intruder to physically lock them down before further countermeasures.
- Message security with the Send Message action whenever you execute a line action in response to some anomaly.
- Reserve a Main Action to run Defend Device in response to any attempts to hack devices in the node you’re in.

### 4.4.2 Watchdogs

Demons are tireless, vigilant, and require no wages.
Human hackers, despite being deficient in these regards, are substantially more flexible and intelligent. Watchdog hackers are usually hired in 24/7 shifts in order to maintain round-the-clock observation of important networks that can afford their considerable expense, though some poorer facilities hire them only during working hours.

Watchdogs use the same hacking rules as PCs do, albeit they can ignore network barriers and do not need to pay Access to run their programs.
To avoid a single point of failure, the vast majority use personal cyberdecks and their own programs rather than relying on the facility’s server computing power.

As watchdogs are intelligent, they can and will maintain full communications with the facility’s physical security staff.
PCs who alert them to their presence can expect local security to come running to the physical location of any compromised device nodes.

Below are statistics for some of the common types of watchdogs encountered by PC hackers.
Minor corps or gangs might be able to hire only minimally-competent hackers, but major black sites and important corporate facilities will have the best talent money can buy, with hacking cyber and decks to match.


| Watchdog            | Program Skill | Int Mod. | Monthly Salary |
|---------------------|---------------|---------:|---------------:|
| Code Drone (1 HD)   | Level-0       |       +1 |         $2,000 |
| Veteran Tech (2 HD) | Level-1       |       +1 |         $5,000 |
| Talented Pro (4 HD) | Level-2       |       +1 |        $10,000 |
| Big Name (6 HD)     | Level-3       |       +2 |        $50,000 |
| Dire Legend (9 HD)  | Level-4       |       +2 |            N/A |

## 4.5.0 Network Servers and Architecture

The server classes listed here are some of the most common, and a corporation can be relied upon to use the smallest iron they can get away with.
There can only be one primary server on a single network; others might be connected to it, but only the primary server counts for determining node connection limits, barriers, or active demons.

Servers have node connection limits.
This is the maximum number of nodes that can be connected to the server. If you want to connect 4 cameras, 3 doors, a security panel, and the server itself, you need to have at least 9 connections available.

Hackable devices without a connection to the server can’t communicate with it, but still function normally.
An electronically-locked door with no connection can still be hacked, and PCs can still jack in to its cyberspace node, but there’s no way for the server to monitor the door or control its status.

Servers have barrier limits.
This is the maximum number of barriers that the network can support. Barriers can be bidirectional or only work one-way as the network architect desires.

Servers have Demon limits.
A given server can only support so many Demons, and only so many of those can be active in one node at a time. The table adjacent lists both maximum total Demons and, parenthetically, how many can act in one node at a time.

Servers have effectively unlimited Memory.
Even the smallest, simplest server can contain as many Verbs, Subjects, or datafiles as a user needs. Any Demons supported by the server can use its Verbs and Subjects. As a result, prudent network architects try to keep invading hackers out of the primary server node; a well placed Erase Datafile execution can erase critical Verbs from the server and defang the network’s Demons. Human watchdogs use their own cyberdecks, so are not subject to this risk.


| Server Class |    Cost | Max Nodes | Max Barriers | Max Demons |
|--------------|--------:|----------:|-------------:|------------|
| Databank     |  $2,000 |         0 |            0 | 0          |
| Alpha        | $10,000 |        10 |            1 | 2 (1)      |
| Beta         | $50,000 |        15 |            2 | 3 (2)      |
| Gamma        |   $100K |        20 |            4 | 5 (2)      |
| Delta        |   $500K |        25 |            6 | 8 (2)      |
| Epsilon      |     $1M |        30 |           10 | 12 (3)     |

# 5.0.0 Antagonists and NPCs

This section gives rules for creating NPC statistics and handling common encounters.
Each of the NPCs and creatures listed in the following sections are given a default stat block. These blocks are meant to represent a typical person of their type; individuals will doubtless vary as the situation recommends. Each of the stat lines are described below.

- **HD**: The hit dice of the NPC, along with their average hit points.
If they’re wearing armor that has Damage Soak, these bonus hit points are noted as well, such as “5 HP+2” for someone wearing armor with 2 points of Damage Soak.
- **AC**: Both ranged and melee AC are noted here, accounting for any cyber or armor they may have.
- **TT**: The NPC’s Trauma Target is given here, modified for any cyber, armor, or Edges they have.
- **Skill**: The default total skill bonus applied to any skill check the NPC would reasonably be good at.
Thus, if a burglar with a Skill of +2 had to make a Sneak check, they’d roll 2d6+2.
- **Save**: An NPC only has one saving throw, rolled for all types of saves.
By default, it’s equal to their hit dice divided by two, rounded down, and subtracted from 15.
- **Atk**: The NPC’s ranged and melee attack bonuses, taking into account any cyber or Foci they may have.
An NPC’s base attack bonus is usually equal to their hit dice plus a plausible level of combat skill. Some wild beasts may get more than one attack from a single Main Action. This multiattack is represented by a “x2” or “x3” notation.
- **Dmg**: For creatures, this is the damage their natural fangs or claws may do.
NPCs use damage of the weapon they’re currently wielding, as noted in their gear entry.
- **Shock**: The Shock damage the NPC’s usual melee attack does.
Shock that applies to “Any” AC pierces anything short of Shock immunity from shields, Foci, or some cyber systems.
- **Move**: The number of meters the NPC can move in a single Move action.
- **ML**: The Morale score of the NPC.
When a Morale check is forced by events, they must roll 2d6. If they roll over their Morale score, they break and begin to retreat, surrender, or otherwise flee.
- **Danger Value**: A rough estimate of the NPC’s lethality in personal combat, as per page 193.
- **Gear**: The armor and weaponry the NPC carries on a regular basis.
Powerful or well-connected NPCs may use modded gear as explained in section 2.8.2. Incidental equipment is not listed; corp cops will have radios, first aid kits, handcuffs, and other minor items that fit their job role. NPCs may switch out armor or weapons if they expect trouble or are preparing for a major fight.


GMs should add in the effects of any cyber the NPCs are using to their stat line, and make a brief note of its effects for reference during play.

## 5.1.0 Reaction Rolls

When the PCs encounter someone, the GM should make a reaction roll.
This roll is 2d6 modified by the Charisma modifier of the PC spokesman or face.


|  2d6 | Reaction                                 |
|-----:|------------------------------------------|
|   2- | As hostile and violent as is plausible   |
|  3-5 | Hostile and unfriendly to the PCs        |
|  6-8 | A neutral or expected reaction from them |
| 9-11 | Unusually friendly or cooperative        |
|  12+ | As friendly and helpful as is plausible  |


The reaction will be keyed to the situation.
A socialite braced at a nightclub who rolls a low reaction won’t physically attack the PCs, but they can’t expect much courtesy from her. An assassin with a good reaction might still be hired to kill the PC, but could offer to accept a counter-bid from them instead. It’s up to the GM to decide why the reaction worked out the way it did.

## 5.2.0 Morale Checks

Every NPC combatant has a Morale score ranging from 2 to 12.
The higher the score, the calmer and more determined the fighter is while under enemy fire. An NPC with a Morale score of 2 will flee at the slightest threat to their lives, while one with a Morale of 12 will fight until the last flicker of hope is gone, or to the death if their cause is important enough.

Some combat events will force a Morale check on NPCs.
To make this check, the GM rolls 2d6 and compares it to their Morale scores; if the roll is equal or higher than their score, they break. What that means will depend on the situation and their level of training, but it always means that they want to get away from the fight as rapidly as possible.

PCs do not have Morale scores and do not ever need to make Morale checks.
They fight or flee as the players decide, regardless of the situation.

### 5.2.1 Morale Check Situations

A group of NPCs usually needs to make a Morale check under the following circumstances.
Other situations might force one as well at the GM’s discretion.

* **One of their allies just died.** The first blood of an encounter can crack the resolve of a group, even if they’re otherwise stronger or more numerous.
* **More than half of their allies are dead or down.** Most groups will have rationally retreated long before this level of casualties, but even desperate or oblivious fighters might crack at this point.
* **They seem likely to lose.** Even if they haven’t suffered crippling casualties yet, if a rational fighter doesn’t expect to win a fight, they may well choose to leave it.
* **Some dramatic reverse just happened.** If a CASRA suddenly swoops down and starts blasting a gang mob or their target wheels around to rip the head off their boss, the first instinct of the gangers is going to be flight.
If some shocking loss is suffered the group will need to check Morale.

Usually these Morale checks will be rolled with an unmodified 2d6.
A GM can tweak this slightly, with a 1 or 2 point bonus or penalty for exceptionally dramatic displays or ambiguous situations, but any modifier larger than that is likely to result in pre-ordained success or failure.

## 5.3.0 Animals

::: {.npcs}

These are some example animals the PCs might encounter.

#### Vicious Guard Dog

|        |                            |        |       |
|--------|----------------------------|--------|-------|
| HD:    | 2 (10 HP)                  | Atk:   | +3m   |
| AC:    | 13r/13m                    | Dmg:   | 1d6+1 |
| TT:    | 6+                         | Shock: | None  |
| Skill: | +2                         | Move:  | 15m   |
| Save:  | 14+                        | ML:    | 9     |
| Bite   | (1d6+1 dmg, Trauma 1d6/x2) |        |       |


#### Cybered Attack Beast

|                  |                               |        |         |
|------------------|-------------------------------|--------|---------|
| HD:              | 5 (25 HP)                     | Atk:   | +8m x 2 |
| AC:              | 13r/13m                       | Dmg:   | 1d8+4   |
| TT:              | 7+                            | Shock: | 4/-     |
| Skill:           | +3                            | Move:  | 20m     |
| Save:            | 13+                           | ML:    | 10      |
| Cybernetic Claws | (1d8+4, Tr 1d8/x3, Shock 4/-) |        |         |



#### Experimental Bioweapon

|                        |                                     |        |          |
|:-----------------------|-------------------------------------|--------|----------|
| HD:                    | 8 (50 HP)                           | Atk:   | +12m x 3 |
| AC:                    | 13r/13m                             | Dmg:   | 2d6+8    |
| TT:                    | 9+                                  | Shock: | 8/-      |
| Skill:                 | +4                                  | Move:  | 20m      |
| Save:                  | 11+                                 | ML:    | 12       |
| Body Weaponry          | (2d6+8, Tr 1d10+1/x3, Shock 8/-)    |        |          |
| Veteran’s Luck (Edge): | The beast can use Veteran’s Luck    |        |          |
| Killing Blow (Edge):   | +4 dmg to each hit and +1 to Trauma |        |          |

:::

## 5.4.0 Corp Guards and Law Enforcement

::: {.npcs}

The muscle described in this section represents the usual run of guards and hired protection that the PCs might encounter.
Small businesses or other marginal concerns will usually hire minimally-capable gunmen or street muscle to watch their property, while even the smallest megacorp subsidiary will normally have cybered guards on-site, with veterans looking after black sites and other sensitive locations.

If the PCs cause chaos, they can expect to encounter city SWAT teams or corp fast-response groups dispatched to the site of their rampage.
Truly unlucky operators might face elite breaching teams charged with violently terminating the most dangerous and troublesome lawbreakers. At the top end, the only limit to megacorp elite guards is their capacity to endure massive amounts of implanted chrome.


#### Low-End Private Security and Street Muscle

|                     |                               |        |               |
|---------------------|-------------------------------|--------|---------------|
| HD:                 | 1 (5 HP+2)                    | Atk:   | +1r/+1m       |
| AC:                 | 13r/10m                       | Dmg:   | Wpn           |
| TT:                 | 6+                            | Shock: | 1/18 non-leth |
| Skill:              | +1                            | Move:  | 10m           |
| Save:               | 15+                           | ML:    | 7             |
| Reinforced Clothing |                               |        |               |
| Heavy Pistol        | (1d8, Trauma 1d6/x3, 8 mag)   |        |               |
| Club                | (1d4, Shock 1/18, non-lethal) |        |               |


#### Basic Corp Guard or City Cop

|                      |                                                                                                |        |               |
|----------------------|------------------------------------------------------------------------------------------------|--------|---------------|
| HD:                  | 2 (10 HP+5)                                                                                    | Atk:   | +3r/+3m       |
| AC:                  | 16r/13m                                                                                        | Dmg:   | Wpn           |
| TT:                  | 8+                                                                                             | Shock: | 2/18 non-leth |
| Skill:               | +1                                                                                             | Move:  | 10m           |
| Save:                | 14+                                                                                            | ML:    | 8             |
| Light Armored Suit   |                                                                                                |        |               |
| Heavy Pistol         | (1d8, Trauma 1d6/x3, 8 mag)                                                                    |        |               |
| Advanced Club        | (1d8, Shock 2/18, non-lethal)                                                                  |        |               |
| Cybereyes (Sensory): | Integral flash protection                                                                      |        |               |
| Gunlink (Sensory):   | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |               |


#### Veteran Corp Guard/City Cop or SWAT Team

|                                 |                                                                                                |        |               |
|---------------------------------|------------------------------------------------------------------------------------------------|--------|---------------|
| HD:                             | 3 (15 HP+5)                                                                                    | Atk:   | +5r/+4m       |
| AC:                             | 17r/14m                                                                                        | Dmg:   | Wpn           |
| TT:                             | 8+                                                                                             | Shock: | 2/18 non-leth |
| Skill:                          | +2                                                                                             | Move:  | 10m           |
| Save:                           | 14+                                                                                            | ML:    | 9             |
| Light Armored Suit              |                                                                                                |        |               |
| Heavy Pistol                    | (1d8+1, Trauma 1d6/x3, 8 mag)                                                                  |        |               |
| Advanced Club                   | (1d8, Shock 2/18, non-lethal)                                                                  |        |               |
| Coordination Augment I (Nerve): | +1 Dex mod to AC/hit/damage/Shock related rolls                                                |        |               |
| Cybereyes (Sensory):            | Integral flash protection                                                                      |        |               |
| Gunlink (Sensory):              | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |               |


#### Response Team Commander or SWAT Captain

|                                 |                                                                                                |        |         |
|---------------------------------|------------------------------------------------------------------------------------------------|--------|---------|
| HD:                             | 4 (20 HP+10)                                                                                   | Atk:   | +7r/+6m |
| AC:                             | 19r/15m                                                                                        | Dmg:   | Wpn     |
| TT:                             | 8+                                                                                             | Shock: | 3/15    |
| Skill:                          | +2                                                                                             | Move:  | 10m     |
| Save:                           | 13+                                                                                            | ML:    | 10      |
| Medium Armored Suit             |                                                                                                |        |         |
| Combat Rifle                    | (1d12+1, Tr. 1d8/x3, 30 mag, burst)                                                            |        |         |
| Advanced Knife                  | (1d6+1, Shock 3/15, Tr. 1d8/x3)                                                                |        |         |
| Coordination Augment I (Nerve): | +1 Dex mod to AC/hit/damage/Shock related rolls                                                |        |         |
| Cybereyes (Sensory):            | Integral flash protection                                                                      |        |         |
| Enhanced Reflexes I (Nerve):    | Once/scene as an On Turn, gain a bonus Main action.                                            |        |         |
| Gunlink (Sensory):              | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |         |


#### Breaching Team Member

|                                     |                                                          |        |         |
|-------------------------------------|----------------------------------------------------------|--------|---------|
| HD:                                 | 3 (15 HP+15)                                             | Atk:   | +5r/+6m |
| AC:                                 | 21r/19m                                                  | Dmg:   | Wpn     |
| TT:                                 | 9+                                                       | Shock: | 5/15    |
| Skill:                              | +2                                                       | Move:  | 10m     |
| Save:                               | 14+                                                      | ML:    | 9       |
| Heavy Armored Suit                  |                                                          |        |         |
| Combat Shotgun                      | (3d4, Tr. 1d10/x3, 12 mag, burst)                        |        |         |
| 3 flash grenades/3 frag             | (2d6, Trauma 1d8/x3)                                     |        |         |
| Body Blades II (Limb):              | 2d6+1 melee, Shock 5/15, Trauma 1d10/x3                  |        |         |
| Cybereyes (Sensory):                | Integral flash protection                                |        |         |
| Eye Mod/Low Light Vision (Sensory): | Sees in low light                                        |        |         |
| Enhanced Reflexes I (Nerve):        | Once/scene as an On Turn action gain a bonus Main action |        |         |
| Muscle Fiber Replacement I (Limb):  | Effective +1 Str mod for hit/damage/Shock                |        |         |
| Stick Pads (Limb):                  | Climb sheer surfaces at Move rate.                       |        |         |


#### Breaching Team Commander

|                                     |                                                                       |        |          |
|-------------------------------------|-----------------------------------------------------------------------|--------|----------|
| HD:                                 | 5 (25 HP+15)                                                          | Atk:   | +8r/+10m |
| AC:                                 | 21r/19m                                                               | Dmg:   | Wpn      |
| TT:                                 | 9+                                                                    | Shock: | 8/Any    |
| Skill:                              | +2                                                                    | Move:  | 10m      |
| Save:                               | 13+                                                                   | ML:    | 10       |
| Heavy Armored Suit                  |                                                                       |        |          |
| Combat Shotgun                      | (3d4, Tr. 1d10/x3, 12 mag, burst)                                     |        |          |
| 3 flash grenades/3 frag             | (2d6, Trauma 1d8/x3)                                                  |        |          |
| Body Blades II (Limb):              | Base 2d6+4 melee, Shock 8/Any, Trauma 1d10/x3                         |        |          |
| Cybereyes (Sensory):                | Integral flash protection                                             |        |          |
| Eye Mod/Low Light Vision (Sensory): | Sees in low light                                                     |        |          |
| Enhanced Reflexes II (Nerve):       | Once/scene as an Instant action gain a bonus Main and Move action     |        |          |
| Muscle Fiber Replacement II (Limb): | Effective +2 Str mod for hit/damage/Shock and feats of great strength |        |          |
| Stick Pads (Limb):                  | Climb sheer surfaces at Move rate.                                    |        |          |
| Veteran’s Luck (Edge):              | Can use the Veteran’s Luck Edge                                       |        |          |
| Armsmaster (Focus):                 | Armsmaster-2 Focus benefits.                                          |        |          |


#### Corp or LEO Elite Sniper

|                                  |                                                                                                |        |          |
|----------------------------------|------------------------------------------------------------------------------------------------|--------|----------|
| HD:                              | 6 (30 HP+10)                                                                                   | Atk:   | +11r/+8m |
| AC:                              | 21r/19m                                                                                        | Dmg:   | Wpn      |
| TT:                              | 8+                                                                                             | Shock: | 4/15     |
| Skill:                           | +3                                                                                             | Move:  | 10m      |
| Save:                            | 12+                                                                                            | ML:    | 10       |
| Medium Armored Suit              |                                                                                                |        |          |
| Modded Sniper Rifle              | (2d8+7, Tr. 1d10/x4, 1 mag)                                                                    |        |          |
| Modded Heavy Pistol              | (1d8+7, Tr. 1d6/x3, 8 mag)                                                                     |        |          |
| Advanced Knife                   | (1d6+2, Shock 4/15, Tr. 1d8/x3)                                                                |        |          |
| Coordination Augment II (Nerve): | +2 Dex mod to AC/hit/damage/Shock related rolls, +10m Move                                     |        |          |
| Cybereyes (Sensory):             | Integral flash protection                                                                      |        |          |
| Enhanced Reflexes II (Nerve):    | Once/scene as an Instant, gain a bonus Main and Move action.                                   |        |          |
| Eye Mod/Infrared Vision:         | Can see in infrared                                                                            |        |          | | Gunlink (Sensory):               | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |          |
| Stick Pads (Limb):               | Climb sheer surfaces at Move rate.                                                             |        |          |
| Veteran’s Luck (Edge):           | Can use the Veteran’s Luck Edge                                                                |        |          |
| Deadeye (Focus):                 | Deadeye-2 Focus benefits                                                                       |        |          |


#### Megacorp CEO’s Personal Guard

|                                         |                                                                                  |        |           |
|-----------------------------------------|----------------------------------------------------------------------------------|--------|-----------|
| HD:                                     | 10 (80 HP)                                                                       | Atk:   | +17r/+19m |
| AC:                                     | 22r/22m                                                                          | Dmg:   | Wpn       |
| TT:                                     | 8+                                                                               | Shock: | 15/Any    |
| Skill:                                  | +4                                                                               | Move:  | 20m       |
| Save:                                   | 10+                                                                              | ML:    | 12        |
| Ridiculously Expensive Suit             |                                                                                  |        |           |
| Modded Light Pistol                     | (1d6+10, Tr. 1d8+1/x2, 15 mag), 3 flash grenades/3 frag (2d6+5, Trauma 1d8+1/x3) |        |           |
| Body Blades II (Limb):                  | Base 2d6+11 melee, Shock 15/Any, Trauma 1d10+1/x3                                |        |           |
| Coordination Augment II (Nerve):        | +2 Dex mod to AC/hit/damage/Shock related rolls, +10m Move                       |        |           |
| Cyberears/eyes (Sensory):               | Integral noise and flash protect                                                 |        |           |
| Dermal Armor III (Skin):                | AC 20, ignore first Shock/round                                                  |        |           |
| Ear Mod/Positional Detection (Sensory): | Map sounds                                                                       |        |           |
| Eye Mod/Low Light Vision (Sensory):     | Sees in low light                                                                |        |           |
| Enhanced Reflexes III (Nerve):          | Twice/scene as an Instant action gain a bonus Main and Move action               |        |           |
| Iron Hand Aegis (Limb):                 | Once/scene, deflect a ranged hit                                                 |        |           |
| Muscle Fiber Replacement II (Limb):     | Effective +2 Str mod for hit/damage/Shock and feats of great strength            |        |           |
| Reaction Booster II (Nerve):            | Automatically win initiative                                                     |        |           |
| Stick Pads (Limb):                      | Climb sheer surfaces at Move rate.                                               |        |           |
| Killing Blow (Edge):                    | +1 to Trauma Dice, +5 to all damage                                              |        |           |
| Veteran’s Luck (Edge):                  | Can use the Veteran’s Luck Edge                                                  |        |           |
| Armsmaster (Focus):                     | Armsmaster-2 Focus benefits.                                                     |        |           |

:::

## 5.5.0 Gangers and Criminals

::: {.npcs}

While less well-funded than corporate muscle or city government enforcers, street gangs make up in numbers what they lack in financing.
The poorest mobs make do with homemade knives and zip guns, while the princes of the city finance entire production runs of high-end weaponry to settle their disagreements. The corps are happy to sell to them, so long as they don’t get in the way of their business.

Aside from gang-affiliated fighters, the PCs might also run into assassins or other hired killers.
The common run of these hit men are scarcely distinguishable from any other thug with a pistol, but those who manage to earn a name can be expected to have a great deal of cutting-edge chrome implanted. The greatest of them can end up more metal than flesh, all in pursuit of a more perfect kill.


#### Unaffiliated Street Thug

|                   |                                  |        |         |
|-------------------|----------------------------------|--------|---------|
| HD:               | 1 (5 HP)                         | Atk:   | +1r/+1m |
| AC:               | 10r/10m                          | Dmg:   | Wpn     |
| TT:               | 6+                               | Shock: | 1/15    |
| Skill:            | +1                               | Move:  | 10m     |
| Save:             | 15+                              | ML:    | 7       |
| Ordinary Clothing |                                  |        |         |
| Heavy Pistol      | (1d8, Trauma 1d6/x3, 8 mag)      |        |         |
| Knife             | (1d4, Shock 1/15, Trauma 1d6/x3) |        |         |


#### Gang Member

|                                           |                                                                                                |        |         |
|-------------------------------------------|------------------------------------------------------------------------------------------------|--------|---------|
| HD:                                       | 1 (5 HP+3)                                                                                     | Atk:   | +1r/+1m |
| AC:                                       | 13r/12m                                                                                        | Dmg:   | Wpn     |
| TT:                                       | 6+                                                                                             | Shock: | 1/15    |
| Skill:                                    | +1                                                                                             | Move:  | 10m     |
| Save:                                     | 15+                                                                                            | ML:    | 8       |
| Street Leathers                           |                                                                                                |        |         |
| Heavy Pistol                              | (1d8, Trauma 1d6/x3, 8 mag)                                                                    |        |         |
| Knife                                     | (1d4, Shock 1/15, Trauma 1d6/x3)                                                               |        |         |
| One of the following cybersystems:        |                                                                                                |        |         |
| Body Blades I (Limb):                     | 1d8 dmg, Shock 2/15, Trauma 1d8/x3                                                             |        |         |
| Cybereyes and Eye Mod/Infrared (Sensory): | Flash protection and the ability to see infrared                                               |        |         |
| Gunlink (Sensory):                        | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |         |
| Shock Fists (Limb):                       | +1d8 unarmed damage                                                                            |        |         |


#### Veteran Gang Gunman

|                                 |                                                                                                |        |         |
|---------------------------------|------------------------------------------------------------------------------------------------|--------|---------|
| HD:                             | 3 (15 HP+3)                                                                                    | Atk:   | +6r/+6m |
| AC:                             | 14r/13m                                                                                        | Dmg:   | Wpn     |
| TT:                             | 6+                                                                                             | Shock: | 2/15    |
| Skill:                          | +2                                                                                             | Move:  | 10m     |
| Save:                           | 14+                                                                                            | ML:    | 8       |
| Street Leathers                 |                                                                                                |        |         |
| Combat Rifle                    | (1d12+1, Tr. 1d8/x3, 30 mag, burst)                                                            |        |         |
| Knife                           | (1d4+1, Shock 2/15, Trauma 1d6/x3)                                                             |        |         |
| Coordination Augment I (Nerve): | +1 Dex mod to AC/hit/damage/Shock related rolls                                                |        |         |
| Gunlink (Sensory):              | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |         |


#### Veteran Gang Bruiser

|                                    |                                           |        |         |
|------------------------------------|-------------------------------------------|--------|---------|
| HD:                                | 3 (15 HP+5)                               | Atk:   | +4r/+6m |
| AC:                                | 13r/14m                                   | Dmg:   | Wpn     |
| TT:                                | 6+                                        | Shock: | 3/15    |
| Skill:                             | +2                                        | Move:  | 10m     |
| Save:                              | 14+                                       | ML:    | 8       |
| War Harness                        |                                           |        |         |
| Heavy Pistol                       | (1d8, Trauma 1d6/x3, 8 mag)               |        |         |
| Body Blades I (Limb):              | 1d8+1 dmg, Shock 3/15, Trauma 1d8/x3      |        |         |
| Muscle Fiber Replacement I (Limb): | Effective +1 Str mod for hit/damage/Shock |        |         |


#### Gang Lieutenant or Minor Boss

|                                    |                                                          |        |          |
|------------------------------------|----------------------------------------------------------|--------|----------|
| HD:                                | 6 (30 HP+5)                                              | Atk:   | +9r/+10m |
| AC:                                | 16r/14m                                                  | Dmg:   | Wpn      |
| TT:                                | 7+                                                       | Shock: | 8/Any    |
| Skill:                             | +3                                                       | Move:  | 10m      |
| Save:                              | 12+                                                      | ML:    | 10       |
| Armored Clothing                   |                                                          |        |          |
| Heavy Pistol                       | (1d8, Trauma 1d6/x3, 8 mag)                              |        |          |
| Body Blades II (Limb):             | 2d6+4 melee, Shock 8/Any, Trauma 1d10/x3                 |        |          |
| Enhanced Reflexes I (Nerve):       | Once/scene as an On Turn action gain a bonus Main action |        |          |
| Muscle Fiber Replacement I (Limb): | Effective +1 Str mod for hit/damage/Shock                |        |          |
| Veteran’s Luck (Edge):             | Can use the Veteran’s Luck Edge                          |        |          |
| Armsmaster (Focus):                | Armsmaster-2 Focus benefits.                             |        |          |


#### Major Gang Boss

|                                  |                                                                                                |        |           |
|----------------------------------|------------------------------------------------------------------------------------------------|--------|-----------|
| HD:                              | 8 (40 HP+10)                                                                                   | Atk:   | +13r/+11m |
| AC:                              | 21r/19m                                                                                        | Dmg:   | Wpn       |
| TT:                              | 7+                                                                                             | Shock: | 7/15      |
| Skill:                           | +3                                                                                             | Move:  | 20m       |
| Save:                            | 11+                                                                                            | ML:    | 11        |
| Modded Armored Clothing          |                                                                                                |        |           |
| Modded Heavy Pistol              | (1d8+8, Tr. 1d6+1/x3, 8 mag)                                                                   |        |           |
| Advanced Knife                   | (1d6+3, Trauma 1d8+1/x3)                                                                       |        |           |
| Coordination Augment II (Nerve): | +2 Dex mod to AC/hit/damage/Shock related rolls, +10m Move                                     |        |           |
| Enhanced Reflexes II (Nerve):    | Once/scene as an Instant, gain a bonus Main and Move action.                                   |        |           |
| Gunlink (Sensory):               | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |           |
| Reaction Booster II (Nerve):     | Automatically win initiative                                                                   |        |           |
| Veteran’s Luck (Edge):           | Can use the Veteran’s Luck Edge                                                                |        |           |
| Deadeye (Focus):                 | Deadeye-2 Focus benefits.                                                                      |        |           |


#### Mid-Level Professional Assassin

|                                  |                                                                                                |        |          |
|----------------------------------|------------------------------------------------------------------------------------------------|--------|----------|
| HD:                              | 6 (30 HP+5)                                                                                    | Atk:   | +11r/+9m |
| AC:                              | 18r/16m                                                                                        | Dmg:   | Wpn      |
| TT:                              | 7+                                                                                             | Shock: | 4/15     |
| Skill:                           | +3                                                                                             | Move:  | 10m      |
| Save:                            | 12+                                                                                            | ML:    | 10       |
| Armored Clothing                 |                                                                                                |        |          |
| Modded Light Pistol              | (1d6+8, Tr. 1d8+1/x2, 15 mag)                                                                  |        |          |
| Advanced Knife                   | (1d6+2, Shock 4/15, Tr. 1d8/x3)                                                                |        |          |
| Coordination Augment II (Nerve): | +2 Dex mod to AC/hit/damage/Shock related rolls, +10m Move                                     |        |          |
| Cybereyes (Sensory):             | Integral flash protection                                                                      |        |          |
| Enhanced Reflexes II (Nerve):    | Once/scene as an Instant, gain a bonus Main and Move action.                                   |        |          |
| Eye Mod/Infrared Vision:         | Can see in infrared                                                                            |        |          |
| Gunlink (Sensory):               | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |          |
| Stick Pads (Limb):               | Climb sheer surfaces at Move rate.                                                             |        |          |
| Veteran’s Luck (Edge):           | Can use the Veteran’s Luck Edge                                                                |        |          |
| Deadeye (Focus):                 | Deadeye-2 Focus benefits                                                                       |        |          |


#### Legendary Hired Killer

|                                    |                                                                           |        |           |
|------------------------------------|---------------------------------------------------------------------------|--------|-----------|
| HD:                                | 10 (80 HP)                                                                | Atk:   | +17r/+15m |
| AC:                                | 21r/19m                                                                   | Dmg:   | Wpn       |
| TT:                                | 8+                                                                        | Shock: | 9/15      |
| Skill:                             | +4                                                                        | Move:  | 20m       |
| Save:                              | 10+                                                                       | ML:    | 12        |
| Modded Armored Clothing            |                                                                           |        |           |
| Modded Light Pistol                | (1d6+14, Tr. 1d8+1/x2, 15 mag)                                            |        |           |
| Advanced Knife                     | (1d6+2, Shock 9/15, Tr. 1d8/x3)                                           |        |           |
| Coordination Augment II (Nerve):   | +2 Dex mod to AC/hit/damage/Shock related rolls, +10m Move                |        |           |
| Cybereyes/ears (Sensory):          | Integral flash/noise protection                                           |        |           |
| Enhanced Reflexes III (Nerve):     | Twice/scene as an Instant action gain a bonus Main and Move action        |        |           |
| Eye Mod/Flechette Launcher (Head): | Surprise Light Pistol attack that does double dmg on hit, regular on miss |        |           |
| Eye Mod/Impostor (Sensory):        | Fake a retinal pattern                                                    |        |           |
| Eye Mod/Infrared Vision (Sensory): | Has IR vision                                                             |        |           |
| Eye Mod/Zoom (Sensory):            | Can see clearly up to 500m                                                |        |           |
| Reaction Booster II (Nerve):       | Automatically win initiative                                              |        |           |
| Stick Pads (Limb):                 | Climb sheer surfaces at Move rate.                                        |        |           |
| Hard to Kill (Edge):               | +2 HP/HD, +1 Trauma Target                                                |        |           |
| Killing Blow (Edge):               | +1 to Trauma Dice, +5 to all damage                                       |        |           |
| Veteran’s Luck (Edge):             | Can use the Veteran’s Luck Edge                                           |        |           |
| Deadeye (Focus):                   | Deadeye-2 Focus benefits                                                  |        |           |

:::

## 5.6.0 Hackers and Pilots

::: {.npcs}

Infiltration jobs sometimes require facing corporate watchdog hackers, whether in cyberspace or in the living flesh.
The statistics here are for a general range of watchdogs and their usual hardware.

Most hackers are terrible meatspace combatants, though the anti-cyber specialists of corporate breaching teams can be a nasty surprise for heavily chromed PCs.

The watchdogs are listed with their usual cyberdecks and program loadouts.
Larcenous PCs should know that corporate decks are so raddled with spy hardware that there’s almost never any profit in looting them. Freelancers might have something worth stealing, however.

Along with the selection of hackers, a number of example drone pilots are included for encounters.


#### Small-Time Corper Code Drone

|                   |                                                                                                                                       |        |         |
|-------------------|---------------------------------------------------------------------------------------------------------------------------------------|--------|---------|
| HD:               | 1 (5 HP)                                                                                                                              | Atk:   | +0r/+0m |
| AC:               | 10r/10m                                                                                                                               | Dmg:   | Wpn     |
| TT:               | 6+                                                                                                                                    | Shock: | None    |
| Skill:            | +1                                                                                                                                    | Move:  | 10m     |
| Save:             | 15+                                                                                                                                   | ML:    | 7       |
| Ordinary Clothing |                                                                                                                                       |        |         |
| Heavy Pistol      | (1d8, Trauma 1d6/x3, 8 mag)                                                                                                           |        |         |
| Cranial Jack:     | Links to jack-equipped gear                                                                                                           |        |         |
| Cyberdeck:        | Memory 10, Shielding 10, CPU 3 with verbs Defend, Paralyze, Stun, Lock, Terminate and subjects Avatar, Barrier, Camera, Door, Program |        |         |


#### Experienced Corporate Watchdog

|                   |                                                                                                                                                       |        |         |
|-------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|--------|---------|
| HD:               | 2 (10 HP)                                                                                                                                             | Atk:   | +1r/+1m |
| AC:               | 10r/10m                                                                                                                                               | Dmg:   | Wpn     |
| TT:               | 6+                                                                                                                                                    | Shock: | None    |
| Skill:            | +2                                                                                                                                                    | Move:  | 10m     |
| Save:             | 14+                                                                                                                                                   | ML:    | 8       |
| Ordinary Clothing |                                                                                                                                                       |        |         |
| Heavy Pistol      | (1d8, Trauma 1d6/x3, 8 mag)                                                                                                                           |        |         |
| Cranial Jack:     | Links to jack-equipped gear                                                                                                                           |        |         |
| Cyberdeck:        | Memory 14, Shielding 10, CPU 3 with verbs Defend, Paralyze, Sense, Silence, Stun, Lock, Terminate and subjects Avatar, Barrier, Camera, Door, Program |        |         |


#### Veteran Corporate IT Security

|                                     |                                                                                                                                                                                                                                                 |        |         |
|-------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|---------|
| HD:                                 | 4 (20 HP)                                                                                                                                                                                                                                       | Atk:   | +2r/+2m |
| AC:                                 | 10r/10m                                                                                                                                                                                                                                         | Dmg:   | Wpn     |
| TT:                                 | 6+                                                                                                                                                                                                                                              | Shock: | None    |
| Skill:                              | +2                                                                                                                                                                                                                                              | Move:  | 10m     |
| Save:                               | 13+                                                                                                                                                                                                                                             | ML:    | 8       |
| Ordinary Clothing                   |                                                                                                                                                                                                                                                 |        |         |
| Heavy Pistol                        | (1d8, Trauma 1d6/x3, 8 mag)                                                                                                                                                                                                                     |        |         |
| Cranial Jack:                       | Links to jack-equipped gear                                                                                                                                                                                                                     |        |         |
| Cybereyes (Sensory):                | Integral flash protection                                                                                                                                                                                                                       |        |         |
| Eye Mod/Low Light Vision (Sensory): | Sees in low light                                                                                                                                                                                                                               |        |         |
| Enhanced Reflexes I (Nerve):        | Once/scene as an On Turn action gain a bonus Main action                                                                                                                                                                                        |        |         |
| Stick Pads (Limb):                  | Climb sheer surfaces at Move rate.                                                                                                                                                                                                              |        |         |
| Cyberdeck:                          | Memory 15, Shielding 0, CPU 5 with verbs Blind, Deactivate, Frisk, Glitch, Sabotage, Sense and subjects Cyber, Body Cyber, Nerve Cyber, Limb Cyber, Sensory Cyber, Skin Cyber, Enhanced Reflexes I, Enhanced Reflexes II, Enhanced Reflexes III |        |         |


#### High-End Corporate IT Talent

|                              |                                                                                                                                                                                      |        |         |
|------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|---------|
| HD:                          | 6 (30 HP)                                                                                                                                                                            | Atk:   | +2r/+2m |
| AC:                          | 10r/10m                                                                                                                                                                              | Dmg:   | Wpn     |
| TT:                          | 6+                                                                                                                                                                                   | Shock: | None    |
| Skill:                       | +5                                                                                                                                                                                   | Move:  | 10m     |
| Save:                        | 12+                                                                                                                                                                                  | ML:    | 8       |
| Ordinary Clothing            |                                                                                                                                                                                      |        |         |
| Heavy Pistol                 | (1d8, Trauma 1d6/x3, 8 mag)                                                                                                                                                          |        |         |
| Cranial Jack:                | Links to jack-equipped gear                                                                                                                                                          |        |         |
| Neural Buffer:               | Adds +18 HP vs. cyberspace damage                                                                                                                                                    |        |         |
| Reaction Booster II (Nerve): | Automatically win initiative                                                                                                                                                         |        |         |
| Cyberdeck:                   | Memory 17, Shielding 10, CPU 4 with verbs Defend, Glitch, Kill, Paralyze, Sense, Sabotage, Silence, Stun, Lock, Terminate and subjects Avatar, Barrier, Cyber, Camera, Door, Program |        |         |
| Hacker (Edge):               | Can use the Hacker Edge                                                                                                                                                              |        |         |


#### Dire Legend of the Net

|                                |                                                                                                                                                                                                   |        |           |
|--------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----------|
| HD:                            | 9 (63 HP)                                                                                                                                                                                         | Atk:   | +12r/+12m |
| AC:                            | 20r/18m                                                                                                                                                                                           | Dmg:   | Wpn       |
| TT:                            | 8+                                                                                                                                                                                                | Shock: | 2/15      |
| Skill:                         | +6                                                                                                                                                                                                | Move:  | 10m       |
| Save:                          | 11+                                                                                                                                                                                               | ML:    | 10        |
| Modded Armored Clothing        |                                                                                                                                                                                                   |        |           |
| Modded Light Pistol            | (1d6+4, Tr. 1d8/x2, 15 mag)                                                                                                                                                                       |        |           |
| Advanced Knife                 | (1d6+4, Shock 2/15, Tr.
1d8/x3)                                                                                                                                                                   |        |           |
| Cranial Jack:                  | Links to jack-equipped gear                                                                                                                                                                       |        |           |
| Enhanced Reflexes III (Nerve): | Twice/scene as an Instant action gain a bonus Main and Move action                                                                                                                                |        |           |
| Neural Buffer:                 | Adds +27 HP vs. cyberspace damage                                                                                                                                                                 |        |           |
| Reaction Booster II (Nerve):   | Automatically win initiative                                                                                                                                                                      |        |           |
| Cyberdeck:                     | Memory 17, Shielding 20, CPU 7 with verbs Defend, Glitch, Kill, Paralyze, Sense, Sabotage, Silence, Stun, Lock, Terminate and subjects Avatar, Barrier, Cyber, Camera, Door, Nerve Cyber, Program |        |           |
| Hacker (Edge):                 | Can use the Hacker Edge                                                                                                                                                                           |        |           |
| Hard to Kill (Edge):           | +2 HP/HD, +1 Trauma Target                                                                                                                                                                        |        |           |


#### Gang Drone Pilot

|                                 |                                                                                                |        |         |
|---------------------------------|------------------------------------------------------------------------------------------------|--------|---------|
| HD:                             | 3 (15 HP+3)                                                                                    | Atk:   | +5r/+5m |
| AC:                             | 14r/13m                                                                                        | Dmg:   | Wpn     |
| TT:                             | 6+                                                                                             | Shock: | 2/15    |
| Skill:                          | +2                                                                                             | Move:  | 10m     |
| Save:                           | 14+                                                                                            | ML:    | 8       |
| Street Leathers                 |                                                                                                |        |         |
| Combat Rifle                    | (1d12+1, Tr. 1d8/x3, 30 mag, burst)                                                            |        |         |
| Knife                           | (1d4+1, Shock 2/15, Trauma 1d6/x3)                                                             |        |         |
| Coordination Augment I (Nerve): | +1 Dex mod to AC/hit/damage/Shock related rolls                                                |        |         |
| Gunlink (Sensory):              | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |         |
| Headcomm (Sensory):             | Built-in radio comms                                                                           |        |         |
| Remote Control Unit (Nerve):    | Controls drones/vehicles                                                                       |        |         |


#### Veteran Corporate Drone Jockey

|                                 |                                                                                                |        |         |
|---------------------------------|------------------------------------------------------------------------------------------------|--------|---------|
| HD:                             | 4 (20 HP+10)                                                                                   | Atk:   | +7r/+6m |
| AC:                             | 19r/15m                                                                                        | Dmg:   | Wpn     |
| TT:                             | 8+                                                                                             | Shock: | 3/15    |
| Skill:                          | +3                                                                                             | Move:  | 10m     |
| Save:                           | 13+                                                                                            | ML:    | 10      |
| Medium Armored Suit             |                                                                                                |        |         |
| Combat Rifle                    | (1d12+1, Tr. 1d8/x3, 30 mag, burst)                                                            |        |         |
| Advanced Knife                  | (1d6+1, Shock 3/15, Tr. 1d8/x3)                                                                |        |         |
| Coordination Augment I (Nerve): | +1 Dex mod to AC/hit/damage/Shock related rolls                                                |        |         |
| Cybereyes (Sensory):            | Integral flash protection                                                                      |        |         |
| Enhanced Reflexes I (Nerve):    | Once/scene as an On Turn, gain a bonus Main action.                                            |        |         |
| Gunlink (Sensory):              | Ignore range penalties and up to -2 of cover or concealment. Once/scene, reroll a missed shot. |        |         |
| Remote Control Unit (Nerve):    | Controls drones/vehicles                                                                       |        |         |

:::

# 6.0.0 Magic

Some campaigns include magic along with their chrome.
While the content below is included in the for-pay version of Cities Without Number, it is included in this document because it is largely mechanical in nature.


## 6.1.0 Mages and Spellcasting

Some campaigns involve spell-flinging PCs.
This section covers rules for such characters.

All would-be Mages must take the Spellcaster Edge at first level.
The education needed to become a mage is usually too intense to pick up later in an operator’s career, and in many settings a certain natural talent is also required to become a sorcerer.

Taking the Spellcaster Edge grants Cast as a bonus skill and allows the PC to pick four spells from the following list as starting incantations.

The Cast skill reflects a caster’s talent at spellcasting and their intellectual mastery of the complex formulas, rituals, and incantations necessary to use their spells.
Anyone can learn it as a strictly intellectual exercise, but only those with the Spellcaster Edge can actually use it to cast spells.

### 6.1.1 Mage Effort

Spellcasting is powered by a limited resource called Mage Effort.
A caster’s maximum Mage Effort is equal to the higher of their Intelligence or Wisdom modifiers plus their Cast skill level, to a minimum of 1 point.

Mage Effort refreshes completely each morning, assuming the caster has gotten eight uninterrupted hours of reasonably comfortable sleep.
Casters who are starved, freezing, thirsting, sick, or otherwise physically distracted cannot refresh Mage Effort.

Mage Effort is “Committed” when used to cast spells.
There are three different kinds of Commitment for Effort.

Commitment for the day means the Effort returns only the next morning.
These spells are powerful, and make persistent demands on the caster’s energy.

Commitment for the scene means the Effort returns at the end of the scene.
This may be in fifteen minutes under ordinary circumstances, or at the end of each fight or specific activity when operating under mission time.

Commitment for the duration means that the Effort can be reclaimed whenever the caster wishes as an Instant action, but whatever power it fueled ends as soon as the Effort is returned.

Each spell indicates how long Effort must be Committed for when the mage casts it.

### 6.1.2 Mages and Cyberware

For each cybernetic system implanted in a mage, their Mage Effort maximum decreases by the System Strain of the implant, to a minimum of one point even for trivial cosmetic mods.
Thus, a mage who implanted hardware that had System Strain costs of 2, 0, and 0.5 would lose four points of maximum Mage Effort. This Effort can be restored by removing the cyberware.

### 6.1.3 Spells

A character with the Spellcaster Edge can cast spells.
The ones listed below are common to many campaign settings, but others may exist in the hands of private researchers.

#### 6.1.3.1 Learning and Preparing Spells

A character with the Spellcaster Edge starts play knowing four spells from the list below.
Mages must otherwise learn their spells from a cooperative tutor or a very expensive enchanted grimoire capable of imparting the multidimensional arcane construct of the spell. Learning a spell requires one week of practice with a grimoire or tutor.

Tutors generally charge $5,000 to teach a spell, though special favors can cut that price substantially.
Grimoires can only be obtained through Contacts, and start at $10,000 for the most common spells.

Once a spell is learned, it must be prepared for casting.
A mage can prepare a number of spells equal to half their level, rounded up, plus their Cast skill. Thus, a first level PC with Cast-1 skill could prepare two different spells. A tenth level master with Cast-4 could have nine on hand at any one time. Prepared spells can be changed when Effort is refreshed in the morning. No special grimoires or spellbooks are required to change prepared spells; just having learned the spell is sufficient.

#### 6.1.3.2 Casting Spells

To cast a spell, a Mage must have at least one hand free and the ability to speak.
Spells can be cast even while wearing heavy armor or being partially restrained so long as one arm is free and the caster is not being severely jostled or disturbed.

Mages who have suffered hit point damage, grappling, or other severe distractions in a round cannot cast spells that round.
If the mage is struck mid-casting by someone who held their action to do so, the spell is lost and the Effort required is wasted.

Mages can cast only the spells they have prepared, but may do so as often as the spell or their Effort allows them to.

The actual spellcasting is a Main Action.
The caster must speak at a conversational volume and gesture plainly with one hand. Spells that have a physical effect on the world project beams, glows, or other signs that clearly indicate the mage who cast the spell. Spells that have only mental effects, such as Stun or Stunwave, have no such obvious tracers. Most professionals in a magical cyberpunk world will know enough about spellcasting to recognize it if the caster is being observed.

Unless specified otherwise, spells only require one point of Effort to be Committed in order to trigger them.

#### 6.1.3.3 Overcasting

If forced to cast beyond their Effort capacities, a mage can perform overcasting.
A mage declares overcasting as an On Turn action. The next spell they cast that same round has no Effort cost, but after the spell is cast the mage then rolls on the overcasting table below, adding their Cast skill and Constitution modifier to the die, and subtracting 2 if the spell requires Committing Effort for the day. Spells with a Commit-for-duration Effort cost last for one scene when overcast.

If the System Strain inflicted by overcasting would put the mage above their maximum, they fall unconscious for an hour before waking up with 1 hit point.


|   D20 | Overcasting Consequence                                            |
|------:|--------------------------------------------------------------------|
|    1- | Instant and unavoidable death                                      |
|   2-4 | Mortally wounded and at zero HP                                    |
|   5-8 | Gain 4 System Strain, fall unconscious for one minute              |
|  9-15 | Gain 2 System Strain, stunned and unable to act for the next round |
| 16-19 | Gain 1 System Strain, lose your next round’s Main Action           |
|   20+ | Gain 1 System Strain                                               |

#### 6.1.3.4 Spell Targets and Per Level Effects

Some spells specify a “visible target”.
Unless noted otherwise, this constitutes a point or creature within 200 meters that the caster can clearly identify. A target hiding behind a door would not be a suitable target, but one ducked behind a drape or other light concealment could be targeted if their location was known.

Cybernetic or gear-augmented sight does not help when determining visibility; only natural, normal human senses can channel arcane energies.

Where effects are “per level”, they always refer to the caster’s character level or hit dice, with a maximum of 10 levels or hit dice of effect.

Unless specified otherwise, spells do not roll a Trauma Die and cannot inflict Traumatic Hits, even if they do lethal damage.

#### 6.1.3.5 Spell List

| Spell                 | Effort    | Duration      |
|:----------------------|:----------|:--------------|
| Arson                 | Day       | Instant       |
| Blast                 | Day       | Instant       |
| Blastwave             | Day       | Instant       |
| Bless                 | Scene     | Scene         |
| Cleanse Toxin         | Day       | Instant       |
| Cloak                 | Day       | Scene         |
| Combat Precognition   | Day       | Scene         |
| Compulsion            | Scene     | Scene         |
| Curse                 | Scene     | 1 round/level |
| Detect Target         | Scene     | Scene         |
| Dispel                | Day       | Instant       |
| Disrupt               | Day       | Instant       |
| Emotion               | Scene/Day | Scene         |
| Far Senses            | Day       | Concentration |
| Glide                 | Scene     | Instant       |
| Haste                 | Day       | Scene         |
| Heal Injury           | Day       | Instant       |
| Illumination          | None      | Scene         |
| Mirage                | Scene     | Scene         |
| Muffle                | Scene     | Scene         |
| Paralysis             | Day       | Scene         |
| Psychic Interrogation | Day       | Instant       |
| Slow                  | Day       | 1 round/level |
| Soul Link             | Scene     | Scene         |
| Stun                  | Scene/Day | Instant       |
| Stunwave              | Day       | Instant       |
| Triage                | Scene/Day | Instant       |


- **Arson**: Target a visible creature or flammable object.
If a creature, roll a Shoot attack to hit it, adding your Cast skill as a bonus to the hit roll. If the spell hits, the target is wreathed in flames, suffering 1d8 fire damage per level. Flammable objects continue to burn normally. A target can be hit by Arson only once per scene.
- **Blast**: A single visible target, whether a creature or object, takes 1d6 damage per level in concussive impact.
Creatures can make a Physical save for half damage. Each use of this spell after the first in a scene adds one System Strain to the caster.
- **Blastwave**: Select a visible target, whether creature or object; it and everything within five meters of it takes 1d6 damage per level in concussive impact.
Creatures can make a Physical save to halve the damage. Each time you cast this spell, gain one System Strain, and it cannot be used more than once per scene unless you overcast it.
- **Bless**: Target a visible creature.
They gain a +1 bonus to all hit rolls and damage, including Shock, for one round per caster level. Only one Bless effect can be on a target at once.
- **Cleanse Toxin**: You touch an ally who has been poisoned; the effects of the poison immediately end, though the damage it’s already done is not cured.
- **Cloak**: Target up to one visible creature per two caster levels, rounded up.
The creature becomes nearly transparent for the rest of the scene, gaining a +2 bonus on all Sneak checks and giving most creatures no chance to spot them at all unless they are specifically vigilant for intruders or the cloaked target gets within five meters of them. The effect ends instantly if the cloaked creature moves quickly, such as by running, fighting, or casting.
- **Combat Precognition**: A single visible target is given a brief glimpse of the future while in combat, the effect lasting the rest of the scene.
This precognition is enough to allow them to reroll a missed hit roll or low damage roll as an Instant action, taking the better of the two rolls. A roll may be rerolled only once, and after each use of the power the target gains System Strain equal to the number of times they’ve used it that same day.
- **Compulsion**: Target a visible intelligent creature and give them a one-sentence telepathic command that isn’t extremely contrary to their wishes.
They must make a Mental save or spend their next round’s actions performing your command to the best of their ability, unaware of your mental influence. If used against a target that is not in combat or feeling threatened, they might continue to obey for up to a scene if not alarmed. Victims will not realize they have been mentally attacked unless they make their saving throw. Once a save is made, a creature is immune to Compulsion for the rest of the scene.
- **Curse**: Target a visible creature.
They suffer a -1 penalty to all hit rolls and damage, including Shock, for one round per caster level. They can make a Mental save at the end of each round to throw off the effect. This penalty increases to -2 at level 5, and -3 at level 9. Only one Curse can be active on a creature at once.
- **Detect Target**: When preparing this spell, choose a type of target from this list: a species of living creature, a specific person you’ve met, a general class of object, intelligent creatures that currently mean to physically harm you, or enchanted objects.
When cast, for the rest of the scene you can take a Move action to sense the selected target within five meters per level, gaining a basic sense of their presence and location. You may prepare this spell more than once for different targets.
- **Dispel**: Target a visible spell effect or creature.
Any spell effects on the creature or targeted area are immediately ended. If the original caster had an equal or higher Cast skill, the dispeller must succeed in an opposed Int/Cast spell check against them to dispel their magic. This spell only functions against spell effects, and not standing magical effects created by items or other methods.
- **Disrupt**: The next time you touch a creature or object, inflict normal unarmed damage plus 1d8 per level.
Creatures can make a Physical save to negate this bonus damage. A Punch attack is necessary to touch a target in combat, but the mage may add their Cast skill to the hit roll. Only one charge of this spell may be active at once, and it dissipates if not used by the end of the scene.
- **Emotion**: Target a visible creature, choose an emotion, and optionally pick a target or subject for that emotion.
The victim must make a Mental save or immediately feel that emotion for the rest of the scene, optionally directed toward that target. This emotion is strong enough to get them to act to the limit of their normal character and rationality. In combat, a powerful fear may be enough to force a Morale check. If this spell is cast with Effort for the day instead of the scene, the target and all creatures within five meters must save or be affected, all sharing an emotion and target.
- **Far Senses**: Pick a location within ten meters per character level.
Unless the area is shielded against magical intrusion, your unaided human senses focus on the location as if you were standing there. You maintain this clairvoyance as long as you do nothing but concentrate, up to a scene in duration.
- **Glide**: Target a number of visible creatures equal to your character level plus three.
For the rest of the scene, these creatures are immune to falling damage. When leaping from a height, they can land at any point within 5 meters for each meter of height they have, gliding at a rate of 40 meters per round.
- **Haste**: Target a visible ally.
For the next 1d4+1 rounds, once per round, as an On Turn action, they can perform an extra Main Action. This bonus action cannot involve spellcasting, summoning, or activating magical items. Using this bonus action adds 0 System Strain to the user for the first use per day, 1 for the second that same day, 2 for the third, and so forth.
- **Heal Injury**: You touch an ally, curing 1d6 damage per three caster levels, rounded up, plus your Heal skill.
This healing adds one System Strain to the target but removes any Frailty they may be suffering. It cannot repair Major Injuries, but can stabilize those downed by them.
- **Illumination**: Such a minor spell that it doesn’t even require Effort, this incantation causes a touched object to glow as you wish, illuminating up to 10 meters around it for up to an hour.
Only one Illumination can be active at a time.
- **Mirage**: Pick a visible target location.
You can create a visual and audible illusion within ten meters of that location. This illusion cannot turn objects invisible, but can conceal them behind opaque phantasms. The illusion can involve motion and activity, but once programmed, it cannot be changed, nor can it move out of the target radius. The illusion is intangible and cannot inflict damage, and those aware of its falsity can see through it with a successful Mental save taken as a Main Action. Only one Mirage can be summoned at a time.
- **Muffle**: Pick a visible target location and a radius of up to ten meters.
For the rest of the scene, no sound made within this zone will be audible outside the zone.
- **Paralysis**: Target a visible creature and roll 1d8 per level.
If the total is greater than the target’s current hit points, they must make a Mental save or become paralyzed and helpless for the rest of the scene. On a successful save, they still lose their next Main Action. The spell can target a given creature only once per scene.
- **Psychic Interrogation**: Target a visible intelligent creature that is cooperative or restrained.
Ask them one question of no more than two sentences. They may make a Mental save; on a success, they give you three telepathic sentences of their choice. On a failure, those three sentences are a truthful and candid answer to your question. The target is then immune to this spell for a week. A target’s saving throw result is not obvious to you.
- **Slow**: Target a visible creature.
For one round per level thereafter, each round they must either lose their Main Action or take 1d6 psychic damage per two caster levels, rounded up, with a Mental save for half damage. This damage can leave them unconscious, but not kill them. At the end of each of their rounds, they can make a Mental save to end the spell early. A creature can be affected by Slow only once per scene.
- **Soul Link**: You connect your mind to that of a willing visible intelligent target.
For the rest of the scene, you can communicate telepathically no matter the distance between you, and each know each other’s location and physical status at all times. Once during the spell, the two of you may agree to share a skill, both using it at the same level for one round per caster level. Only one Soul Link can be active at a time.
- **Stun**: A single visible creature takes 1d6 damage per level in psychic shock, with a Mental save to take only one point of damage per level instead.
This damage can strike a creature unconscious, but it awakens ten minutes later with one hit point. A creature targeted by this spell is thereafter immune to it and Stunwave for the rest of the scene. Using this spell more than once a scene requires committing Effort for the day instead of the scene.
- **Stunwave**: Target a visible point; every living target within five meters takes 1d6 damage per level in psychic shock, with a Mental save to take only one point of damage per level instead.
This damage can strike a creature unconscious, but it awakes ten minutes later with one hit point. A creature targeted by this spell is thereafter immune to it and Stun for the rest of the scene. This spell cannot be used more than once per scene unless you overcast it.
- **Triage**: You touch an adjacent living creature that is Mortally Wounded, stabilizing them immediately.
This process adds one System Strain to the target, though you can avoid this by Committing Effort for the day when casting it instead of for the scene.

## 6.2.0 Summoners and Summoning

Those who would beckon the spirits must take the Summoner Edge at first level, as the Edge is too demanding to be taken later on in a PC’s career.
Taking the Summoner Edge grants Summon as a bonus skill.

The Summon skill reflects the summoner’s knowledge of spiritual entities and the rigors of their training in channeling the otherworldly energies of the spirits.
Anyone can learn it as a purely intellectual study, but only those with the Summoner Edge can actually use it to beckon or banish spirits.

### 6.2.1 Summoner Effort

Much as mages have Mage Effort, summoners have Summoner Effort, and someone who has both the Spellcaster Edge and the Summoner Edge has both pools.
Summoner Effort is equal to the higher of their Constitution or Charisma modifiers plus their Summon skill level, to a minimum of 1 point.

Summoner Effort refreshes completely each morning in the same way as Mage Effort, but Effort bound up in summoned spirits cannot be reclaimed until the morning after the spirits are dispelled.
Those that are merely dismissed still act as a drain on the summoner’s energies. In all other regards, Summoner Effort works the same way as Mage Effort. As usual, the pools cannot be mixed; a spellcaster who has exhausted their Mage Effort can’t borrow from their Summoner Effort to hurl one more spell.

### 6.2.2 Summoners and Cyberware

For each cybernetic system implanted in a summoner, their Summoner Effort maximum decreases by the System Strain of the implant, to a minimum of one point even for trivial cosmetic mods.
Thus, a summoner who implanted hardware that had System Strain costs of 2, 0, and 0.5 would lose four points of maximum Summoner Effort. This Effort can be restored by removing the cyberware.

### 6.2.3 Calling and Dismissing Spirits

A ritually summoned spirit is in attendance on its summoner, but it does not always have to be immediately present.
As a Main Action, the summoner can dismiss a spirit they command within 30 meters, causing it to vanish. As another Main Action, the summoner can call it, causing the vanished spirit to reappear within 5 meters, provided there is no obstacle between them and the summoner too big for the spirit to fit through. Dismissed spirits leave behind any objects they were carrying or wearing. Time passes normally for dismissed spirits, so persistent effects on them continue to tick over. Spirits beckoned with immediate summoning cannot be called or dismissed, and remain present until they are destroyed or dispelled.

#### 6.2.3.1 Ritual Spirit Summoning

Ritually-summoned spirits require less Effort and can be called and dismissed, but take more time to summon.
To ritually summon a spirit, follow the steps below.

1.
Spend two hours in a complex ritual. No special magical components or facilities are required for this rite, but you cannot be disturbed during it or it will fail.
2.
Choose a nature for your summons. Pick a single noun describing the basic type of spirit you’re calling: Fire, Water, Streets, Trees, Night, or some other term. All the powers you choose for the spirit must be somehow related to its nature. The spirit’s skill bonus applies to all skill checks pertinent to its nature, which may be very few of them for the more esoteric concepts.
3.
Choose a physical or spirit form for your summons. Pick either a Physical Form or Spirit Form to manifest your spirit. At first level, you can only pick Physical Form I or Spirit Form I. At 5th level you can pick the second level of those forms, and at 9th, you can choose the third level. You can’t have more total hit dice of spirits summoned at once than your character level.
4.
Choose an appearance for your summons. Spirits can appear in any shape no larger than a rabbit and no smaller than a draft horse, but they appear unnatural without a Mortal Disguise.
5.
Choose powers for your summons. You can freely pick as many powers as you wish, each one adding one point of cost to the spirit. Some powers require a minimum character level to add them.
6.
Commit Effort to summon the spirit. You must Commit Summoner Effort for the day equal to the spirit’s cost minus one. This cost is also decreased by your Summon skill. No spirit can cost less than one point of Effort to summon. You cannot recover this Effort until the morning after the spirit is dispelled or destroyed.
7.
The spirit will manifest at the end of the ritual and will obediently serve you until it is destroyed or dispelled. It may be dismissed or called freely while it remains summoned. Spirits are dispelled instantly if the summoner dies.

#### 6.2.3.2 Immediate Spirit Summonings

Immediate summonings can be made with no more than a Main Action, but they cannot be called and dismissed and they require slightly more Effort to raise.

1.
Spend a Main Action to quickly call a spirit. This calling cannot be disrupted by physical harm the way spellcasting can be disrupted.
2.
Choose a nature for your summons. Pick a single noun describing the basic type of spirit you’re calling: Fire, Water, Streets, Trees, Night, or some other term. All the powers you choose for the spirit must be somehow related to its nature. The spirit’s skill bonus applies to all skill checks pertinent to its nature, which may be very few of them for the more esoteric concepts.
3.
Choose a physical or spirit form for your summons. Pick either a Physical Form or Spirit Form to manifest your spirit. At first level, you can only pick Physical Form I or Spirit Form I. At 5th level you can pick the second level of those forms, and at 9th, you can choose the third level. You can’t have more total hit dice of spirits summoned at once than your character level.
4.
Choose an appearance for your summons. Spirits can appear in any shape no larger than a rabbit and no smaller than a draft horse, but unless the Mortal Disguise power is taken they will always look uncanny and unnatural.
5.
Choose powers for your summons. You can freely pick as many powers as you wish, each one adding one point of cost to the spirit. Some powers require that you have a minimum character level to select them.
6.
Commit Effort to summon the spirit. You must Commit Summoner Effort for the day equal to the spirit’s cost. This cost is also decreased by your Summon skill. No spirit can cost less than one point of Effort to summon, and the total summoning cost increases by 1 Effort point for each successive immediate summons within the same scene.
7.
The spirit instantly appears within 5 meters of the summoner, and will serve them for up to an hour before vanishing. The spirit begins taking actions on the next round after being summoned, acting on its summoner’s initiative. Spirits summoned this way cannot be called or dismissed; they always remain present in this world until they are destroyed or dispelled. Spirits are dispelled instantly if the summoner dies.

### 6.2.4 Destroying and Banishing Spirits

Spirits are destroyed by reducing their hit points to zero, whereupon they immediately vanish.
A specific destroyed spirit with the Namebearer power can be summoned again by the summoner, but nameless spirits cannot be specifically beckoned back once destroyed. A visible spirit within thirty meters may also be dispelled by their summoner as a Main Action, sending them back from whence they came and allowing any Effort invested in their summoning to be restored the following morning.

A summoner may also banish spirits offensively, even those they did not summon.
Each banishment attempt requires a Main Action and that they Commit Summoner Effort for the day and target a visible spirit within 30 meters. They then make an opposed Cha/Summon skill check against the spirit’s summoner, or against the spirit’s skill bonus +2 if the spirit is uncontrolled. On a success, they inflict 1d8 damage per Summon skill level to the spirit.

### 6.2.5 Spirit Abilities and Limits

Every summoned spirit shares a few basic qualities.
Spirits can:

* Think with human intelligence and carry out verbal orders reasonably and intelligently.
* Exist without food, water, or air, and resist all mundane toxins, diseases, and radiation.
* Speak, read, and write any languages known to their summoner.
* Gain their skill bonus when performing skill checks related to their nature.
* Ignore Morale checks, being solely subject to their summoner’s commands within the limits of their nature.

Along with these abilities, there are some things that spirits simply can’t do.

* Use weapons or armor, as spirits can use only such things as are part of their own being.
* Operate human technology beyond pushing a single button.
They understand the uses of such devices, but simply cannot conceive of using them personally.
* Attack or directly harm humans, creatures, or other spirits without the Violent Will power.
Only spirits in physical form can take this power.
* Act directly contrary to their nature or accept a suicidal order without attempting to resist it.

Some of these limits can be at least partially circumvented with the right spirit powers.

#### 6.2.5.1 Physical and Spirit Forms

Every spirit must be summoned with a manifestation, either a physical or spiritual one.
These manifestations do not count as a power when it comes to calculating a spirit’s cost.

Physical forms must be no smaller than a rabbit but not larger than a draft horse.
They can be humanoid, but invariably appear uncanny and unnatural. They have ordinary human degrees of strength, speed, and other physical qualities when those are relevant.

Spirit forms must be of the same size, but are visible, intangible and unable to directly affect the mundane world except through their powers.
They cannot pass through living creatures large enough to see, as their spiritual energies are impermeable. They also cannot pass through walls, closed doors, barriers, or the earth, as all these things have a psychic significance of obstruction that ordinary inanimate matter does not. They can pass through liquids. They can be harmed by spells, magic weapons, fire, other immaterial energies, and the psychic force of unarmed attacks, not including cyberclaws or other inanimate add-ons.

### 6.2.6 Commanding Spirits

Spirits will intelligently obey their summoner’s verbal commands to the limit of their abilities under most cases.
In combat or other time-sensitive situations, spirits act independently on the same initiative as their summoner. They’ll carry out orders intelligently, albeit their actions are usually colored by the spirit’s own nature. Provided their master does not demand anything too contrary to their nature or their interests, a spirit can be relied upon to be an incorruptible servant.

If a spirit is ordered to do something opposed or repugnant to its nature or is commanded to do something that it thinks certain to end in its destruction, it will resist the order.
The summoner can either accept its refusal or try to force it into obedience.

Compelling a spirit requires that the summoner Commit Effort for the scene and make a successful Cha/Summon skill check at a difficulty ranging from 8, for a deeply distasteful command, to 11, for a suicidal order, to 13, for an act that is directly contrary to the spirit’s basic nature.
On a success, the spirit grudgingly obeys.

On a failure, the spirit breaks free from the summoner’s control.
The good news is that the Effort required to summon it can now be recovered the following morning. The bad news is that the spirit now is entirely outside the command of the summoner, and is likely very angry with them. Some spirits will flee, while others may attack or bedevil the summoner. These uncontrolled spirits can persist for as long as they can continue to find some source of sympathetic energy to fuel them, and over time they can grow to be quite dangerously powerful. Most uncontrolled spirits fail to find such a source and fade away in a few days, but “most” is small comfort when that renegade fire spirit gets into the aluminum foundry or the spirit of battle finds a gang war to join.

Intentionally freeing a spirit is theoretically possible, but such forced rejection tends to cause unpredictable negative consequences to the summoner's abilities.

### 6.2.7 Spirit Manifestations

The statistics below are provided for the various manifestation powers used to embody a spirit.


| Manifestation   | HD | HP |  AC | Trauma Target | Atk. |   Dmg. | Shock | Move | Skill | Save | Min Lvl. |
|-----------------|---:|---:|----:|--------------:|-----:|-------:|-------|-----:|------:|-----:|---------:|
| Spirit Form I   |  1 |  5 | 10* |             6 | None |   None | None  |  10m |    +1 |  15+ |        1 |
| Spirit Form II  |  3 | 15 | 13* |             6 | None |   None | None  |  15m |    +1 |  14+ |        5 |
| Spirit Form III |  5 | 25 | 15* |             6 | None |   None | None  |  20m |    +2 |  13+ |        9 |
| Phys. Form I    |  1 |  5 |  12 |             6 |  +2@ |   1d10 | None  |  10m |    +0 |  15+ |        1 |
| Phys. Form II   |  4 | 20 |  15 |             6 |  +5@ | 1d10+2 | 2/15  |  10m |    +1 |  13+ |        5 |
| Phys. Form III  |  8 | 40 |  18 |             6 |  +8@ | 1d10+4 | 4/15  |  10m |    +2 |  11+ |        9 |

\*: Spirit Forms can only be harmed by unarmed attacks, spells, magical weapons, fire, or other energies<br>
@: Only Physical Forms with Violent Will can fight.
Spirit Forms cannot directly harm creatures.

### 6.2.8 Spirit Powers

The powers listed here are simply the most common among summoned spirits.
A given power can only be taken once by any given spirit. Rarer abilities are said to be possessed by some entities, and some magical traditions are thought to teach the knowledge of special arts to their initiates.


| Power               | Min. Level | Effect                                                                              |
|---------------------|-----------:|:------------------------------------------------------------------------------------|
| Airy Steps          |          3 | The spirit can fly at its normal Move rate. Physical spirits can carry a load.      |
| Artifice-Wise       |          1 | The spirit can use common modern technology, but not weapons or armor.              |
| Aura of Destruction |          5 | Gain a damage bonus of half its hit dice and damage melee attackers                 |
| Curse               |          1 | Target a foe with -1 skill checks and -2 hit/damage. Increases with levels.         |
| Ensnare             |          1 | Make a target unable to move from their location until freed                        |
| Favor               |          1 | Target an ally with a bonus to checks or combat rolls for a particular end          |
| Feral Pact          |          1 | Speak with and command animals of the same type as the animal-spirit                |
| Finned Swiftness    |          1 | Swim at twice the spirit’s usual Move rate                                          |
| Flesh of Clay       |          1 | The spirit cannot be affected by Traumatic Hits.                                    |
| Ghostly Presence    |          5 | The spirit is invisible until it takes a violent action or draws attention.         |
| Hideous Talons      |          3 | Gain a bonus to the combat statistics granted by Violent Will.                      |
| Iron Eidolon        |         10 | A physical form is immune to non-magic weapons, but not unarmed hits.               |
| Merciful Hands      |          1 | Accept damage to the spirit in order to heal damage to a wounded ally.              |
| Mighty Form         |          1 | Gain tremendous physical strength, though it’s too slow to use in combat.           |
| Mislead             |          3 | Cause a moving target to move in whatever direction the spirit wishes.              |
| Misty Form          |          1 | The spirit can flow through any barrier that isn’t watertight.                      |
| Mortal Disguise     |          1 | The spirit can look like a normal human being, except under close study.            |
| Myrmidon's Shield   |          3 | When your summoner takes a Traumatic Hit, take it in their place.                   |
| Namebearer          |          1 | Gain a personal identity that persists through multiple summonings.                 |
| Natural Immunity    |          1 | Become immune to a substance or source of harm in line with its nature.             |
| Poltergeist         |          1 | Telekinetically manipulate objects at a distance with a weak strength.              |
| Skilled             |          1 | Pick a skill or role and add +2 to the spirit’s skill checks related to it.         |
| Spiderlegs          |          1 | Walk on vertical or overhanging surfaces as if they were flat ground.               |
| Spirit of War       |          1 | Enhance Violent Will’s combat abilities, but become merciless in combat.            |
| Spirit Whispers     |          3 | Make a suggestion to a listener that they do not find deeply objectionable.         |
| Spirit's Eye        |          1 | Enhance the spirit’s senses and grant it a +2 bonus to Notice checks.               |
| Swift Pursuit       |          1 | The spirit’s Move increases by 10 meters and it gets +2 to chase things.            |
| Violent Will        |          1 | The spirit is capable of fighting, but will not use mortal arms or armor.           |

- **Airy Steps**: The spirit can fly at its usual movement rate, though it cannot attack while flying.
Physical spirits can carry up to 25 kilograms, or up to 500 if they have a Mighty Form.
- **Artifice-Wise**: A spirit is capable of using daily human technology such as telephones, cars, and other common devices at an ordinary human level of skill.
They are still unable to use human weapons or armor, however, including grenades, and they cannot function as hackers, technicians, or other tech-skilled professions regardless of their other powers.
- **Aura of Destruction**: The spirit is mantled in toxic gases, bitter cold, leaping flames, crushing depression, or some other aura that causes harm to those who get too close.
If the spirit can make melee attacks, it gains a damage bonus equal to half its hit dice, rounded up. If someone makes a melee attack against it, they must make an appropriate save or suffer the spirit’s hit dice+2 in damage before the attack can be made. The save type will depend on the nature of the aura. Non-lethal auras can strike a victim unconscious but will not kill them. The spirit cannot “turn off” this aura.
- **Curse**: As a Main Action, the spirit can blight a visible creature’s luck, forcing a -1 to all skill checks and a -2 to all hit and damage rolls for the scene.
At 7th level these penalties double. A creature can bear only one curse at a time, and the spirit can’t curse more people in a single day than its hit dice.
- **Ensnare**: The spirit can trap human-sized targets as a Main Action, provided they’re within 30 meters and fail an Evasion save.
If snared, it’s two Main Actions for an ally to cut them free, or one successful Str/Exert check against difficulty 10. Ensnared enemies can still act, but cannot move from their location until the snare fades in five minutes. The spirit can’t have more targets snared at once than its hit dice. Only spirits in physical form can take this power.
- **Favor**: As a Main Action, the spirit graces a target’s luck in a particular activity related to its nature, giving them a +1 to all related skill checks for a scene.
If a combat blessing is given, the subject gets a +2 to hit and damage rolls. At 7th level, these benefits double. It’s up to the GM to decide whether or not a particular activity is related to the spirit. A creature can bear only one favor at a time and a spirit cannot favor more people in a day than its hit dice.
- **Feral Pact**: Animals of the same general type as the spirit obey its commands, provided its commands are not contrary to their nature.
This power is useless to spirits without an animal nature.
- **Finned Swiftness**: The spirit gains a swimming movement rate equal to twice its normal Move.
- **Flesh of Clay**: The spirit is not subject to Traumatic Hits, rather than having the usual Trauma Target of 6.
- **Ghostly Presence**: The spirit is invisible to sight, including IR and similar exotic senses, until it takes a violent action, makes a disturbance, or uses a power on a target that allows a saving throw.
Once this invisibility is lost, it cannot be regained for ten minutes.
- **Hideous Talons**: The melee attack of Violent Will is improved.
Its Shock now affects any AC and both damage and Shock are increased by +2. These benefits do not apply to any ranged attacks the spirit may make. Spirits summoned with this power always look dangerous or vicious, and cannot be disguised as something harmless.
- **Iron Eidolon**: A Physical Form spirit is now immune to non-magical weapons that aren’t Heavy.
The psychic force inherent in unarmed attacks can still harm it, however, even if the assailant isn’t magically-gifted.
- **Merciful Hands**: The spirit can expend its power to heal an injured victim.
As a Main Action, it can heal 1d8 HP to an adjacent ally. Each use of this power inflicts 1d8 damage to the spirit and adds one System Strain to the target. A spirit with this power will willingly use it even if it faces destruction by doing so.
- **Mighty Form**: Requiring a physical form, this power enhances the spirit’s strength.
While this strength is too slowly-deployed to be useful in combat, with a Main Action the spirit can smash down an interior wall or standard exterior door, lift and carry up to five hundred kilograms, or otherwise perform similar feats of raw strength. If used against a helpless or immobile target, the spirit must have the Violent Will power. If so, the spirit’s melee attacks against the helpless are an automatic Traumatic Hit and can cause harm even to vehicles.
- **Mislead**: Once per round, as an Instant action, the spirit can target a moving person.
If the target fails a Mental save, the spirit controls their intended movement, directing it in any direction that is not obviously dangerous out to the maximum of their normal Move action. If the victim is not aware of the spirit’s interference, they will think their course was somehow their own idea and try to rationalize why they did it.
- **Misty Form**: The spirit can flow through any barrier that isn’t watertight without impeding its movement.
Even entities in Spirit Form can use this to seep through holes in barriers their manifested shape would otherwise be unable to pass.
- **Mortal Disguise**: The spirit appears to be a normal human being on casual inspection, with the appearance and clothing the summoner wishes.
Specific humans cannot be duplicated by this power, and close, careful inspection of the spirit will reveal its uncanny nature.
- **Myrmidon’s Shield**: As an Instant action, when the spirit’s summoner takes a Traumatic Hit while the spirit is within 10 meters, the spirit can change the hit to a regular one.
The spirit, however, then takes the Traumatic Hit. This damage affects the spirit even if it is otherwise immune to the weapon or to Traumatic Hits. A spirit with this power will willingly use it even if it faces destruction by doing so.
- **Namebearer**: The spirit is named by the summoner.
Even if destroyed, this specific spirit can be summoned again with all the knowledge and experiences it had before its destruction. While this preserves its personality and experiences, this also means that you must summon it with the same physical or spiritual manifestation and can only ever add powers to it; once you imbue it with Hideous Talons, for example, every future summoning of it must also have that power. Namebearers are slightly more durable, gaining +2 hit points per hit die. They’re also more willful, adding +2 to the difficulty of imposing an unnatural command on them.
- **Natural Immunity**: The spirit is impervious to some substance appropriate to its nature: a fire spirit could be immune to fire, a storm spirit to electricity, or so forth.
This imperviousness cannot render it immune to normal weapons or attacks, and the GM rules on edge cases.
- **Poltergeist**: As a Main Action, the spirit can telekinetically manipulate unattended objects within 20 meters as if with two unseen hands.
This manipulation can lift no more than 20 kilograms and is too imprecise to deliver effective attacks or impede a resisting subject. Actions taken with this power do not disrupt Ghostly Presence, even if they cause a disturbance.
- **Skilled**: The spirit is talented at a particular non-combat skill, gaining an additional +2 bonus to all rolls related to its use.
- **Spiderlegs**: The spirit can stand or walk on sheer or overhanging surfaces as if they were flat ground.
- **Spirit of War**: A spirit with a Violent Will has the power enhanced, granting it a tremendous zeal for bloodshed.
It gains a bonus to its hit roll equal to your Summon skill and no longer resists commands to engage in suicidal battles. It is incapable of inflicting non-lethal damage with its attacks, however, and will resist commands to spare downed opponents.
- **Spirit Whispers**: As a Main Action, the spirit can make a suggestion to a listener that isn’t longer than one sentence.
If the suggestion is not deeply contrary to the target’s nature or interests, they must make a Mental save or else carry it out for at least one round. If not given a reason to desist, the target may carry out the suggestion until it is complete or an hour has passed. A target can be subject to Spirit Whispers only once per scene.
- **Spirit’s Eye**: The spirit is capable of seeing clearly in the dark, hearing whispers at twenty meters, and identifying people by the scent they leave on objects they have recently touched.
All Notice skill checks are made at +2.
- **Swift Pursuit**: The spirit’s Move rating is increased by 10m.
It gains a +2 bonus on all skill checks related to chasing down a target.
- **Violent Will**: A Physical Form spirit gains a base attack bonus equal to its hit dice +1 and does 1d10 damage on a melee hit plus one-half its hit dice, rounded down, with a melee Shock rating equal to half its hit dice, rounded down, against AC 15.
At 5th level this attack can be used at range, up to 100 meters distant. If used lethally, the attack has a Trauma Rating of 1d8/x3. The spirit acts independently on its summoner’s initiative in combat. Spirit Forms cannot take this power, and cannot directly harm creatures.

## 6.3.0 The Graced

The Graced are those whose innate magical powers manifest in the form of enhanced physical, mental, or supernatural abilities.
In order to gain these magical abilities, a PC must pick the Graced Edge during character creation. Like other magical Edges, PCs cannot become Graced later in their career.

Graced characters cannot take the Spellcaster or Summoner Edges; their magical abilities are manifesting through their physical prowess already, and the game balance value of their cyberware prohibition is minimal when their other knack has no use for it.

### 6.3.1 Graced and Arts

A Graced gets a certain number of art points with which to buy their special abilities.
At first level, they get 2 points, which they can use to buy a single two-point art such as Basic Alacrity, or two one-point arts. As they increase in level, they get additional points. They can save these points for later purchases if they wish, but it takes a week or so to master an art once the PC decides to buy it.

Once an art is purchased, it’s permanent, and cannot be traded in for another.
The exception is the Alacrity series of arts; lower-level versions of those can be upgraded later.

| Level | Art Points Gained |
|------:|------------------:|
|     1 |                 2 |
|     2 |                 1 |
|     3 |                 1 |
|     4 |                 2 |
|     5 |                 0 |
|     6 |                 1 |
|     7 |                 0 |
|     8 |                 2 |
|     9 |                 1 |
|    10 |                 1 |


#### 6.3.1.1 Graced Art List

Graced arts are listed with their art point cost in parentheses.
Once an art is selected, it cannot be changed. Some arts improve the modifiers for physical statistics; such bonuses can’t increase the total above +3.

Graced arts can be used at will, and are not hindered by injury, jostling, heavy armor, or other obstacles to spellcasting.

- **Alacrity, Basic (2)**: Your supernatural reflexes function in the same way as the Enhanced Reflexes I cyberware.
You may later upgrade any level of Alacrity to its higher levels by paying the difference in point cost.
- **Alacrity, Improved (3)**: Your reflexes have been honed to be equivalent to Enhanced Reflexes II cyberware, with the same System Strain costs.
- **Alacrity, Sublime (4)**: Your blinding haste functions as if it were Enhanced Reflexes III cyberware, with the same System Strain costs.
- **Arcane Senses (1)**: You can see clearly in complete darkness, hear whispered conversations at twenty meters, and identify people and their belongings by scent.
For any skill checks involving the acuity of your senses, you get a +1 bonus.
- **Arcane Skill (1)**: Pick a skill you have at level-0 or better that isn’t a combat skill, Magic, or Program.
Gain a +1 bonus to the skill’s checks. If it requires human-portable tools or implements to function, such as a medkit or a toolbox, your art magically substitutes for the need.
- **Blades of Will (1)**: You can manifest arcane melee weapons of your choice as an On Turn action, using the statistics of any normally-available melee weapon.
You can throw these weapons to attack, but they vanish if you merely drop them or fall unconscious. The weapons gain a bonus to hit, damage, and Shock equal to your character level divided by three, rounded down.
- **Brazen Thews (1)**: Your muscles are supernaturally reinforced, granting a +1 bonus to your Strength modifier, up to a maximum of +3.
- **Faultless Grace (1)**: Supernatural grace infuses your motions, and you gain a +1 bonus to your Dexterity modifier, up to a maximum of +3.
- **Flying Steps (1)**: When you move, you can leap horizontal or vertical distances up to your normal Move rating.
You subtract your Move rating from any falling damage distance.
- **Hand of Doom (1)**: Your hands and feet have the hardness and durability of steel, and are impervious to most forms of damage.
Your unarmed attacks now do 1d10 damage and Shock 2/15. They get a hit, damage, and Shock bonus equal to your level divided by three, rounded down. You may use unarmed attacks to inflict lethal damage with a 1d8 Trauma Die and a x3 Trauma Rating. The Unarmed Combatant Focus doesn’t stack with this.
- **Hundred Faces (2)**: As a Main Action, shift your physical appearance to be that of any other humanoid between 1.5 and 2 meters in height.
You can perfectly mimic people you’ve met personally, including retinal and fingerprint patterns, but not DNA. Your clothing alters its appearance as well, but armor subtlety can’t be changed. Changes revert when you die or change back as an Instant action.
- **Inexhaustible Vigor (1)**: Your physical stamina and hardiness is enhanced, granting a +1 bonus to your Constitution modifier, up to +3.
- **Martial Instinct (1)**: You have a number of bonus combat rolls equal to your highest combat skill, to a minimum of one.
As an On Turn action, you may choose to use one of these rolls to make an additional hit roll or damage roll, rolling multiple dice for the same attack. Only the best die is used, however, so if you spend a bonus roll to make a d20 hit roll twice or a 2d8 weapon damage twice, you use only the better of your results. As this is an On Turn action, you must choose to use this bonus before you make the roll. Your rolls refresh after a night’s good sleep.
- **Occult Pavis (2)**: You are unusually resistant to hostile magic.
You may roll all saves versus spells or magical effects twice, and take the better result. This cannot help against effects without a save.
- **Personal Void (1)**: You are linked with a personal extradimensional space that can contain up to 20 items of encumbrance or 50 kilos of bulk matter.
Storing or retrieving one or more items takes a Main Action, and you must be holding an unattached, uncontested item to store it. Retrieved items such as weapons or armor can be Readied as part of the action if you have the strength for it. Living creatures can’t be stored, and time passes normally within the space. If you die, your stored items spill out around you.
- **Refulgent Vitality (1)**: You automatically stabilize when brought to zero hit points, unless instantly killed by the damage, and you can regenerate Major Injuries with a week’s rest.
You heal twice your level in hit points with each night’s rest, instead of your level alone.
- **Skin of Steel (2)**: Your flesh is highly resilient against harm.
You gain a base armor class equal to fifteen plus half your level, rounded up, and a +1 bonus to your Trauma Target that increases to +2 at 5th level. This art does not stack with armor.
- **Sorcerous Sight (1)**: By examining an object, area, or person as a Main Action, you can see any active magic or enchantments and gain a one-sentence description of their effect.
- **Spiritbane (1)**: You add your level to all damage inflicted on spirits or magical constructs, including Shock.
Once per round, as an Instant action, gain one System Strain to reroll a missed attack against such creatures.

### 6.3.2 Graced and Cyberware

Graced arts rely on a finely-balanced flow of magical energy within the user, a flow that cyberware badly disrupts.
Each cyber system implanted in a Graced lowers their available art points by the cyber’s System Strain cost, to a minimum of a one point penalty even for minor or cosmetic cyber. This may cost the Graced the use of one or more arts until the cyber is removed. If necessary, Graced can use magical prosthetics to overcome Major Injuries much as mages and summoners can.

## 6.4.0 Magical Items

At the GM's discretion, a campaign involving magic may also involve magical items.
The specifics of such items are left for the GM's devising, but a few basic mechanics can be helpful in managing them.

### 6.4.1 Magic Item Identification

Magic items do not necessarily appear wondrous at first glance.
While all of them are exquisitely crafted, some appear to be no more than a well-forged knife or slender wand of carved wood to a mundane eye. Magically-active observers, whether Graced, mage, or summoner, can identify the presence of magic in an object by touch alone, though not its exact effects.

Discerning the nature of the magical effect within an object requires time and expertise.
If the item has a minimum magic skill level required for its use, that level of skill is necessary to identify its abilities. If not, anyone with Cast-0 or Summon-0 skill can identify the object’s powers with a day’s study.

### 6.4.2 Linking With Magic Items

Most magic items require that a user mystically link with them before they will function.

Linkage requires a Main Action and inflicts one permanent point of System Strain on the wielder that cannot be lost so long as the item remains linked to them.
This effect is particularly pronounced for mages and summoners, as the foreign magical currents disrupt their own carefully-controlled energies; such arcanists must also permanently Commit one point of Mage Effort or Summoner Effort while the item is linked, recovering the point the morning after the linkage is ended.

Linking or unlinking from an item requires a certain minimal amount of magical expertise.
Anyone with a magical Edge knows enough to do it or guide a teammate through the process, but those without such a helper who do not have Cast or Summon skills of their own cannot perform the process unaided. A purely intellectual knowledge of the process is sufficient; it is not necessary to actually be a spellcaster or summoner.

An item can be linked to only one user at a time.
Linkages end when a new user imprints on the object or the existing owner spends an hour in careful meditation, unraveling their spiritual energies from the item. They need not have the item present to unlink from it.

### 6.4.3 Using Magic Items

employment, such as magic weapons which are used as part of an attack.
No special action is required to trigger their benefits. The same applies to certain arcane devices that are designed to protect the wearer or grant an automatic special benefit.

Other items, such as magic wands, require a Main Action to trigger their effects.
This action is not disrupted by damage or physical jostling and can be performed by any user who is holding the object.

The effects of a magic item are generally obvious and connected with the item.
Onlookers will be able to tell that something supernatural has happened, and will be able to connect it to the wand or device that launched the effect. Many such devices make loud noises, display brilliant lights, or otherwise make their use extremely obvious.
