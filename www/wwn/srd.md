# An Introduction

The document that follows is intended as a reference for game designers who wish to produce role-playing games using the mechanics of the **Worlds Without Number** fantasy RPG.
It is not intended to substitute for the free PDF version of the game as a player resource or other reference, so the text included here is largely devoid of examples or other instructional text and it is not written to be convenient as a table reference.

Instead, it is meant to give other RPG designers an explicit document showing exactly what I consider to be freely-usable game mechanics or trivially implemented descriptive text.

Regarding this document, its copyright and related rights are waived via CC0, with a summary of the waiver at https://creativecommons.org/publicdomain/zero/1.0/ and the legal text itself at https://creativecommons.org/publicdomain/zero/1.0/legalcode.

While I cannot offer legal advice to the reader, I am providing this introduction as a plain English description of my position.

## The Reason For The Document

To my understanding, the only right that the following document confers that a user did not already have is the right to use its text verbatim in a commercial product.
As game mechanics cannot be copyrighted, I have always felt it fair and reasonable for other designers to duplicate the mechanics of my games for their own ends, and the following document simply specifies clearly what I consider to be those unprotected mechanics.

Along with these mechanics, trivially-derived ideas such as the damage dice of weapons, the particular layout of tables, and other details of implementation are laid out so as to make clear that I claim no legal or moral right to restrict their use.

Certain content such as specific spell or art names might arguably be said to be too unique in implementation to be freely copyable.
As I have no desire to compel other designers to rework dozens of spells and abilities simply to avoid a name, I have intentionally placed these spell names and other specific text into this document.

## A Note On Approvals

As the following document is affirmed under CC0 terms, I have absolutely no right to edit, approve, police, or restrict materials created with it.
While I might have personal opinions regarding products derived from the document, their existence may in no way be represented as official or approved Sine Nomine Publishing products, nor should my endorsement be assumed for anything created.
Designers may create what they wish, and I neither have nor desire any ability to control that.

## What You Can Do

While certain uses are implicit in a CC0 affirmation, I list some of them out here in plain English for the sake of clarity.
These uses apply to both personal and commercial uses of the following document, whether for tabletop RPGs, VTTs, online utilities, computer games, or any other work.

- You may copy verbatim any text in the following document for your own products, including tables and their contents.
- You may copy, derive, modify, or expand any content in the following document in your own products.
- You may publish products derived from the following document without crediting either me or this document, as I waive all moral rights to it to the extent permissible by law.
- You do not need to release your products under a CC0 affirmation or any other license.
- You may explicitly advertise your products as being compatible with **Worlds Without Number** or other Sine Nomine RPGs if, in your opinion, the product is compatible.
- You may not represent your products as “official” Sine Nomine offerings or as otherwise licensed, produced, associated with, or approved by Sine Nomine Publishing.
- You may not replicate text or setting material included in the **Worlds Without Number** free or deluxe game but not included in the following document. Setting details such as the default setting, the Latter Earth as described, or particular NPCs, places, or events are reserved as intellectual property and have been intentionally omitted from the following text, as have the bulk of the GM tools and other non-trivial creative content. These elements are actual creative works, and so I do not consider them free for common public use. Note that this does not apply to the basic concepts involved in the GM tools; while I keep my world tag text to myself, for example, I have no objection to someone else writing their own tags with the same enemy/friend/complication/thing/ place structure, or creating one-roll tables with their own text.

_Kevin Crawford_\
_3.13.24_

# 1.0.0 Character Creation

## 1.1.0 Attributes

A character has six attributes ranging from 3 to 18, reflecting a range from the minimum viable capacity for a playable character to the maximum normal human level.

Three of these attributes are physical.

- **Strength**, reflecting physical prowess, melee combat, carrying gear, and brute force.
- **Dexterity**, reflecting speed, evasion, manual dexterity, reaction time, and combat initiative.
- **Constitution**, reflecting hardiness, enduring injury, and tolerating large amounts of magical healing.

Three are mental attributes.

- **Intelligence**, reflecting memory, reasoning, technical skills, and general education.
- **Wisdom**, reflecting noticing things, making judgments, reading situations, and intuition.
- **Charisma**, reflecting commanding, charming, attracting attention, and being taken seriously.

NPCs do not normally have attributes. If necessary, the GM can choose them as appropriate, but usually they are assumed to have average scores if it’s ever relevant.

### 1.1.1 Generating Attributes

To generate their six attributes, the player rolls 3d6 in order, once for each attribute.
At any point before section 1.5.0 in character creation the player may substitute a score of 14 for one rolled score.

Optionally, a player may choose to assign their stats from the following array of numbers: 14, 12, 11, 10, 9, and 7, divided up as desired among the attributes.
If an array is used a score may not be substituted with a 14 later.

### 1.1.2 Attribute Modifiers

Each attribute has a modifier, usually ranging from -2 to +2.
This modifier is added to skill checks, attack rolls, damage rolls, Shock damage, and the relevant saving throw targets.

An attribute score of 3 has a modifier of -2.
A score of 4-7 has a modifier of -1.
A score of 8-13 has a modifier of +0.
A score of 14-17 has a modifier of +1.
A score of 18 has a modifier of +2.

Some Foci and abilities may add bonuses or penalties to an at tribute’s base modifier.
Such bonuses or penalties cannot increase the modifier above +2 or below -2 unless explicitly indicated.
Some injuries or character advancements may alter an attribute score; this new score may change the attribute’s modifier.

## 1.2.0 Skills

A character’s skills are the PC’s learned abilities.
A newly-created character starts with a few relevant skills and may acquire more as they advance in level.
NPCs do not have individual skills, instead relying on their combat stat line’s skill bonus when relevant.
See section 2.3.0 for the rules for making skill checks.

### 1.2.1 Skill Levels

Skills are rated on a scale between level-0 and level-4.
A character must reach a certain minimum experience level to develop a skill to level-2 or beyond.

| Skill Level |                                                                            |
|-------------|----------------------------------------------------------------------------|
| Level-0     | Basic competence in the skill, such as an ordinary practitioner would have |
| Level-1     | An experienced professional in the skill, clearly better than most         |
| Level-2     | Veteran expert, one respected even by those with considerable experience   |
| Level-3     | Master of the skill, likely one of the best in the city                    |
| Level-4     | Superlative expertise, one of the best in the world                        |

### 1.2.2 Gaining Skills in Character Creation

Characters gain skills from their Backgrounds as described in the section below and from a single free pick in section 1.7.4.
Some Foci also grant particular skills.
The first time a skill is picked or given, a character obtains it at level-0.
The second time it is picked or given, the skill becomes level-1.
The third and further times a skill is picked or given during character creation, the player instead picks any other skill that is not already level-1.
No character can begin play with skills above level-1.

### 1.2.3 The Skill List

The following skills are standard to most fantasy campaigns.
GMs may add or subtract from this list for specialized settings.
Some skills may overlap at points in their application; the character may use either skill at their discretion.

- **Administer**: Keep an organization running smoothly, scribe things well, plan out logistics, identify incompetent or treacherous workers, analyze records or archives, or otherwise do things that an executive or middle-manager would need to do.
- **Connect**: Find or know people who are useful to your purposes, make friendships or social acquaintances, know who to talk to get favors or services, and call on the help or resources of organizations you belong to.
Connect covers your PC’s ability to find the people you need, though convincing them to help may require more than this.
- **Convince**: Persuade a listener that something you are saying is true.
Naturally, the more implausible the claim or more emotionally repugnant it is to them, the more difficult it is to persuade them.
Furthermore, how they act on their newfound conviction is up to them and their motivations, and may not be perfectly predictable.
- **Craft**: Craft or repair goods and technology appropriate to the PC’s background and society.
The Craft skill can be used for a wide range of artisan pursuits, though a GM is within their rights to keep the PC from building complex things that are too far away from their past background and experience.
- **Exert**: Run, swim, climb, jump, labor for long periods, throw things, or otherwise exert your physical strength, stamina, and coordination.
Even a PC with poor physical attributes might have a good Exert skill reflecting athletic training and expertise in making the most of their available talents.
- **Heal**: Treat wounds, cure diseases, neutralize poisons, diagnose psychological health issues, and otherwise tend to the wounds of body and mind.
The Heal skill cannot cure lost hit points directly, but it’s a vital skill in stabilizing Mortally Wounded allies or ensuring clean recovery.
- **Know**: Know matters of history, geography, natural science, zoology, and other academic fields appropriate to a sage or scholar.
While some sages might specialize in particular fields, most learned men and women in this age have a broad range of understanding, and will rarely be unable to even attempt to answer a question relevant to this skill.
- **Lead**: Inspire others to follow your lead and believe in your plans and goals.
Manage subordinates and keep them focused, loyal, and motivated in the face of danger or failure.
A successful leader will keep their subordinate’s faith and confidence even when reason might make the leader’s plan appear questionable at best.
- **Magic**: Cast or analyze magic and know things about famous mages or notable magical events.
Classes that can’t cast spells obtain only intellectual and scholarly benefits from this skill.
- **Notice**: Notice small details, impending ambushes, hidden features, or concealed objects.
Detect subtle smells, sounds, or other sensory input.
Notice cannot be used simply to detect a lie, but keen attention can often discern a subject’s emotional state.
- **Perform**: Sing, act, dance, orate, or otherwise perform impressively for an audience.
Compose music, plays, writings, or other works of performance art.
Most performers will have a particular field they excel at, though polymaths might exist if the PC’s background is appropriate for such versatility.
- **Pray**: Perform the clerical rites of your religion, and be familiar with the gods, demons, and taboos of major and minor faiths, and identify iconography and persons of religious importance.
Pray also helps you know the state of local faiths and the important persons in their hierarchies.
- **Punch**: Fight unarmed or with natural body weaponry.
Punch, kick, grapple, or otherwise brawl without the benefit of man-made tools.
This mode of fighting is inefficient at best without some special Focus to improve it, but it’s reliably non-lethal.
- **Ride**: Ride an animal, drive a cart or carriage, or otherwise deal with land transportation.
This skill also includes competence at mount care and tending, basic cart or carriage repair, judging good horseflesh, and other skills appropriate to a beast-rider of whatever society the PC comes from.
- **Sail**: Sail or repair a ship, build small craft, navigate by the stars, read sea weather, manage sailors, and otherwise conduct the business of a professional mariner.
This skill may apply to more esoteric means of vehicular travel in some societies.
- **Shoot**: Fire a bow or crossbow or throw a hurled weapon.
Maintain ranged weaponry and fletch arrows.
- **Sneak**: Move silently, hide in shadows, avoid notice, pick pockets, disguise yourself, pick locks, defeat traps, or otherwise overcome security measures.
- **Stab**: Fight with melee weapons or throw a hurled weapon.
Maintain and identify weaponry.
- **Survive**: Hunt, fish, navigate by the stars, mitigate environmental hazards, identify plants and wildlife, and craft basic survival tools and shelter.
A PC’s Survive skill is most pertinent to the environments in their background, but the basic principles can be applied in all but the most alien environments.
- **Trade**: Buy and sell at a profit, identify the worth of goods or treasures, deal with merchants and traders, find black-market goods and services, and know laws regarding smuggling and contraband.
- **Work**: This skill is a catch-all for any profession that might not other wise merit its own skill, such as a painter, lawyer, farmer, or herdsman.
The precise skill it represents will vary with the PC’s background.

## 1.3.0 Backgrounds

Every character has a Background, a past reflecting their career before they took up adventuring.
A background may be chosen from the list below or a new one made up with GM permission.

### 1.3.1 Background Skills

When a Background is chosen, a PC immediately gets its free skill at level-0.
At that point, the player decides whether to gain further skills randomly or to pick specific choices.

If they choose randomly, they may make three rolls divided be tween the Growth and Learning tables of their background in any way they wish, including taking all three from just one table.
If they pick specific choices they can pick any two skills from the Learning table, including picking the same one twice to raise it to level-1.
They may not pick the “Any Skill” option, if it exists.

A skill pick of “Any Combat” means the player can pick Shoot, Stab, or Punch.
A skill roll of “Any Skill” means they can pick any skill from the list above.
Some campaigns may involve special magical or psychic skills; these cannot be chose with the Any Skill pick.

If an attribute bonus is rolled, such as “+2 Physical”, the player may apply it to any physical attribute or split the bonus between two physical attributes.
The same principle applies to Mental attribute increases. “Any Stat” increases may be applied to either physical or mental attributes.
No attribute can be raised above 18.

### 1.3.2 New Backgrounds

To create a new background, the player describes it to the GM and picks an existing background table that best fits the concept.
Assuming the GM approves it, they may then roll or pick as usual.
If no existing background table fits, they may make a new one with the GM’s permission.

### 1.3.3 Example Backgrounds

The two following backgrounds are examples of those possible for a player, with others to be created as the designer thinks appropriate for their setting.
The free granted skill is listed after each background’s name.

| Artisan |             | Craft |            |
|---------|-------------|-------|------------|
| d6      | *Growth*    | d8    | *Learning* |
| 1       | +1 Any Stat | 1     | Connect    |
| 2       | +2 Physical | 2     | Convince   |
| 3       | +2 Physical | 3     | Craft      |
| 4       | +2 Mental   | 4     | Craft      |
| 5       | Exert       | 5     | Exert      |
| 6       | Any Skill   | 6     | Know       |
|         |             | 7     | Notice     |
|         |             | 8     | Trade      |

| Barbarian |             | Survive |            |
|-----------|-------------|---------|------------|
| d6        | *Growth*    | d8      | *Learning* |
| 1         | +1 Any Stat | 1       | Any Combat |
| 2         | +2 Physical | 2       | Connect    |
| 3         | +2 Physical | 3       | Exert      |
| 4         | +2 Mental   | 4       | Lead       |
| 5         | Exert       | 5       | Notice     |
| 6         | Any Skill   | 6       | Punch      |
|           |             | 7       | Sneak      |
|           |             | 8       | Survive    |

## 1.4.0 Classes

PCs are uncommon in that each of them has a **class**.
A class represents the particular skills, abilities, or talents that make the PC a viable adventurer.
NPCs do not normally have classes, though they may have special abilities or powers of their own.

### 1.4.1 The Three Base Classes

The three base classes are Expert, Mage, and Warrior.
Each of these base classes has its own hit die, attack progression, and Focus advancement.
PCs use the tables for the class they choose.

### 1.4.2 Full and Partial Classes

There are two kinds of classes: Full classes and Partial classes.
A PC may choose to pick a single Full class, committing completely to that class’ profession, or they may choose to become an Adventurer and pick two Partial classes instead, mixing their abilities.

The Warrior, Expert, High Mage, Necromancer, Invoker, and Elementalist classes may be taken either as a Full class or as a Partial class.
In the former case, the PC gains the full range of powers granted to the profession.
If taken as a Partial class, a more restricted range of abilities are granted.

The Healer, Vowed, Bard, Thought Noble, Accursed, Wise, Mageslayer, Skinshifter, Duelist, Beastmaster, and Blood Priest can only be taken as Partial classes.
These classes are limited in scope, and must be paired with another Partial class.

### 1.4.3 The Adventurer Class

The Adventurer is a special class that allows the PC to take two Partial classes and mix them together.
These may be Partial-only classes, such as a Healer/Vowed, or they may be Partial versions of Warrior, Expert, or Mage classes, such as a Partial Expert/Thought Noble.
A dual-specialization spellcaster is also possible by mixing a Partial High Mage/Partial Elementalist together, for example.

### 1.4.4 Classes and Effort

Some classes have special abilities that are fueled with magical power, inner focus, or some other reserve of energy.
This capacity is measured in points of Effort, a resource the PC can commit to activate their abilities.

Every class that uses **Effort** has its own unique pool: High Mage Effort, Vowed Effort, Healer Effort, and so forth.
Adventurers with two Partial classes that use Effort have two separately-tracked pools, one for each class.
Points of Effort from one class cannot be used to fuel powers from another.

The maximum Effort of a Full class is equal to 1 plus a relevant skill level and ability modifier given by the class.
A Partial class uses the same formula, but with 1 fewer point of Effort, to a minimum of 1.

#### 1.4.4.1 Committing Effort

Most class powers require that Effort be Committed.
Committing Effort is done as part of activating a power, and ties up the Effort for a varying amount of time depending on the ability used.

Some powers **Commit Effort for the duration**.
This means the Effort remains Committed as long as the power is active.
The PC can reclaim the Committed Effort at any time as an Instant action, deactivating the power.

Some powers **Commit Effort for the scene**.
The Effort remains Committed until the end of the scene, after which it returns to the PC.
It cannot be reclaimed earlier, even if the ability it fuels is only good for a single attack or action.
A scene usually amounts to a single fight, event, or dungeon turn, almost never lasting more than fifteen minutes.
Further specifics are provided in the time section of the game rules.

Some powers **Commit Effort for the day**.
Once Committed, this Effort can be recovered only after a comfortable night’s rest.
If the PC is hungry, cold, sick, or sleeping without the benefits of a bedroll or other basic comforts, this Effort cannot be recovered.

Activating a power only ever requires Committing a single point of Effort, unless the power itself says otherwise.

## 1.5.0 The Class List

The classes that follow are flavor-stripped versions of those found in **Worlds Without Number** and its supplement, **The Atlas of the Latter Earth**.
While mainstays such as the Warrior, the Expert, or the High Mage are appropriate to almost any fantasy setting, others may not fit specific campaigns.
A GM always has the right to omit classes that do not fit the flavor of the game they are running.

### 1.5.1 The Adventurer

Not every hero is perfectly reflected by one of the three main classes, even with the wide latitude of concepts each one allows.
For those heroes that straddle the roles, there remains the class of Adventurer.

An Adventurer picks two classes to reflect their own particular talents.
A spell-slinging swordsman might choose to be a Partial Mage/Partial Warrior, while a stealthy assassin might be a Partial Expert/Partial Warrior, and a grifting mountebank-wizard might be a Partial Expert/Partial Mage.
The player should pick whichever pairing serves best.

The tables below provide the hit dice, attack bonus, and Foci picks gained by each of the three possible pairings.
Thus, a first level Partial Expert/Partial Warrior would roll 1d6+2 for their hit points, have a +1 attack bonus, and pick three Foci: one expert, one warrior, and one free pick.

Adventurers tend to have a wider range of abilities than a more focused PC, and the extra Focus pick can make a significant difference at low levels.
The absence of the strongest class abilities of Experts and Warriors make a difference in the longer run, however, and a Partial Mage will never attain the same magical power in their tradition as a focused specialist.

#### Partial Expert

A Partial Expert is treated just as a full Expert, including gaining the benefits of the **Quick Learner** ability.
They do not have the **Masterful Expertise** ability, however, as they lack the versatility of a full Expert.

#### Partial Mage

A Partial Mage is treated as a Mage, and gains the **Arcane Tradition** ability, allowing them to pick a magical tradition for their powers.
That tradition’s abilities will be more limited for Partial Mages, however, as described under each of the paths.

It’s even possible for a PC to pick the Partial Mage class twice for two different magical traditions, gaining portions of both arcane powers.
They then use the usual full Mage chart for hit dice, attack bonus, and Foci, and the spellcasting table for dual casters if both partial classes cast spells.

A Partial Mage must adhere to the restrictions and limits of their magical tradition in order to use its abilities, regardless of whatever other partial class they may have.

#### Partial Warrior

A Partial Warrior gains certain of the benefits of a full Warrior, including the improved hit die and a somewhat improved attack bonus.
They do not have the **Veteran’s Luck** special ability or the **Killing Blow** power, however, and must trust to their own talents to land blows and crush their enemies.

#### Partial Expert/Partial Warrior

| Level | Hit Dice | Attack Bonus | Focus Picks                        |
|------:|---------:|-------------:|------------------------------------|
|     1 |    1d6+2 |           +1 | 1 Expert<br> +1 Warrior<br> +1 Any |
|     2 |    2d6+4 |           +2 | +1 Any                             |
|     3 |    3d6+6 |           +2 |                                    |
|     4 |    4d6+8 |           +3 |                                    |
|     5 |   5d6+10 |           +4 | +1 Any                             |
|     6 |   6d6+12 |           +5 |                                    |
|     7 |   7d6+14 |           +5 | +1 Any                             |
|     8 |   8d6+16 |           +6 |                                    |
|     9 |   9d6+18 |           +6 |                                    |
|    10 |  10d6+20 |           +7 | +1 Any                             |

#### Partial Expert/Partial Mage

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |      1d6 |           +0 | 1 Expert<br> +1 Any |
|     2 |      2d6 |           +1 | +1 Any              |
|     3 |      3d6 |           +1 |                     |
|     4 |      4d6 |           +2 |                     |
|     5 |      5d6 |           +2 | +1 Any              |
|     6 |      6d6 |           +3 |                     |
|     7 |      7d6 |           +3 | +1 Any              |
|     8 |      8d6 |           +4 |                     |
|     9 |      9d6 |           +4 |                     |
|    10 |     10d6 |           +5 | +1 Any              |

#### Partial Mage/Partial Warrior

| Level | Hit Dice | Attack Bonus | Focus Picks          |
|------:|---------:|-------------:|----------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br> +1 Any |
|     2 |    2d6+4 |           +2 | +1 Any               |
|     3 |    3d6+6 |           +2 |                      |
|     4 |    4d6+8 |           +3 |                      |
|     5 |   5d6+10 |           +4 | +1 Any               |
|     6 |   6d6+12 |           +5 |                      |
|     7 |   7d6+14 |           +5 | +1 Any               |
|     8 |   8d6+16 |           +6 |                      |
|     9 |   9d6+18 |           +6 |                      |
|    10 |  10d6+20 |           +7 | +1 Any               |

### 1.5.2 The Accursed

The Accursed are those men and women who have made pacts with otherworldly beings, intentionally or otherwise, and gained powers from that bond.

#### Benefits Of The Accursed

The Accursed is a partial Mage class that must be joined with a second partial class by an Adventurer.
A Partial Warrior/Accursed might be a grim demonic warrior, a Partial Expert/Accursed might be a sinister tempter, while a Partial Mage/Accursed could be a sorcerer willing to pact with foul creatures for their powers.

All Accursed gain Magic-0 during character creation.
Even those who are not spellcasters must understand the ways of eldritch beings and the subtleties of sorcery.

While the arts of **Accursed Blade** and **Accursed Bolt** use Magic as their combat skill, Foci that normally apply to mundane weapons such as Armsmaster or Deadeye also benefit their relevant melee or ranged attacks.
For these Foci, read “Magic” in place of “Shoot” or “Stab” when gaining their benefits, including the granted skill and the stacking damage bonus that the first level of the Focus may grant.

#### 1.5.2.1 Accursed Arts

An Accursed has an Effort score equal to their Magic skill level plus their Intelligence or Charisma modifier, to a minimum of one point.
Every Accursed gains either **Accursed Bolt** or **Accursed Blade** as a starting art, plus one more of their choice.
Accursed arts may be used in or out of armor.

- **Accursed Blade**: As an On Turn action, manifest an occult melee weapon as a one-handed 1d8 weapon or a two-handed 2d weapon.
Both add your Magic skill to the damage roll, have a Shock rating of 2/15, and use Magic as the attack skill and the best of Str, Dex, Int, or Cha as its modifying attribute.
- **Accursed Bolt**: As Accursed Blade, but you launch blasts of occult force instead of meleeing.
Their damage is 1d8 plus your Magic skill, their range is 200’, and the bolts can be thrown in melee at a -4 penalty to hit.
These bolts need both hands free to hurl them.
- **Bewitching Distraction**: Commit Effort for the day as a Main Action when talking to an intelligent target when not in combat.
They must make a Mental save or become dazed, oblivious to their surroundings and forgetting you and all else that happened in that scene.
Danger ends the daze but not the forgetting.
- **Compelling Shriek**: Once per scene, Commit Effort for the day as a Main Action and shout a command of no more than seven words.
Chosen targets who hear and understand must make a Mental save or perform that action for one round, provided it is not totally contrary to their character.
- **Devil’s Bargain**: As a Main Action, consecrate a deal you’ve made with an uncoerced person.
If they violate the deal or its spirit, you know instantly and may inflict 1d6 damage per level on them if desired.
- **Dire Pact**: Foes suffer a penalty equal to your Magic skill on all saves versus your Accursed arts.
If they succeed, however, you gain one System Strain.
- **Lying Face**: Commit Effort as a Main Action; while it remains Committed, you can disguise yourself as any humanoid of the same general size, including clothing, scent, and voice.
- **Night-Black Eyes**: You can see clearly in perfect darkness.
As a Main Action, focus on a particular visible object, person, or lo cation; you can tell if it is enchanted, though no details about the magic are seen.
- **Pacted Protection**: Choose a type of harmful energy: fire, frost, acid, electricity, or the like.
You become immune to natural degrees of this energy and take half damage from magical attacks involving it.
- **Rob Vitality**: Once per scene, as an On Turn action, Commit Effort for the scene and target a visible foe.
They must make a Physical save or lose their next Main Action, which you immediately gain instead.
- **Scourging Curse**: Commit Effort for the scene as a Main Action and target a visible foe.
Your curse inflicts a -1 penalty to their hit, damage, and saving throw rolls for one round per level.
At 4th level this penalty becomes -2, and at 9th it becomes -3.
Only one such curse can be active at a given time.
- **Shadowed Steps**: As a Move Action, Commit Effort for the scene and teleport up to your Move distance.
You cannot bypass walls or physical obstacles, but you can teleport vertically or into high places.
- **Snaring Speech**: Once per round as an Instant action, Commit Effort for the day when failing a skill check to persuade or tempt someone.
They must make a Mental save or agree with your proposal if it’s something they would normally consider doing.
Gain one System Strain when using this art.
- **Sorcerous Battery**: Once per day, as an On Turn action, Commit Effort for the day.
You or a visible ally refresh the spell slot of a spell that has been cast since the start of the prior round.
Gain one System Strain.
- **Soul Consumption**: As an Instant action, Commit Effort for the day when you fell an intelligent target with Accursed Bolt or Accursed Blade.
They die instantly.
You heal 1d6 hit points plus your level and lose one accumulated System Strain.
- **Tendrils of Night**: Commit Effort as an On Turn action.
While Committed, you exude numerous tentacles or eldritch arms that can manipulate objects with your strength up to 20’ away.
You gain no bonus actions, but the arms can melee at range.
These arms have your AC, and you are damaged if they are hurt.
- **Unseen Steps**: As an On Turn action, Commit Effort for the day to turn invisible for 1d6 rounds plus your level.
This invisibility breaks before you attack, cast spells, or perform other vigorous or violent actions.
- **Weight of Sin**: As a Main Action, Commit Effort for the day and target a visible foe.
They must make a Physical save or lose their Move action for 1d6 rounds plus your level.
- **Weeping Wounds**: Once per round, Commit Effort for the scene as an Instant action when a visible enemy takes damage.
They must make a Physical save or suffer 1d6 damage per round for one round per level.
They cannot heal or regenerate any hit point damage during this effect.
This art does not stack.

#### Partial Expert/Accursed

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |      1d6 |           +0 | 1 Expert<br> +1 Any |
|     2 |      2d6 |           +1 | +1 Any              |
|     3 |      3d6 |           +1 |                     |
|     4 |      4d6 |           +2 |                     |
|     5 |      5d6 |           +2 | +1 Any              |
|     6 |      6d6 |           +3 |                     |
|     7 |      7d6 |           +3 | +1 Any              |
|     8 |      8d6 |           +4 |                     |
|     9 |      9d6 |           +4 |                     |
|    10 |     10d6 |           +5 | +1 Any              |

#### Partial Mage/Accursed

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/Accursed

| Level | Hit Dice | Attack Bonus | Focus Picks          |
|------:|---------:|-------------:|----------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br> +1 Any |
|     2 |    2d6+4 |           +2 | +1 Any               |
|     3 |    3d6+6 |           +2 |                      |
|     4 |    4d6+8 |           +3 |                      |
|     5 |   5d6+10 |           +4 | +1 Any               |
|     6 |   6d6+12 |           +5 |                      |
|     7 |   7d6+14 |           +5 | +1 Any               |
|     8 |   8d6+16 |           +6 |                      |
|     9 |   9d6+18 |           +6 |                      |
|    10 |  10d6+20 |           +7 | +1 Any               |

### 1.5.3 The Bard

Bards are those who have an uncanny ability to move others through the power of their performance.

#### Benefits Of The Bard

The Bard is a partial Expert class that must be joined with a second partial class by an Adventurer.
A Partial Warrior/Bard might be a martial skald, a Partial Expert/Bard might be a light-fingered troubadour, while a Partial Mage/Bard might mix true magic in with their artful performances.

All Bards gain Perform-0 during character creation.
Their abilities hinge on their power to move a listener, whether that is by song, musicianship, or stirring oratory.

Unlike the normal Partial Expert class, Bards do not get a bonus non-combat Focus at first level, nor do they get a Partial Expert’s bonus skill point when advancing a character level.
One who takes the standard Partial Expert for their other class gains these things normally.

#### 1.5.2.1 Bard Arts

A Bard has an Effort score equal to their Perform skill level plus their Charisma modifier, to a minimum of one point.
Bard arts may be used in or out of armor, and do not count as magical effects for the purposes of abilities that counter or detect magic.
Unless specified otherwise, their range is out to normal unaided voice range.

Every Bard gains **A Thousand Tongues** as a starting art, plus one more of their choice.
As they advance in levels, they can pick additional arts.

- **A Thousand Tongues**: Your arts that require communication are intuitively understood by all intelligent creatures, whether or not you speak their language.
You can learn a new language with no more than a week of study with a native speaker.
- **Battle Cry**: Commit Effort for the day as an On Turn action to bolster your allies.
They gain +1 to hit, saving throws, and damage, including Shock, for one round per level.
At 4th level this bonus increases to +2, and at 9th, to +3.
- **Cursed Tune**: Commit Effort for the scene as a Main Action while targeting an intelligent creature.
Your imprecations cause them to falter, inflicting a -1 penalty to their hit, damage, and saving throw rolls for one round per level.
At 4th level this penalty becomes -2, and at 9th it becomes -3.
- **Deft Fingers**: You’re accustomed to juggling your belongings while holding an instrument.
Your maximum Readied item count increases by two.
- **Entangle Incantation**: Commit Effort for the scene as an Instant action when an foe within 60’ incants a spell.
Your voice tangles with their words, forcing them to make an opposed Int/Magic vs Cha/Perform skill check against you, with a +2 bonus to your roll.
If you tie or succeed, the spell fizzles and is wasted.
This art can be used only once per round.
- **Evoke Emotion**: Commit Effort for the day as a Main Action while performing.
You evoke a desired emotion in listeners, whether intelligent or animal, granting a +1 bonus to relevant social skill checks for the scene for you and your allies.
If desired, you can force a new Reaction Roll, taking it if it’s more favorable than the original.
This art does not work in combat and can be used only once per scene.
- **Inspire Dread**: Commit Effort for the day as an Instant action when your enemies are forced to make a Morale check.
You goad their fears, forcing them to make two Morale checks instead of one.
Those who succeed on both must still make an Instinct check immediately after.
- **Keen Senses**: This art gives you the equivalent of sight out to 60’ even if blinded.
You cannot distinguish colors, but you can perceive physical details as readily as if with your eyes.
Physically-invisible creatures and objects are obvious to you.
- **Liberating Song**: You are immune to mind- or emotion-controlling effects.
You can Commit Effort for the scene as an Instant action to grant any ally in range a Mental saving throw to throw off such effects, assuming they allowed a save to begin with.
If the second save is failed, this art cannot help them.
- **Rally**: Commit Effort for the day as a Main Action to exhort a wounded or fallen ally, removing any Frailty.
The target regains 2d6 plus your level in hit points, but you incur one System Strain.
- **Soothe the Savage**: Commit Effort for the day as a Main Action.
For as long as you keep spending a Main Action performing each round, animals and other living bestial creatures will not attack you or your allies unless commanded by their masters or threatened by the party.
This art can’t be triggered once actual combat has begun.
- **Soothing Graces**: Your arts restore and hearten those with you.
You and your allies lose an extra point of System Strain after each night’s comfortable rest.
This does not stack with similar rest-aiding powers.
- **Swift Misdirection**: Commit Effort for the scene as a Main Action and target one intelligent creature within earshot.
Tell them something in no more than one sentence; unless what you say is physically impossible or emotionally unendurable, they must make a Mental save or believe it for one round.
After that, they can judge your words with their normal reason.
This art can affect a creature only once per scene.

#### Partial Expert / Bard

| Level | Hit Dice | Attack Bonus | Focus Picks          |
|------:|---------:|-------------:|----------------------|
|     1 |      1d6 |           +0 | +1 Expert<br> +1 Any |
|     2 |      2d6 |           +1 | +1 Any               |
|     3 |      3d6 |           +1 |                      |
|     4 |      4d6 |           +2 |                      |
|     5 |      5d6 |           +2 | +1 Any               |
|     6 |      6d6 |           +3 |                      |
|     7 |      7d6 |           +3 | +1 Any               |
|     8 |      8d6 |           +4 |                      |
|     9 |      9d6 |           +4 |                      |
|    10 |     10d6 |           +5 | +1 Any               |

#### Partial Mage / Bard

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |      1d6 |           +0 | 1 Any       |
|     2 |      2d6 |           +1 | +1 Any      |
|     3 |      3d6 |           +1 |             |
|     4 |      4d6 |           +2 |             |
|     5 |      5d6 |           +2 | +1 Any      |
|     6 |      6d6 |           +3 |             |
|     7 |      7d6 |           +3 | +1 Any      |
|     8 |      8d6 |           +4 |             |
|     9 |      9d6 |           +4 |             |
|    10 |     10d6 |           +5 | +1 Any      |

#### Partial Warrior / Bard

| Level | Hit Dice | Attack Bonus | Focus Picks          |
|------:|---------:|-------------:|----------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br> +1 Any |
|     2 |    2d6+4 |           +2 | +1 Any               |
|     3 |    3d6+6 |           +2 |                      |
|     4 |    4d6+8 |           +3 |                      |
|     5 |   5d6+10 |           +4 | +1 Any               |
|     6 |   6d6+12 |           +5 |                      |
|     7 |   7d6+14 |           +5 | +1 Any               |
|     8 |   8d6+16 |           +6 |                      |
|     9 |   9d6+18 |           +6 |                      |
|    10 |  10d6+20 |           +7 | +1 Any               |

#### Bard Art Progression

| Level | Arts Gained at This Level      |
|------:|--------------------------------|
|     1 | A Thousand Tongues and Any One |
|     2 | Any One                        |
|     3 |                                |
|     4 | Any One                        |
|     5 |                                |
|     6 | Any One                        |
|     7 |                                |
|     8 | Any One                        |
|     9 |                                |
|    10 | Any One                        |

### 1.5.4 The Beastmaster

Beastmasters are those PCs with an unnatural ability to control and influence animals.
For some, these abilities are the fruit of rigorous magical study, while others obtain them from cultural practices, a feral upbringing, or natural aptitude.

#### Beastmaster Benefits

The Beastmaster exists only as a partial Mage class, meant to be taken by an Adventurer along with another partial class.
A Partial Warrior/Beastmaster may be a savage barbarian or wood-wise ranger, while a Partial Expert/Beastmaster might be a masterful hunter or zoologist.

All Beastmasters get Survive as a bonus skill at level-0, or at level-1 if they already have it at level-0.
The amount of time they spend in the wilderness learning of its ways can’t help but teach them how to survive in hard conditions and navigate the perils of an untamed land.

#### Beastmaster Companions

Many Beastmaster arts apply to the PC’s animal companion.
There are a few basic guidelines for such companions that a GM should keep in mind during play.

A companion animal cannot have a number of hit dice greater than the Beastmaster’s level plus one.
Thus a 2 hit die wolf is an acceptable companion for a first-level Beastmaster, but a 5 hit die tentacular horror is too potent to be bound to service.

A companion animal cannot have human levels of intelligence.
It must be more-or-less animal in its thought patterns, though it can be a magical beast or supernatural entity of some kind.

A Beastmaster can normally have only one companion animal at a time.
They can leave an animal behind in any terrain suitable for its survival and expect it to be in the same vicinity later if they choose to return for it and no one has killed it in the meanwhile.
Released creatures are usually friendly to the Beastmaster still, unless they have been mistreated or are exceptionally savage.

A companion animal functions as an independent allied creature.
It will obey commands from the Beastmaster that are not contrary to its own nature or beyond its intellect, but it will fight, move, check Instinct and Morale as normal, and act independently in combat.
If the creature is mistreated or used as mere cannon fodder, it can flee or turn on its former master.

A companion animal must be given a name by the Beastmaster.
Without a name to focus the PC’s arts, the creature cannot be controlled.

#### The Chosen Friend

Some Beastmasters form a lifelong bond with a single dear companion.
If a PC chooses this option, they can only ever Bind one companion, but it gains the benefit of the **Shared Vitality** and **Mind Call** arts automatically.
So long as the Beastmaster lives, this chosen friend cannot truly die; if reduced to zero hit points, it disappears, but will manifest again at the Beastmaster’s side the following dawn with its full hit points.
Of course, the master is expected to treat this companion as the beloved friend it is, and not as mere battle-fodder.

#### 1.5.4.1 Beastmaster Arts

Beastmaster Effort is calculated with Survive, and is equal to the PC’s Survive skill level plus the higher of their Wisdom or Charisma modifiers, to a minimum of one point.
All Beastmasters start play with the **Bind Companion** art and one more of their choosing.
They learn additional arts as they gain experience.
Unlike some Mage traditions, Beastmasters can use their arts normally while armored.

- **Bind Companion**: With a day’s work in a location you can find and bind a suitable animal companion.
If looking for a particular type of companion, it must be found in the area and may require a Wis/Survive skill check at a difficulty of 10 or more if it’s a particularly rare creature.
If you encounter a suitable animal on an adventure, you may bind it as a Main Action, with the creature allowed a Mental saving throw to resist and become impervious to your powers.
You may release a companion as a Main Action if you no longer wish its service or want to bind a different beast.

#### Other Beastmaster

- **Arts Beast Ward**: Commit Effort as an On Turn action.
So long as the Effort remains Committed, the Beastmaster will not be attacked by unintelligent beasts unless the beast is commanded to do so by its master, the beast is starving, or the Beastmaster or their allies per forms some hostile act against it.
Once the ward ends or is broken, it cannot be re-established in the same scene.
Magical beasts and those trained specifically as guardians of an area get a Mental save to resist this power.
- **Eyes of the Beast**: Commit Effort for the scene as an On Turn action.
For the rest of the scene, you can share the senses of your companion.
Neither of you can be surprised unless both are surprised.
- **Feral Toughness**: Your maximum hit point total is increased by the natural, unmodified hit dice of your companion; thus, a 5 hit die companion increases your maximum hit points by 5.
You are impervious to normal outdoor extremes of heat or cold.
Your base Armor Class is equal to 13 plus half your level, rounded up.
This AC can be modified by shields or Dexterity modifiers, but not by armor.
- **Howl of Distant Summons**: You can call former animal companions to your side by Committing Effort for the day as a Main Action.
The howl extends through time as well as space, and the subject will have received the message in time to reach your side within five minutes after you call for it.
Once summoned, you may bind it again if you have no current companion.
If the journey is exceptionally dangerous, difficult, or long, you’ll need to make a Cha/Survive skill check against a difficulty of 10 or more; on a failure, the beast could not make the journey and cannot be called by this power for another week.
- **Know the Weak Spot**: Whenever inflicting damage on a beast with a weapon or physical attack, roll the damage twice and take the higher score.
Optionally, Commit Effort for the scene as an Instant action when an ally within earshot hits a beast; your shouted advice or distracting help allows them to roll damage twice and take the better result as well.
- **Mind Call**: Commit Effort for the scene as an On Turn action.
For the rest of the scene you forge a telepathic bond with your animal companion.
You can give it orders and receive information from the creature, at least insofar as its limited intellect allows.
- **Natural Weaponry**: You may have physically grown fangs and claws, or it may be that you just intuitively absorbed effective unarmed fighting techniques from your animal companions.
When unarmed, your attacks count as weapons that use either Str or Dex as their modifier, use Punch or Stab as the skill, do 1d8 damage, and have a Shock value of 2/AC 13.
These unarmed attacks can harm even creatures immune to non-magical weapons.
The damage and Shock of these weapons gains a +1 bonus at level 3, a + bonus at level 6, and a +3 bonus at level 9.
- **Savage Senses**: Commit Effort for the scene as an On Turn action.
For the remainder of the scene you are capable of seeing clearly even in pitch blackness, can scent out trails and belongings as perfectly as a wolf could, and gain a +2 bonus on all Notice skill checks involving sight, sound, or smell.
- **Shared Vitality**: The animal companions you bind have hit dice no lower than your level, even if they normally have far fewer.
Thus, a 2 hit die beast bound by a 7th level Beastmaster would have 7 hit dice while it remained bound.
Its hit bonus can’t be less than half its hit dice, rounded up.
A Beastmaster can stabilize a Mortally Wounded companion by Committing Effort for the scene as an Instant action.
- **Swift Healing**: Commit Effort for the day as a Main Action to heal 1d6 damage per character level to a wounded companion.
This healing can be extended only once per scene or fifteen minutes.
- **Tongue of the Beasts**: Commit Effort as an On Turn action.
While the Effort remains committed, you can speak with any animal that has fur, feathers, scales, or skin.
This art allows them to temporarily speak as if they had human intellect, though their interests, knowledge, and desires do not change.

#### Partial Expert/Beastmaster

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +1 | +1 Any             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 Any             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 Any             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 Any             |

#### Partial Mage/Beastmaster

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/Beastmaster

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +2 |                     |
|     4 |    4d6+8 |           +3 |                     |
|     5 |   5d6+10 |           +4 | +1 Any              |
|     6 |   6d6+12 |           +5 |                     |
|     7 |   7d6+14 |           +5 | +1 Any              |
|     8 |   8d6+16 |           +6 |                     |
|     9 |   9d6+18 |           +6 |                     |
|    10 |  10d6+20 |           +7 | +1 Any              |

#### Partial Beastmaster


| Level | Arts Gained                |
|------:|----------------------------|
|     1 | Bind Companion and Any One |
|     2 | Any One                    |
|     3 |                            |
|     4 | Any One                    |
|     5 | Any One                    |
|     6 | Any One                    |
|     7 |                            |
|     8 | Any One                    |
|     9 |                            |
|    10 | Any One                    |


### 1.5.5 The Blood Priest

As the devotee of a sanguinary god, a blood priest has magical powers for strengthening and supporting their allies.

#### Blood Priest Benefits

The Blood Priest exists only as a partial Mage class, to be taken by an Adventurer alongside another partial class.
A Partial Warrior/ Blood Priest might be a crusader for their god, wielding steel and terror against the evils of this world.
A Partial Expert/Blood Priest might be an inquisitor, investigating dubious figures and sifting out the truth of mysterious events.

All Blood Priests gain Pray as a bonus skill, acquiring it at level-0, or level-1 if they already have it at level-0.
Even a “Blood Priest” who has only learned their abilities through study of their scriptures must master a wide variety of ecclesiastical rites and rituals in order to activate their powers.

#### 1.5.5.1 Blood Priest Arts

Blood Priest Effort is calculated with Pray, with their total maximum Effort being equal to their Pray skill plus the higher of their Wisdom or Charisma modifiers, to a minimum of one point.

At first level, the Blood Priest may choose two of the following miracles to master, and gain more as they advance in levels.
Once chosen, a miracle pick is permanent and cannot be changed later.
Miracles are not hindered by the wearing of armor and require no gestures or free hand to employ, though usually a prayer must be vocalized as part of the action.

- **A Thousand Tongues**: Commit Effort as an On Turn action; while it remains Committed, you can speak with and understand any sentient creature with a language of their own.
To any listener, it will seem as if you are perfectly fluent in their own language, even as you speak in your own native tongue.
This ability does not allow you to read or write unknown languages.
- **Armor of God**: Commit Effort as an On Turn action.
While the Effort remains Committed, you have a natural Armor Class of 15 plus half your level, rounded down.
This AC can be modified by shields or your Dexterity modifier, but not by other worn armor.
- **Divine Guidance**: Commit Effort for the day as a Main Action and meditate upon a choice or potential action before you.
The GM will tell you whether the likely outcome of that choice is weal, woe, a mix of both, or nothing significant, using their own best estimation.
This insight cannot perceive likely outcomes more than an hour or so into the future.
- **Fear No Flame**: Commit Effort for the day as an On Turn action while nominating a visible target.
For the rest of the scene, they are immune to non-magical flame, smoke, or explosive damage, and decrease any sources of such magical damage they take by five points per level of Pray you have.
- **God Wills It**: Commit Effort for the day as an Instant action and loudly call down your god’s blessing on your comrades.
For the rest of the scene, up to six allies per character level in earshot gain an effective Morale of 12 and a +1 bonus to hit and damage rolls, including any Shock that may be inflicted.
At fifth level this becomes a +2 bonus, and at tenth level it becomes a +3 bonus.
- **Merciful Healing**: Commit Effort for the day as a Main Action and touch a target within melee range.
The target receives 1d6+2 points of magical healing.
If done in combat or used to revive a Mortally Wounded subject, the rushed haste of the healing adds 1 System Strain to the target, but no System Strain is added if it is applied outside of combat.
At fourth level the healing done increases to 2d6+4 and at eighth level it becomes 4d6+8.
- **Sanctified Ward**: Commit Effort as an On Turn action.
While the Effort is maintained and until you take some hostile action against a creature, you are immune to Shock damage and gain a +4 bonus to your Armor Class and all saving throws.
Enemies must pass an Instinct check each round to target you with attacks; on a failure, they may take some other action instead.
Once this art is ended it cannot be reactivated for the rest of the scene.
Healing and other indirect support of fighting comrades does not count as a hostile action.
- **Smite the Wicked**: Commit Effort for the day as a On Turn action whenever you attack or take some damaging action toward a single target.
You gain a +4 bonus to any hit roll, you inflict your character level as automatic damage, and you can roll any damage dice twice and take the higher result.
This ability can only be used once per scene on a given target.
- **The Light of Faith**: Commit Effort as an On Turn action; while it re mains Committed, you can cast a clear, bright radiance that extends up to thirty feet away from you.
At your discretion, this light is visible only to you and your comrades, including up to a dozen allies.
- **Transubstantiation**: Commit Effort for the day as a Main Action to transform up to a gallon of any liquid into sacred blood.
A draught of this holy liquid is sufficient to sustain a drinker for a day without further food or drink and will heal 1d6 plus the user’s Pray skill in lost hit points.
A gallon of blood is sufficient to help a half-dozen people.
The benefits of this art can be enjoyed only once per day, and any undrunk blood sublimates away within a minute.
- **Turn False Life**: Commit Effort for the day as a Main Action and make the sign of your god before one or more visible targets within sixty feet.
Roll 2d6 and add your character level; that many hit dice of undead, automatons, or other synthetic, extraplanar, or unnatural life forms must make Mental saving throws or cower for the rest of the scene or until they or their comrades are attacked.
Cowering enemies will take no hostile action and may flee unless bound to their location.
This power does not affect foes with twice as many hit dice as the blood priest has levels.
If the rolled hit die total isn’t enough to fully affect a creature, it is unaffected.
- **Words of Mercy**: Commit Effort for the day as an Instant action to reroll any failed social skill check related to maintaining peace, granting help, or encouraging virtuous acts of compassion, mercy, or truthfulness.
This art can be used only once on any given skill check.
- **Wrath of the Most High**: Commit Effort for the scene as a Main Action while you rebuke a visible target.
The first time the target takes damage before the end of the next round, it suffers automatic additional damage equal to 1d8 plus your level.

#### Partial Expert/Blood Priest

| Level | Hit Dice | Attack Bonus |        Focus Picks |
|-------|----------|--------------|-------------------:|
| 1     | 1d6      | +0           | 1 Expert<br>+1 Any |
| 2     | 2d6      | +1           |             +1 Any |
| 3     | 3d6      | +1           |                    |
| 4     | 4d6      | +2           |                    |
| 5     | 5d6      | +2           |             +1 Any |
| 6     | 6d6      | +3           |                    |
| 7     | 7d6      | +3           |             +1 Any |
| 8     | 8d6      | +4           |                    |
| 9     | 9d6      | +4           |                    |
| 10    | 10d6     | +5           |             +1 Any |

#### Partial Mage/Blood Priest

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/Blood Priest

| Level | Hit Dice | Attack Bonus | Focus Picks          |
|------:|---------:|-------------:|----------------------|
|     1 |    1d6+2 |           +1 | +1 Warrior<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any               |
|     3 |    3d6+6 |           +2 |                      |
|     4 |    4d6+8 |           +3 |                      |
|     5 |   5d6+10 |           +4 | +1 Any               |
|     6 |   6d6+12 |           +5 |                      |
|     7 |   7d6+14 |           +5 | +1 Any               |
|     8 |   8d6+16 |           +6 |                      |
|     9 |   9d6+18 |           +6 |                      |
|    10 |  10d6+20 |           +7 | +1 Any               |

#### Blood Priest Arts

| Level | Blood Priest Arts |
|------:|-------------------|
| 1     | Any Two           |
| 2     | Any One           |
| 3     |                   |
| 4     | Any One           |
| 5     | Any One           |
| 6     | Any One           |
| 7     |                   |
| 8     | Any One           |
| 9     |                   |
| 10    | Any One           |

### 1.5.6 The Duelist

Some warriors indulge in unique training methods that draw on arcane forces to enhance their speed and agility.

#### Duelist Benefits

The Duelist is a partial Mage class, meant to be taken by an Adventurer in conjunction with another partial class.
Partial Warrior/ Duelist is the most common pairing, for a skilled light-armor combatant with high mobility and a number of useful combat techniques.
Partial Expert/Duelist might represent an assassin who relies on stealth as much as steel, while a rare few Mage/Duelist combinations speak of adventuring wizards who perhaps have more talent for the sword than for the spell.

All Duelists gain Stab as a bonus skill.
No Duelist worth the name is entirely incapable of effective use of melee weapons, though the style lends itself heavily to specialization.

#### The Flaw Of Fragility

While the canons of the duelist make for an excellent one-on-one combatant, their practitioners spend much less time at raw physical conditioning than their more traditional warrior peers.
The techniques they use are swift and lethal, but they aren’t as generally applicable to enduring harm as are standard training methods.

As such, Partial Warriors/Partial Duelists use 1d6 for their hit dice, rather than the Partial Warrior’s usual 1d6+2.
Other partial class mixes are unaffected by this flaw.

#### 1.5.6.1 Duelist Arts

Duelist Effort is based on Stab, and is equal to the PC’s Stab skill plus the highest of their Dexterity or Intelligence modifiers, to a minimum of one point.
All Duelists begin with the **Favored Weapon** art and one more of their choice.
Additional arts are learned as the PC advances in experience.

The arts of the Duelist require agility and free motion.
The Duelist cannot benefit from any art of this class while wearing medium or heavy armor or carrying a large shield.
Dual-wielding is practiced by some Duelists, but most prefer the defensive advantages of a small offhand shield.

- **Favored Weapon**: Choose one specific type of non-unarmed me lee weapon.
You begin play with such a weapon, and when using this type you may use the Favored Weapon attack bonus column on the Duelist table to determine your base hit bonus, unless it’s already better for some other reason.
If your second partial class is Partial Warrior, your class’ base hit bonus with the weapon is instead equal to your level.
You cannot apply this or other Duelist arts to thrown weapons.

#### Other Duelist Arts

- **Blood for Blood**: Commit Effort for the scene as an Instant action when an enemy hits you with a physical attack.
If you attack that enemy with your favored weapon before the end of the next round, the first blow hits automatically and can do no less damage than was done to you, up to the weapon’s maximum.
- **Burst of Speed**: Commit Effort for the day as an On Turn action.
You may move your full normal movement rate as an On Turn action.
This art can be used only once per round.
- **Code Duello**: Commit Effort for the day as an On Turn action when engaged with a single foe.
So long as no other combatant attacks either your target or you, and you attack no one but your target, you gain a +4 bonus to your Armor Class and can roll your hit rolls twice, taking the better result.
Once the Code Duello has been disrupted or ended, it cannot be invoked again during that scene.
- **Crushing Superiority**: Your favored weapon gains the Less Lethal quality if it doesn’t already have it.
Commit Effort for the scene as an Instant action when you hit a target; they must immediately make an Instinct check.
This ability can be used only once per scene on any given target.
- **Dauntless Step**: Commit Effort for the scene as an On Turn action.
The Move actions you make for the rest of the round can cross vertical surfaces or difficult terrain at your full normal movement rate, provided you end the round standing upright on a surface that can bear your weight.
- **Dodge Doom**: Commit Effort for the day as an Instant action when caught in some explosion or other burst effect.
You take half dam age, or no damage if the effect allows a save and you succeed at it.
You can move yourself up to ten feet away from your original location, provided the new location is behind cover or away from the blast’s point of origin.
- **Forced Engagement**: Commit Effort for the scene as an Instant action when making an attack with your favored weapon; you may ignore one foe’s Screen Ally action for that attack.
- **Gentleman’s Withdrawal**: Commit Effort for the scene as a Move action.
As part of this, you may move at your full movement rate without being hindered by armed foes unless they completely block your path.
You count as having made a Fighting Withdrawal against any enemies engaged with you at the time.
- **Graceful Leap**: Commit Effort for the scene as an On Turn action to immediately leap up to ten feet horizontally or vertically, counting it as a Fighting Withdrawal.
You can’t use this art more than once per round or after you’ve attacked.
You are immune to falling damage from plunges of less than 30 feet.
- **Lightning Draw**: Commit Effort for the day as an Instant action at the start of hostilities; you win initiative against anyone without a similar ability to act first and may ready a Stowed favored weapon Instantly.
- **Piercing Strike**: Commit Effort for the scene as an On Turn action and choose a visible target.
For the rest of the scene, their AC is treated as 10 for purposes of resisting the Shock of your favored weapon.
- **Spiritual Weapon**: You are able to translate examples of your favored weapon into a spiritual template that you may manifest as you wish.
Any favored weapon can be turned into such a template, including a magical weapon, but the process destroys the object’s physical form.
To manifest a template, Commit Effort as an Instant action; a ghostly copy of the weapon appears Readied in hand for you until you release the Effort or stop touching the weapon.
Dual-wielders can summon the same weapon into both hands with one use of this art.
- **Unbindable**: Commit Effort for the day as an On Turn action when ever you wish to escape chains, grapples, shackles, ropes, or even a magical spell of physical binding.
You automatically wriggle free from mundane restraints and gain an Evasion saving throw to instantly end a physical magical binding of some sort.
You can use this art only once per round.
- **Unworthy Rabble**: Commit Effort for the day as an On Turn action.
For the rest of the scene, when using your favored weapon, reroll any failed hit roll against foes with one hit die.
At eighth level, this ability applies to foes with two hit dice.
- **Whirling Evasion**: Your base Armor Class becomes equal to 13 plus half your level, rounded up.
This AC can be modified by small shields and your Dexterity modifier, but not by armor or large shields.

#### Partial Expert/Duelist

| Level | Hit Dice | Attack Bonus |           Focus Picks |
|-------|---------:|-------------:|----------------------:|
| 1     |      1d6 |           +0 | 1 Expert<br>+1 Expert |
| 2     |      2d6 |           +1 |             +1 Expert |
| 3     |      3d6 |           +1 |                       |
| 4     |      4d6 |           +2 |                       |
| 5     |      5d6 |           +2 |             +1 Expert |
| 6     |      6d6 |           +3 |                       |
| 7     |      7d6 |           +3 |             +1 Expert |
| 8     |      8d6 |           +4 |                       |
| 9     |      9d6 |           +4 |                       |
| 10    |     10d6 |           +5 |             +1 Expert |

#### Partial Mage/Duelist

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/Duelist

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |      1d6 |           +1 | 1 Warrior<br>+1 Any |
|     2 |      2d6 |           +2 | +1 Any              |
|     3 |      3d6 |           +2 |                     |
|     4 |      4d6 |           +3 |                     |
|     5 |      5d6 |           +4 | +1 Any              |
|     6 |      6d6 |           +5 |                     |
|     7 |      7d6 |           +5 | +1 Any              |
|     8 |      8d6 |           +6 |                     |
|     9 |      9d6 |           +6 |                     |
|    10 |     10d6 |           +7 | +1 Any              |

#### Duelist Arts and Favored Weapon Bonus

| Level | Favored Weapon Bonus |                Arts Gained |
|------:|---------------------:|---------------------------:|
|     1 |                   +1 | Favored Weapon and Any One |
|     2 |                   +2 |                    Any One |
|     3 |                   +3 |                            |
|     4 |                   +4 |                    Any One |
|     5 |                   +5 |                    Any One |
|     6 |                   +6 |                    Any One |
|     7 |                   +7 |                            |
|     8 |                   +8 |                    Any One |
|     9 |                   +9 |                            |
|    10 |                  +10 |                    Any One |

### 1.5.7 The Elementalist

Elementalists are spellcasting Mages who focus on the manipulation of the material world around them.
While they are capable of using High Magic, their specialist spells focus chiefly on wielding the classical elements of fire, air, water, and earth.

#### Elementalist Benefits

All Elementalists gain Magic as a bonus skill, acquiring it at level-0, or level-1 if was already level-0.

Elementalists can prepare and cast High Magic spells in addition to the spells specific to Elementalists.
As usual for spellcasters, Elementalists can’t cast spells or use arts while armored or holding a shield.

Elementalists are not as talented at general High Magic research as High Mages are, but their studies still bear fruit in time.
Each time they advance a level, they can pick a new High Magic spell or an Elementalist spell to add to their spellbook.
They must be able to cast the spell to add it to their selection.

Elementalists gain the **Elemental Resilience** and **Elemental Sparks** arts as part of their basic training, and may pick one additional art from the list below.
Further arts are learned as they advance in character level, as given in the tables below.
Once chosen, an art cannot be changed.

#### 1.5.7.1 Elementalist Arts

Elementalist Effort is calculated as usual, with each PC’s maximum being equal to one plus their Magic skill level plus the better of their Intelligence or Charisma modifiers.
Partial Elementalists have a score one point lower than this, albeit not less than one.

- **Elemental Resilience**: You are unharmed by mundane extremes of cold or by heat less than that of a furnace.
You suffer only half damage from magical or extremely intense flame or frost attacks.
- **Elemental Sparks**: You can conjure petty amounts of flame, water, ice, stone, or wind, sufficient to do small tricks, chill drinks, light candles, or do other minor things.
Conjured substances last no longer than a scene, and conjured water cannot lastingly quench thirst.
This art cannot actually be useful in solving a problem or overcoming a challenge more than once per game session.

#### Other Elementalist Arts

- **Beckoned Deluge**: Commit Effort for the scene as a Main Action to conjure a considerable amount of water at a visible point with in fifty feet per caster level.
This water is sufficient to drench one 10-foot cube of matter per character level, making non-magical bowstrings useless, extinguishing flames, and inflicting 1d6 dam age per caster level on fiery supernatural creatures.
This water persists indefinitely after its conjuration and is sufficient to hydrate ten people per caster level.
- **Earthsight**: Commit Effort for the scene as an On Turn action.
For the rest of the scene, you can see the outlines of solid objects even in perfect darkness, and can peer through a number of feet of earth or stone equal to your character level.
- **Elemental Blast**: Commit Effort for the scene as a Main Action to hurl a blast of some elemental force at a visible target within fifty feet per character level.
The attack is made with Magic as the combat skill, Int, Cha, or Dex as the attribute, and a bonus to hit equal to your character level.
It is not hindered by melee foes.
On a hit, the attack does 1d6 damage plus your character level and attribute mod.
The blast may have collateral effects on objects in the case of hurled fire or a torrent of pressurized water, but any conjured matter vanishes at the end of the round.
- **Flamesight**: Commit Effort as an On Turn action.
While the Effort remains Committed, you can see thermal gradients sufficient to distinguish surfaces and living creatures, even in perfect darkness.
Optionally, you may cause your own eyes to cast a light sufficient to illuminate your surroundings clearly out to a range of 30 feet.
- **Pavis of Elements**: Commit Effort as an On Turn action to conjure an elemental barrier around yourself.
The barrier improves your Armor Class by +4 and remains as long as the Effort remains Committed.
This bonus stacks with other effects, but cannot increase AC above 18, regardless of the combinations.
- **Petrifying Stare**: Commit Effort for the day as a Main Action and target a visible creature.
The creature must make a Physical save or become partially petrified, losing its Move action for a number of rounds equal to half your caster level, rounded up.
Flying creatures are forced to land by this art and swimming creatures will inevitably sink to the bottom.
- **Rune of Destruction**: Commit Effort for the day as a Main Action and target an adjacent solid surface.
A glowing rune the size of a handprint forms on the surface and persists for one hour per caster level.
Any creature who gets within two feet of the rune will trigger it, causing it to explode in a five-foot radius with an elemental force of your choice and suffering 2d6 damage plus your caster level.
Creatures already within five feet of the rune when it is laid will not trigger it until they re-enter the area, nor will the caster trigger their own runes.
Runes cannot overlap their areas of effect.
- **Steps of Air**: Commit Effort for the scene as an On Turn action and target a visible ally; for one round per caster level, the target can fly at their usual movement rate.
If the art ends while they are still in the air, they descend harmlessly to the ground.
This art may also be used as an Instant action to negate falling damage for any single target.
- **Stunning Shock**: Commit Effort for the day as a Main Action and target a visible creature within fifty feet per caster level.
The target creature must be wearing or holding at least a pound worth of conductive metal or be considerably dampened.
An electrical bolt leaps from the caster to stun the target, causing them to lose their next Main Action.
A Physical saving throw can mitigate the effect, causing the target to lose their Move action instead of their Main.
A creature can be targeted only once per scene by this.
- **Thermal Shield**: Commit Effort for the scene as an Instant action to immediately negate one instance of fire or frost damage to any single visible ally or object.
This defense lasts only long enough to nullify the single instance of damage.

#### Full Elementalist

| Level | Hit Dice | Bonus Attack | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Full Elementalist Arts And Spells

| Level | Max Level | Spells Cast | Spells Prepared | Arts Gained                                         |
|------:|----------:|------------:|-----------------|-----------------------------------------------------|
|     1 |         1 |           1 | 3               | Elemental Resilience, Elemental Sparks, and Any One |
|     2 |         1 |           1 | 3               | Any One                                             |
|     3 |         2 |           2 | 4               |                                                     |
|     4 |         2 |           2 | 5               | Any One                                             |
|     5 |         3 |           3 | 6               |                                                     |
|     6 |         3 |           3 | 7               | Any One                                             |
|     7 |         4 |           4 | 8               |                                                     |
|     8 |         4 |           4 | 9               | Any One                                             |
|     9 |         5 |           5 | 10              |                                                     |
|    10 |         5 |           6 | 12              | Any One                                             |

#### Partial Elementalist Arts And Spells

| Level | Max Level | Spells Cast | Spells Prepared | Arts Gained                                         |
|------:|----------:|------------:|-----------------|-----------------------------------------------------|
|     1 |         1 |           1 | 2               | Elemental Resilience, Elemental Sparks, and Any One |
|     2 |         1 |           1 | 3               |                                                     |
|     3 |         1 |           1 | 3               | Any One                                             |
|     4 |         1 |           2 | 4               |                                                     |
|     5 |         2 |           2 | 5               |                                                     |
|     6 |         2 |           3 | 6               | Any One                                             |
|     7 |         2 |           3 | 7               |                                                     |
|     8 |         2 |           3 | 7               |                                                     |
|     9 |         3 |           4 | 8               | Any One                                             |
|    10 |         3 |           4 | 9               |                                                     |

#### Partial Expert/Elementalist

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 One |
|     2 |      2d6 |           +1 | +1 One             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 One             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 One             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 One             |

#### Partial Other Mage/Elementalist

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/Elementalist

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +2 |                     |
|     4 |    4d6+8 |           +3 |                     |
|     5 |   5d6+10 |           +4 | +1 Any              |
|     6 |   6d6+12 |           +5 |                     |
|     7 |   7d6+14 |           +5 | +1 Any              |
|     8 |   8d6+16 |           +6 |                     |
|     9 |   9d6+18 |           +6 |                     |
|    10 |  10d6+20 |           +7 | +1 Any              |

### 1.5.8 The Expert

Your hero is an expert at some useful skill.
Thieves, diplomats, healers, scholars, explorers, artisans, and other such heroes should pick the Expert class if they wish to focus on developing their special skills and performing tremendous feats of mastery with them.
Experts gain the widest variety of non-combat skills and are the quickest to learn more of them.

An Expert has an uncanny knack for wielding their skills success fully at a crucial moment, whether or not it’s a skill they’ve taken for their specialty.
Once per scene, the Expert can reroll a failed non-combat skill check, gaining a second chance to yank victory from the jaws of otherwise certain failure.
Their natural focus on personal development and determined refinement of their skills bleeds through even into those talents they don’t make their special domain.

Experts are also capable combatants, fully able to hold their own in the midst of a murderous fray.
It’s not unknown for some Experts to specialize in professions related to martial pursuits, such as an assassin who relies heavily on their superb powers of stealth and deception to reach their unwary foes.


#### Class Ability: Masterful Expertise

Once per scene, a Full Expert may reroll any non-combat skill check as an Instant action.
This allows the Expert to make a roll and then immediately use this ability if the resulting total isn’t good enough to succeed.
In cases where it matters, the better of the two rolls may be used.

Note that the typical ten-minute dungeon exploration turn generally counts as a scene, allowing the Full Expert to use this ability every turn if they so wish.

Partial Experts do not receive this ability.

#### Class Ability: Quick Learner

When you advance a character level, you gain an extra skill point which may only be spent on gaining or improving non-combat skills or raising attributes.
You may save this point to spend later if you wish.

Partial Experts do receive this ability.

#### Full Expert

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +1 | +1 Any             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 Any             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 Any             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 Any             |

#### Partial Mage/Partial Expert

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +1 | +1 Any             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 Any             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 Any             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 Any             |

#### Partial Warrior/Partial Expert

| Level | Hit Dice | Attack Bonus | Focus Picks                      |
|------:|---------:|-------------:|----------------------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Expert<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any                           |
|     3 |    3d6+6 |           +2 |                                  |
|     4 |    4d6+8 |           +3 |                                  |
|     5 |   5d6+10 |           +4 | +1 Any                           |
|     6 |   6d6+12 |           +5 |                                  |
|     7 |   7d6+14 |           +5 | +1 Any                           |
|     8 |   8d6+16 |           +6 |                                  |
|     9 |   9d6+18 |           +6 |                                  |
|    10 |  10d6+20 |           +7 | +1 Any                           |

### 1.5.9 The Healer

The Healer is a partial Mage class that does not use spells.
Instead, they gain specific arts that allow them to mend wounds, cure illnesses, and otherwise sustain their allies.

#### Healer Benefits

The Healer class exists only as a partial Mage class, to be taken by an Adventurer along with another partial class.
Thus, a Partial Warrior/Partial Healer might be a grizzled combat medic, a Partial Expert/Partial Healer might be an erudite physician gifted in both mundane and magical healing methods, and a Partial Necromancer/Partial Healer might be an adept of life and death.

All Healers gain Heal as a bonus skill, acquiring it at level-0, or level-1 if they already have it at level-0.
A basic grounding in mundane healing techniques is necessary in order to learn their more sophisticated magical arts.

Healers do not learn how to cast spells.
Instead, they focus on their special arcane healing arts.
Wielding these arts usually requires nothing more than touching the target and concentrating on the desired effect, and the process is direct and simple enough to perform even when burdened by armor or carrying a shield.

These arts are usually quite subtle, and don’t produce visible or audible indications of their use.

At first level, a Healer gains the **Healing Touch** art and can pick one more of their choice.
As they advance in levels afterwards, they can learn new arts.
Once chosen, an art is permanent and cannot be exchanged.

#### 1.5.9.1 Healer Arts

Healer Effort is calculated with Heal rather than Magic, with each PC’s maximum being equal to their Heal skill level plus the better of their Intelligence or Charisma modifiers, to a minimum of one point.

All Healers are trained in the **Healing Touch** art, but may develop other techniques with time.

- **Healing Touch**: Commit Effort for the scene as an Instant action; for the rest of the scene, you may heal 2d6 damage plus your Heal skill to a touched ally as a Main Action.
This healing adds 1 System Strain to the target each time it is applied.

#### Other Healer Arts

- **Empowered Healer**: Your Healing Touch becomes more powerful, adding your level to any healing.
- **Facile Healer**: Your Healing Touch ability is improved;, and you no longer need to Commit Effort to activate it.
- **Far Healer**: Your Healing Touch ability is improved, and may be used on a visible target within ten feet per character level.
- **Final Repose**: Target a visible living creature and Commit Effort for the day as an Instant action.
For the rest of the scene, they take a Physical saving throw penalty equal to your Heal skill.
If they are reduced to zero hit points before the end of the scene, they die with no chance for stabilization or revival.
- **Healer’s Eye**: Commit Effort as an On Turn action; while the Effort remains committed, you can use a Main Action to visually detect diseases and poisons, diagnose creatures flawlessly, perceive their physiology, and learn their current hit point totals.
As a side effect, you can detect living creatures by sight regardless of available light or obscuring mists.
- **Limb Restoration**: Only expert healers can master this art, which cannot be learned earlier than 8th level.
You must Commit all remaining Effort for the day, a minimum of one point, to regenerate a missing limb or organ for a target you are touching, or efface some dramatic scar or other physical debility.
The target’s System Strain is automatically maximized.
- **Purge Ailment**: Commit Effort for the day as a Main Action.
An ally you are touching is cured of one poison or disease.
Creatures killed by poison can be revived by this art if it is used within six minutes of death.
Magical poisons and diseases may require a Wis/Heal or Cha/Heal skill check against a difficulty of 8 or more.
At seventh level, you need only Commit Effort for the scene to use this ability.
- **Refined Restoration**: You and up to a dozen allies you tend be fore they sleep will all lose 2 System Strain from a good night’s rest instead of 1.
- **Revive the Fallen**: Only expert healers are capable of mastering this art, which cannot be learned earlier than 8th level.
Commit Effort for the day as a Main Action to revive a recently-slain living creature that you are touching.
This ability must be used on a target within one minute per caster level of their death, and will not work on a corpse that has been dismembered, incinerated, or otherwise disjected.
The target’s System Strain is automatically maximized and they’ll be unconscious for twenty-four hours after their restoration before awakening with 1 hit point.
- **Swift Healer**: Your Healing Touch ability is improved, and may be used as an On Turn action once per day per character level, though not more than once per round on any given target.
- **The Healer’s Knife**: Your Healing Touch ability is altered, and may be used to inflict damage to a living target instead of healing it.
The damage done is equivalent to the healing that would normally be done, albeit you receive 1 System Strain instead of the target.
Using this power in melee requires a successful Punch attack with a hit bonus equal to your Heal skill, with the damage added to any done by the blow, or simply touching an unwary target.
Ranged use with Far Healer is impossible with this art.
- **Tireless Vigor**: Commit Effort; while it remains Committed your need to eat, drink, breathe, or sleep does not grow further.
You may exert yourself tirelessly and regenerate 1 lost hit point per hour.
- **Vital Furnace**: Your tremendous life energy can be used to instantly regenerate any non-mortal wound you have received.
Commit Effort for the day as an Instant action to negate the damage from an injury you just received that did not reduce you to zero hit points.
Aside from this ability, you automatically stabilize if reduced to zero hit points and awaken ten minutes later with 1 hit point.
This ability cannot undo damage you intentionally inflict on yourself via some power or magical exchange.

#### Partial Expert/Healer

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +1 | +1 Any             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 Any             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 Any             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 Any             |

#### Partial Mage/Healer
| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/Healer

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +2 |                     |
|     4 |    4d6+8 |           +3 |                     |
|     5 |   5d6+10 |           +4 | +1 Any              |
|     6 |   6d6+12 |           +5 |                     |
|     7 |   7d6+14 |           +5 | +1 Any              |
|     8 |   8d6+16 |           +6 |                     |
|     9 |   9d6+18 |           +6 |                     |
|    10 |  10d6+20 |           +7 | +1 Any              |

#### Healer Arts

| Level |               Arts Gained |
|------:|--------------------------:|
|     1 | Healing Touch and Any One |
|     2 |                   Any One |
|     3 |                           |
|     4 |                   Any One |
|     5 |                   Any One |
|     6 |                   Any One |
|     7 |                           |
|     8 |                   Any One |
|     9 |                           |
|    10 |                   Any One |

### 1.5.10 The High Mage

High Mages represent the default spellcaster in most campaigns.
Their spells tend to be generalist in nature rather than focusing on particular specific themes, but their arts make them extremely skilled at manipulating and augmenting the magics they use.

#### High Mage Benefits

All High Mages gain Magic as a bonus skill, acquiring it at level-0, or level-1 if was already level-0.
Every High Mage is well-educated in the principles of magic as understood by the sorcerers of this world.

High Mages can prepare and cast High Magic spells, and have a number of arts dedicated to improving their use of these incantations.
As usual for spellcasters, they cannot wear bulky clothing or armor while casting or using arts, nor use shields.
Mages who have trained to overcome these limits with the **Armored Magic** Focus have more latitude.

High Mages conduct extensive experimentation and study as part of their daily activities.
Each time they advance a level, they may pick two High Magic spells from the list in this book to add to their repertoire.
These spells must be of a level they can cast.

High Mages also gain arts specific to their tradition.
At first level, a full High Mage picks two arts and a partial High Mage picks one.
As they gain levels and experience, they’ll learn additional arts from the adjacent list.
Once picked, an art cannot be changed.

#### 1.5.10.1 High Mage Arts

High Mage Effort is calculated as usual, with each PC’s maximum being equal to one plus their Magic skill level plus the better of their Intelligence or Charisma modifiers.
Partial High Mages have a score one point lower than this, albeit not less than one.

- **Arcane Lexicon**: Commit Effort for the scene.
For the rest of the scene, you can read any script that was not intentionally obfuscated or encoded by its writer.
Extremely esoteric or nonhuman scripts may not be comprehensible this way; the “plain meaning” of the text might be utterly foreign to human logic.
- **Counter Magic**: Commit Effort for the day as an Instant action when a visible enemy mage casts a spell.
Both of you make op posed Int/Magic or Cha/Magic skill checks; if you win, their spell fizzles and is wasted.
This art only works on actual spellcasters and not creatures that merely activate magical powers.
You can use this art no more than once per round.
- **Empowered Sorcery**: Commit Effort for the day as an Instant action to re-roll any variable die roll associated with a spell’s effects, such as a damage roll; take the roll you prefer.
- **Hang Sorcery**: Commit Effort for the scene as an Instant action when casting a spell.
The spell does not go off, but remains “hung” and waiting to be triggered as an On Turn action, with details of targeting and effect determined at that time.
Damage does not disrupt a hung spell, but no additional spells can be cast until the hung spell is released or allowed to dissipate.
- **Inexorable Effect**: Commit Effort for the day as an Instant action to force an enemy to re-roll a successful saving throw and take the worse result.
You may use this art only once per scene.
- **Iron Resolution**: Commit Effort for the day when injured or disturbed in combat; you may make a Physical saving throw to resist spell disruption and ignore the damage for purposes of spellcasting.
- **Preparatory Countermagic**: Commit Effort for the scene when you are affected by a spell you have prepared, including when you are standing in the area of effect of one of your own damaging spells.
You are unaffected by the spell’s direct effects.
- **Psychic Conversion**: Once per day as an On Turn action, expend one casting slot to remove one point of accrued System Strain and heal 2 hit points per level.
- **Restrained Casting**: Commit Effort for the day as an Instant action when casting a High Magic spell.
You may do so in perfect silence and without need for somatic gestures, though damage will still disrupt the casting.
- **Retain Sorcery**: Commit Effort for the day as an On Turn action after casting a spell; it does not count against your casting limits for the day.
You can use this art no more than once per day, and the stress of using it prevents you from casting another spell before the end of your next turn.
- **Sense Magic**: Commit Effort as an Instant action; while it remains committed, you can visually perceive magical energy and get a one-sentence description of the effect of any standing magics or magical items you inspect.
The ambient magical energies in most areas allow you to see clearly even in conditions of perfect darkness.
- **Suppress Magic**: Commit Effort for the day as an On Turn action and target a visible or known magical effect within one hundred feet.
The effect is suppressed as if by the Extirpate Arcana spell for 1d6 rounds plus the caster’s character level.
Spells cast by more powerful casters may not be successfully suppressed, as noted in the spell description.
The caster can attempt to suppress an effect only once.
- **Swift Casting**: Once per scene, you may Commit Effort for the day to turn a spell that normally requires a Main Action to cast into an On Turn action.
You can cast no other spell this round and you can’t use this art if you’ve already cast a spell this round or been injured or otherwise disqualified from casting.
- **Ward Allies**: Commit Effort for the day as an Instant action to omit up to six allies from the effects of an area-effect spell you cast, allowing them to avoid any damage or other negative effect that would be directly produced by the spell.
This does not protect them from indirect consequences, however, such as destroying the building they are standing in.
- **Wizard’s Grandeur**: Commit Effort as an On Turn action.
As long as it remains Committed, you will not become dirty, sweaty, stained, or rumpled regardless of the circumstances.
Noxious substances will slide off you without staining and you will remain comfortable regard less of your attire in any normal climate.
You may sleep comfortably without shelter or bedding as per the privation rules.

#### Full High Mage

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Full High Mage Arts And Spells

| Level | Max Level | Spells Cast | Spells Prepared | Arts Gained |
|------:|----------:|------------:|-----------------|-------------|
|     1 |         1 |           1 | 3               | Any Two     |
|     2 |         1 |           1 | 3               | Any One     |
|     3 |         2 |           2 | 4               |             |
|     4 |         2 |           2 | 5               | Any One     |
|     5 |         3 |           3 | 6               |             |
|     6 |         3 |           3 | 7               | Any One     |
|     7 |         4 |           4 | 8               |             |
|     8 |         4 |           4 | 9               | Any One     |
|     9 |         5 |           5 | 10              |             |
|    10 |         5 |           6 | 12              | Any One     |

#### Partial High Mage Arts And Spells

| Level | Max Level | Spells Cast | Spells Prepared | Arts Gained |
|------:|----------:|------------:|-----------------|-------------|
|     1 |         1 |           1 | 2               | Any One     |
|     2 |         1 |           1 | 3               | Any One     |
|     3 |         1 |           1 | 3               | Any One     |
|     4 |         1 |           2 | 4               |             |
|     5 |         2 |           2 | 5               |             |
|     6 |         2 |           3 | 6               | Any One     |
|     7 |         2 |           3 | 7               |             |
|     8 |         2 |           3 | 7               |             |
|     9 |         3 |           4 | 8               | Any One     |
|    10 |         3 |           4 | 9               |             |

#### Partial Expert/High Mage

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +1 | +1 Any             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 Any             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 Any             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 Any             |

#### Partial Other Mage/High Mage

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/High Mage

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +2 |                     |
|     4 |    4d6+8 |           +3 |                     |
|     5 |   5d6+10 |           +4 | +1 Any              |
|     6 |   6d6+12 |           +5 |                     |
|     7 |   7d6+14 |           +5 | +1 Any              |
|     8 |   8d6+16 |           +6 |                     |
|     9 |   9d6+18 |           +6 |                     |
|    10 |  10d6+20 |           +7 | +1 Any              |

### 1.5.11 The Invoker

The Invoker is an optional spellcasting Mage class that allows a caster to gain flexibility in casting weaker spells.
In exchange, an Invoker sacrifices arts and finds more powerful spells to be more draining to cast.

#### Invoker Benefits

All Invokers gain Magic as a bonus skill, acquiring it at level-0, or level-1 if was already level-0.
Their arcane versatility requires a thorough grounding in theory.

Invokers can learn and prepare spells of the High Magic tradition, and have the same limits on casting in armor as they do.
Each Invoker can prepare a number of known spells as indicated by the table each day, plus their Intelligence modifier.

Each Invoker has a number of spell points based on their level, to which is added their Intelligence modifier.
When they cast the spell, they subtract its level from their available spell points for the day.
So long as they have sufficient spell points left, they can cast the same spell repeatedly.
Spell points refresh each morning.

Invokers do not normally gain arts; their versatility requires complete focus, barring those who take the Invoker-specific **Traditional Education** Focus.
While it’s possible to be a Partial Invoker, this partial class may not be mixed with another partial spellcasting class.

Invokers begin play knowing four first level High Magic spells, or two if a partial class.
When they advance a level, they learn two more of any level or tradition from which they can cast, or one if a partial Invoker.

#### Focus: Traditional Education

Your Invoker has received a great deal of education in a specialist arcane spellcasting tradition such as High Magic arts, Elementalism or Necromancy.
Only Invokers can take this Focus, and it can only be taken once.

**Level 1**: You may learn and prepare spells of your chosen tradition as well as those of High Magic.
You gain arts as if a first level practitioner of that tradition, full or partial based on whether you’re a full or partial Invoker, and your maximum Effort score is equal to your Magic skill, to a minimum of 1.
You cannot gain further arts from this tradition.

#### Partial Invoker Hit Dice And Attack Bonuses

Partial Invokers use the High Mage partial-class tables for hit dice and attack bonuses when blending with other classes.

#### Full Invoker

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Full Invoker Spells

| Level | Max Spell Level |      Spell Points |  Spells Prepared |
|------:|----------------:|------------------:|-----------------:|
|     1 |               1 |  2 + Int modifier | 2 + Int modifier |
|     2 |               1 |  3 + Int modifier | 3 + Int modifier |
|     3 |               2 |  4 + Int modifier | 4 + Int modifier |
|     4 |               2 |  5 + Int modifier | 4 + Int modifier |
|     5 |               3 |  6 + Int modifier | 5 + Int modifier |
|     6 |               3 |  7 + Int modifier | 6 + Int modifier |
|     7 |               4 |  8 + Int modifier | 7 + Int modifier |
|     8 |               4 |  9 + Int modifier | 7 + Int modifier |
|     9 |               5 | 10 + Int modifier | 8 + Int modifier |
|    10 |               5 | 11 + Int modifier | 9 + Int modifier |

#### Partial Invoker Spells

| Level | Max Spell Level |     Spell Points | Spells Prepared  |
|------:|----------------:|-----------------:|-----------------:|
|     1 |               1 | 1 + Int modifier | 2 + Int modifier |
|     2 |               1 | 1 + Int modifier | 3 + Int modifier |
|     3 |               1 | 2 + Int modifier | 3 + Int modifier |
|     4 |               1 | 2 + Int modifier | 4 + Int modifier |
|     5 |               2 | 3 + Int modifier | 4 + Int modifier |
|     6 |               2 | 3 + Int modifier | 5 + Int modifier |
|     7 |               2 | 4 + Int modifier | 5 + Int modifier |
|     8 |               2 | 4 + Int modifier | 6 + Int modifier |
|     9 |               3 | 5 + Int modifier | 6 + Int modifier |
|    10 |               3 | 6 + Int modifier | 7 + Int modifier |

### 1.5.12 The Mage

The Mage class is a general header under which all heroes with fundamentally supernatural powers or spellcasting ability are place.
Once this class is chosen, the PC must choose a specific arcane tradition to follow; one is a High Mage, or a Vowed, or an Elementalist, for example.

Not all magical traditions necessarily involve classical spell-flinging and conjury.
Some traditions are much more physical in nature, granting the practitioners remarkable bodily prowess or unique magical gifts they can exercise.
Some Mage traditions involve no spellcasting at all, restricting their focus entirely to the strange arcane gifts their forebears have developed.

While these spells and occult powers are impressive, they tend to come at a cost.
A Mage must spend so much time focused on their studies and training that they have little time to master any other art.
Most are notably weak combatants with little ability to survive hardships that would merely wound or weary a Warrior.

In addition to this, many traditions have their own specific limits on practitioners.
Initiates of the High Mage tradition, for example, cannot cast their spells while wearing anything heavier than normal clothing, thus making it impossible for them to wear armor and still wield their spells.
The hindrances of some traditions extend beyond physical limitations to social penalties or difficulties in dealing with mundane humanity; a necromancer may have impressive powers of magic, but they are often unwelcome in civilized lands and are sometimes subject to the panicked justice of frightened locals and their lords.

The ways of magic tend to be specific to settings in a way that swordplay or skillful arts are not.
Your GM may disallow certain Mage traditions or partial classes based on the particulars of their own campaign world or the specific sort of game they want to play.
Some campaigns set in historical periods or very low-magic worlds might not include Mages at all, leaving heroes reliant on the strength of their own arms and the cunning of their own native wits.

Whatever the setting, Mage heroes need to rely on careful planning and a cooperative party to get the most from their abilities.
While potent, the spells they wield are few in number and usually narrow in effect; even the mightiest sorcerer is just not going to be as effective as their Warrior peers in slaying fearsome enemies or their Expert companions in carrying out the mundane activities of stealth, persuasion, or investigation.
Instead, Mages excel at providing carefully-planned impossibilities, those selective defiances of reality that allow their teammates to pull off incredible schemes or overcome otherwise insurmountable odds.

#### Class Ability: Arcane Tradition

The Mage may pick one magical tradition to represent their occult powers, as listed in this section.
This tradition may give them a number of additional benefits and restrictions.

### 1.5.13 The Mageslayer

Mageslayers are those specialist combatants who trade a more general proficiency in battle for certain special arts that make them exceptionally lethal against spellcasters.

#### Benefits Of The Mageslayer

The Mageslayer is a partial Warrior class that must be joined with a second partial class by an Adventurer.
As no Mage class is allowed to pair with this profession, this usually means a Partial Warrior/ Mageslayer or Partial Expert/Mageslayer.

Unlike the normal Partial Warrior class, Mageslayers do not get a bonus combat Focus at first level, nor do they grant a Partial Warrior’s +2 hit points per hit die.
If their other Partial class is Warrior, they gain these benefits normally.

All Mageslayers gain Magic-0 during character creation.
While it cannot be used for item creation or other magical pursuits, it allows them an intellectual understanding of their prey’s abilities.
Mageslayers gain special arts at first level and as they advance.

#### Spellcasters And Spells

Many Of These Arts Refer To “Casters” And “Spells”.
Each Of These Terms Has a Specific Definition For Mageslayers.

A “spell” is learned power that a creature casts with the usual spellcasting rules or an art used by a Mage class.
Magical effects created by portable magic items also count as spells, such as a wand that hurls fiery bolts, or an amulet that conjures impenetrable barriers.

Passive magical qualities, like the hit bonus of a magic sword, do not count as spells, nor do magical effects created by Workings or other large-scale edifices.
Neither are a magical creature’s innate supernatural powers or magical abilities that mimic specific spells.

A “caster” is any creature that is capable of casting spells using the usual spellcasting rules or triggering Mage class arts, whatever their brand of magic may be.

Potent as the Mageslayer’s arts may be, they are not strong enough to negate magic generated by gods, demi-divinities, or other quasi-divine entities.

#### 1.5.13.1 Mageslayer Arts

A Mageslayer has an Effort score equal to their Magic skill level plus their highest attribute modifier among Intelligence and Constitution, to a minimum of one point.
Mageslayer arts may be used in or out of armor, and require no free hands, incantations, or other overt actions, though some sects have favored prayers or oaths.

A Mageslayer gains their arts at a set rate, beginning with **Antimage** and **Magebane** at first level, and acquiring other talents as they gain experience.

- **Absolute Negation**: As an Instant action, Commit Effort for the day when you are about to be affected or impeded by an unwanted spell or a standing magical effect created by a spell.
You may ignore the magic.
- **Antimage**: Get one level of the Nullifier Focus for free, or a free Focus if you somehow have both levels.
- **Dispel Enchantment**: As a Main Action, touch an ally and Commit Effort for the day.
Your touch acts as an Extirpate Arcana spell.
This art can only be applied to creatures, not objects, and can be applied to yourself or a given target only once per day.
- **Disrupt Sorcery**: As an On Turn action, Commit Effort for the scene.
The next attack you make that round will leave the target unable to cast spells or trigger Mage arts for the rest of the round, even if the attack misses.
Currently-active arts are not disrupted.
If the attack is successfully Screened by an ally, however, the target is unaffected.
- **Immaculate Body**: Magical transformations, curses, or lingering negative magical penalties last a maximum of one scene on you.
Such malisons cannot kill you, reducing you to 1 HP at worst.
This applies to all magical effects, and not only to hostile spells.
- **Immaculate Mind**: You are impervious to unwanted mind-affecting or mind-reading spells or magical effects, and you automatically see through illusions and magical invisibility.
Note that this applies to all such magical effects, not merely spells.
- **Know Your Prey**: Your Magic skill is treated as one point higher than it is, up to a maximum of level-4 skill.
This benefit adds to your Effort as usual, and may allow you to have a higher Magic skill than your character level would usually allow.
- **Magebane**: You add half your level, rounded up, to all damage you inflict on casters from any source, be it by weapon, Shock, or violent action.
This does not stack with a Warrior’s Killing Blow class ability.
- **Share the Pain**: As an Instant action, Commit Effort for the day when you are targeted by a hostile spell or are in its area of effect.
Make a Mental saving throw; on a success, the spell’s caster is treated as also having affected by the spell that just targeted you.
This art can be used only once per round.
- **Spellshield**: When a spell inflicts hit point damage on you, it’s automatically halved, rounded down, before any saves or resistances are applied.
- **Ward Ally**: As an Instant action, Commit Effort for the scene when an ally within thirty feet is affected by a spell; you are affected by the spell in place of your ally.
If you are both in the same area of effect, the spell hits you twice.
Any defenses or arts you have against sorcery may be applied as usual to this transferred effect.
- **Witchfinder**: As a Main Action, Commit Effort for the scene.
For the rest of the scene, you gain the benefits of the Apprehend the Arcane Form spell, except for the spell’s dark-vision ability.

#### Partial Expert/Mageslayer

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +1 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +2 | +1 Any             |
|     3 |      3d6 |           +2 |                    |
|     4 |      4d6 |           +3 |                    |
|     5 |      5d6 |           +4 | +1 Any             |
|     6 |      6d6 |           +5 |                    |
|     7 |      7d6 |           +5 | +1 Any             |
|     8 |      8d6 |           +6 |                    |
|     9 |      9d6 |           +6 |                    |
|    10 |     10d6 |           +7 | +1 Any             |

#### Partial Warrior/Mageslayer

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +3 |                     |
|     4 |    4d6+8 |           +4 |                     |
|     5 |   5d6+10 |           +5 | +1 Any              |
|     6 |   6d6+12 |           +6 |                     |
|     7 |   7d6+14 |           +7 | +1 Any              |
|     8 |   8d6+16 |           +8 |                     |
|     9 |   9d6+18 |           +9 |                     |
|    10 |  10d6+20 |          +10 | +1 Any              |

#### Mageslayer Art Progression

| Level | Arts Gained at This Level |
|------:|--------------------------:|
|     1 |        Antimage, Magebane |
|     2 |  Witchfinder, Spellshield |
|     3 |           Disrupt Sorcery |
|     4 |            Know Your Prey |
|     5 |            Share the Pain |
|     6 |        Dispel Enchantment |
|     7 |                 Ward Ally |
|     8 |           Immaculate Body |
|     9 |           Immaculate Mind |
|    10 |         Absolute Negation |

### 1.5.14 The Necromancer

Necromancers are those spellcasting Mages who deal with the energies of life and death.

#### Necromancer Benefits

All Necromancers gain Magic as a bonus skill, acquiring it at Level-0, or Level-1 if was already Level-0.

Necromancers can prepare and cast High Magic spells in addition to the spells specific to Necromancers.
Some of the latter are given on the following pages, but others doubtless exist.
As usual for spellcasters, Necromancers can’t cast spells or use arts while armored or holding a shield.

Necromancers are not as talented at general High Magic re search as High Mages are, but their studies still bear fruit in time.
Each time they advance a level, they can pick a new High Magic spell or a Necromancer spell to add to their spellbook.
They must be able to cast the spell to add it to their selection.

Necromancers can pick an art specific to their tradition from the adjacent list.
Further arts are learned as they advance in character level, as given in the tables below.
Once chosen, an art cannot be changed.

#### 1.5.14.1 Necromancer Arts

Necromancer Effort is calculated as usual, with each PC’s maximum being equal to one plus their Magic skill level plus the better of their Intelligence or Charisma modifiers.
Partial Necromancers have a score one point lower than this, albeit not less than one.

- **Bonetalker**: You can see and communicate with any undead creature, regardless of a shared language or the creature’s natural state of invisibility.
By Committing Effort for the scene you can sense the surface thoughts of any visible undead, including an impression of any commands or behavior it has been ordered to carry out.
Unintelligent undead will not attack you or your companions unless specifically compelled to do so by a command or a master.
Even intelligent undead will generally pause at least for an initial parley before attacking.
- **Cold Flesh**: You no longer require sleep and feel pain only in an abstract sense.
You can suffer no more than 2 points of damage from any given instance of Shock and you have a natural Armor Class equal to 12 plus half your level, rounded down.
Consume Life Energy: By making a Punch attack or using a melee weapon you have spent at least an hour properly consecrating, you can absorb a portion of the damage you inflict on others as healing to yourself.
For each successful attack you make with such implements, you heal 1d6 damage, up to a maximum of the damage done by the attack.
You cannot drain more life than the target has remaining hit points.
- **False Death**: Commit Effort as an Instant action; while it remains Committed you appear dead to all mundane examination.
You lose your Main Actions while “dead”, but can move and perceive normally and do not need to eat, drink, breathe, or perform other bodily functions.
Poisons and diseases do not progress in you while you are “dead”.
You can maintain this state of death for up to one day per level before needing at least an hour to recover.
- **Gravesight**: Commit Effort as an On Turn action; while it remains Committed, you can see the life energies of living creatures around you as various glowing patterns, regardless of the mundane illumination available.
You can perceive sicknesses, poisons, and other physical qualities on sight.
As a side effect of this ability, you can see normally even in perfect blackness.
- **Keeper of the Gate**: At your discretion, creatures within twenty feet of you per character level that are Mortally Wounded will die instantly and cannot be revived by magic or medicine.
Conversely, you can Commit Effort for the day to automatically stabilize any or all within that range, increasing their System Strain by 1 point.
This benefit cannot aid creatures that have been dismembered, shredded, or otherwise suffered unsurvivable injuries.
- **Life Bridge**: You can transfer life force between willing or helpless participants.
Commit Effort for the day; for the rest of the scene, you can shift hit points from one willing or helpless target no smaller than a dog to another as a Main Action, provided you are touching both.
You can shift enough hit points to Mortally Wound a donor, but you can’t give more to the recipient than would refresh their maximum allowed hit points.
- **Master of Bones**: Undead must roll twice to save versus your abilities or spells and take the worse roll.
You may Commit Effort for the scene as an Instant action to negate any single attack, magical power or spell an undead uses against you.
Undead with more than twice as many hit dice as you have levels cannot be foiled this way.
- **Red Harvest**: You are empowered by death.
As an Instant action, whenever an intelligent living creature with at least one hit die perishes within fifty feet of you, Commit Effort for the day to either heal 1d6 plus your level in lost hit points or gain a +4 bonus on your next hit roll this scene.
This art cannot be stacked, and it can be used only once per round.
- **Unaging**: You no longer naturally age, and will remain perfectly hale and vigorous up to your species’ natural maximum age plus 20% per character level, after which you will collapse into dust and decay.
Immortality beyond this point is possible, but generally re quires consistent supplies of life energy, occult materials, or other difficult-to-acquire or morally questionable materials.
You also become immune to poisons and diseases.
- **Uncanny Ichor**: Your blood is not like the blood of normal beings.
Predators find it nauseating and will not bite you unless provoked.
Unintelligent predators will not consider you edible.
This ichor is reluctant to leave your body, and stabbing or puncture injuries can Mortally Wound you but cannot result in your death unless you are entirely pincushioned by your foes or suffer catastrophic physical damage.
- **Unliving Persistence**: Commit Effort for the day as an On Turn action to automatically stabilize when Mortally Wounded.
You may use this ability to benefit others if you are able to touch them.
This ability cannot save a subject that has experienced dismemberment or other extremely final deaths.

#### Full Necromancer

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Full Necromancer Arts And Spells

| Level | Max Level | Spells Cast | Spells Prepared | Arts Gained |
|------:|----------:|------------:|----------------:|-------------|
|     1 |         1 |           1 |               3 | Any One     |
|     2 |         1 |           1 |               3 | Any One     |
|     3 |         2 |           2 |               4 |             |
|     4 |         2 |           2 |               5 | Any One     |
|     5 |         3 |           3 |               6 |             |
|     6 |         3 |           3 |               7 | Any One     |
|     7 |         4 |           4 |               8 |             |
|     8 |         4 |           4 |               9 | Any One     |
|     9 |         5 |           5 |              10 |             |
|    10 |         5 |           6 |              12 | Any One     |

#### Partial Necromancer Arts And Spells

| Level | Max Level | Spells Cast | Spells Prepared | Arts Gained |
|------:|----------:|------------:|----------------:|-------------|
|     1 |         1 |           1 |               2 | Any One     |
|     2 |         1 |           1 |               3 |             |
|     3 |         1 |           1 |               3 | Any One     |
|     4 |         1 |           2 |               4 |             |
|     5 |         2 |           2 |               5 |             |
|     6 |         2 |           3 |               6 | Any One     |
|     7 |         2 |           3 |               7 |             |
|     8 |         2 |           3 |               7 |             |
|     9 |         3 |           4 |               8 | Any One     |
|    10 |         3 |           4 |               9 |             |

#### Partial Expert/Necromancer

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +1 | +1 Any             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 Any             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 Any             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 Any             |

#### Partial Other Mage/Necromancer

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/Necromancer

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +2 |                     |
|     4 |    4d6+8 |           +3 |                     |
|     5 |   5d6+10 |           +4 | +1 Any              |
|     6 |   6d6+12 |           +5 |                     |
|     7 |   7d6+14 |           +5 | +1 Any              |
|     8 |   8d6+16 |           +6 |                     |
|     9 |   9d6+18 |           +6 |                     |
|    10 |  10d6+20 |           +7 | +1 Any              |

### 1.5.15 The Skinshifter

The Skinshifter is a partial Mage class that grants the user shapeshifting abilities, whether to adopt the form of beasts or configurations of their own devising.

#### Skinshifter Benefits

Skinshifter is a partial Mage class, meant to be taken by an Adventurer in conjunction with another partial class.
A partial Warrior/Skinshifter might be a shapeshifting assassin or feral savage warrior, while a Partial Expert/Skinshifter could be an undetectable impostor or con artist.

All Skinshifters gain Survive-0 as a bonus skill, or Survive-1 if they already have Survive-0 as a skill.

#### Skinshifter Forms

The Arts Of a Skinshifter Revolve Around Improving Or Adjusting Their Alternate Forms.
These Alternate Forms Have a Few Basic Rules That Apply To All Their Permutations.

A Skinshifter can master one alternate form per character level.
A Skinshifter who wants to change a form selection can do so with a day of careful practice and adjustment.
Skinshifter PCs should note down the special powers or traits of their alternate forms and keep in mind which of them they have available at any one time.

A Skinshifter’s alternate forms use the same hit points, character attributes, and movement speed as their normal form, unless modified by particular arts.
Thus, a Skinshifter shifted into a wolf’s shape would do no more damage with its bite than the human’s unarmed attack would do without some augmenting art.
Alternate forms do grant a minimum level of combat competence, however, and so the “Form Bonus” hit bonus is used in place of the PC’s basic class hit bonus when in a non-humanoid shape, assuming they don’t already have a better basic bonus from their other partial class.

Arts that improve the alternate forms can apply their benefits even if the creature’s natural shape wouldn’t normally grant it.
Thus, a Skinshifter with the Manifest Wings art could have a wolf-form that had wings and a flying speed if they wished.
A given form can have no more than three arts applied to it.

A Skinshifter’s alternate forms can be any size between a horse and a kitten, barring modification by some art.
They do not need to emulate a specific type of creature; a Skinshifter could have a clawed and winged humanoid form as an alternate shape, or a dog with the head of a man, or a cat with razor-sharp mandibles.

The belongings carried by a Skinshifter meld into any new form unless the PC decides otherwise, granting any benefit to Armor Class or other worn advantage they may normally give.
Melded objects can be manifested and dropped as an On Turn action, but cannot be re-melded again unless the PC changes form with them again.

Skinshifter forms cannot perfectly duplicate another individual creature without an appropriate art.

#### 1.5.15.1 Skinshifter Arts

Skinshifter Effort is based on Survive, and is equal to the PC’s Survive skill level plus the highest of their Constitution or Charisma modifiers, to a minimum of one point.
All Skinshifters begin play with the Change Form art and one more of their choice.
Additional arts are learned as the PC advances in experience.

- **Change Form**: Commit Effort for the day as a Main Action to transform into an available Alternate form.
This shift persists until you die, shift to a new form, or end the transformation as an On Turn action.
Your hit points, System Strain, and other scores remain the same in any new form, but in a non-humanoid form your class’ base attack bonus can be no worse than the one listed on the table.
If your shape is quadrupedal, its Move is 10’ faster than your base.

#### Other Skinshifter Arts

- **Eyes Of The Hawk**: This Shape Has Remarkably Acute Senses.
You Can Scent Trails And Objects Like a Dog, Hear Very Faint Noises, See Objects Clearly Enough To Identify Faces At a Thousand Feet Of Distance, And See Clearly In Anything Short Of Complete Darkness.
You Gain a +2 bonus on all sensory Notice checks.
- **Feline Leap**: This form can vault great distances.
You can jump your full Move rating as a Move action, crossing the full distance horizontally or up to half the distance vertically.
You subtract your Move rating from fall distance for damage calculations.
- **Feral Prowess**: This form has an augmented degree of speed, strength, or hardiness.
For this form, pick either Strength, Dexterity, or Constitution; that attribute modifier is improved by +1 in this form, to a maximum of +2.
These alterations are obvious and extreme; it is impossible to masquerade as a normal baseline human if using this art.
Hit points gained from Con improvements are lost when shifting back.
- **Intrinsic Armor**: This shape has tough scales, hide, shell, or other defensive integuments.
Your base Armor Class is 14 plus half your character level, rounded down.
This AC can be modified by your Dexterity modifier, and by shields if the shape can hold one.
- **Manifest Wings**: This shape can form usable wings.
You can’t effectively attack while concentrating on remaining aloft, but you can fly at your normal movement rate +10’ per action.
If unhindered and flying overland you can travel up to ten miles an hour and fly up to five hours a day.
- **Octopus’ Embrace**: This shape can have more than four usable limbs.
These additional tentacles, arms, legs, or grippers can hold and manipulate things; up to four Stowed objects can be held Ready by these limbs without counting against your Readied Encumbrance limit.
These extra limbs cannot effectively employ extra shields, perform additional attacks or actions, or do other things that require significant focus beyond your current action.
- **Perfect Mimicry**: This alternate form can exactly duplicate another creature, provided you either have a lock of their hair, a still-liquid drop of their blood, or ten minutes of observation of them from within five feet.
Once learned, you need no further samples.
- **Pliant Flesh**: You’ve mastered your shapeshifting skills, and Change Form can be used as an On Turn action, albeit only once per round.
Alternately, it can take as long as a minute but cost no Effort.
When ever you spend Effort to shift shape, you heal lost hit points equal to 1d6 plus your level; this does not apply when merely reverting a form’s change.
This art is intrinsic to you, and doesn’t count against the total number of arts allowed to apply to a form.
- **Savage Talons**: This shape has extremely dangerous claws, fangs, pincers, or other body weapons.
At first level these weapons do 1d8 damage, use either Str or Dex as the modifying attribute, Punch as the combat skill, and have a Shock value of 2/AC 13.
At level 3 the damage and Shock increase by +1, at level 6 they increase to +2, and at level 9 they increase to +3.
These weapons can harm even creatures immune to non-magical weaponry.
- **Sculptor’s Beauty**: You’ve developed a refined aesthetic sense that can directly appeal to the subconscious beauty preferences of viewers.
This shape improves your Charisma modifier by +1, to a maximum of +2.
There is always something alien about this beauty, and such forms can never be mistaken for an entirely normal human or animal, though they may be taken for some exotic demihuman or altered baseline.
- **Serpent’s Kiss**: This shape can exude a venom on its claws or fangs.
Successful hits allow you to Commit Effort for the scene as an Instant action to inflict an additional 1d8 damage plus your Survive skill.
You can elect to make the toxin paralytic rather than fatal; foes reduced to zero hit points by such a blow are paralyzed and helpless for an hour rather than mortally wounded.
- **The Monkey’s Road**: This shape has an arboreal agility when it comes to climbing.
You can travel up sheer surfaces at your usual movement rate provided at least three limbs are available for use and the surface is not glassily smooth.
If you fall, you can halt your plunge without damage so long as a serviceable surface to cling to is within ten feet of you.
- **Warform**: You gain a flat +2 bonus to all melee hit rolls in this shape and can use your form hit bonus even in a humanoid body.
This shape is blatantly martial in appearance, however, and cannot pass as anything harmless or wholly human.
- **Wisdom of Fin and Scale**: This shape can breathe water, swim at double your usual Move rate, and is impervious to any normal climatic extreme of cold or undersea pressure.
It can see up to sixty feet while underwater even in the absence of normal light.

#### Partial Expert/Skinshifter

| Level | Hit Dice | Base Attack | Focus Picks        |
|------:|---------:|------------:|--------------------|
|     1 |      1d6 |          +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |          +1 | +1 Any             |
|     3 |      3d6 |          +1 |                    |
|     4 |      4d6 |          +2 |                    |
|     5 |      5d6 |          +2 | +1 Any             |
|     6 |      6d6 |          +3 |                    |
|     7 |      7d6 |          +3 | +1 Any             |
|     8 |      8d6 |          +4 |                    |
|     9 |      9d6 |          +4 |                    |
|    10 |     10d6 |          +5 | +1 Any             |

#### Partial Other Mage/Skinshifter

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/Skinshifter

| Level | Hit Dice | Base Attack | Focus Picks         |
|------:|---------:|------------:|---------------------|
|     1 |    1d6+2 |          +1 | 1 Warrior<br>+1 Any |
|     2 |    2d6+4 |          +2 | +1 Any              |
|     3 |    3d6+6 |          +2 |                     |
|     4 |    4d6+8 |          +3 |                     |
|     5 |   5d6+10 |          +4 | +1 Any              |
|     6 |   6d6+12 |          +5 |                     |
|     7 |   7d6+14 |          +5 | +1 Any              |
|     8 |   8d6+16 |          +6 |                     |
|     9 |   9d6+18 |          +6 |                     |
|    10 |  10d6+20 |          +7 | +1 Any              |

#### Skinshifter Arts and Form Bonus

| Level | Form Bonus | Arts Gained             |
|------:|-----------:|-------------------------|
|     1 |         +1 | Change Form and Any One |
|     2 |         +1 | Any One                 |
|     3 |         +2 |                         |
|     4 |         +2 | Any One                 |
|     5 |         +3 | Any One                 |
|     6 |         +3 | Any One                 |
|     7 |         +4 |                         |
|     8 |         +4 | Any One                 |
|     9 |         +5 |                         |
|    10 |         +6 | Any One                 |

### 1.5.16 The Thought Noble

This partial Mage class allows a PC to subtly manipulate the thoughts of others and control their own mind and body’s operation.

#### Thought Noble Benefits

A Thought Noble Exists Only As a Partial Mage Class, To Be Taken By An Adventurer Alongside Another Partial Class.
A Partial Warrior/Thought Noble Might Be a Cunning Duelist Who Chooses His Enemies With Uncanny Foreknowledge, While a Partial High Mage/ Thought Noble might be an arcanist who has managed to unearth the secrets of these psychic arts in the course of their own studies.

All Thought Nobles gain Notice as a bonus skill, acquiring it at level-0, or level-1 if they already have it at level-0.
An ability to clearly sense perceptual inputs is fundamental to a Thought Noble’s training, and it is all but impossible to make sense of foreign mental inputs without a disciplined and trained sensorium.

Thought Nobles do not cast spells.
Instead, they develop various cognitive and telepathic abilities based on the focus of their studies.

#### 1.5.16.1 Thought Noble Arts

Thought Noble Effort is calculated with Notice rather than Magic, with each PC’s maximum being equal to their Notice skill level plus the better of their Intelligence or Wisdom modifiers, to a minimum of one point.

All Thought Nobles are trained in the Open Mind art, but may develop other techniques with time.
If an art allows a saving throw to resist it, success renders the target immune to that art for the rest of the scene.
Thought Noble arts are all entirely invisible to any but magical senses and provide no hint as to who is using them, even on a successful Mental saving throw.

- **Open Mind**: Commit Effort as an On Turn action.
While Effort remains Committed, you may detect the current emotional state of a visible living creature as a Main Action.
This discernment isn’t fine enough to identify the object of their emotions unless it’s obvious from the context.

#### Other Thought Noble Arts

- **Block Memory**: Target a Visible Living Creature And Commit Effort For The Day As a Main Action While Specifying a Particular Event Or Situation That Didn’t Extend Longer Than a Scene.
The Target Gets a Mental Saving Throw; On a Failure, They Simply Cannot Remember The Event And Will Construct a Plausible False Memory To Paper Over The Gap.
The Memory Can Be Restored By Use Of This Power Once More Or a Magical Dispelling Ability, But Detecting The Alteration Is Difficult; any power that detects enchantments on a target does so with a +2 difficulty penalty to any skill check made to discern the ban.
- **Elicitation of Truth**: This art can only be taken by those who have mastered Surface Apprehension.
It can only be used on a helpless or cooperative target; the Thought Noble may Commit Effort for the day and ask the target a question that takes no more than two sentences to ask.
If the target fails a Mental saving throw, they must fully and completely answer the question.
They need not exercise any personal judgment or speculation, but they must answer directly and without conscious deception.
- **Facile Speech**: An improvement to the Far Speech art, you may now nominate up to a half-dozen companions to be favored targets for Far Speech.
You may link with a favored target as an On Turn action without Committing Effort.
The range to contact a favored target is one mile per Thought Noble level.
You can change your nominated companions with an hour’s meditation in the new companion’s presence.
- **Far Speech**: Commit Effort for the scene as an On Turn action and target an intelligent creature known to you that is within two hundred feet per level.
If they consent, you forge a telepathic link with them that can convey thoughts, speech, and images even without a common language.
The link lasts for the rest of the scene.
While linked with someone, both of you may reroll failed Mental saving throws and neither of you can be surprised unless both are.
- **Hypercognition**: You can drive your mind to tremendous feats of calculation, estimation, and recall.
You have an eidetic memory and can perform extremely advanced calculations in your head.
As an Instant action, Commit Effort for the day and reroll any failed Int-based skill check.
This ability can be used only once on any given check.
- **Iconograph**: Commit Effort for the scene as a Main Action and tar get a visible written text, piece of sculpture, iconographic symbol, or other visible artifact.
You receive a short description of the message or idea the maker was trying to convey with the object.
Optionally, you may use this art when making a sigil, art object, or text of your own; any valid target will automatically understand your intended message when they see the item.
You may restrict your audience to particular people or types if you wish.
- **Impress Imperative**: Commit Effort for the day as a Main Action while targeting a visible living creature.
You may give it an over whelming urge to perform some action that takes no longer than a round and is not completely contrary to its nature or wishes.
It can make a Mental save to resist, but on a failure it spends its next action carrying out the imperative.
- **Mind Light**: Your Open Mind art is enhanced.
While active, you become aware of the location of all living minds within sixty feet of you provided there isn’t a physical barrier between you and the subject and they are not actively hiding from you.
You can identify the species of these minds, if the species is known to you, any strong emotional states, and can recognize known individuals.
While Open Mind is active you cannot be surprised by a living creature.
- **Mind Over Matter**: Your mental control over your physical process es is considerable.
Commit Effort for the day as an Instant action to gain one of the following benefits: automatically stabilize if Mortally Wounded, go without sleep for twenty-four hours, heal 1d6 hit points of damage you’ve suffered, or completely ignore normal climatic extremes of heat or cold for one day.
Similar tricks of bodily control may be allowed at the GM’s discretion.
- **Mirror Mask**: You become instantly aware of any mind-affecting power directed at you and can identify the user if they’re within your current sensory range.
You may reroll any Mental saving throw the attacking ability allows you; if it doesn’t allow a save, you get a single Mental save to resist anyway.
If you succeed, you may Commit Effort for the day as an Instant action to give the power false information of your choice or a false impression of success.
- **Positive Association**: As a Main Action, Commit Effort for the day and target a visible sentient creature.
It gets a Mental saving throw to resist; on a failure, any thought of you or your actions is suffused by a glow of benign happiness and admiration.
They will treat you as an admired friend and find all your actions to be eminently reasonable and wise until you or your allies attack them, bring harm to something they love, or leave their presence for more than an hour.
Affected creatures that were not misused are unlikely to think themselves bespelled when this power ends unless given reason to.
This ability can affect only one target at a time.
- **Read Intent**: As an Instant action, Commit Effort for the scene and target a visible living creature.
It gets a Mental saving throw to resist; on a failure, the GM will tell you exactly what it is currently intending to do on its next turn, or its next relevant action if used out of combat.
If you perform a Total Defense action this round, your insight into its motions ensures that no attack it makes that you could theoretically physically avoid can hit you for the rest of the round.
- **Surface Apprehension**: Commit Effort for the scene as a Main Action while targeting a visible living creature.
If it fails a Mental saving throw, you become aware of its immediate surface thoughts for the rest of the round.
If you ask specific questions of the creature during this round it will instinctively think of the answers, though the detail of these surface thoughts is generally limited to one or two sentences of information.
- **Thoughts Like Razors**: As a Main Action, Commit Effort for the day to launch a psychic assault on any visible living creature within sixty feet.
The target gets a Mental saving throw to resist; on a failure, it takes 1d8 damage per level of the Thought Noble.
The damage can knock it unconscious but cannot kill.
On a successful save, the Thought Noble is stunned by the backlash, taking one quarter of the damage, rounded down, and losing their next turn’s actions.
This ability can be used only once per scene.
- **Unthinkable Thought**: Target a visible living creature and Commit Effort for the day as a Main Action while specifying a particular person, object, or situation that is currently ongoing in their presence.
The target may make a Mental save to resist; on a failure, they simply cannot consciously acknowledge the existence of the subject or its actions unless ignoring it would obviously put their life at risk.
This forced ignorance lasts for up to a scene; at its end, the target will have no memory of the subject.

#### Partial Expert/Thought Noble

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +1 | +1 Any             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 Any             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 Any             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 Any             |

#### Partial Other Mage/Thought Noble

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |    1d6-1 |           +0 | 1 Any       |
|     2 |    2d6-2 |           +0 | +1 Any      |
|     3 |    3d6-3 |           +0 |             |
|     4 |    4d6-4 |           +0 |             |
|     5 |    5d6-5 |           +1 | +1 Any      |
|     6 |    6d6-6 |           +1 |             |
|     7 |    7d6-7 |           +1 | +1 Any      |
|     8 |    8d6-8 |           +1 |             |
|     9 |    9d6-9 |           +1 |             |
|    10 |  10d6-10 |           +2 | +1 Any      |

#### Partial Warrior/Thought Noble

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +2 |                     |
|     4 |    4d6+8 |           +3 |                     |
|     5 |   5d6+10 |           +4 | +1 Any              |
|     6 |   6d6+12 |           +5 |                     |
|     7 |   7d6+14 |           +5 | +1 Any              |
|     8 |   8d6+16 |           +6 |                     |
|     9 |   9d6+18 |           +6 |                     |
|    10 |  10d6+20 |           +7 | +1 Any              |

#### Thought Noble Arts

| Level | Thought Noble Arts    |
|------:|-----------------------|
|     1 | Open Mind and Any One |
|     2 | Any One               |
|     3 | Any One               |
|     4 | Any One               |
|     5 | Any One               |
|     6 | Any One               |
|     7 | Any One               |
|     8 | Any One               |
|     9 | Any One               |
|    10 | Any One               |

### 1.5.17 The Vowed

Adepts of fist and bodily discipline, the Vowed are a partial Mage class dedicated to unarmed combat, physical training, and mental discipline.

#### Vowed Benefits

The Vowed Class Exists Only As a Partial Mage Class, To Be Taken By An Adventurer Along With Another Partial Class.
Thus, a Partial Warrior/Partial Vowed Might Be a Hardened Temple Warrior-Monk, a Partial Expert/Partial Vowed Might Be a Sage Teacher Of Religious Or Philosophical Truths, And a Partial Elementalist/Partial Vowed Might Be a Mountain Sage Who Wields The Elemental Forces Of Nature.
Regardless of classes, a Vowed’s hit dice can’t be worse than 1d6 per level thanks to **Martial Style**.

All Vowed gain a non-combat bonus skill appropriate to their order, acquiring it at level-0, or level-1 if they already have it at level-0.
Orders that focus on physical training might grant Exert, while scholarly ones might give Know, religious ones Pray, or occult sects grant Magic.
You may pick whatever non-combat skill suits your order, assuming the GM finds it reasonable.

Vowed do not learn how to cast spells.
Instead, they refine their inner powers and physical capabilities.
These arts are too precise and delicate to bear the encumbrance of heavy clothing, armor, or shields, and cannot be used when so burdened.
The Armored Magic Focus can mitigate this, but Vowed are all trained in effective unarmored defense techniques.

At first level, a Vowed gains the Martial Style, Unarmed Might, and Unarmored Defense arts, in addition to one more art of their choice as given on the adjacent list.
Further advancement will grant additional arts.

#### 1.5.17.1 Vowed Arts

Vowed Effort is based on the skill they chose to represent their order’s main focus of study, whether Exert, Know, Magic, Pray, or some more esoteric skill.

Their maximum Vowed Effort is equal to this skill level plus their best attribute modifier, whatever it may be, to a minimum of one point.
All Vowed automatically gain the Martial Style, Unarmed Might, and Unarmored Defense arts as part of their training.

- **Martial Style**: Regardless of class, your hit die can’t be worse than 1d6 per level.
When attacking with the Punch skill, your class hit bonus can be no worse than that of an Expert of your same char acter level.
At third level, any attack using the Punch skill counts as a magic weapon.
- **Unarmed Might**: Your unarmed attack damage increases as you gain levels, as noted on the chart.
You may add your Punch skill to the damage done by these attacks as usual, but Foci such as Unarmed Combatant that replace or improve your usual Punch damage do not apply to you.
- **Unarmored Defense**: When not wearing armor or using a shield, your base Armor Class is equal to 13 plus half your character level, rounded down.

#### Other Vowed Arts

- **Brutal Counter**: Commit Effort For The Scene As An Instant Action After Resolving An Enemy Melee Attack Against You, Whether It Hits Or Misses.
You May Make a Free Physical Attack Against Your Assailant, Using Either a Normal Attack Or Some Other Offensive Ability That takes no more than one Main Action to execute.
You cannot use this art more than once per any given attack, but you may use it while performing a Total Defense.
- **Faultless Awareness**: Your awareness is such that you cannot be surprised, and will even wake from a sound sleep in time to respond normally to some imminent peril.
- **Hurling Throw**: After you make a successful attack, you may Commit Effort for the scene as an Instant action.
The target must make a Physical saving throw or be thrown up to ten feet in any direction, falling prone on landing and suffering the damage rolled for your attack.
If the Physical save is successful, the target simply takes damage as normal.
This art can be used only once per round on any given target, and the target must be no larger than an ox.
- **The Inward Eye**: Commit Effort as an On Turn action.
For as long as it remains Committed, you are mystically aware of your surroundings with a sense equivalent to normal eyesight regardless of darkness, obscuring mists, or your actual eyes being closed or blinded.
- **Leap of the Heavens**: Commit Effort for the scene as a Move action to leap up to your full Move action horizontally or half that vertically.
You may also Commit Effort for the scene as an Instant action to negate falling damage, no matter the distance.
- **Master’s Vigor**: Your body retains its vigor and youthful vitality to the full normal span of your life.
You regain two lost hit points per hour due to your natural restorative powers.
- **Mob Justice**: As an Instant action, Commit Effort for the day to be come impervious to the Make a Swarm Attack maneuver.
Your assailants cannot use this maneuver against you for the rest of the scene, and you become immune to Shock as long as you remain in melee with at least two foes.
Making use of this benefit disrupts any spell casting you may attempt, however, due to the violent motion required.
- **Nimble Ascent**: Commit Effort for the scene as an On Turn action.
For the remainder of the scene you may move up vertical and over hanging surfaces and across difficult terrain at your full movement rate with no chance of slipping or falling, provided the surface is not glass-smooth or enchanted.
You require only one free hand to cling to a wall or ceiling.
- **Purified Body**: You may Commit Effort for the day as an Instant action to cure any disease or poison currently affecting you, or instead to negate any need for sleep, food, water, or air for the next 24 hours.
- **Revivifying Breath**: Commit Effort for the day as an On Turn action to heal yourself for 1d6 hit points plus your level.
This healing does not increase System Strain.
This ability may be used on your turn even when you are at zero hit points, but in such a case it Commits all your remaining Effort for the day.
This art can be used once per scene.
- **Shattering Strike**: Commit Effort for the day and take a full round of motionlessness to prepare.
On the next round, as a Main Action, your unarmed attack can shatter a wooden door, wooden wall, or other similar object up to a depth of one foot per level and a width sufficient to allow a man-sized creature to pass through.
At fourth level this ability improves to affect even a stone wall and at seventh it can affect even a metal wall or solid iron door.
The blow is useless against a target that can move, but against an immobilized creature the attack does 1d12 damage per character level.
- **Style Weaponry**: Pick three general classes of weapons, such as “swords”, “bows”, “axes”, “daggers”, or the like.
When using weapons of those classes, you may use Punch for hit rolls instead of Stab or Shoot.
Your Punch skill does not add to the damage done by these weapons, however, though the benefits of Martial Style apply to their hit rolls.
- **Unobtrusive Step**: You not only possess a considerable skill for disguise and obfuscation, you can become extremely hard to detect.
Once per scene, you may Commit Effort for the day as an Instant action to reroll a failed Sneak skill check or skill check related to impersonating someone else.

#### Partial Expert/Vowed

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +1 | +1 Any             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 Any             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 Any             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 Any             |

#### Partial Other Mage/Vowed

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |      1d6 |           +0 | 1 Any       |
|     2 |      2d6 |           +0 | +1 Any      |
|     3 |      3d6 |           +0 |             |
|     4 |      4d6 |           +0 |             |
|     5 |      5d6 |           +1 | +1 Any      |
|     6 |      6d6 |           +1 |             |
|     7 |      7d6 |           +1 | +1 Any      |
|     8 |      8d6 |           +1 |             |
|     9 |      9d6 |           +1 |             |
|    10 |     10d6 |           +2 | +1 Any      |

#### Partial Warrior/Vowed

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +2 |                     |
|     4 |    4d6+8 |           +3 |                     |
|     5 |   5d6+10 |           +4 | +1 Any              |
|     6 |   6d6+12 |           +5 |                     |
|     7 |   7d6+14 |           +5 | +1 Any              |
|     8 |   8d6+16 |           +6 |                     |
|     9 |   9d6+18 |           +6 |                     |
|    10 |  10d6+20 |           +7 | +1 Any              |

#### Vowed Arts and Unarmed Abilities

| Level | Punch Hit Bonus | Punch Damage | Punch Shock | Arts Gained                                                |
|------:|----------------:|-------------:|-------------|------------------------------------------------------------|
|     1 |              +0 |          1d6 | 1/15        | Unarmed Might, Unarmed Style, Unified Defense, and Any One |
|     2 |              +1 |          1d6 | 2/15        | Any One                                                    |
|     3 |              +1 |          1d8 | 2/15        |                                                            |
|     4 |              +2 |          1d8 | 2/15        | Any One                                                    |
|     5 |              +2 |         1d10 | 2/15        | Any One                                                    |
|     6 |              +3 |         1d10 | 3/15        | Any One                                                    |
|     7 |              +3 |       1d10+1 | 3/15        |                                                            |
|     8 |              +4 |       1d10+1 | 3/15        | Any One                                                    |
|     9 |              +4 |       1d10+2 | 4/15        |                                                            |
|    10 |              +5 |       1d10+3 | 4/15        | Any One                                                    |

### 1.5.18 The Warrior

The Warrior is a hero born to the blade, a man or woman gifted with a superb capacity for physical violence.
Savage barbarians, hardened mercenaries, courageous young farm boys, and ordinary laborers who just happen to have an undiscovered capacity for massive bloodshed all might qualify as Warriors.

Warriors aren’t all formal soldiers or recognized veterans of the blade.
Any adventurer who excels in dealing with their problems through violence might qualify as a Warrior, however peaceful their background.
It’s possible that their very talent for killing was what forced them out of their old life after some terrible event or awful encounter compelled them to recognize their gifts.

Warriors have more hit points than heroes of other class, and are capable of surviving wounds and hardships that would kill an ordinary man.
They’re also gifted with a superior attack bonus, and a native ability to inflict more damage than other PCs.
Full Warriors even have the ability to ensure a hit or force a miss by an enemy once per scene, making them lethal foes to common combatants.

#### Class Ability: Killing Blow

Whenever a Warrior Inflicts Damage With Any Attack, Spell, Or Special Ability They May Add Half Their Character Level, Rounded Up, To The Damage Done.
This Damage Is Also Added To Any Shock They May Inflict.

Combined with Foci meant to improve Shock attacks such as **Armsmaster**, **Close Combatant**, or **Shocking Assault**, this ability ensures that an experienced Warrior will almost always kill any ordinary human soldier or minor monster, regardless of their attack roll result.
Partial Warriors do not get this ability.

#### Class Ability: Veteran’s Luck

Once per scene, as an instant action, the warrior may turn a missed attack they have made into a hit.
Alternately, they may turn a successful attack against them into a miss, also as an instant action.
This ability is particularly lethal when used with the Make a Snap Attack action and leveled against weaker monsters or ordinary human warriors.

Only one exercise of this ability is possible in a scene, either to force a miss or ensure a hit on a foe; both options may not be employed in the same fight.

A Warrior may use this ability with crew-served weapons they are assisting in firing.
This ability cannot be used to negate environmental damage or damage done to a vehicle or mount they are riding.

Partial Warriors do not get this ability.

#### Full Warrior

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Any<br>+1 Warrior |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +3 |                     |
|     4 |    4d6+8 |           +4 |                     |
|     5 |   5d6+10 |           +5 | +1 Any              |
|     6 |   6d6+12 |           +6 |                     |
|     7 |   7d6+14 |           +7 | +1 Any              |
|     8 |   8d6+16 |           +8 |                     |
|     9 |   9d6+18 |           +9 |                     |
|    10 |  10d6+20 |          +10 | +1 Any              |

#### Partial Mage/Partial Warrior

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |    1d6+2 |           +1 | 1 Any<br>+1 Warrior |
|     2 |    2d6+4 |           +2 | +1 Any              |
|     3 |    3d6+6 |           +2 |                     |
|     4 |    4d6+8 |           +3 |                     |
|     5 |   5d6+10 |           +4 | +1 Any              |
|     6 |   6d6+12 |           +5 |                     |
|     7 |   7d6+14 |           +5 | +1 Any              |
|     8 |   8d6+16 |           +6 |                     |
|     9 |   9d6+18 |           +6 |                     |
|    10 |  10d6+20 |           +7 | +1 Any              |

#### Partial Warrior/Partial Expert

| Level | Hit Dice | Attack Bonus | Focus Picks                      |
|------:|---------:|-------------:|----------------------------------|
|     1 |    1d6+2 |           +1 | 1 Warrior<br>+1 Expert<br>+1 Any |
|     2 |    2d6+4 |           +2 | +1 Any                           |
|     3 |    3d6+6 |           +2 |                                  |
|     4 |    4d6+8 |           +3 |                                  |
|     5 |   5d6+10 |           +4 | +1 Any                           |
|     6 |   6d6+12 |           +5 |                                  |
|     7 |   7d6+14 |           +5 | +1 Any                           |
|     8 |   8d6+16 |           +6 |                                  |
|     9 |   9d6+18 |           +6 |                                  |
|    10 |  10d6+20 |           +7 | +1 Any                           |

### 1.5.19 The Wise

Some low-magic settings have a particular place for concepts that aren’t exactly magical, but have some special place in society.
Whether a mundane priest of the dominant faith, a feared forest warlock, or a sacred oracle of the gods, these Wise have a special role to fill.

Some of these Wise may have very minor or subtle magical powers suitable to a low-magic campaign setting.

In general, the Wise class is usually only viable in a low- or no-magic setting where their limited powers are still more than almost anyone else has access to.
If standard magic exists in your campaign the Wise may not offer enough to appeal to players.

#### Benefits Of The Wise

The Wise is a partial expert class that must be taken by an adventurer with another partial class.
It’s generally suited for low-magic campaigns, but strictly mundane versions of it may pass muster for a no-magic setting.

While the Wise count as a partial Expert class for other purposes, they do not gain the advantage of the partial Expert’s **Quick Learner** class ability or the bonus non-combat Focus that a partial Expert normally gets at first level.
An Adventurer who is a standard partial Expert/partial Wise does get these perks, however.

All Wise gain level-0 in a bonus skill appropriate to their concept, be it Pray, Know, Magic, Survive, or some other skill that makes sense to the GM.

The Wise do not use Effort.
All of their arts are either constantly in effect or can be used under particular circumstances or a particular number of times each day.

The arts the Wise learn are generally fixed by their concept and role.
A mundane priest will learn certain arts as they advance in level, while a witch or an oracular seer will learn others.
At some levels, the Wise might have a choice; they can pick whichever art they wish, but most arts can be taken only once.

The example tables here provide progression patterns for a non-magical priest of a faith, a witch with minor magical powers of cursing and foresight, and an oracle with limited powers of divination.
For GMs that wish to allow player-made concepts, the Esoteric column can be used, or a specific progression can be worked up in cooperation with the player.

#### 1.5.19.1 Wise Arts

#### General Arts

- **Dread Awe**: Your Kind Are Figures Of Fear And Mistrust.
Gain a +1 bonus on all skill checks related to intimidation or threat.
You can spend a Main Action cursing your foes in battle; those who hear suffer a -1 penalty to their next Morale check.
You can do this only once per scene.
- **Elite Ties**: You have ties with figures of societal importance.
You can get audiences with them with minimal effort.
Once per game session, get a favor from them that would not particularly inconvenience them to grant.
- **Erudite**: You are uncommonly learned.
Once per day, reroll a failed skill check related to intellectual pursuits or things a scholar would know about.
- **Folk-Friend**: Commoners and other lower-class natives consider you a useful healer and helper.
Once per day, get a modest favor from a commoner who doesn’t hate you in exchange for a blessing or bit of advice.
- **Healer**: You are a trained and capable healer.
Gain a +1 on all Heal skill checks; once per day, as an On Turn action, automatically succeed at a skill check to stabilize a Mortally Wounded subject.
- **Holy Sanctity**: You are honored and respected by most in this region.
Gain a +1 bonus on social skill checks with locals who are not naturally opposed to your religion or social institution.
- **Personal Impunity**: You bear signs of sanctity, and physically harming you is considered taboo in this region.
Only the most vicious or desperate humans will initiate violence against you, though any will fight if you attack them.
Hostile groups will hesitate to attack your companions unless provoked.
- **Skilled**: Gain three skill points to spend on any non-combat skills, up to the maximum skill level allowed by your character level.
This art may be taken more than once.

#### Divination Arts

- **Compel Truth**: The Diviner Compels a Person To Drink a Particular Brew, Participate In a Specific Ritual, Or Otherwise Undergo a Scene Long Ordeal.
Afterwards, The Diviner May Ask The Target a Single Question And The Gm Secretly Rolls a Cha/Magic Skill Check Against a Difficulty Of The Target’s Morale+1, Or Level+6 For PCs.
On a Success, The Subject Must Answer The Question Truthfully And Fully.
On a Failure, They May Give Any Answer They Wish.
The Same Or Substantially Same Question May Not Be Asked Twice With This Art.
- **Deliver Oracle**: A petitioner asks about the outcome of a particular course of action.
The diviner then spends a day in various rituals, seeking an oracular answer.
At the end, the GM makes a secret Wis/ Magic skill check.
If the skill check beats difficulty 8, an oracular sentence with some truth in it is produced.
If the skill check beats difficulty 12, a direct and obvious oracle is produced.
If the skill check is 7 or less, a hopelessly ambiguous oracle is produced, and if the dice roll a natural 2 an intentionally misleading oracle is delivered.
Oracles are usually delivered as metaphors, poetry, or visions.
The GM should ensure that some element of a true oracle actually does come to pass.
- **Find Object**: The diviner spends a scene performing a ritual to locate a particular animal, object or type of substance known to them, be it gold, water, or a specific stolen cow.
People cannot be found with this art.
The GM makes a secret Wis/Magic skill check against a difficulty of 8 to 11, depending on how close the nearest suitable object is; something in the same village is 8, something a mile or more away is 11, and something more than five miles away is undetectable.
If the check is successful, the diviner gets an impression of the direction the object is in.
On a failure, roll 1d12 and use it as a clock face to give a random direction.
This art can be used once per day.
- **Read Omens**: A petitioner proposes a course of action to the PC.
After a scene-long ritual involving at least 10 sp worth of livestock or sacrificial material, the GM makes a secret Wis/Magic skill check against difficulty 10.
On a success, the PC gets an impression of whether the course of action will lead to success or failure, as the GM thinks most likely.
On a failure, the GM gives a random result.
Multiplying the ritual costs by 10 adds +1 to the skill check, thus, a sacrifice of 1,000 sp worth of livestock gets a +2 on the check.
The same course of action cannot be divined more than once a month.

#### Curses And Blessings

The Bringing Of Fortune Or Ill Fate Is Not a Trivial Thing.
A Given Creature Can Be Cursed Or Blessed By One Of These Arts Only Once a Day.

- **Auspicious Undertaking**: As a Main Action, bless a particular plan or undertaking currently sought by the target.
During the plan’s execution or while seeking the desired end, the blessed subject can reroll one failed skill check or one missed hit roll.
The blessing lasts until it is used, the plan is finished, or one day per caster level has passed.
Only one plan can be blessed at a time.
- **Evil Eye**: This art improves your curse and blessing arts.
You can now use these arts as an Instant action, albeit still only one such art per round, and you need do no more than look at the target rather than vocalizing any particular curse or blessing.
- **Ill Fate**: As a Main Action, curse a visible target.
You may specify a particular kind of undertaking or apply the curse to anything they may do.
The next pertinent skill check they make is made at a -1 penalty.
If their raw die roll is equal or less than the PC’s Magic skill plus one their effort goes disastrously wrong.
The curse lasts until the next relevant skill check.
- **Luck Blessing**: As a Main Action, confer a blessing on a visible target.
The next skill check they make that day will have a +1 bonus.
Only one luck blessing can be in place for a creature at a time.
- **War Curse**: As a Main Action, curse a visible enemy.
They suffer a -2 penalty to hit rolls, damage rolls, and Shock damage for the rest of the scene.
If they miss an attack with a raw to-hit die roll less than the Wise’s Magic skill, they accidentally hurt themselves for normal damage.
Only one war curse can be applied at a time.

#### Partial Expert/Wise

| Level | Hit Dice | Attack Bonus | Focus Picks        |
|------:|---------:|-------------:|--------------------|
|     1 |      1d6 |           +0 | 1 Expert<br>+1 Any |
|     2 |      2d6 |           +1 | +1 Any             |
|     3 |      3d6 |           +1 |                    |
|     4 |      4d6 |           +2 |                    |
|     5 |      5d6 |           +2 | +1 Any             |
|     6 |      6d6 |           +3 |                    |
|     7 |      7d6 |           +3 | +1 Any             |
|     8 |      8d6 |           +4 |                    |
|     9 |      9d6 |           +4 |                    |
|    10 |     10d6 |           +5 | +1 Any             |

#### Partial Mage/Wise

| Level | Hit Dice | Attack Bonus | Focus Picks |
|------:|---------:|-------------:|-------------|
|     1 |      1d6 |           +0 | 1 Any       |
|     2 |      2d6 |           +0 | +1 Any      |
|     3 |      3d6 |           +0 |             |
|     4 |      4d6 |           +0 |             |
|     5 |      5d6 |           +1 | +1 Any      |
|     6 |      6d6 |           +1 |             |
|     7 |      7d6 |           +1 | +1 Any      |
|     8 |      8d6 |           +1 |             |
|     9 |      9d6 |           +1 |             |
|    10 |     10d6 |           +2 | +1 Any      |

#### Partial Warrior/Wise

| Level | Hit Dice | Attack Bonus | Focus Picks         |
|------:|---------:|-------------:|---------------------|
|     1 |      1d6 |           +1 | 1 Warrior<br>+1 Any |
|     2 |      2d6 |           +2 | +1 Any              |
|     3 |      3d6 |           +2 |                     |
|     4 |      4d6 |           +3 |                     |
|     5 |      5d6 |           +4 | +1 Any              |
|     6 |      6d6 |           +5 |                     |
|     7 |      7d6 |           +5 | +1 Any              |
|     8 |      8d6 |           +6 |                     |
|     9 |      9d6 |           +6 |                     |
|    10 |     10d6 |           +7 | +1 Any              |

#### Example Art Progressions For Different Types Of Wise

| Level | Mundane Priest            | Witch                               | Seer                   | Esoteric |
|------:|---------------------------|-------------------------------------|------------------------|----------|
|     1 | Holy Sanctity             | Dread Awe                           | Erudite or Folk-Friend | Any Art  |
|     2 | Dread Awe or Folk-Friend  | Any Curse or Blessing               | Any Divination         |          |
|     3 |                           |                                     |                        | Any Art  |
|     4 | Skilled or Erudite        | Healer or Any Curse or Blessing     | Any Divination         |          |
|     5 | Personal Impunity         | Any Divination                      | Any Divination         | Any Art  |
|     6 | Elite Ties or Folk-friend | Skilled                             | Skilled or Elite Ties  |          |
|     7 |                           |                                     |                        | Any Art  |
|     8 | Any General Art           | Any Divination or Curse or Blessing | Skilled or Elite Ties  |          |
|     9 |                           |                                     |                        | Any Art  |
|    10 | Any General Art           | Any Curse or Blessing               | Any Divination         |          |

## 1.6.0 Foci

Foci are special talents that a PC can possess.
They aren’t as powerful as Edges, but a PC will develop more of them over time.
Any PC can select any Focus, barring a few with specific requirements; they do not need to fit the class they selected.
So long as the player can explain how or why they acquired the talent they can have it.

Foci usually come in two levels, though some have only one.
The first time a Focus is chosen, the benefits of the first level are gained.
The second time it’s chosen, the benefits of the second level are added to those of the first.

A new character may choose one Focus of any kind.
Warriors and Partial Warriors may select another combat-related Focus, and Experts and Partial Experts may select another non-combat-related Focus.
If a Focus is ambiguous, the GM decides whether or not it falls into a particular category for the PC.

### 1.6.1 Focus List

The Foci below are common to most fantasy settings.
Others might be added to support particular campaigns.

#### Alert

You are keenly aware of your surroundings and virtually impossible to take unaware.

**Level 1**: Gain Notice as a bonus skill.
You cannot be surprised, nor can others use the Execution Attack option on you.
If the GM rolls initiative by sides, you can add a +1 bonus to your side’s initiative roll, though multiple Alert PCs don’t stack this bonus.
If you roll initiative individually, you can roll it twice and take the better result.

**Level 2**: You always act first in a combat round unless someone else involved is also this Alert.

#### Armored Magic

Usable only by mage heroes who would otherwise be prevented from casting spells or using arts while armored, this focus reflects special training in channeling magic through the hindering materials of conventional armor.

**Level 1**: You can cast spells or use arts while wearing armor that has an Encumbrance value of no more than two.
You can use a shield while casting, provided your other hand is empty for gesturing.

**Level 2**: You can cast spells while wearing armor of any Encumbrance.
You’ve also learned to cast spells while both your hands are full, though not bound.

#### Armsmaster

You have an unusual competence with thrown weapons and melee attacks.
This focus’ benefits do not apply to unarmed attacks or non thrown projectile weapons.
This focus’ bonuses also don’t stack with deadeye or other foci that add a skill’s level to your damage or shock.

**Level 1**: Gain Stab as a bonus skill.
You can Ready a Stowed melee or thrown weapon as an Instant action.
You may add your Stab skill level to a melee or thrown weapon’s damage roll or Shock damage, assuming it has any to begin with.

**Level 2**: The Shock from your melee attacks always treats the target as if they have AC 10.
Gain a +1 bonus to hit with all thrown or melee attacks.

#### Artisan

You have remarkable gifts as a crafter and can often improvise techniques even in fields unrelated to your usual background.
You are able to create mods for equipment even if you are not an expert.

**Level 1**: Gain Craft as a bonus skill.
Your Craft skill is treated as one level higher, up to a maximum of 5, for purposes of crafting and maintaining mods.
Mods you build require one fewer unit of arcane salvage, down to a minimum of one.
Your Craft skill is applicable to any normal crafting profession’s work, allowing you to fashion their wares without penalty.

**Level 2**: The first mod you add to an item requires no Maintenance and only half the silver piece cost usually required.
This benefit is in addition to the benefits of installing a mod in masterwork gear you build.
You automatically succeed at any attempt to build masterwork gear, and once per month you can reduce a created mod’s salvage cost by one further unit, down to a minimum of zero.

#### Assassin

You Are Practiced At Sudden Murder, And Have Certain Advantages In Carrying Out An Execution Attack.

**Level 1**: Gain Sneak as a bonus skill.
You can conceal an object no larger than a knife from anything less invasive than a strip search.
You can draw or produce this object as an On Turn action, and your point-blank thrown or melee attacks made during a surprise round with it cannot miss the target.

**Level 2**: You can take a Move action on the same round as you make an Execution Attack, closing rapidly with a target before you attack.
You may split this Move action when making an Execution Attack, taking part of it before you murder your target and part of it afterwards.
This movement happens too quickly to alert a victim or to be hindered by bodyguards.

#### Authority

You Have An Uncanny Kind Of Charisma About You, One That Makes Others Instinctively Follow Your Instructions And Further Your Causes.
At level 1, this is a knack of charm and personal magnetism, while level 2 might suggest latent magical powers or an ancient bloodline of sorcerous rule.
Where this Focus refers to followers, it means NPCs who have voluntarily chosen to be in your service.
PCs never count as followers.

**Level 1**: Gain Lead as a bonus skill.
Once per day, you can make a request from an NPC who is not openly hostile to you, rolling a Cha/Lead skill check at a difficulty of the NPC’s Morale score.
If you succeed, they will comply with the request, provided it is not significantly harmful or extremely uncharacteristic.

**Level 2**: Those who follow you are fired with confidence.
Any NPC being directly led by you gains a Morale and hit roll bonus equal to your Lead skill and a +1 bonus on all skill checks.
Your followers and henchmen will not act against your interests unless under extreme pressure.

#### Close Combatant

You’ve had all too much practice at close-in fighting and desperate struggles with drawn blades.
You’re extremely skilled at avoiding injury in melee combat, and at level 2 you can dodge through a melee scrum without fear of being knifed in passing.

**Level 1**: Gain any combat skill as a bonus skill.
You can use knife sized thrown weapons in melee without suffering penalties for the proximity of melee attackers.
You ignore Shock damage from melee assailants, even if you’re unarmored at the time, but invoking this benefit disrupts any spellcasting you might do that round due to the need for violently active evasion.

**Level 2**: The Shock damage from your melee attacks treats all targets as if they were AC 10.
The Fighting Withdrawal combat action is treated as an On Turn action for you and can be performed freely.

#### Connected

You’re remarkably gifted at making friends and forging ties with the people around you.
Wherever you go, you always seem to know somebody useful to your ends.

**Level 1**: Gain Connect as a bonus skill.
If you’ve spent at least a week in a not-entirely-hostile location, you’ll have built a web of contacts willing to do favors for you that are no more than mildly illegal.
You can call on one favor per game day and the GM decides how far they’ll go for you.

**Level 2**: Once per game session, if it’s not entirely implausible, you meet someone you know who is willing to do modest favors for you.
You can decide when and where you want to meet this person, but the GM decides who they are and what they can do for you.

#### Cultured Through

Wide travel, careful observation, or extensive study, you’ve obtained a wide experience of the cultures of your region and an ability to navigate their customs, laws, and languages.
You know what to do and say to impress others with the reasonableness of your wishes.

**Level 1**: Gain Connect as a bonus skill.
You can fluently speak all the common languages of your native region and convey at least basic information in the uncommon or esoteric ones.
You can learn a new language with only a week’s practice with a native speaker.
Once per game day, your polished ways automatically gain a minor favor from an NPC that would not put them to significant expense or risk, assuming the NPC isn’t hostile to you.

**Level 2**: Once per game session, reroll a failed social skill check as you use your cultural knowledge to push your interlocutor toward the desired result.

#### Die Hard

You are surprisingly hard to kill.
You can survive injuries or bear up under stresses that would incapacitate a less determined hero.

**Level 1**: You gain an extra 2 maximum hit points per level.
This bonus applies retroactively if you take this Focus after first level.
You automatically stabilize if Mortally Wounded, provided you have not been incinerated, dismembered, or otherwise torn apart.

**Level 2**: The first time each day that you are reduced to zero hit points by an injury, you instead survive with one hit point remaining.
This ability can’t save you from large-scale, instantly-lethal trauma.

#### Deadeye

You have a gift with ranged weapons.
While this talent most commonly applies to bows, it is also applicable to thrown weapons or other ranged weapons that can be used with the shoot skill.
For thrown weapons, you can’t use the benefits of the armsmaster focus at the same time as deadeye.

**Level 1**: Gain Shoot as a bonus skill.
You can Ready a Stowed ranged weapon as an Instant action.
You may use a bow or two-handed ranged weapon even when an enemy is within melee range, albeit at a -4 hit penalty.
You may add your Shoot skill level to a ranged weapon’s damage roll.

**Level 2**: You can reload crossbows or other slow-loading weapons as an On Turn action, provided they don’t take more than a round to reload.
You can use ranged weapons of any size in melee without penalty.
Once per scene, as an On Turn action when target shooting at an inanimate, non-creature target, you automatically hit unless you roll a 2 on your Shoot skill check or the shot is physically impossible.

#### Dealmaker

You have an uncanny ability to sniff out traders and find good deals, licit or otherwise.
Even those who might not normally be disposed to bargain with you can sometimes be persuaded to pause and negotiate, if you have something they want.

**Level 1**: Gain Trade as a bonus skill.
With a half hour of effort you can find a buyer or seller for any good or service that can be traded in the community, legal or otherwise.
Finding a marginally possible service, like an assassin willing and able to target a king, or some specific precious ancient artifact, may require an adventure if the GM allows it at all.

**Level 2**: Once per session, target a sentient who is not just then trying to kill you or your allies and make a request of it that it can comprehend.
If it’s at all plausible for it to make such terms, it will do so for a price or favor it thinks you can grant, though the price for significant favors might be dear.

#### Developed Attribute

Your hero has a remarkable degree of development to one or more of their attributes.
This may be derived from an eldritch bloodline, native brilliance, or sheer, stubborn determination.
This focus cannot be taken by heroes with the mage or partial mage classes.

**Level 1**: Choose an attribute; its modifier is increased by +1, up to a maximum of +3.
The actual score does not change, but the modifier increases, and may increase again if later advancement improves the attribute enough.
You can choose this Focus more than once to improve different attributes.

#### Diplomatic Grace

Your skill at personal negotiations is enormous and uncanny.
Some might even think it supernatural in nature.

**Level 1**: Gain Convince as a bonus skill.
You speak all the languages common to your region of the world and can learn new ones to a workable level in a week, becoming fluent in a month.
Reroll 1s on any skill check dice related to negotiation or diplomacy.

**Level 2**: Once per day, silently consecrate a bargain; the target must make a Mental save to break the deal unless their life or something they love as much is imperiled by it.
Most NPCs won’t even try to break it.
The deal must be for something specific and time-limited, and not an open-ended bargain.

#### Gifted Chirurgeon

You have an unusual gift for saving mortally wounded allies and quickening the natural recovery of the wounded in your care.

**Level 1**: Gain Heal as a bonus skill.
You may attempt to stabilize one Mortally Wounded adjacent person per round as an On Turn action.
When rolling Heal skill checks, roll 3d6 and drop the lowest die.
You heal twice as many hit points as usual when applying first aid after a battle.

**Level 2**: Your curative gifts count as magical healing.
You can heal 1d6+Heal skill in damage to an adjacent wounded ally as a Main Action, potentially reviving them without any lingering Frailty.
Each such application of healing adds 1 System Strain to the target, and the gift cannot be used on targets already at their maximum System Strain.

#### Henchkeeper

You have a distinct knack for picking up lost souls who willingly do your bidding.
You might induce them with promises of money, power, excitement, sex, or some other prize that you may or may not eventually grant.
A henchman obtained with this focus will serve in loyal fashion until clearly betrayed or placed in unacceptable danger.
Henchmen are not “important” people in their society, and are usually marginal sorts, outcasts, the desperate, or other persons with few options.

You can use more conventional pay or inducements to acquire additional henchmen, but these extra hirelings are no more loyal or competent than your pay and treatment can purchase.

**Level 1**: Gain Lead as a bonus skill.
You can acquire henchmen within 24 hours of arriving in a community, assuming anyone is suitable hench material.
These henchmen will not fight except to save their own lives, but will escort you on adventures and risk great danger to help you.
Most henchmen will have the combat statistics of a normal adult from their culture.
You can have one henchmen at a time for every three character levels you have, rounded up.
You can release henchmen with no hard feelings at any plausible time and pick them back up later should you be without a current henchman.

**Level 2**: Your henchmen are remarkably loyal and determined, and will fight for you against anything but clearly overwhelming odds.
Whether through natural competence or their devotion to you, they’re treated as 2 HD combatants from their culture.
You can make faithful henchmen out of skilled and highly-capable NPCs, but this requires that you actually have done them some favor or help that would reasonably earn such fierce loyalty.

#### Impervious Defense

Whether through uncanny reflexes, remarkable luck, supernatural heritage, or magical talent, you have natural defenses equivalent to high-quality armor.
The benefits of this focus don’t stack with armor, though dexterity or shield modifiers apply.

**Level 1**: You have an innate Armor Class of 15 plus half your char acter level, rounded up.

**Level 2**: Once per day, as an Instant action, you can shrug off any single weapon attack or physical trauma inflicted by a foe.
Environmental damage, falling damage, or other harm that couldn’t be forfended by strong armor cannot be resisted this way.

#### Impostor

You are exceedingly skilled at presenting yourself as something you are not, including disguises, voice mimicry, and lightning-fast ward robe changes.
Some impostors rely on the acting skills of perform, while others lean more to the nefarious tricks of sneak.

**Level 1**: Gain Perform or Sneak as a bonus skill.
Once per scene, reroll any failed skill check or saving throw related to maintaining an imposture or disguise.
Create one false identity of no great social importance; you can flawlessly pretend to be that person, such that only extremely persuasive proof can connect you with it.
You can change this identity with a week’s worth of effort in building a new one.

**Level 2**: You can alter your clothing and armor such that a single Main Action lets you swap between any of three chosen appearances.
In addition to your original false identity, you can establish a new false identity in each city or significant community you spend at least a day in.

#### Lucky

Some fund of remarkable luck has preserved your life at least once in the past, and continues to give you an edge in otherwise hopeless situations.
This luck does not favor the already-blessed; this focus can only be taken by a PC with at least one attribute modifier of -1 or less.

**Level 1**: Once per week, a blow or effect that would otherwise have left you killed, mortally wounded, or rendered helpless somehow fails to connect or affect you.
You make any rolls related to games of chance twice, taking the better roll.

**Level 2**: Once per session, in a situation of need or peril, you can trust to your luck and roll 1d6.
On a 2 or more, something fortunate will happen to further your goal, provide an escape from immediate peril, or otherwise give you an advantage you need, if not immediate victory.
On a 1, the situation will immediately grow much worse, as the GM sees fit.

#### Nullifier

Something about your hero interferes with easy use of magic on them.
It may be a strangely powerful birth blessing, a particular supernatural bloodline, or simple occult incompatibility.
This Focus cannot be taken by Mages or Partial Mages.

**Level 1**: You and all allies within twenty feet gain a +2 bonus to all saving throws against magical effects.
As an On Turn action, you can feel the presence or use of magic within twenty feet of you, though you can’t discern details about it or the specific source.
The first failed saving throw against a magical effect you suffer in a day is turned into a success.

**Level 2**: Once per day, as an Instant action, you are simply not affected by an unwanted magical effect or supernatural monstrous ability, even if it wouldn’t normally allow a saving throw.
Immunity to a persistent effect lasts for the rest of the scene.

#### Poisoner

You are a skilled poisoner, capable of compounding toxins out of readily-available flora and minerals.
It takes an hour to brew a poison, and you can keep as many doses fresh as you have levels.
Blade venoms take a Main Action to apply and last for ten minutes or until a hit or shock is inflicted, whichever comes first.
Detecting poisoned food is a Wis/Notice Skill Check against 10, or 12 if the diner’s not a noble or otherwise normally wary of poison.
One dose can poison up to a half-dozen diners.

**Level 1**: Gain Heal as a bonus skill.
Gain a reroll on any failed saving throw versus poison.
Your toxins inflict 2d6 damage plus your level on a hit or Shock, with a Physical save for half.
Your incapacitating or hallucinogenic toxins do the same, but those reduced to zero hit points are simply incapacitated for an hour.

**Level 2**: You are immune to poison and can apply a universal antidote to any poisoned ally as a Main Action.
Any attempt to detect or save against your poisons takes a penalty equal to your Heal skill.
Your ingested poisons count as an Execution Attack against unsuspecting targets, with Heal used for the Physical saving throw penalty and 1d6 damage per level done on a success.
Such poisons can be non-lethal at your discretion.

#### Polymath

You have a passing acquaintance with a vast variety of practical skills and pastimes, and can make a modest attempt at almost any exercise of skill or artisanry.
Note that the phantom skill levels granted by this focus don’t stack with normal skill levels or give a skill purchase discount.
Only experts or partial experts can take this focus.

**Level 1**: Gain any one bonus skill.
You treat all non-combat skills as if they were at least level-0 for purposes of skill checks, even if you lack them entirely.

**Level 2**: You treat all non-combat skills as if they were at least level-1 for purposes of skill checks.

#### Rider

Anyone with any level of ride skill can fight competently on horseback or keep their mount healthy.
You have an almost supernatural bond with your steeds, however, and can push them beyond normal limits.

**Level 1**: Gain Ride as a bonus skill.
Your steeds all count as Morale 12 in battle, use your AC if it’s higher than theirs, and can travel 50% further in a day than normal for their kind.
You can intuitively communicate with riding beasts, gaining as much information from it as its intellect can convey.

**Level 2**: Once per scene, negate a successful attack against your steed as an Instant action.
Once per scene, reroll any failed Ride skill check.
You can telepathically send and receive simple warnings, thoughts, and commands to and from your steed so long as it’s within two hundred feet.
You can so bond with one steed at a time, taking an hour to do so.

#### Shocking Assault

You’re extremely dangerous to enemies around you.
The ferocity of your melee attacks stresses and distracts enemies even when your blows don’t draw blood.

**Level 1**: Gain Punch or Stab as a bonus skill.
The Shock damage of your weapon treats all targets as if they were AC 10, assuming your weapon is capable of harming the target in the first place and the target is not immune to Shock.

**Level 2**: In addition, you gain a +2 bonus to the Shock damage rating of all melee weapons and unarmed attacks that do Shock.
As usual, regular hits never do less damage than this Shock would do on a miss.

#### Sniper’s Eye

You are an expert at placing a thrown knife or arrow on an unsuspecting target.
These special benefits only apply when making an execution attack with a bow, hurlant, or thrown weapon.

**Level 1**: Gain Shoot as a bonus skill.
When making a skill check for a ranged Execution Attack or target shooting, roll 3d6 and drop the lowest die.

**Level 2**: You don’t miss ranged Execution Attacks.
A target hit by one takes a -4 penalty on the Physical saving throw to avoid immediate mortal injury.
Even if the save is successful, the target takes double the normal damage inflicted by the attack.

#### Special Origin

Heroes in this game are assumed to be human, or close enough as to make no real difference.
PCs who want to belong to some more exotic species or demihuman kind can pick the origin focus appropriate to their chosen species, such as those given in the bestiary chapter for different types of creatures.

The availability of these special origins will depend on the campaign and the GM’s permission.
Even if elves and dwarves do exist in the campaign world, the GM is not obliged to let players use them as PCs if that choice doesn’t fit the tone or location being used.

Note also that a PC who just wants to be different without asking for any special mechanical benefits does not need to buy any special Focus.
If their particular demihuman or alien has no real advantages over a human, then they can just proclaim their nature as such, assuming the GM allows such beings in their campaign.

#### Specialist

You are remarkably talented at a particular skill.
whether a marvelous cat burglar, a famed athlete, a brilliant scholar, or some other savant, your expertise is extremely reliable.
You may take this focus more than once for different skills.

**Level 1**: Gain any skill as a bonus, except for Magic, Stab, Shoot, or Punch.
Roll 3d6 and drop the lowest die for all skill checks in this skill.

**Level 2**: Roll 4d6 and drop the two lowest dice for all skill checks in this skill.

#### Spirit Familiar

You have a minor spirit, devil, construct, magical beast, or other creature as a devoted companion.
While its abilities are limited, it is absolutely loyal to you.

**Level 1**: Choose a form for your familiar no smaller than a cat nor larger than a human.
It has the traits and abilities of an entity created by Calculation of the Evoked Servitor but may be summoned or dismissed as a Main Action, appearing within melee range of its owner.
It cannot carry objects with it during its vanishment aside from the clothing natural to its shape.
It has no need for food, water, or sleep.
If killed, it vanishes and cannot be re-summoned for 24 hours.
Once per day, it can refresh one point of Committed Effort for you.

**Level 2**: Pick two benefits from the list below for your familiar.
This level may be taken more than once, adding two additional options each time.
- It has hit points equal to three times your level
- It gains the ability to attack with a hit bonus equal to half your level, rounded up, doing 1d8 damage on a hit with no Shock
- It gains a +1 skill check bonus and can apply it to a range of situations equivalent to one normal human background
- It gains another shape of your choice which it can adopt or discard as an On Turn action
- It can hover or fly at its usual movement rate
- It can communicate freely with others in any language you know

#### Trapmaster

You have uncommon expertise in handling traps and snares, both mundane ones and the magical perils sometimes found in dungeons or the lairs of sorcerers.
You know how to improvise traps with materials you easily carry.

**Level 1**: Gain Notice as a bonus skill.
Once per scene, reroll any failed saving throw or skill check related to traps or snares.
Given five minutes of work you can trap a portal, container, passageway, or other relatively narrow space with foot snares, caltrops, toxic needles, or other hazards.
Non-lethal traps cause the first victim to trigger it to lose a round of actions while dangerous ones inflict 1d6 damage plus twice the character’s level, with an appropriate saving throw for half.
Only one such improvised trap can be maintained at a time.
More fearsome traps may be laid with congenial circumstances and the GM’s permission.

**Level 2**: You know secrets for unraveling even magical traps or arcane hazards that would normally require a wizard to dispel them.
Once per scene, your efforts count as an **Extirpate Arcana** spell against the trap or hazard, cast as if a Mage of twice your level, with any relevant skill check being Int/Notice or Dex/Notice.
This ability can be used against any stationary magical effect that’s susceptible to being dispelled by **Extirpate Arcana**.

#### Unarmed Combatant

Your empty hands are more dangerous than swords in the grip of the less gifted.
Your unarmed attacks are counted as melee weapons when it comes to binding up opponents wielding bows and similar ranged long arms, though you need at least one hand free to do so.

**Level 1**: Gain Punch as a bonus skill.
Your unarmed attacks become more dangerous as your Punch skill increases.
At level-0, they do 1d6 damage.
At level-1, they do 1d8 damage.
At level-2 they do 1d10, level-3 does 1d12, and level-4 does 1d12+1.
At Punch-1 or better, they have the Shock quality equal to your Punch skill against AC 15 or less.

**Level 2**: Even on a miss with a Punch attack, you do an unmodified 1d6 damage, plus any Shock that the blow might inflict on the target.

#### Unique Gift

Your hero has some unusual ability or magical knack that can’t be adequately described by an existing focus.
This choice is a catch all meant to represent a special power that’s in some way worth a Focus pick.

The exact effect of the ability should be defined by the player and the GM together, working out some result that seems fair and reasonable.
This will vary from table to table and from campaign to campaign; an innate ability to breathe water is little more than a novelty in a desert setting, while a campaign based on piracy in an endless archipelago might make it far more significant.

As with any power, the group should be willing to reconsider the gift if it turns out to be exceptionally weak in play or a stronger power than was anticipated.

#### Valiant Defender

You are a bodyguard, shieldbearer, or other gifted defender of others, accustomed to the roil of bloody battle and desperate struggle.
You have an exceptional ability to shield your allies from the attacks of those who would slay them.

**Level 1**: Gain Stab or Punch as a bonus skill.
Gain a +2 on all skill checks for the **Screen Ally** combat action.
You can screen against one more attacker per round than your skill would normally allow.
Once per round, you can **Screen Ally** against even intangible spells or magical attacks or bodily shield them from an area-effect explosion or magic.
Such attempts require the usual successful opposing skill check, with the assailant using their Magic skill.

**Level 2**: The first **Screen Ally** skill check you make in a round is always successful.
Gain +2 AC while screening someone.
You can screen against foes as large as ogres or oxen.

#### Well Met

You have a striking ability to charm and pacify people and creatures you’ve just met.
Once they get to know you, however, their opinions are more likely to be based on experience; this focus works only once on a target.

**Level 1**: Reaction rolls made by those the party meets are given a +1 bonus so long as you are present, whether or not you do the talking.
Even hostile encountered beings will usually give the party a round to parley before attacking unless they’re in ambush or have a clear reason for immediate violence.

**Level 2**: Once per game session, when a reaction roll is made, cause the subject to be as friendly and helpful to you and your party as it’s plausibly possible for them to be.
It’s up to the GM to decide why the creature becomes so; it might be mistaken about your nature, or find you hilarious, or perhaps want a favor from you and your allies.

#### Whirlwind Assault

You are a frenzy of bloody havoc in melee combat, and can hack down numerous lesser foes in close combat...
assuming you survive being surrounded.

**Level 1**: Gain Stab as a bonus skill.
Once per scene, as an On Turn action, apply your Shock damage to all foes within melee range, assuming they’re susceptible to your Shock.

**Level 2**: The first time you kill someone in a round with a normal at tack, either with its rolled damage on a hit or with the Shock damage it inflicts, instantly gain a second attack on any target within range using any Ready weapon you have.

#### Xenoblooded

You have been both blessed and cursed by some exotic supernatural or alien bloodline.

**Level 1**: Choose one set of benefits from the list below to reflect your alien heritage.
Other gifts may exist.
- You are immune to heat damage and can breathe and see through smoke without hindrance.
- You are water-adapted and can breathe water and see through it up to 120’ regardless of light.
You swim at double your normal Move rate.
- You were built to heavier or lighter gravity conditions; gain a +1 to either your Strength or Dexterity modifiers, to a maximum of +3, and a -1 penalty to the modifier of the other attribute.
- You are nourished by invisible radiations and need neither eat, sleep, nor breathe.
You can see clearly even in the absence of any light.

## 1.7.0 Final Character Creation Steps

The player now records their character’s final statistics and chooses their name and current goal.

### 1.7.1 Record Maximum Hit Points

Your character’s hit points measure their distance from defeat or death.
If your character is reduced to zero hit points, they are either dying or incapacitated based on the nature of the injury.

A new character rolls the hit die for their class, adding their Constitution modifier to it.
If they have chosen the Die Hard Focus they may add +2 to the roll.
The final value for a given die cannot be less than 1 hit point.

A character gains hit points as they advance in character level, rerolling their prior levels and taking the new score if it’s higher, as explained in the rules section.

### 1.7.2 Record Attack Bonus

Your character has a certain degree of basic combat competence based on their class.
This bonus increases as you advance in character levels and is added to your attack roll.

A new character’s attack bonus is usually +0, though Full Warriors start with a +1 base attack bonus.

### 1.7.3 Record Saving Throws

When faced with unusual dangers such as fireball explosions, toxic darts, pit traps, or magical curses, the character may need to make a saving throw to resist or mitigate the peril.
Saving throws are rolled on a d20 and are explained in the rules section.

**Physical** saving throws are used to resist exhaustion, disease, poison, or other biological harms.
A new character’s Physical save target is equal to 15 minus the better of their Strength or Constitution modifiers.

**Evasion** saving throws are used to avoid explosions, traps, or other dangers requiring fast reactions.
A new character’s Evasion save target is equal to 15 minus the better of their Intelligence or Dexterity modifiers.

**Mental** saving throws are used to resist intangible spells, mental attacks, or other tests of willpower or self-control.
A new character’s Mental save target is equal to 15 minus the better of their Wisdom or Charisma modifiers.

**Luck** saving throws are rolled when facing a danger that only blind chance can spare them from, such as landslide, bridge collapse, or a sniper’s random choice of victims.
A new character’s Luck save target is always 15.

A character’s save targets all decrease by 1 point each time they advance an experience level.

### 1.7.4 Pick a Free Skill

Your character has developed some side interest that may be unrelated to your background or class.
You can pick any one skill of your choice.
This skill pick is gained at level-0, or level-1 if it’s already level-0.
You cannot pick a skill that is already at level-1.

### 1.7.5 Mages Choose Starting Spells

Spellcasting full Mages begin play knowing four first-level spells and partial Mages begin play knowing two.
Adventurers with two partial spellcasting Mage classes, such as a partial Necromancer/partial High Mage know four.
These spells may be chosen from any spell list available to them.
A novice High Mage, for example, would pick first-level spells from the High Magic spell list, while a new Elementalist could pick them from either the High Magic or Elementalist spells.

### 1.7.6 Choose Starting Languages

Your PC begins play speaking the lingua franca of the campaign’s current city along with their native tongue if it happens to be different.
They also have fluency in additional languages based on their Know or Connect skills.
Either skill at level-0 grants one extra language, or two extra if it’s at level-1.
Thus, a PC with both Know-1 and Connect-1 skills could pick four additional languages.

PCs can learn additional languages to a conversational level by spending a few months immersed in it or studying it diligently during downtime.
Obtaining native fluency is at the GM’s discretion.

### 1.7.7 Choose Starting Gear

You can either pick a starting equipment package provided by your GM, or roll 3d6 x 10 to find out your starting silver pieces to spend on gear or keep in your pocket.
The starting packages will generally give you more equipment than the random roll would, but items can be swapped at the GM’s discretion.

### 1.7.8 Record Weapon and Armor Statistics

Now that you know what kind of weaponry or armor your hero has, take a moment to record the total hit bonus for your weaponry.
This is equal to your base attack bonus plus your relevant Stab, Shoot, or Punch skill, and the relevant attribute modifier for the weapon given on the weapon table.
If two attributes are listed for a weapon, use whichever is better for you.
If you lack even level-0 skill in the weapon, take a -2 hit penalty with it.

For each weapon’s damage and Shock, note down the information from the table.
You add your attribute modifier to both damage rolls and Shock.
Punch weapons or unarmed attacks can also add your Punch skill.

For your PC’s Armor Class, record the AC of the armor you usually wear.
Unarmored humans have an AC of 10.
Armor Class is modified by your Dexterity modifier.

### 1.7.9 Choose a Name and Goal

As final step, the player should pick a name and initial goal for the PC.
This goal can be anything so long as it gives a compelling reason for the PC to be seeking perilous adventure and associating with suspicious fellows.
The player must make up a good reason for the PC to be associating with the other players; it is not the GM’s job to justify the party’s existence, and if the player decides that their PC can’t reasonably run with the other party members it’s up to them to create a new character who can.

# 2.0.0 The Rules of the Game

This section summarizes the rules of the game.
They are intended to be functional for the average play group with typical needs; individual GMs may find it useful to alter them based on the specific interests or makeup of their own player group.

## 2.1.0 Scenes, Rounds, And Mission Time

During play, three special measures of time are used: scenes, rounds, and mission time.

### 2.1.1 Scenes

A scene is a time measurement used to determine how often certain abilities or actions can be taken.
Some powers can be triggered only so many times per scene, while some special abilities only work once per scene.

A scene is one particular fight, event, activity, or effort that usually doesn’t take more than ten or fifteen minutes.
A fight is a scene.
A chase is a scene.
A tense backroom negotiation is a scene.
So long as the PCs are doing the same general activity in the same general location, it’s probably one scene.
Most scenes don’t last more than fifteen minutes, though a GM can stretch this if it seems logical.

### 2.1.2 Rounds

Combat is made up of rounds, each one lasting approximately six seconds.
A single combat may involve multiple rounds of action.
A round begins with the actions of the side that wins initiative and ends after the actions of the side that lost initiative.

### 2.1.3 Turns

Sometimes it’s important to track the time of a more complex operation, like exploring a dungeon or navigating the trackless depths of some ancient ruin.
In such cases, the turn is a common measure of time.
Each turn lasts ten minutes and is equivalent to one scene for those situations when it matters.

## 2.2.0 Saving Throws

Saving throws are rolled to resist some unusual danger or chance hazard.
To make a saving throw, a person rolls 1d20 and tries to get equal or higher than their saving throw target.
Sometimes a save might have bonuses or penalties applied to the roll, but a natural roll of 1 on the die always fails the save, and a natural roll of 20 is always a success.

There are four types of saving throws.
Usually it will be obvious which type is most appropriate for a threat, but the GM can decide in marginal situations.

**Physical** saves resist exhaustion, poisons, diseases, or other bodily afflictions.
A PC’s Physical saving throw target is equal to 16 minus their character level and the highest of their Strength or Constitution modifiers.

**Evasion** saves apply when dodging explosions, avoiding traps, reacting to sudden peril, or other occasions where speed is of the essence.
A PC’s Evasion saving throw target is equal to 16 minus their character level and the highest of their Dexterity or Intelligence modifiers.

**Mental** saves apply when resisting mental attacks, insubstantial magic spells, psychological trauma, and other mental hazards.
A PC’s Mental saving throw target is equal to 16 minus their character level and the highest of their Wisdom or Charisma modifiers.

**Luck** saves are used when only blind chance can save a PC, regardless of their native abilities.
A PC’s Luck saving throw target is equal to 16 minus their character level, unmodified by their attributes.

### 2.2.1 NPC Saving Throws

NPCs have a single saving throw target equal to 15 minus half their rounded-down hit dice.
Thus, an NPC with 3 HD would have a saving throw target of 14+ for any particular hazard.
The GM may modify this in special circumstances, but it’s usually not worth tracking more closely.

## 2.3.0 Skill Checks

Most characters are skilled, competent men and women who are perfectly capable of carrying out the ordinary duties of their role.
Sometimes, however, they are faced with a situation or challenge beyond the usual scope of their role and the GM calls for a skill check.

To make a skill check, roll 2d6 and add the most relevant skill level and attribute modifier.
If the total is equal or higher than the check’s difficulty, the check is a success.
On a failure, the PC either can’t accomplish the feat at all, bad luck cheats them, or they achieve it at the cost of some further complication.
The GM determines the specific consequence of a failure.

If the character doesn’t even have level-0 in the pertinent skill, they suffer a -1 penalty to the roll.
In the case of particularly technical or esoteric skills they might not even be able to attempt the skill check at all.

The GM is always the one who calls for a skill check, and they do so at their discretion.
The player simply describes what their PC is attempting to do, and the GM will tell them what skill and attribute combination to roll.
If multiple skills or attributes might plausibly fit the action, the player can pick the one most favorable to them.
If the combination is only marginally relevant, but still reasonably plausible, it might suffer a -1 or -2 penalty at the GM’s discretion.

### 2.3.1 Skill Check Difficulties

The following difficulties ratings reflect common challenges.

| Difficulty | Skill Check                                                                                                                                                                   |
|-----------:|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 6          | A relatively simple task that is still more than the PC would usually be expected to manage in their regular background. Anything easier than this isn’t worth a skill check. |
| 8          | A significant challenge to a competent professional that they’d still succeed at more often than not.                                                                         |
| 10         | Something too difficult to be expected of anyone but a skilled expert, and even they might fail.                                                                              |
| 12         | Only a true master could expect to carry this off with any degree of reliability.                                                                                             |
| 14+        | Only a true master has any chance of achieving this at all, and even they will probably fail.                                                                                 |

Helpful or hostile circumstances can modify a skill check by -2 to +2.
Usually, no combination of situational modifiers should alter the roll by more than this, or else it becomes a near-foregone conclusion.
This does not include modifiers applied by gear mods, magic items, or PC aid.

### 2.3.2 NPC Skill Checks

When an NPC needs to make a skill check, they roll 2d6 and add their listed skill modifier if their action is something they ought reason ably to be good at.
If it isn’t, they roll at +0, or even at -1 if it seems like something they’d be particularly bad at doing.
If the NPC is special enough to have actual attribute scores and skill levels, they use those instead.

### 2.3.3 Aiding a Skill Check

To aid a comrade’s skill check, a player explains what their PC is doing to help.
If the GM agrees that it’s plausible, they may roll a relevant skill and attribute modifier against the same difficulty as the check they are aiding.
If they succeed, their ally gains a +1 on their skill check.
If they fail, no harm is done.
Multiple PCs can try to aid if their actions are plausible, but the total bonus can’t exceed +1.

Aiding a comrade is usually done in ways that let the aiding PC leverage their own special talents or skills.
A PC may not have the skills to attempt to Sneak past a vigilant guard, for example, but they might have a good Perform skill they can use to create a distraction that helps their comrade skulk past.

### 2.3.4 Opposed Skill Checks

When skills oppose each other, each participant makes a skill check and the winner is the one who rolls higher.
In cases of ties, the PC wins.
Thus, a PC trying to sneak past a guard might roll 2d6 plus their Dex/Sneak against the guard’s 2d6 plus their skill modifier.
If the guard was significant enough to actually have attributes and skill levels, it might be a Dex/Sneak challenge versus their Wis/Notice.

## 2.4.0 Combat

Violence is inevitable in most fantasy campaigns.
The rules below handle its most common manifestations.

### 2.4.1 The Combat Sequence

When combat begins, the fight progresses in the following sequence.
The sections below explain each step in the process.

First, each participating side rolls for initiative.
The side that rolled highest acts first.

Second, each member of a side gets to take their actions.
Members of a side act in whatever order they wish.
NPC sides act in whatever order the GM wishes.

Third, once every member of a side has acted, the side that rolled next-highest gets to act.
If NPCs have taken losses or are facing defeat, they may need to roll a Morale check as explained in section 5.2.0.
PCs never check Morale.

Fourth, once every side has acted the process repeats from the top in the same order.
Initiative is not re-rolled.

### 2.4.2 Combat Initiative

When combat begins, each side involved in it rolls initiative, rolling 1d8 and adding their group’s best Dexterity modifier.
NPCs usually add nothing.
The groups then act in order from highest to lowest rolls, with PC sides winning ties.
When the slowest group has acted, the round ends and a new round starts in the same initiative order.
Members of a side can act in any order the group agrees upon when it is that side’s turn to act, performing their allowed actions as explained in the section below.

#### 2.4.2.1 Individual Initiative

As an optional rule, the GM may use individual initiative.
In this case, each combatant rolls 1d8 individually, adding their Dexterity modifier, and acting in order from highest to lowest with PCs winning ties.
This leaves a group less likely to be caught flat-footed by enemies, but makes it harder for a group to coordinate actions.

#### 2.4.2.2 Surprise

If a group is caught entirely unawares they may suffer surprise, automatically granting their enemies a full free round of action be fore initiative can be rolled.
The GM decides when surprise applies, possibly calling for an opposed Dex/Sneak skill check versus the target’s Wis/Notice.
Groups cannot be surprised if they are actively anticipating the possibility of combat; at most, they might suffer an initiative penalty at the GM’s discretion.

#### 2.4.2.3 Automatic Initiative Powers

A PC with certain Foci or abilities may be immune to surprise or gain automatic initiative.
In such cases they automatically act first during a combat round, even if the rest of their side is slower.
If multiple combatants have these powers, they roll initiative normally amongst themselves to see which of them acts first.

### 2.4.3 Combat Action Types

Attacks, movement, spellcasting, and other combat activities all re quire one of the following four types of actions.

**Main actions** are a character’s primary action during a combat round, such as attacking an enemy, applying first aid to a downed ally, casting a spell, frantically evading incoming spears, or something else that takes less than six seconds to do.
A combatant gets one Main action per round.

**Move actions** involve moving the character’s normal movement rate of 30 feet or performing some other relatively brief bodily action, such as getting up from prone.
A combatant gets one Move action per round, but can spend their main action to get a second.

**On Turn actions** are brief, simple acts that require only a moment’s concentration.
Activating certain abilities or speaking a few words might constitute an On Turn action.
A character can take as many On Turn actions on their round as the GM finds plausible.

**Instant actions** are special, most being provided only by certain powers or certain special actions.
Instant actions can be performed even when it’s not your turn in the round, even after the dice have already been rolled.
The Veteran’s Luck class ability provides one such Instant action, allowing the PC to treat a missed attack roll as an automatic hit.
A PC can use as many Instant actions in a round as the GM finds plausible.
Instant actions performed at the same time are resolved simultaneously, with the GM adjudicating any ambiguities.

### 2.4.4 Common Combat Actions

The actions listed below are merely some of the most common taken in combat.

**Make a Melee Attack (Main Action)**: Attack a target in melee range with an unarmed attack or melee weapon.
Such weapons use either the Punch or the Stab skill, depending on the type of attack.

**Make a Ranged Attack (Main Action)**: Attack a target with a bow or thrown weapon.
The Shoot skill is used for these attacks, though Stab or Exert can optionally be used for thrown weapons.
If there is an enemy attacker in melee range, one-handed ranged weapons and thrown weapons suffer a -4 penalty to hit, while bows and other two-handed ranged weapons cannot be fired at all.

**Make a Snap Attack (Instant Action)**: As an Instant action, give up your Main Action and either Make a Melee Attack or Make a Ranged Attack at a -4 penalty to hit.
As an Instant action, you can Make a Snap Attack even when it’s not your turn, but you must not have taken your Main Action this round yet.
Only well-trained and disciplined NPCs have enough focus to Make a Snap Attack.

**Make a Swarm Attack (Main Action)**: Target an enemy within range of your weapon and take this action until up to four allies have Made a Swarm Attack on that target this round.
At that point or any point beforehand, one of these assailants can Make a Melee Attack or Make a Ranged Attack on the target with a +2 bonus to hit and +1 bonus to damage for every other assailant, up to a maximum bonus of +6 to hit and +3 damage.
This bonus damage does not add to the attack’s Shock and cannot make it do more than its usual maximum damage.
Any Shock inflicted by this attack is always applicable, however, even if the target’s AC is too high, they’re using a shield, or have some power that makes them immune to Shock; the damage a Swarm Attack does isn’t really Shock, but a reflection of the inevitable hazards of being swarmed by numerous armed foes.

**Charge (Special Action)**: Spend both your Main Action and your Move action to move up to twice your normal movement rate in a straight line, making a melee or thrown ranged attack at the end of it with a +2 bonus to hit.
You must be able to charge at least 3 meters to build up sufficient momentum and you suffer a -2 penalty to your Armor Classes until the end of the round.

**Screen an Ally (Move Action)**: Move up to your normal movement rate to get adjacent to an ally.
You then physically block attacks against them until the start of your next turn, provided they remain within 3 meters of you.
Enemies who attack your ward must make a successful opposed combat skill check against you using either Str or Dex and the most applicable combat skill.
If the enemy succeeds, their attack targets your ward normally.
If you succeed, their attack instead targets you.
You can screen against a number of attackers each round equal to your highest combat skill; thus, you need at least level-1 in a combat skill to successfully screen.
Multiple defenders can screen the same target, in which case the opposed skill check is compared to all defenders and targets the worst-rolling successful defender.
You can only screen against attacks you could feasibly physically parry or body-block.

**Total Defense (Instant Action)**: Give up your Main Action to focus entirely on dodging and evading incoming perils.
Your Melee and Ranged Armor Classes increase by +2 and you become immune to Shock until the start of your next turn, including the otherwise-unavoidable damage from a Swarm Attack.
You cannot take this action if you have already spent your Main Action for the round.

**Run (Move Action)**: Move your normal movement rate in combat, which is 30 feet for an ordinary human.
If you start your movement adjacent to an armed melee combatant, they get a free melee attack against you as you flee.
To avoid this, you must make a Fighting Withdrawal first.

**Make a Fighting Withdrawal (Main Action)**: Disengage from an adjacent melee attacker, allowing you to move away from them without incurring a free attack as you retreat.
You do not actually leave melee range with this action alone, and your enemy can simply re-engage you next round if you don’t actually take a move action to retreat.

**Use a Skill (Main Action)**: Perform first aid on a downed comrade, cry out an appeal for parley, or otherwise use a skill that wouldn’t normally take more than six seconds.

**Ready or Stow an Item (Main Action)**: A character can Ready an item for use from their pack or stowage or Stow it, as per the encumbrance rules.
Sheathing or holstering a Readied weapon without actually Stowing it does not require this action, though the GM may disallow rapid weapon swaps if they start to become implausible.

**Reload a Weapon (Main Action)**: Reload a crossbow from a Readied case of quarrels.
Bows and crossbows may be reloaded as an On Turn action if the shooter has at least Shoot-1 skill; otherwise it’s a Move action to nock a new arrow in a bow.

**Drop an Item (Instant Action)**: Drop an item you are holding.
This may be done at any time to free up a hand.

**Pick up an Item (Move Action)**: Scoop up a dropped item within melee range, leaving it Readied in your hand.

**Stand Up (Move Action)**: Rise from a prone position, picking up any dropped items as you do so.

**Go Prone (On Turn Action)**: Fall prone, applying a -2 penalty to ranged attacks against you and a +2 bonus to melee-range attacks against you.
Your normal movement rate is halved while you remain prone.

**Hold An Action (Move Action)**: Spend your Move action to delay acting on your side’s turn.
You may trigger the rest of your turn’s actions as an Instant action at any point until the end of the round, after which they are lost.
If your held action is taken in response to someone else’s action, yours resolves first.

### 2.4.5 Combat Attack Rolls

When an assailant makes an attack, they roll 1d20 and add their base attack bonus, the weapon’s relevant attribute modifier, and their relevant combat skill level.
If they lack even level-0 in the appropriate combat skill, they apply a -2 penalty to the roll.
If the total is equal or greater than the target’s relevant Melee or Ranged Armor Class, they hit.
If less, they miss.

Every weapon listed in the equipment section is listed as using one or more attributes, such as either Str or Dex for a dagger.
The attacker may choose either attribute for modifying the weapon’s attack and damage rolls.

#### 2.4.5.1 NPC Attack Rolls

NPCs usually do not have attribute modifiers or skill levels.
Instead, the attack bonus of a trained NPC combatant is usually equal to their hit dice, often with an additional bonus to reflect particularly good training or talent.

#### 2.4.5.2 Attack Roll Modifiers

Some common situations can modify an attack roll, granting a bonus or penalty.
GMs may add others depending on the situation.

| Situation                                                                                   | Mod |
|---------------------------------------------------------------------------------------------|----:|
| Shooting at a distant prone foe                                                             |  -2 |
| Attacking an adjacent prone foe                                                             |  +2 |
| Melee attacking while prone                                                                 |  -4 |
| Your target is past your bow or thrown weapon’s normal range, up to its maximum long range. |  -2 |
| The target is at least half behind cover                                                    |  -2 |
| The target is almost completely in cover                                                    |  -4 |
| Making a thrown attack while in melee                                                       |  -4 |
| Throwing a weapon while in melee                                                            |  -4 |
| Shooting a bow or crossbow while in melee                                                   | N/A |
| You are shooting at a target you can’t see but you know where they are.                     |  -4 |
| You are shooting at a target you can’t see and don’t know their exact position.             | N/A |

### 2.4.6 Damage and Shock

If an attack hits, it inflicts hit point damage equal to the weapon’s damage die plus the weapon’s relevant attribute modifier.
Special weapon mods or abilities may increase this damage.

#### 2.4.6.1 Non-Lethal Damage

You may attack non-lethally with an appropriate weapon or unarmed attack.
Your attacks will only incapacitate the target if you reduce them to zero hit points.

#### 2.4.6.2 Punch Weapon Damage

If you are making a purely unarmed attack you may add your Punch skill to the damage.
You may not add the skill to the damage done by artificial weaponry that uses the Punch skill.

#### 2.4.6.3 Trauma

Trauma Dice are optional in the typical fantasy setting of this game, though GMs may choose to use them if they wish to further increase the peril of combat.
Statistics for weapon Trauma Dice and armor Trauma Target improvements are given in the Cities Without Number SRD.

If you use this rule, then when you hit with a weapon or lethally-intended unarmed attack, roll the weapon’s associated Trauma Die.
If it equals or exceeds the victim’s Trauma Target, which is usually 6 for a normal unarmored human, you have inflicted a Traumatic Hit.

Traumatic Hits multiply the total damage of the hit by the weapon’s listed Trauma Rating.
Thus, if a shotgun with a x3 Trauma Rating would normally have done 9 damage in total, it instead does 27.
If this damage or any later damage in the same fight reduces the victim to zero hit points, they risk a Major Injury.

Some abilities or heavy armor may increase a subject’s Trauma Target.
Some other abilities might grant a bonus to the Trauma Die roll.
To speed the process, it’s generally best to roll the Trauma Die at the same time as the attack or damage roll.

Vehicles and other inanimate objects are immune to Traumatic Hits from weapons that could not reasonably inflict catastrophic structural damage on them.

#### 2.4.6.4 Shock

Some melee weapons inflict Shock on a missed attack roll.
This dam age reflects the inevitable harm a poorly-armored combatant suffers when engaging in armed combat.
Shock for a weapon is recorded as a point value and target Armor Class, such as “Shock 2/15”.
If the wielder misses a target with this weapon that has a Melee Armor Class equal or less than the weapon’s Shock rating, they suffer the listed amount of damage anyway.
Thus, if that weapon were to miss a victim with Melee AC 13, it would still do 2 points of damage.

Some attacks apply Shock on a miss regardless of the target’s Armor Class.
This benefit may be granted by certain abilities, or it may be part of a dangerous NPC’s talents.
Such Shock ratings are recorded with “-” as the affected AC, such as “Shock 5/-”.
This automatic Shock is still negated by shields or abilities that grant a subject immunity to Shock.

The only modifiers that add to Shock damage are the wielder’s relevant attribute modifier for the weapon and any damage bonuses that explicitly add to Shock.
Thus, the Killing Blow class ability adds to Shock because it specifically says so, while a weapon mod that merely says it adds +2 damage would not.

A person using a shield can ignore the first source of Shock they would normally suffer in a round.
Some other Foci or special actions such as Total Defense can also render a subject immune to Shock.

An attack that hits can never do less damage than the Shock that would have been inflicted on a miss.
If using the Trauma rules, dam age inflicted by Shock cannot cause a Traumatic Hit.

### 2.4.7 Special Combat Maneuvers

There are certain special maneuvers or activities that commonly arise in combat.

#### 2.4.7.1 Shoving and Grappling

To shove a target the attacker must make a successful melee attack.
This attack does no damage, but forces an opposed Str/Exert or Str/Punch skill check.
If the attacker wins, the target is shoved back up to 3 meters or knocked prone at the attacker’s discretion.

To grapple, the attacker must make a successful unarmed melee attack while having both hands free.
This attack does no damage but forces an opposed Str/Punch skill check.
If the attacker wins, the victim is grappled.
A grappled victim remains so until they take a Main Action to perform a successful opposed Str/Punch skill check against their assailant.

While grappled, neither the assailant or the target can move from their location, nor can they fight with anything but unarmed attacks, including fangs or claws for creatures equipped with such.
At the end of each round, a grappled victim automatically suffers damage as if hit by their assailant’s unarmed attack.

If the attacker wishes to move the grappled target, they must spend a Main Action and make an opposed Str/Punch skill check.
On a success, they move the target up to 10 feet along with them or throw them 5 feet and leave them prone.
On a loss or tie, the target escapes.

An attacker can grapple only one target at a time, but a defend er can be grappled by multiple assailants, within reason.
Any skill checks forced on a multiply-grappled target are compared against all assailants, and win only if all assailant rolls are beaten.

These rules assume both assailant and target are relatively human-sized.
Grappling or shoving humanoid but substantially larger targets is done with a -2 penalty on all skill checks, while trying to handle quadrupeds or those only barely plausible to wrestle is done at a -4 penalty.

#### 2.4.7.2 Dual-Wielding Weapons

Some attackers prefer to use two weapons at once.
PCs who wish to do so must have at least level-1 in the relevant weapon skills, such as Stab-1 and Shoot-1 for dual-wielding a knife and hand crossbow.

When making an attack while dual-wielding, the attacker chooses which weapon they wish to use, rolling the attack roll accordingly.
On a hit, the weapon does +2 damage so long as the target is within range of both wielded weapons.
This bonus does not add to Shock.

Managing two weapons at once is difficult, and applies a -1 penalty to all hit rolls.

#### 2.4.7.3 Execution Attacks

A target that is entirely unsuspecting of danger is subject to execution attacks.
A subject that is expecting danger or alert to potential harm cannot be targeted by an execution attack.

A ranged execution attack requires one full minute of aiming, waiting, and adjusting on the part of the would-be sniper.
Any disturbance during this time will spoil the shot.
After spending this time, the assassin may make a Dex/Shoot skill check.
The difficulty is 6 for an attack within two meters, 8 for an attack within the weapon’s normal range, or 10 for one at the weapon’s long range.
On a success, the attack hits; the victim’s Armor Class is ignored.

A melee execution attack requires one full minute of near proximity to the target, watching for just the right opening and getting to within melee range of the victim.
If this time is granted, the assassin may make a melee attack, automatically hitting.

When a target is hit with an execution attack they must make a Physical saving throw at a penalty equal to the assailant’s combat skill.
On a failure, they are immediately reduced to zero hit points and Mortally Wounded, or knocked unconscious if the weapon was non-lethal.

If they succeed on the save, they still take maximum damage from the hit.
If using the Trauma rules, damage is rolled normally but successful execution attacks always count as Traumatic Hits, so the ensuing multiplied damage or Major Injury may be enough to kill in of itself.

## 2.5.0 Injury, Healing, And System Strain

Injury is almost inevitable in an adventurer’s career.
Some forms of it can be longer-lasting than others.

### 2.5.1 Mortal Injury and Stabilization

When a PC is reduced to zero hit points by a lethal attack, they are Mortally Injured.
They will die at the end of the sixth round after their incapacitation unless stabilized by an ally or some special ability.
A Mortally Wounded character is helpless, and can take no actions and do nothing useful.

Stabilizing an ally is usually a Main Action that requires a Dex/Heal or Int/Heal skill check.
The difficulty is 8 plus the number of full rounds since the target fell.
If the medic lacks a healer’s kit or other tools, this difficulty is increased by 2.
Only one ally can try to stabilize a victim per round, though others can attempt to aid their check, but attempts may be retried each round for as long as hope lasts.

Once stabilized the victim remains incapacitated for ten minutes before recovering with 1 hit point and the Frail condition.
They may act normally after they recover, but if they are reduced to zero hit points again while still Frail, they die instantly.
Frailty is removed by a week of bed rest and medical care.
A physician can also make one attempt to remove Frailty with a healer’s kit and an hour of work, rolling a Dex/Heal or Int/Heal skill check against difficulty 10.

#### 2.5.1.1 NPCs and Mortal Injury

NPCs who aren’t important enough to merit a name usually die instantly when reduced to zero hit points.

#### 2.5.1.2 Catastrophic Damage

Targets reduced to zero hit points by some injury or cause that could not be reasonably survivable are instantly killed.
An arrow hole might be patched; a direct hit with a house-sized boulder or a plunge off a thousand-foot precipice is less survivable.
What counts as “not reasonably survivable” may vary with inhumanly durable targets.

#### 2.5.1.3 Non-Lethal Incapacitation

If a target is brought to zero hit points by a non-lethal attack, they are incapacitated for ten minutes before regaining 1 hit point.
They do not become Frail.

### 2.5.2 System Strain

Magical forms of healing or use of powerful augmenting magic can take a toll on a user’s physiology.
Their System Strain total reflects the total amount of stress their body has undergone.

A healthy character normally starts at zero System Strain and has their Constitution score as their allowed maximum.
A character cannot accumulate more than this maximum in System Strain.

Magical healing and certain spells and abilities will add to a subject’s System Strain.
If this addition would put them over their maximum they cannot activate the spell, benefit from healing, or otherwise gain any use from the ability.
If they are forced over the maximum by some unavoidable effect, they are instead knocked unconscious for at least an hour.

Characters lose one point of accumulated System Strain after each night’s rest, assuming they are warm, fed, and comfortable and can get at least eight uninterrupted hours of sleep.
Cold camps, stony bedding, and other sources of privation prevent this recuperation.

### 2.5.3 Natural Healing

A wounded creature can recover hit points by getting a good night’s rest and adequate food.
Provided they are warm, fed, and comfort able, they regain hit points each morning equal to their experience level, or equal to their hit dice if they are NPCs.
Characters suffering some form of privation do not recover hit points through sleep.

Frail creatures do not recover hit points through natural healing.
They must cure their Frail condition first or rely on pharmaceuticals.
Removing the Frail condition requires a full week of bed rest and the medical attention of someone with at least Heal-0 skill and a healer’s kit.
Frail victims without this level of medical care must make a Physical save after a week; on a failure they die sometime in the next week, while success means they lose their Frailty after another month’s rest.

### 2.5.4 First Aid

Healers can patch up victims in a hurry, albeit at a cost to their physical resilience.
By spending one minute patching up an ally with a healer’s kit, a healer can heal 1d6 points of damage plus their Heal skill.
If they lack any Heal skill at all, they restore 1d6-1 points.
Each such application of first aid adds one System Strain to the target.
First aid can restore hit points to a Frail target, but it cannot remove their Frailty.

One ten-minute turn is enough time for a healer to apply as much first aid as is wanted to the rest of their party.

### 2.5.5 Poisons and Diseases

Most toxins force a victim to make a Physical saving throw to resist their effects or mitigate their harm.
Weak perils might grant as much as a +4 to the saving throw, while dire threats might apply a -4 penalty.

If the save is failed, the poison or disease takes hold.
Most poisons act quickly, inflicting hit point damage, adding System Strain to the target, or applying long-lasting penalties.
Diseases can have a slower onset but often apply the same sort of harms.

A medic who gets to a poisoned person within a minute of the poisoning can use a healer’s kit to give them a better chance to resist.
They may add twice their Heal skill level to the victim’s saving throw roll, or +1 if they have only Heal-0 skill.
Specialized antitoxins may be able to neutralize such poisons entirely.

## 2.6.0 Chases And Pursuit

Adventurers have a habit of chasing after others and being chased in turn.
The specific rules used vary depending on whether it’s a foot chase or mounted pursuit.

### 2.6.1 Foot Chases

The group member in the fleeing party with the best Dex/Exert or Con/Exert total rolls a skill check.
Their result is the fleeing group’s pace, as they help and encourage the slower members.

Other fleeing group members then hinder pursuit in whatever ways they think are helpful.
Sometimes a skill check is needed, while other times a GM will simply decide it works.
Each successful effort adds a +1 bonus to the pace, up to +3 maximum.
Botched efforts are either unhelpful or apply a -1 penalty if they’re actively harmful.
If the fleeing group is made up of NPCs, it’s the GM’s judgment as to whether any of them try to do something clever to stall the PCs.

The pursuing group then makes a single Dex/Exert or Con/Exert skill check, modified by the table below.
If they beat the fleeing group’s total they catch up to them, and if they tie or roll less the fleeing party has escaped immediate pursuit.

| Situation                                 |       Mod |
|-------------------------------------------|----------:|
| There are more pursuers than pursued      |        -1 |
| The pursued have no head start at all     |        +2 |
| “ have one round’s head start             |        +1 |
| “ have less than a minute’s head start    |        +0 |
| “ have more than a minute’s head start    |        -2 |
| Who knows the local terrain better?       | -2 to  +2 |
| The pursuit is half-hearted or obligatory |        -1 |
| The pursuers are enraged or vengeful      |        +1 |

### 2.6.2 Mounted Chases

For mounted or vehicular chases, the fleeing rider makes a Ride skill check, usually modified by Dexterity.
This is the fleeing mount’s pace.

Each pursuer then makes its own Dex/Ride skill check to catch up with the quarry, modified by the table below.
Any of them who don’t equal or exceed the fleeing mount’s pace fall behind and are lost from the pursuit.
Any of them who do make the roll catch up to the mount, and it usually becomes a matter of combat until the quarry can make another escape attempt or win the ensuing battle.

Unmounted pursuers cannot generally hope to catch up with mounted evaders over a short-term chase.

| Situation                                   | Modifier |
|---------------------------------------------|---------:|
| The pursuer can’t directly see the pursued  |       -2 |
| The pursuer is flying but the pursued isn’t |       +3 |
| The pursued is flying but the pursuer isn’t |       -3 |
| A spotter is relaying the target’s position |       +1 |
| Who knows the local terrain better? -2 to   |       +2 |
| The pursuit is half-hearted or obligatory   |       -1 |
| The pursuers are enraged or vengeful        |       +1 |

## 2.7.0 Character Advancement

Characters accumulate experience points through successful ad venture completion or other activities appropriate to the campaign’s focus.
By default, PCs gain three experience points for an average successful mission.
When enough experience points have been accumulated, they advance an experience level.
New characters begin at first level and can rise to a maximum of tenth under the default rules.

The requirements listed below are for “fast” campaigns, where PCs advance in level relatively rapidly, and “slow” campaigns, where the advancement is more measured.
Individual GMs may alter these rates to suit their table’s needs.
Experience points do not reset on leveling up; the totals listed are total points accumulated.

### Experience Point Requirements

| Level | Fast | Slow |
|------:|-----:|-----:|
|     1 |    0 |    0 |
|     2 |    3 |    6 |
|     3 |    6 |   15 |
|     4 |   12 |   24 |
|     5 |   18 |   36 |
|     6 |   27 |   51 |
|     7 |   39 |   69 |
|     8 |   54 |   87 |
|     9 |   72 |  105 |
|    10 |   93 |  139 |

### 2.7.1 Advancement Benefits

Whenever a character advances a level, they obtain certain benefits.

#### 2.7.1.1 Additional Hit Points

To determine their new maximum hit points, they roll their hit die for each level they now possess, adding their Constitution modifier to each die, and a further +2 if they have the **Die Hard** Focus.
No individual die can be reduced below 1 point, even with a Constitution penalty.
If the total roll is greater than their current maximum hit points, they take the roll.
If less or equal, their maximum hit points increase by one.

#### 2.7.1.2 Improved Saving Throw

Their saving throw scores decrease by one, making it easier to succeed on saving throws by rolling equal or over it.
As a first level character has saving throw scores of 15, reaching second level would lower them to 14, modified by their appropriate attributes.

#### 2.7.1.3 Improved Attack Bonus

A PC’s base attack bonus improves according to their level and their chosen class.

#### 2.7.1.4 Gaining and Spending Skill Points

A PC who advances a level gains three skill points they can spend on improving their skills or save to spend later.
Experts and Partial Experts with the **Quick Learner** class ability gain an extra bonus skill point to spend, giving them four points each time they advance.

Skills that are gained or improved immediately on gaining a level are assumed to have been perfected over the past level and require no training time or teaching.
If they save their skill points to spend them later then they’ll need to find some teacher or other explanation for developing them in the meanwhile.

The cost for improving a skill is listed below.
Every skill level must be purchased in order; to gain level-1 in a skill you need to pay one point for level-0 and then two points for level-1.
A PC must be the requisite minimum level to increase a skill to certain levels.
Less hardened adventurers simply don’t have the focus and real-life experience to attain such a pitch of mastery.

A PC cannot “partially buy” a skill level.
If they don’t have enough skill points to buy a new level, they need to save them up until they can.
A PC cannot develop skills beyond level-4.

| Skill Level | Point Cost | Min. Character Level |
|------------:|-----------:|---------------------:|
|           0 |          1 |                    1 |
|           1 |          2 |                    1 |
|           2 |          3 |                    3 |
|           3 |          4 |                    6 |
|           4 |          5 |                    9 |

#### 2.7.1.5 Improving Attributes

A PC may optionally choose to use their new skill points to improve their attribute scores.

The first time a PC improves an attribute, it costs 1 skill point and adds +1 to an attribute of their choice.
The second improvement to their attributes costs 2 skill points, the third 3, and so forth.
Each improvement adds +1 to the attribute, potentially improving its modifier.
A PC can only ever make five such improvements total; not five per attribute.

PCs must be third level before buying their third boost, sixth level before buying their fourth boost, and ninth level before buying their fifth boost.
No more than five attribute boosts can ever be purchased by a PC.

#### 2.7.1.6 Choosing a new Focus

Finally, the PC might be eligible to pick an additional level in a Focus.
At levels 2, 5, 7, and 10 a PC can add a level to an existing Focus or pick up the first level in a new Focus.

If this is the first level they’ve taken in the Focus, they might be granted a skill as a free bonus pick, depending on the Focus’ benefits.
During character creation, this bonus skill pick is treated like any other skill pick.
If the Focus is taken as part of advancement, however, it instead counts as three skill points spent toward increasing the skill.
This is enough to raise a nonexistent skill to level-1, or boost a level-1 skill to level-2.
They may do this even if they aren’t high-enough level to normally qualify for a skill level that high.

If the skill points aren’t quite enough to raise the skill to a new level, they remain as credit toward future advances.
If applied to a skill that is already at level-4, the PC can spend the three skill points on any other skill of their choice.

#### 2.7.1.7 Learning New Spells and Arts

Mage characters capable of spellcasting learn new spells when they advance a level.
High Mages and Full Invokers learn two new High Magic spells they are capable of casting.
All other spellcasting mages learn one spell they can cast, either from High Magic or their own specialist spells.

Mages who use arts may also gain a new art pick, depending on their class.
Each such class has a table that lists the schedule for gaining new arts.

## 2.8.0 Crafting and Modifying Gear

Any Expert or Partial Expert PC with at least Craft-1 skill, or any PC with the **Artisan** Focus, can modify equipment with ancient salvage.
Their crafting background does not need to precisely match the gear they’re modifying; the basic principles of using ancient salvage are the same among all disciplines.

### 2.8.1 Crafting Gear

An artisan requires a workshop that could plausibly build the gear in question.
This may be nothing more than a sharp knife for a simple device, or a full-fledged alchemy lab for others.
They also require a plausible source of parts for the device.
This is usually a given if in a city or other salvage-rich area, but it may not be practical in a wilderness.

Building gear takes a month for a wagon-sized vehicle or a week for a weapon, suit of armor, or other portable complex device.
Very simple devices may be built faster at the GM’s discretion.

Crafted gear is made at three levels of quality.
Jury-rigged gear takes one-half the time to build and costs one-quarter the market cost in parts.
If scrap salvage is available it can be built at no cost but normal build times.
As an improvised device, it counts as a mod requiring Craft-0 to keep functional, as explained below in the mod maintenance rules.
If it goes 24 hours without maintenance, it stops functioning.
Jury-rigged devices cannot be further modded.

Normal devices cost the same amount in parts as the market cost and take the normal amount of time to build.
They cannot be built with salvaged parts unless the GM decides the salvage is perfectly suited for it.
Consumable devices such as torches must be crafted as normal devices rather than jury-rigged or mastercrafted ones.

Mastercrafted devices cost ten times as much in parts as the usual market cost and take twice as long to build.
They are ideal platforms for an artisan’s mods, however, and the first mod their creator installs in them requires no maintenance.
Mastercrafted weapons grant a +1 to hit.
Mastercrafted armor counts as 1 fewer point of encumbrance, down to a minimum of 1 point.
This lightening does not affect the armor’s suitability for a user of the **Armored Magic** Focus.

### 2.8.2 Modifying Gear

Crafted or purchased gear can be modified by a skilled artisan.
Crafting mods also requires a minimum Fix skill.
Without this skill level the tech cannot install the mod or maintain it afterwards.

Crafting and installing mods has a cost in silver and sometimes in arcane salvage.
The latter consists of rare monster parts, esoteric materials, and exotic components that cannot normally be bought on the open market, but must be acquired by adventuring or in payment from patrons.
Arcane salvage is generic in nature; a given piece can be used in any mod that requires salvage.

It takes one week per minimum skill level of the mod to build and install it.
Thus, if the mod requires Craft-1 skill, it takes one week.
If the artisan has an assistant with at least Craft-0 skill, this time is halved.
If they do nothing but eat, sleep, and work, this time is further halved.

### 2.8.3 Maintaining Mods

Mods normally require maintenance to keep functioning correctly, and a given artisan can maintain only so many mods at once.
An artisan can only maintain mods they have the requisite skill levels to build.

An artisan’s maximum maintenance score is equal to the total of their Intelligence and Constitution modifiers plus three times their Craft skill level.
Thus, a tech with a +1 Intelligence mod, a -1 Constitution mod and Craft-1 could maintain up to three mods at any one time.

Maintenance is assumed to take place during downtime and does not require any significantly expensive components.
If an artisan does nothing but maintain mods, they can double their allowed number.
Such work assumes sixteen-hour workdays.

If a mod goes without maintenance for 24 hours, it stops working.
If it goes without maintenance for a week, the item it’s attached to stops working, becoming dangerous or ineffective to use.
A maintenance backlog on an item can be cleared by an hour’s work by an artisan capable of maintaining it.

### 2.8.4 Example Modifications

The mods listed here are merely some of the possibilities for using ancient salvage or large amounts of costly mundane materials.
These mods are almost never available on the open market due to the rarity of usable salvage and the difficulty of maintaining the gear.
Acquiring the salvage needed to make them usually means finding it as part of an adventure, receiving it in payment from a patron, or setting out on specific expeditions to find it.

Multiple modifications can stack, but cannot increase a hit, AC, or damage bonus above +3, or a skill check bonus above +1.
Magical and masterwork weapons and armor can be modified, but mods can’t improve them above this cap.

**Arrow Storm (Craft-2)**: A bow or other projectile weapon automatically generates its own ammunition, albeit the conjured projectiles vanish a round after firing.
This mod does not increase reload speed.
Cost: One unit of salvage and 5,000 silver pieces.

**Assassin’s Trinket (Craft-2)**: A one-handed weapon is modified to adopt the shape of some item of jewelry or adornment.
It can be shifted to or from this shape by the owner as an On Turn action.
Cost: One unit of salvage and 1,000 silver pieces.

**Augmented Gear (Craft-1)**: A tool, medical kit, or other item of equipment is improved for a specific purpose chosen at the time of augmentation.
Skill checks made for that purpose gain a +1 skill bonus with the item.
Cost: One unit of salvage and 5,000 silver pieces.

**Automatic Reload (Craft-2)**: A hurlant can be modified to reload itself, if ammunition is available.
Once per scene, a man-portable hurlant can be reloaded as an On Turn action.
Cost: Two units of salvage and 10,000 silver pieces.

**Customized (Craft-1)**: The weapon or suit of armor has been care fully tailored for a specific user.
When used by them, they gain a +1 to hit with the weapon or +1 Armor Class with the armor.
This mod doesn’t work with shields.
Cost: 1,000 silver pieces.

**Flying Razor (Craft-1)**: A throwing weapon is imbued with various esoteric materials, allowing it to return to the hand of its thrower after each attack.
Cost: One unit of salvage and 5,000 silver pieces.

**Harmonized Aegis (Craft-3)**: A suit of armor is altered to harmonize with the dangerous sorceries of allied casters.
Provided the wearer and the caster have had ten minutes to coordinate the protection, the wearer is unharmed by the caster’s harmful spells for the rest of the day, even if caught in their area of effect.
Cost: One unit of salvage and 10,000 silver pieces.

**Long Arm (Craft-2)**: A ranged or thrown weapon is modified to double its normal and maximum ranges.
Cost: One unit of salvage and 5,000 silver pieces.

**Manifold Mail (Craft-2)**: A suit of armor is augmented to allow it to shift its appearance to any of five or six pre-set choices, mimicking normal clothing or other armor types as an On Turn action.
The armor’s Encumbrance or other statistics are not altered.
Cost: One unit of salvage, 5,000 silver pieces.

**Omened Aim (Craft-2)**: Occult components improve a ranged or thrown weapon’s targeting, adding +1 to hit rolls.
Cost: 4,000 silver pieces.

**Preserving Grace (Craft-1)**: A suit of clothing or armor is specially altered to preserve the wearer.
Once per week, when the wearer is Mortally Wounded, they will automatically stabilize.
Cost: One unit of salvage and 5,000 silver pieces.

**Razor Edge (Craft-2)**: A weapon has been given an improved edge or shifting weight system, adding +2 to the damage and Shock it does, albeit requiring far more care.
Cost: One unit of salvage and 5,000 silver pieces.

**Tailored Harness (Craft-2)**: A suit of armor is altered to perfectly fit a single wearer, decreasing its effective Encumbrance by 1 for them only.
This does not affect skill check penalties or the Armored Magic Focus.
Cost: 5,000 silver pieces.

**Thirsting Blade (Craft-3)**: A weapon is imbued with a fated inclination to harm, adding +1 to hit rolls.
Cost: Two units of salvage and 1,000 silver pieces.

## 2.9.0 Encumbrance

Gear has encumbrance, measured in points, as exampled in the table below.
The more awkward or bulky the object, the greater the encumbrance.
The GM adjudicates ambiguous objects.

| Gear                                    |                              Encumbrance |
|-----------------------------------------|-----------------------------------------:|
| Portable in a small pocket              | 0 (Any reasonable number can be carried) |
| Portable in one hand                    |                                        1 |
| Requires two hands to carry or use it   |                                        2 |
| Requires a whole-body effort to haul it |                                       5+ |
| Dragging an unconscious teammate        |                                       12 |

Gear is either Stowed or Readied.
Stowed gear is packed away carefully in pockets, packs, and harnesses.
It’s easier to carry but harder to quickly access.
Using Stowed gear requires that the bear er take a Main Action to pull it out before using it.
Readied gear is carried in hands, holsters, quick-access pockets, or other easily-accessible places.
It can be used as part of an action without any further preparation.

A character can carry a total number of Stowed encumbrance points equal to their Strength score.
They can carry a number of Readied points equal to half their Strength, rounded down.
Thus, a PC with a Strength score of 11 could carry 11 points of Stowed gear and 5 points of Readied.

PCs can haul much heavier objects if necessary.
If they push their limits for longer terms, they can carry an additional two Ready and four Stowed items.
The first time they do this, their Move speed is cut by 30%, from 30 feet to 20 feet.
The second time, it’s cut by 50%, from 20 feet to 10 feet.
More weight than this can’t be practically hauled over significant distances.

### 2.9.1 Bundled Gear

Small, regularly-shaped objects such as oil flasks, potion bottles, rations, and torches can be wrapped into bundles for easier portability.
Three such items can be tied into a bundle that only counts as one item of encumbrance.
Breaking into this bundle takes an extra Main Action, however.

### 2.9.2 Bulk Weights

Sometimes the PCs need to transport bulk amounts of goods that are measured in pounds.
When it’s necessary to convert these weights into encumbrance points, a GM can just assume that fifty pounds is worth about ten points of encumbrance to a PC hauling a pack out on their back.

When judging the ability of a vehicle to carry encumbrance points of cargo, it can be assumed that a wagon can carry as much as the PCs need it to carry, within reason.

### 2.9.3 Games Without Encumbrance

Not all groups like to track encumbrance or deal with the logistics of an adventuring expedition.
If the GM so elects, then PCs can carry and Ready whatever amount of gear the GM thinks is reasonable.
In such cases the GM should check over character sheets before each adventure to make sure reason is not outraged.

## 2.10.0 Falling and Other Hazards

Some perils occur with some regularity for adventurers.
A few of the most common are detailed here.


**Falling**: Most creatures will take 1d6 damage per 10 full feet they fall, up to 20d6 maximum.
Spikes or other hazardous terrain at the bottom will add at least 1d6 to the total.
A creature that intentionally leaps or skids down in a controlled way may make a Dex or Str/Exert skill check at a difficulty of 7 + 1 for every 10 full feet; on a success, the effective distance fallen is halved.

**Suffocation**: Creatures can fight or act normally without air for one round per point of Constitution, or 10 rounds for most NPCs.
If they don’t move, they can quadruple this time.
Once they run out of air, they must make a Physical save each round or take 1 hit point of damage per HD or level they have.

**Poisons**: Typical dungeon poisons found crusted on needle traps force a Physical save or a loss of half the victim’s maximum hit points.
Very potent ones might kill a victim outright.
Those who die due to poison damage usually take at least 1d6 minutes to actually expire, but are helpless in the meanwhile.
An antidote applied during this time can revive them with 1 hit point.
A skilled healer can try to counteract the toxin with an Int/Heal skill check at a difficult of at least 10 for most poisons, or 12 or more for truly fearsome ones.

Aside from any hit point damage a poison does, many also have lingering side effects, such as penalties to hit rolls or skill checks, or the loss of Move actions for a certain period of time.
Some also add System Strain to the victim due to the stress they put on their bodies.
A victim forced above their maximum System Strain will collapse and die in minutes if the poison is not neutralized.

## 2.11.0 Overland Travel

PCs can generally travel for ten hours a day in most seasons, the rest being absorbed in rest, camp construction, and incidental activities.
For each hour of travel they can cross as many miles of a given terrain as listed in the table.
This travel presumes that the PCs are moving directly toward their destination and not taking any particular time to scout the area for points of interest or investigate their surroundings.
This rate of travel also assumes average walking or wagon speeds; horses can be used by their riders for quick bursts of speed to chase or avoid others, but don’t increase the average travel rate much.

For every day of travel and every night of camping outdoors, the GM rolls one die for a wandering encounter check.
The die used will depend on the terrain, with safer or more peaceful lands using a larger die size.
On a 1, the PCs come upon creatures or a situation that requires their attention.

Assuming it’s not an ambush or a sudden encounter in an obscured area, the groups usually encounter each other at maximum sight range.
An opposed Wis/Notice check can be used to determine who spots who first; PCs who get the edge can usually avoid the other group automatically if they have sufficient cover.

PCs traveling with a caravan or riding a well-stocked travel wagon need not concern themselves with details of food, drink, and shelter, but PCs who plan on making an overland expedition without these ready comforts should consult the rules for overland exploration for details on the supplies and difficulties involved.

| Terrain Type                          | Miles per Hour |
|---------------------------------------|---------------:|
| Plains or savannas                    |              3 |
| Light forest or desert                |              2 |
| Dense forest or rugged hills 1.       |              5 |
| Swamp or marsh                        |              1 |
| Mountains or dire wastelands          |            0.5 |
| There is a road through the terrain x |             2* |
| Foul weather, mud, or heavy rain      |           x0.5 |
| Deep snow on the ground               |           x0.1 |

\* Good roads cannot increase the party’s marching speed above three miles per hour.

### 2.11.1 Sea Travel

A ship can usually manage about six miles an hour of travel when under sail, and can sail around the clock if far from coasts and other perils.
Oar-powered galleys average the same speed, but are heed less of the winds.
An ordinary crew can only row for eight hours a day, however.

Encounters at sea are rarer but potentially more hazardous; the GM should roll daily and nightly checks on 1d10 or 1d12.
On a 1, some creature has come across the ship, a troublesome wind or storm has sprung up, something has been damaged on the ship or gone awry with the crew, or otherwise complicated the vessel’s journey.
Some such encounters can be overcome with a good plan and a decent Sail skill check, while others may require bloodier answers.

## 2.12.0 Wilderness Exploration and Expeditions

These rules assume the PCs are exploring or venturing through a hex mapped wilderness area, one without safe waystations or reliable maps.

### 2.12.1 Exploring a Hex

To lightly explore a standard six-mile hex for points of interest takes a full day of scouting.
If the terrain is especially rugged or concealing, such as a range of mountains or trackless swamp, this time is doubled or tripled.

This much time is sufficient to find most major points of interest that the GM may have placed in the hex.
It won’t necessarily catch small features or provide a detailed survey of the terrain.

### 2.12.2 Supplies for an Expedition

When venturing into the untamed wilderness, a group is going to need certain supplies.
Fire, water, shelter, and food are the four most critical.
In some places, some of these supplies may be easily acquired along the way, such as fresh water from a river the PCs are following, or shelter when the climate is warm and pleasant around the clock, but usually some kind of provision will need to be made for getting them.

**Food** is measured in days of food per person.
Each day’s needed food counts as one item of encumbrance, though they can be packed snugly together as weekly rations that count as four items instead.
Some magical items or Mages might be able to create food; a party who relies entirely on such things had best hope nothing happens to their literal meal ticket.

**Water** is also measured in days of water per person, each unit counting as one item of encumbrance.
Exceptionally hot or dry climates may require multiples of this to avoid dehydration or over heating.

**Shelter** means adequate clothing for the climate and some kind of tent or lean-to to protect from the elements while resting.
Characters with Survival-0 can put together a minimal lean-to of boughs and branches in wooded areas, but in places of torrential rain, fierce snows, or other extreme conditions it may prove more difficult.
Lack of shelter can make it impossible to rest comfortably and regain Effort, HP, or spells.
Severe privation can even threaten a PC’s life.

**Fire** means fuel sufficient to cook food, dry wet clothes, and warm PCs after they’ve stopped moving for the day.
In most places it’s easy enough for even the least wood-wise PC to scavenge enough dry wood or twigs to get a minimally sufficient fire going, but voyages into a land devoid of woody vegetation can mean trouble.
A load of dung cakes, charcoal, or other fuel sufficient to keep a group warm for an ordinary night counts as four items of encumbrance.

#### Supply Encumbrances

| Type                                        | Enc |
|---------------------------------------------|----:|
| One day of food or water                    |   1 |
| One week of carefully-packed food           |   4 |
| One night’s load of fire fuel               |   4 |
| One day’s fodder for a horse or large beast |   4 |
| One day’s fodder for a mule or small beast  |   2 |
| Daily water for a large beast               |   8 |
| Daily water for a small beast               |   4 |

### 2.12.3 Pack Animals and Porters

Pack beasts can carry a certain amount of items of encumbrance, assuming they’re packed carefully.
Professional porters are also common hires for adventurers, though they generally refuse to enter dangerous ruins.
Most beasts can survive on nightly browse and brief water stops for the duration of an expedition.
In barren lands, food and water must be packed in for the beasts as well as the humans.
During combat, porters will hide or fight as normal humans to defend their lives.
Pack beasts might panic if not calmed by a handler’s successful Cha/Ride skill check made as a Move action, usually against a difficulty of 8 or more.
Carters, nomad riders, and other professional stock handlers always succeed at these calming checks barring the most unusual circumstances.
At need, a healthy horse can be butchered into 30 days of rations, and a mule or similar-sized beast into 15 days.
Preserving this meat takes time and fire, as explained in the Foraging section.

#### Pack Animal And Porter Loads

| Type                                       | Enc |
|--------------------------------------------|----:|
| Riding horse or warhorse, with laden rider |   5 |
| Riding horse or warhorse, pack only        |  20 |
| Heavy pack horse                           |  30 |
| Mule or donkey                             |  15 |
| Professional porter                        |  12 |
| Two porters carrying a shared litter       |  30 |

### 2.12.4 Starving, Thirsting, and Freezing

If the PCs don’t have enough food, water, warmth, or shelter, bad things will start to happen.
Each day without these necessities will apply the following penalties.

**System Strain is gained.** If this would put the PC over their maximum, they must make a Physical save or die by dawn if not aided.
On a success, they’re helpless until death or rescue.

**They can’t recover System Strain**, gain nightly hit point healing, refresh daily Committed Effort, or restore expended spells until they’ve had a day of proper food, water, and warm sleep.


#### Situations That Cause Privation

| Circumstance                            | Sys. Str. |
|-----------------------------------------|----------:|
| First day without enough food*          |        +0 |
| Consecutive day without food            |        +1 |
| First day without enough water          |        +2 |
| Consecutive day without water           |        +3 |
| Night without adequate shelter or fire* |        +0 |
| Harsh night without shelter or fire     |        +1 |

\* No System Strain is yet gained, but the PC still suffers privation and cannot recover lost resources.

### 2.12.5 Foraging

PCs who find themselves low on supplies or lacking a particular re source can take time to forage the surrounding terrain.
It’s assumed the group sticks together during this process, sacrificing efficiency for security.
They can split up if desired, but each group then risks encounters.

Some supplies can be gathered as normal parts of travel.
If passing through ordinary, non-arid, non-barren terrain it’s assumed the PCs can refill their waterskins and scrounge firewood whenever they wish.

Foraging requires either a half-day or a full day of effort.
The group’s most apt member makes a Wis/Survive check against the difficulty listed on the table, and on a success earns 1d6 units of forage plus the sum of the group’s Survive skill levels, to a maximum of 10.
Those without the skill at all subtract 1 from the total, to a minimum of one unit found by the group.

Each forage unit is worth either a day of food for a person, a day of water for a person, or a night’s firewood for the party, and the group can decide how much of each was found after the roll is made.
PCs who are barbarians or other natives of the wilds normally never find less than two units of forage barring extreme circumstances.

Foraged food is unpreserved and will spoil in three days.
Smoking or drying it requires use of a day’s worth of firewood and a half day’s labor.
Any reasonable amount of forage can be preserved with a single half-day’s work.

#### Foraging

| Type of Foraged Terrain                      | Diff |
|----------------------------------------------|-----:|
| Woodlands or areas of heavy vegetation       |    8 |
| Mountains, scrublands, savannas              |    9 |
| Deserts, badlands, or normal barrens         |   12 |
| Grim wastes or barely human-survivable lands |   14 |
| A full day foraging rather than a half-day   |   -2 |
| Each successive day foraging the same hex    |   +1 |

On success, 1d6 units of forage are found, plus the total Survive skills of the foragers.
Those without even level-0 skill in it subtract 1 from the total found.

### 2.12.6 Wandering Encounters

PCs risk encountering trouble in their expeditions.
Every so often, a Wandering Encounter check die should be rolled by the GM, with example probabilities on the table below.
If the GM rolls a 1, something comes up.
A GM should roll an encounter check...

- Once per day of travel and once per night of camping outdoors.
Such encounters will commence at the terrain’s usual maximum sight range, with a Wis/Notice opposed check to see who first spotted who.
- Once per foraging attempt.
Whether a half-day or a full day, one check is made per attempt, per foraging group.
If the group forages all day instead of traveling, the daily travel check above is omitted.

The precise nature of a Wandering Encounter will depend on the terrain and the GM’s preparations.
These encounters fit the logic of the situation, not the levels of the PCs, so a swift retreat may be in order.

Not all Wandering Encounters are hostile or involve combat, however.
A pompous noble might be clearing rabble off the road in front of him, or a woodsman might be found with a broken leg, or the bandit crew might be carousing and willing to have visitors join in.
In general, they’re simply situations, creatures, or events that the PCs will need to react to.

#### Wandering Encounter Checks

| Type of Terrain                        | Chance  |
|----------------------------------------|---------|
| Dangerous wilderness area              | 1 in 6  |
| Area of civil unrest or heavy banditry | 1 in 6  |
| Ordinary trade road                    | 1 in 8  |
| Well-policed trade road                | 1 in 10 |
| Borderlands or rural back country      | 1 in 8  |
| Ordinary wilderness                    | 1 in 8  |

## 2.13.0 Dungeon Exploration

These rules are meant for tracking adventures in a dangerous site where perils could spring up at any moment.
They’re not meant for casual exploration of some bosky glade or city street, and in such cases the PC actions can just be followed scene-by-scene as usual.
It’s important that GMs understand the purpose of the Wandering Encounter checks in a site.
They’re intended to put constant pressure on the party to get in, accomplish their purpose, and get out before they’re worn down by encounters.
Not all Wandering Encounters are hostile, but each is a risk of pointless fighting or sudden alarm.

### 2.13.1 The Order of Play

At the start of each turn after the party enters the site:

- **Roll a secret Wandering Encounter check** if necessary.
On a 1, the encounter will happen at some appropriate moment this turn.
- **The PCs decide what they want to do** this turn, be it move into a new room, carefully search their current location, fiddle with some object they’ve found, or something else that takes ten minutes.
- **Tell them the result of their actions**, whether that’s a first-glance description of a new room, notice of the hideous abomination that’s rearing up before them, or the explosive detonation of the crystal they just experimentally rapped.
- **Start over from the top**, assuming their actions have consumed a full turn, until they withdraw from the site or it becomes safe enough to stop counting turns.

### 2.13.2 Timekeeping in the Dungeon

Once the heroes intrude on a ruin, dungeon, corrupt noble’s mansion, or other dangerous site the GM starts tracking time in turns.
Each turn counts as about ten minutes or one scene.
Members of the party can generally do one significant thing per turn.
Different party members can be doing different things in the same turn.

The point of tracking turns isn’t to have a minute-by-minute tally of PC activities, but to have a rough measure of how much activity they’re engaging in within the ruin.
The more they do and the longer they stay, the more likely that they’ll run into Wandering Encounters or the natives will have time to realize that intruders are present.
Eventually, the PCs need to either pull back or clear the site entirely of its dangerous inhabitants.

#### Example Time Costs in Turns

| Activity                                       | Turns |
|------------------------------------------------|------:|
| Move from one room of interest to another      |     1 |
| Pick a lock or disarm a trap                   |     1 |
| Get in a fight with something                  |     1 |
| Perform first aid and looting after a fight    |     1 |
| Search a room carefully                        |     1 |
| Jury-rig something or work a device            |     1 |
|                                                |       |
| Time a torch lasts until burning out           |     6 |
| Time a filled lantern lasts before burning out |    24 |

### 2.13.3 Movement and Fleeing

In simple diagrammatic ruins consisting of points of interest, movement from one point to another takes one turn.
Otherwise, travel is at the rate of 120’ per turn, reflecting the party sneaking, listening, mapping, and carefully examining their surroundings as they go.

If forced to flee, the party needs to decide how exactly they’re retreating and what measures they’re using to slow or dissuade pursuers.
Some discouragements may be enough to work without a roll.

If their foes are determined, the party uses the chase rules given in the rules section of this document.
When fleeing madly, the party should not be allowed to reference their maps; the GM just calls out intersections and doors until the party escapes, the foes give up, or the enemies catch them.

On a successful evasion, the PCs eventually outpace or lose their pursuers.
On a failure, they’re caught somewhere along their escape route.
A successful escape usually takes up one turn worth of time and leaves the PCs in whatever location they’re in when the pursuit stops.

### 2.13.4 Encounters and Surprise

Usually, PCs are sufficiently alert when exploring a site to avoid any chance of surprise, barring a set ambush.
If they burst in on the natives suddenly, however, the locals might be too stunned to act for a round.
If the GM thinks this is possible, give it X-out-of-6 odds.
If the PCs run into a Wandering Encounter in a room, the creatures will enter through one of the available entrances.
In the corridors outside, they’re usually 1d8 x 10 feet away when first spotted or heard, assuming there’s enough space for such a distance.

Remember to make a reaction roll when PCs encounter creatures.
Not every group of dungeon denizens will immediately lunge for their weapons.

Usually, there’s a brief, tense moment of recognition when the PCs encounter creatures, just enough time for a reaction roll and a chance to size up the odds of diplomacy.
The GM should always give the PCs a chance to parley or run when encountering creatures unless the situation makes that completely impractical.

### 2.13.5 Wandering Encounter Checks in the Dungeon

Every so many turns, a GM should roll 1d6 to check for a Wandering Encounter.
On a 1, the PCs will run into one at some point during the turn.
The frequency of the check will depend on how vigorously alert and organized the site’s inhabitants are.

The actual contents of the encounter are decided when the GM puts together the site.
Not all encounters involve running into creatures.
Some are mere events or situations that fit the site.
In the same vein, not all encounters are necessarily hostile, either.
Reaction rolls should be made for all groups of creatures.

#### When To Roll an Encounter Check

| Type of Location                                                    | Turns    |
|---------------------------------------------------------------------|----------|
| Alerted site with organized defenders                               | Every 1  |
| Unalert site with organized defenders                               | Every 2  |
| Site with no organized or active defense                            | Every 3  |
| Site with very few mobile inhabitants                               | Every 4  |
| Abandoned or disused nook in a site                                 | Every 6  |
| Hidden area or concealed chamber unknown to the natives of the site | No check |

# 3.0.0 Equipment, Armor, and Weapons

This section provides a selection of example gear appropriate to most fantasy campaigns.
The GM is naturally expected to add other items that might suit their particular world.

## 3.1.0 Money and Currency

The game assumes copper, silver, and gold coins in general circulation, with ten coppers to a silver and ten silvers to a gold piece.
Silver is the base currency; one silver piece is a typical laborer’s daily wage.

## 3.2.0 Adventuring Gear

#### Adventuring Gear

| Item                          |   Cost | Enc |
|-------------------------------|-------:|----:|
| Arrows, 20                    |   2 sp |   1 |
| Backpack                      |   2 sp |  1§ |
| Boots                         |   2 sp |  1§ |
| Candle                        |   1 cp |   * |
| Cart, one-horse               |  50 sp | N/A |
| Clothes, common               |  25 sp |  1§ |
| Clothes, fine                 | 100 sp |  1§ |
| Clothes, noble                | 500 sp |  2§ |
| Cooking utensils              |   4 sp |   1 |
| Crowbar                       |   4 sp |   1 |
| Firewood, one night's fire    |   2 cp |   4 |
| Flask, metal, one pint        |   3 sp |   1 |
| Grappling hook                |   5 sp |   1 |
| Hammer or small tool          |   2 sp |   1 |
| Healer's pouch                |   5 sp |   1 |
| Hurlant bolts, 20             |  20 sp |   1 |
| Iron spikes, 10               |   1 sp |   1 |
| Lantern                       |  10 sp |   1 |
| Mirror, hand                  |  10 sp |   * |
| Oil, one pint                 |   1 sp |  1# |
| Paper, 10 sheets              |   1 sp |   * |
| Rations, one week             |   5 sp |   4 |
| Rope, 50'                     |   2 sp |   2 |
| Sack                          |   1 sp |   1 |
| Shovel, pick, or similar tool |   4 sp |   2 |
| Tinder box                    |   1 sp |   * |
| Torch                         |   2 cp |  1# |
| Waterskin, one gallon         |   1 sp |   1 |
| Writing kit                   |   3 sp |   1 |

\* the item is effectively weightless in modest numbers<br>
§ the item doesn’t count for encumbrance purposes while being worn<br>
\# can be bundled in units of three for the same encumbrance, with a Main Action to break open a bundle to get at the contents

#### Beasts and Transport

| Item                           |      Cost |
|--------------------------------|----------:|
| Horse, riding                  |    200 sp |
| Horse, draft                   |    150 sp |
| Horse, battle-trained          |  2,000 sp |
| Mule                           |     30 sp |
| Cow                            |     10 sp |
| Ox, plow-trained               |     15 sp |
| Chicken                        |      5 cp |
| Pig                            |      3 sp |
| Dog, working                   |     20 sp |
|                                |           |
| Sheep or goat                  |      5 sp |
| River ferry, per passenger     |      5 cp |
| Ship passage, per expected day |      2 sp |
| Carriage travel, per mile      |      2 cp |
|                                |           |
| Rowboat                        |     30 sp |
| Small fishing boat             |    200 sp |
| Merchant ship                  |  5,000 sp |
| War galleon                    | 50,000 sp |

### 3.2.1 Gear Bundles

Depending on the tastes of the group, some parties might enjoy tracking every torch and carefully weighing their resource expenditures on perilous expeditions.
Others prefer to gloss over the details.
The “gear bundle” options below cover all the non-weapon, non-armor gear expected of a particular role and the usual encumbrance weight of it all.
The specific contents of each bundle are as broad as the GM finds reasonable for the role.
A GM who prefers exact accounting can disallow bundles.

#### Gear Bundles

| Item                   |     Cost | Enc |
|------------------------|---------:|----:|
| Artisan’s Equipment    |    50 sp |   5 |
| Criminal Tools         |   100 sp |   3 |
| Dungeoneering Kit      |   200 sp |   6 |
| Noble Courtier Outfit  | 1,000 sp |   2 |
| Performer’s Implements |   100 sp |   3 |
| Wilderness Travel Gear |   100 sp |   5 |

## 3.2.0 Hirelings and Services

The party may find it useful to employ temporary labor in their ad ventures, either for extra warm bodies in combat or for the special talents they possess.

Adventuring hirelings will demand at least a half-share of treasure in addition to their daily pay and will undertake no risks that their employers don’t share.
Their combat statistics will be as normal for their type, usually equal to a common human soldier for most.
After a particularly dangerous adventure, the hireling must make a Morale check; on a failure, they decide the adventuring life is too risky and leave the group.

On the rare occasions that a mage can be found willing to hire out their services, their skills almost never exceed those of a first or second level Mage.

Most communities have a limited number of men and women willing to risk an awful doom while adventuring.
If the party makes a habit of returning without their employees, others may refuse to join.

Non-adventuring hirelings who are employed to guard the party’s residence, haul their equipment on expeditions, work on their behalf, and otherwise conduct normal business will require no more than their daily wage.
If they can’t go home at the end of the day, food and fit lodgings must be provided as well.

Where it matters, common hirelings can be assumed to have a total +1 bonus on relevant skill checks.

#### Hirelings and Day Labor

| Item                                  | Cost/day |
|---------------------------------------|---------:|
| Bard of Small Repute                  |     2 sp |
| Common Prostitute                     |     2 sp |
| Dragoman or Skilled Interpreter       |    10 sp |
| Elite Courtesan                       |   100 sp |
| Farmer                                |     1 sp |
| Guard, ordinary                       |     2 sp |
| Guard, sergeant, for every ten guards |    10 sp |
| Lawyer or Pleader                     |    10 sp |
| Mage of Minor Abilities               |   200 sp |
| Mundane Physician                     |    10 sp |
| Porter willing to go into the wilds   |     5 sp |
| Porter only for relatively safe roads |     1 sp |
| Navigator                             |     5 sp |
| Sage, per question answered           |   200 sp |
| Sailor                                |     1 sp |
| Scribe or Clerk                       |     3 sp |
| Skilled Artisan                       |     5 sp |
| Unskilled Laborer                     |     1 sp |
| Veteran Sellsword                     |    10 sp |
| Wilderness Guide                      |    10 sp |

### 3.2.1 Services and Living Expenses

Heroes who are sufficiently established as to have their own homes or businesses can live comfortably on their own resources.
Other PCs, however, must pay for their keep when not out adventuring.

**Impoverished lifestyle** costs cover only the bare minimum of food and a mostly-dry squat to sleep in.
Heroes who can afford nothing better suffer a -1 penalty to all social skill checks due to their unkempt state and must make a Physical saving throw each night to benefit from the usual decrease in System Strain.

**Common lifestyle** fees for an adventurer usually cover adequate food and a shabby private inn room.
No penalties or benefits are granted by living this way.

**Rich lifestyle** costs generally include a rented townhouse, a small staff of servants, and social entree into high society circles that are forgiving of the nouveau riche...
at least, as long as their coin remains good.

**Noble lifestyles** provide the very best the community can offer in fine lodging, luxuriant food, sycophantic servants, and the provisional friendship of useful parasites.
Once per game session, the PC can ask a favor of a hanger-on in their retinue, who will perform it if it is not more than mildly humiliating, dangerous or illegal.

Aside from these weekly lifestyle costs, some other services often required by adventurers are listed.

#### Services and Living Expenses

| Item                               |      Cost |
|------------------------------------|----------:|
| Impoverished lifestyle, per week   |      5 sp |
| Common lifestyle, per week         |     20 sp |
| Rich lifestyle, per week           |    200 sp |
| Noble lifestyle, per week          |  1,000 sp |
|                                    |           |
| Magical healing of wounds          | 10 sp/hp* |
| Magical curing of a disease        |   500 sp* |
| Lifting a curse or undoing magic   | 1,000 sp* |
| Casting a minor spell              |   250 sp* |
|                                    |           |
| Bribe to overlook a minor crime    |     10 sp |
| Bribe to overlook a major crime    |    500 sp |
| Bribe to overlook a capital crime  | 10,000 sp |
|                                    |           |
| Hire someone for a minor crime     |     50 sp |
| Hire someone for a major crime     |  1,000 sp |
| Hire someone for an infamous crime | 25,000 sp |

\* These services are rarely available without personal connections or doing special favors, and many communities may lack them entirely.

## 3.3.0 Armor

While some martial adepts or tradition-bound sorcerers shun armor, most adventurers find it necessary to put something solid between them and their enemies.

Armor must be worn as a Readied item, counting against the hero’s encumbrance limit.
Each type of armor grants a different base Armor Class to the wearer, making it more difficult for enemies to land a telling blow.
This Armor Class is modified by the wearer’s Dexterity modifier and by any shield they might carry, as described below.
Multiple suits of armor do not stack; only one can usefully benefit a wearer.

A hero wearing no armor at all has a base Armor Class of 10, modified by their Dexterity modifier.
If they pick up a shield they can improve this to either AC 13 or AC 14, depending on the size of the shield.

### 3.3.1 Light, Medium, and Heavy Armor

**Light armor** may be decidedly heavy, but it is loose and flexible enough to offer minimal hindrance to the wearer’s actions.
Some varieties are also discreet enough to be worn politely in common society. **Medium armor** is significantly noisier and more overt; it cannot be worn discreetly and applies its encumbrance as a penalty to any physical Sneak rolls made by the wearer. **Heavy armor** is the thickest, toughest panoply available on the market, and its bulk and noise make its encumbrance apply as a penalty to Sneak or Exert checks made by the wearer.

### 3.3.2 Shields

**Shields** come in two general varieties.
Small ones, often made of costlier metal, can be strapped to the wearer’s arm and allow them to hold and manipulate objects with that hand, albeit not wield a weapon with it.
Larger shields are of cheaper wood and require a good grip.

A small shield user has a base AC of 13, while a large shield user has a base AC of 14.
Unlike other armor, however, if the user is already wearing equal or better armor the shield grants a +1 bonus to their AC.
Shields allow the bearer to ignore the first instance of Shock they might otherwise suffer in a round.

### 3.3.3 Types of Armor

**War shirts** are nothing more than blessed shirts, lucky cloaks, auspicious warpaint, or whatever tokens of martial victory are favored by the poor and humble of a given culture.
While they may look like nothing but normal clothing, their war-luck is still sufficient to interfere with a spellcaster’s abilities.

**Buff coats** are long coats of thick, supple hide, sometimes worn to cushion the bite of heavier armor and sometimes sported as ornamented street clothing for the gentry who can afford such luxuries. **Linothorax armor** is a stiffer armor of glued, layered cloth.

A **war robe** is a catchall term for various outfits involving layers of reinforced cloth or leather.
Bits of metal, layers of thick hide, or weaves of tough cordage might all go into the various layers of the suit, making it a very heavy, if effective, piece of equipment.

**Pieced armor** is assembled of a thicker or more durable chest piece and piecework limb armor.
While less effective than a proper cuirass and greaves, it’s often the best that poor adventurers can get.

**Mail shirts** are usually of iron or steel wire, though bronze and other more exotic materials are not unknown.
Such shirts cover only the vitals of the wearer, but are less burdensome than a full hauberk.

**Cuirass and greave armor** reflects those different designs that rely on solid metal plating over the wearer’s vitals along with lighter limb armor. **Scaled armor** includes both armor of metal scales on a flexible backing, brigandine, jacks of plate, and other armor made up of small, connected plates that cover most of the wearer’s body.
Most are noisy, heavy suits.

A **mail hauberk** in the listed style covers not only the wearer’s chest, but also their arms, with a long skirt extended to the knee. **Plate armor** is an extremely expensive suit of tailored metal pieces that cover both the vitals and the limbs of the wearer. **Great armor** is less finely tailored, relying instead on stacking layer upon layer of mail, plates, hide, cloth, and other protective materials.

**Grand plate** is so finely-jointed that a weapon must either be very large or very armor-piercing to harm the wearer; they’re immune to non-magical melee or thrown weapons unless the weapon is two-handed, has a Shock rating of AC 16 or more, or the wearer is currently grappled by someone.
This tight protection comes at the cost of the thick plating found in great armor or conventional plate.

#### Armor

| Light Armors | AC |   Cost | Enc |
|--------------|---:|-------:|----:|
| No Armor     | 10 |   None | N/A |
| War Shirt    | 11 |   5 sp |   0 |
| Buff Coat    | 12 |  50 sp |   0 |
| Linothorax   | 13 |  20 sp |   1 |
| War Robe     | 14 |  50 sp |   3 |
| Pieced Armor | 14 | 100 sp |   2 |

| Medium Armors       | AC |   Cost | Enc |
|---------------------|---:|-------:|----:|
| Mail Shirt          | 14 | 250 sp |   1 |
| Cuirass and Greaves | 15 | 250 sp |   2 |
| Scaled Armor        | 16 | 500 sp |   3 |

| Heavy Armors | AC |     Cost | Enc |
|--------------|---:|---------:|----:|
| Mail Hauberk | 16 |   750 sp |   2 |
| Plate Armor  | 17 | 1,000 sp |   2 |
| Great Armor  | 19 | 2,000 sp |   3 |
| Grand Plate  | 16 | 2,000 sp |   3 |

| Shield       | AC | Cost  | Enc |
|--------------|---:|------:|----:|
| Small Shield | 13 | 20 sp | 1   |
| Large Shield | 14 | 10 sp | 1   |

## 3.4.0 Weapons

The tools of a bloody trade are familiar to most adventurers.
While some Vowed or trained pugilists might scorn the use of material weapons, most sentient combatants must rely on something better than their natural gifts.

### 3.4.1 Weapon Statistics

Each of the weapons on the following page has a listed damage it inflicts on a successful hit, an amount of Shock inflicted on a miss to targets with an AC equal or less than that given, and a particular attribute relevant to the weapon’s use.
That attribute’s modifier is applied to all hit rolls, damage rolls, and Shock inflicted by the weapon.
If more than one attribute is listed, the wielder can use whichever one is better.
Using a melee weapon without at least Stab-0 skill inflicts a -2 penalty on hit rolls, as does using ranged weapons without at least Shoot-0 skill.
Thrown weapons can be used with either.

Ranged weapons have both short and long ranges listed in feet.
Attacking a target within short range may be done at no penalty, while hitting a target at long range is done with a-2 penalty to the hit roll.
Two-handed ranged weapons cannot be used while an enemy is locked in melee with the wielder, and even one-handed or thrown weapons suffer a -4 penalty to hit in such circumstances.

Some weapons have additional unique traits, perhaps being particularly slow to reload, or requiring two hands to wield correctly, or being easily hidden in common clothing.
The GM might choose to apply these traits to improvised weapons snatched up by the PCs if any of them seem appropriate.

### 3.4.2 Types of Weapons

**Axes** given here are those fashioned for war; lighter and more agile than their working cousins, though still capable of hacking through a door or hewing a cable if needed. **War axes** are big enough to demand two hands for their use.

**Blackjacks** include not only obvious weapons loaded with sand or iron shot, but any small, stunning fist load.
A blackjack or other small fist load is easily concealed as some ornamental component of ordinary clothing.

**Bows** cover everything from the small self bows of horse archers to the man-tall longbows wielded by foot archers.
Larger bows are more cumbersome and impossible to shoot from horseback, but usually have superior strength.
An archer with a Readied quiver can load a fresh arrow as a Move action each turn, or as an On Turn action if they have at least Shoot-1 skill.

**Claw blades** are the sharper kin of fist loads, being small blades or finger talons that are easily concealed or disguised as metal ornaments.
While they are vicious weapons, they can’t be usefully thrown.

**Clubs**, **staves**, and **maces** are of much the same genus, though the latter is usually made of metal.
While fully capable of killing a man, a careful user can usually avoid inflicting lethal injury.

**Crossbows** come in heavier varieties than the one listed, but such slow, bulky arbalests are rarely in the hands of adventurers.
Reloading a crossbow of this size takes a full Main Action, but due to the simplicity of their operation, someone without Shoot-0 can still use them at no unskilled hit penalty.

**Daggers** come in ten thousand varieties, but the listed kind is a common fighting dirk, big enough to push through light armor while remaining small enough to be discreetly hidden. **Stilettos** and similar armor-piercing daggers aren’t usually effective as thrown weapons.

**Halberds** and other polearms can be somewhat awkward in narrow spaces, but remain popular military weapons in some armies.
The statistics given here can also be used for fauchards, bills, voulges, spetums, bardiches, glaives, guisarmes, guisarme-glaives, glaive-guisarme-glaives, and similar weapons.

**Hammers** listed here are the fighting variety, narrow-headed and made for penetrating or shocking heavy plates of armor.

**Hurlants** statistics are provided as analogs to magically-powered firearms, if the GM’s campaign includes such things.

**Hand hurlants** are usually pistol-sized, most often carried by the wealthy as a single-shot opener at the start of hostilities. **Long hurlants** are rifle-sized weapons favored by elite snipers and assassins who don’t expect a need for a second shot. **Great hurlants** are usually eight feet long and hundreds of pounds in weight, and launch tremendous bolts that can transfix even monstrous targets.
Those able to afford their use generally mount them on ships, gun carriages, or on important fortifications.

**Spears**, and their longer cousin the **pike**, are common military weapons.
Lighter spears are effective thrown weapons, while heavier two-handed versions penetrate armor well.

**Shields** can be an effective weapon when used to bash or pummel an enemy.
If used as a weapon or as part of a dual-wielding attack, a shield grants no AC or Shock protection benefits until the wielder’s next turn.

**Swords** are common sidearms for the gentry.
The expense of forging a large blade makes it a symbol of wealth and status in many cultures, and its convenience makes it a favored arm for street wear.

**Throwing blades** are small leaves or spikes of steel that are not terribly useful as melee weapons but are easy to carry discreetly in considerable numbers.

The **unarmed attack** given here is a common punch or kick, unimproved by a Vowed’s arts or a Focus.
Unarmed attacks add the assailant’s Punch skill to the damage roll as well as the attack roll.

| Weapon             |       Dmg |   Shock | Attribute | Range/Feet | Traits         |      Cost | Enc |
|--------------------|----------:|--------:|-----------|-----------:|----------------|----------:|----:|
| Axe, Hand          |       1d6 | 1/AC 15 | Str/Dex   |      10/30 | T              |     10 sp |   1 |
| Axe, War           |      1d10 | 3/AC 15 | Str       |          - | 2H             |     50 sp |   2 |
| Blackjack          |       1d4 |    None | Str/Dex   |          - | S, LL          |      1 sp |   1 |
| Bow, Large         |       1d8 |    None | Dex       |    100/600 | 2H, R, PM      |     20 sp |   2 |
| Bow, Small         |       1d6 |    None | Dex       |     50/300 | 2H, R, PM      |     20 sp |   1 |
| Claw Blades        |       1d6 | 2/AC 13 | Str/Dex   |          - | S              |     10 sp |   1 |
| Club               |       1d4 |    None | Str/Dex   |      10/30 | T, LL          |         - |   1 |
| Club, Great        |      1d10 | 2/AC 15 | Str       |          - | 2H             |      1 sp |   2 |
| Crossbow           |      1d10 |    None | Dex       |    100/300 | 2H, SR, PM     |     10 sp |   1 |
| Dagger             |       1d4 | 1/AC 15 | Str/Dex   |      30/60 | S, T, PM       |      3 sp |   1 |
| Halberd            |      1d10 | 2/AC 15 | Str       |          - | 2H, L          |     50 sp |   2 |
| Hammer, Great      |      1d10 | 2/AC 18 | Str       |          - | 2H             |     50 sp |   2 |
| Hammer, War        |       1d8 | 1/AC 18 | Str       |          - |                |     30 sp |   1 |
| Hurlant, Great     |      3d10 |    None | Dex       |  600/2,400 | FX, SS, AP     | 10,000 sp |   1 |
| Hurlant, Hand      |      1d12 |    None | Dex       |      30/60 | SS, AP         |  1,000 sp |   1 |
| Hurlant, Long      |       2d8 |    None | Dex       |    200/600 | 2H, SS, AP, PM |  4,000 sp |   2 |
| Mace               |       1d6 | 1/AC 18 | Str       |          - | LL             |     15 sp |   1 |
| Pike               |       1d8 | 1/AC 18 | Str       |          - | 2H, L          |     10 sp |   2 |
| Shield Bash, Large |       1d6 | 1/AC 13 | Str       |          - | LL             |         - |   - |
| Shield Bash, Small |       1d4 |    None | Str/Dex   |          - | LL             |         - |   - |
| Spear, Heavy       |      1d10 | 2/AC 15 | Str       |          - | 2H             |     10 sp |   2 |
| Spear, Light       |       1d6 | 2/AC 13 | Str/Dex   |      30/60 | T              |      5 sp |   1 |
| Throwing Blade     |       1d4 |    None | Dex       |      30/60 | S, T, N        |      3 sp |   1 |
| Staff              |       1d6 | 1/AC 13 | Str/Dex   |          - | 2H, LL         |      1 sp |   1 |
| Stiletto           |       1d4 | 1/AC 18 | Dex       |          - | S, PM          |     10 sp |   1 |
| Sword, Great       |      1d12 | 2/AC 15 | Str       |          - | 2H             |    250 sp |   2 |
| Sword, Long        |       1d8 | 2/AC 13 | Str/Dex   |          - |                |    100 sp |   1 |
| Sword, Short       |       1d6 | 2/AC 15 | Str/Dex   |          - |                |     10 sp |   1 |
| Unarmed Attack     | 1d2+Skill |    None | Str/Dex   |          - | LL             |         - |   - |

### 3.4.3 Weapon Traits

**2H**: Two Handed.
The weapon requires two hands to use in combat.
Ranged two-handed weapons cannot be fired effectively while an enemy is within melee range.

**AP**: Armor Piercing.
This weapon ignores non-magical hides, armor and shields for purposes of its hit rolls.

**FX**: Fixed.
The weapon is too heavy and clumsy to use without a fixed position and at least five minutes to entrench it.

**L**: Long.
The weapon is unusually long, allowing melee attacks to be made at targets up to 10 feet distant, even if an ally is in the way.
Even so, the wielder still needs to be within five feet of a foe to count as being in melee with them for purposes of forcing Fighting Withdrawals, disrupting large ranged weapons, or similar maneuvers.

**LL**: Less Lethal.
Foes brought to zero hit points by this weapon can always be left alive at the wielder’s discretion.

**N**: Numerous.
Five of these count as only one Readied item.

**PM**: Precisely Murderous.
When used for an Execution Attack, the weapon applies an additional -1 penalty to the Physical save and does double damage even if it succeeds.

**R**: Reload.
The weapon takes a Move action to reload.
If the user has at least Shoot-1 skill, they can reload as an On Turn action instead.

**S**: Subtle.
Can be easily hidden in clothing or jewelry.

**SR**: Slow Reload.
It takes a Main Action to reload this weapon.

**SS**: Single Shot.
This weapon takes ten rounds to reload, and the reloading effort is spoiled if an enemy melees the wielder.

**T**: Throwable.
While the weapon can be used in melee, it may be thrown out to the listed range as well, albeit it does no Shock in that case.
Throwing a weapon while in melee applies a -4 penalty to the hit roll.

# 4.0.0 Magic and Spellcasting

High Mages, Necromancers, Elementalists, and Invokers are all capable of casting spells.
In addition, NPC wizards may also have access to spellcasting, though NPCs seldom have classes as PCs do.
This section of the rules describes such sorcerous matters.

## 4.1.0 Spells and Arts

Magical invocations are known conventionally as **spells**, each one ranked in power from level one, the relatively weakest, to level five, the most potent.

The common corpus of ancient spells is known as High Magic.
These spells are the common heritage of all spellcasting mages, and any spellcasting mage can learn and use them.

Specific arcane traditions such as Elementalists or Necromancers also have bodies of spells specific to their tradition.
Only they can learn or cast these enchantments.

In addition to spells, mages commonly have access to **arts**, magical techniques that allow them to produce certain effects quickly and easily.
Most mages learn one or two arts as an apprentice and gradually master more as they advance in skill.

Every tradition has its own body of arts, and only members of that tradition can ever learn them.
Adventurers who take dual Mage classes can learn arts from both, but each class must choose its arts individually; a Partial High Mage/Partial Healer couldn’t use his High Mage art picks to choose Healer arts, or vice-versa.

## 4.2.0 Preparing and Casting Spells

Each spellcaster must first learn a spell before they can cast it.
They may be taught by a mage who already knows the spell, even if they’re from a different tradition, or they must find a grimoire laying out the fine details of the incantation.
Specialist spells specific to a tradition cannot be learned by mages outside that tradition.

### 4.2.1 Learning a Spell

Learning a spell requires one week per spell level, less one week per level of the learner’s Magic skill, down to a minimum of one day.
At the end of this time the student will either have mastered the contents of a written copy of the spell or created such a copy from their master’s tutelage.
They must continue to retain access to this written document if they are to prepare the spell later, as it is an actual thaumaturgically-attuned artifact in itself.
Without its formulas, attunement tables, and enchanted diagrams the mage will be unable to prepare the spell for later use.
The full collection of these documents is generally known as the mage’s “grimoire” or “spellbook”, though some texts actually take the form of teaching artifacts or physical models.

If this spellbook is lost, the mage can re-create it, but it takes as much time to re-scribe each spell as it would to learn it in the first place.
No special costs are required in silver or materials.

A mage must be able to actually cast the spell in order to learn it.
A novice mage can cast only first level spells, so they can learn only first level spells as well, even if they somehow come into possession of a grimoire with a more potent incantation.
They’ll need to wait until their own enlightenment is sufficient to cast the spell before they can finally master it.

### 4.2.2 Preparing a Spell

Once the spellbook is prepared, the mage must perform the necessary attunements and propitiations to prepare the spell for casting.
The number of spells a mage can prepare at once varies with their experience level, from as few as two or three at first level to as many as twelve when at tenth level.
The mage may prepare any spell they have in their spellbook; there’s no obligation to prepare a certain number of low-level spells should the mage wish to fill all their avail able “slots” with their most powerful magics and run the risk of lacking some lesser, yet more useful spell.

Preparing a new set of spells takes an hour, and can only be done after a good night’s rest, when the mage’s mind is most fresh and malleable.
Once spells have been prepared they remain so indefinitely until replaced with a different set of magics.

### 4.2.3 Casting Prepared Spells

Once a set of spells is prepared, the mage may then cast them at their leisure.
Casting a spell usually requires a Main Action and at least one free hand, along with vocalizations at least as loud as clear normal conversation.
The arcane gesticulations and vocal incantations are quite obviously occult to any onlookers, but it’s usually impossible to tell which spell a mage is casting merely by looking at them.

Casting a spell requires focus and undisturbed concentration.
If a mage has taken hit point damage or has been severely jostled in a round, they cannot cast magic that round.
Thus, a mage who acts late in a round runs the risk of being hurt and having their concentration spoiled, thus forcing them to do something other than cast a spell when their turn comes around.

Spells cannot normally be cast while wearing armor or restrictive clothing.
The profane materials, restrictive fit, and unsalutary occult configurations of armor spoil the necessary flow of energy, as does the bulk and clumsiness of a shield.
The same hindrances usually also spoil the use of any arts the tradition may teach, and not just spellcasting itself.

A mage can cast only so many spells each day before needing a full night’s rest to recover.
Novices can cast only one spell, while masters can manage six.
The spell to be cast may be selected from any prepared by the mage; the same power can be used to invoke a mighty fifth-level spell or a relatively modest first-level incantation.
The same spell may be cast more than once, if the mage has multiple castings left for the day.

Adventurers who take the Partial Mage class are more limited than more focused specialists.
They are able to cast fewer spells each day and the maximum level of spell they can cast is also significantly lower as compared to an equivalent full Mage.
The total spells they can prepare for the day are also often somewhat fewer than that of a full Mage.
When they cast the spells, however, any level-based effects also use their full level, so a third-level Partial Necromancer’s invocation of The Coruscating Coffin does 3d8 damage, just as a full Necromancer would.

### 4.2.4 Creatures, Targets, and Visibility

Many spells or powers specify particular targets or creatures, or re quire that a target be visible.
For purposes of these spells, “creature” means any animate entity, whether living, undead, or synthetic.
Plants do not qualify as creatures unless they’re some sort of plant monster.

A spell that can be aimed or used on one or more targets can include the caster as one such potential target, unless the spell indicates otherwise.
If a spell specifies that it applies only to “chosen targets”, the mage can pick and choose; otherwise all in the area are affected indiscriminately.

A “visible” target is a target the mage can see or whose exact location is obvious to the caster.
If a maximum range isn’t given, assume it’s can be no more than a bowshot away from the wizard.

## 4.3.0 Dual Partial Spellcasters

Adventurers who take two different Partial Mage spellcaster classes use the table below to determine how many spells they can prepare and cast each day.
A dual Partial Mage can prepare and cast spells from either of their traditions.
PCs who pick one spellcaster partial and one non-spellcaster, like a Necromancer/Healer, do not use this table, but instead use the usual partial spellcaster table for their class.

#### PCs with Dual Spellcasting Traditions

| Level | Max Level | Spells Cast | Spells Prepared |
|------:|----------:|------------:|----------------:|
|     1 |         1 |           1 |               3 |
|     2 |         1 |           1 |               4 |
|     3 |         1 |           2 |               5 |
|     4 |         2 |           2 |               6 |
|     5 |         2 |           2 |               8 |
|     6 |         2 |           3 |               9 |
|     7 |         3 |           3 |              10 |
|     8 |         3 |           4 |              12 |
|     9 |         3 |           4 |              13 |
|    10 |         4 |           5 |              15 |

**Arts Gained**: As per both partial classes, gaining each level’s art picks as usual.

## 4.4.0 High Magic Spells

Any spellcaster can learn and cast High Magic spells, albeit High Mages tend to have the arts best suited for enhancing and manipulating these sorceries.

### Abdication of Temporal Presence Level 5

The caster and up to one visible ally per caster level briefly step outside of the conventional flow of time, the rest of the world freezing around them.
These subjects can take 1d4+1 free rounds of actions, but they cannot physically affect the world or move any object they were not carrying at the time they cast the spell.
Any spells the caster or other allies cast can affect only their own group, and not those entities still in the normal flow of time.

### Adopt the Simulacular Visage Level 3

A brief incantation transforms one visible, willing target per caster level into a perfect simulacrum of any humanoid creature the caster has seen before, whether a specific individual or a general type.
No special abilities are granted by this transformation, but the target will perfectly resemble a chosen individual, including in voice and scent.
Their non-magical clothing will transform to whatever clothing is appropriate to the target, and they will gain an intuitive ability to speak and understand the target’s native tongue.
The spell lasts until dawn of the day after casting it, whereupon the targets and any transformed clothing revert back to their original seeming.

### Apprehending the Arcane Form Level 1

The mage opens up their mind to the presence of occult energies.
For fifteen minutes per character level, they are capable of seeing active magical effects, curses, enchantments, or other dweomers as colored auras or patterns of light.
They may identify magical items and get a one-sentence description of their purpose or most significant powers, and may get more sophisticated answers with successful Wis/Magic skill checks at difficulties of 8 or more depending on the subtlety of the enchantment.
Mages with prepared spells are visible to the caster, though which spells the subject might have prepared is not knowable.
As a side effect of this spell, the ambient thaumic currents cast enough quasi-light to allow the caster to see normally even in perfect darkness.

### Banishment to the Black Glass Labyrinth Level 5

Aimed at a visible point within three hundred feet, the spell affects all creatures except the caster within a ten-foot radius of the target, translating them into a lightless extradimensional maze of endlessly tall obsidian walls.
The maze is infinite, featureless, and empty.
Transported creatures get a Mental saving throw to end the spell, re-appearing one round after it was cast.
Those who fail their save get to make an additional attempt once an hour afterwards.
Creatures with five or fewer hit dice cannot attempt these saving throws and are trapped forever unless the mage is killed or ends the spell.
Time passes normally while trapped, and creatures snared within may rest, fight, or starve as their situation and wills so recommends.
Dead or escaped creatures appear in the nearest clear space to their original departure.

### Calculation of the Evoked Servitor Level 2

This spell conjures up an intelligent familiar for the caster, one with one hit point per caster level, an AC of 14, a ground movement rate of 30’ per action, saving throws the same as the caster, a +0 skill modifier, and no effective attack.
The familiar always adopts the same shape for the same caster, though the initial casting can set this to any shape the caster wishes provided it’s no larger than a small human.
The familiar retains a telepathic connection with its creator and will obey any command it is given, including suicidal ones.
It can perform any action that a competent human servant could.
If the familiar is slain, it fades away, but can be called forth again by the spell.
The familiar retains its memories of what happens while it’s summoned, and can develop its own personality in time.
The familiar remains in existence until dawn following the spell’s casting or until dismissed by the caster.

### Calculation of the Phantasmal Eidolon Level 4

The caster conjures up a semi-real phantasmal creature in any shape or appearance they desire, provided it is no larger than an ox.
The servitor is as intelligent as a human and will obey the caster with fear less and suicidal devotion, having 4 effective hit dice, 20 hit points, AC 15, a movement rate of 30’ per action, a +1 skill modifier, saves of 13+, and a +6/1d8 damage melee attack with 2/AC 15 Shock.
As the creature is partly phantasmal, the damage it inflicts cannot kill a subject, but only knock them unconscious for an hour before they awaken with 1 hit point.
The caster may pick one special quality for the creature when it is summoned: the ability to fly at a rate of 30’/move, the perfect duplication of a particular person they’ve seen, the ability to form a telepathic speech connection with the caster, or the ability to effectively use normal weaponry and armor.
The servitor lasts until destroyed or the dawn after the spell is cast.

### Casting Forth the Inner Eye Level 2

A pool of water, mirror, open flame, polished crystal, or other luminous or reflective surface is used to peer at a distant location.
The location must either be within one hundred feet per caster level or be a location the caster has physically occupied before.
The caster perceives the location as if he were standing at the targeted point within it, seeing and hearing events there as if he were present.
This spell cannot scry locations that have been magically warded, and scrying the same location repeatedly is hindered by the resonance created by the spell.
The spell cannot perceive the same area twice within the same week, or position its target point so as to overlap a prior area with its perceptions within that time.
If a target present in the area has more hit dice than the caster has levels, they get a Mental saving throw to get an uncanny sense of being watched, something that will alert those cognizant of this spell.
This spell lasts until the caster performs some action other than focusing on the scrying.

### Cognitive Supersession of the Inferior Orders Level 1

The mage targets a visible, normal, non-magical animal or insect, obtaining temporary control of the beast and the ability to share its senses.
The animal gains a telepathic bond with the caster, obeying any non-suicidal command and allowing the mage to perceive everything it perceives.
The beast will not fight for the caster, but can perform complex actions entirely out of character for it while under the mage’s control.
The mage must focus to share the beast’s senses, requiring a Main Action and leaving the mage unable to act physically while so focused.
The spell lasts until the mage releases the beast, it is dispelled, or it is cast again.

### Conjunct the Vital Viscera Level 3

The caster renders the forms and life energy of up to one visible subject per caster level into something plastic and transferable.
Hit points, poisons, and diseases can be transferred from one creature to another, if both are willing or helplessly bound, healing up to the maximum allowed hit points of the target creature.
Body parts can be exchanged or gifted so long as each subject retains at least half of their original corpus.
A willing target can even be absorbed into the body of another subject of the spell, disappearing into them until the spell ends or they choose to spring back out, fully-equipped.
As similated subjects can continue to see and hear what goes on outside their carrier.
A given subject can absorb up to five other human-sized targets.
The spell lasts until dispelled, released, or one hour per caster level has passed, though transferred hit points or afflictions do not return to their original subject.
If a subject is killed while “borrowing” another’s body parts, those parts do not return.

### Conjunction of the Inexorable Step Level 2

A visible target within a hundred feet is immediately teleported to any visible, solid resting point within a half-mile, provided the target point has enough room to accept them and it is not a position of imminent physical peril.
Unattended objects can be translocated by the spell, but they can be no larger than a horse.
Unwilling creatures targeted by this spell may make a Mental saving throw to resist; on a success, it is the caster who is transported to the intended destination instead.

### Contingent Excision of Arcana Level 4

The caster sets up a triggered resonance of dispelling magic which may later be invoked as an Instant action.
Until the next dawn, the mage may negate magic as if with an **_Extirpate Arcana_** spell as an Instant action.
Once this negation is triggered, the spell ends.

### The Coruscating Coffin Level 1

A thaumic discharge is focused on a single visible creature within one hundred feet per caster level, wreathing them in a lethal mantle of crackling energy.
The target suffers 1d8 damage per caster level, with a Physical save for half.
NPC targets with only 1 hit die will inevitably be slain regardless of the damage done.
The spell cannot be blocked by non-magical intervening barriers, provided the caster can see the target with their unaided vision.

### Damnation of the Sense Level 1

The caster targets a visible creature within two hundred feet.
The target gets a Mental saving throw to resist; on a failure, one sense of the caster’s choice is entirely under the caster’s control for the rest of the scene, while a success leaves them bound only for the next round.
Any false impression may be given, or any true one concealed, and a creature may be left effectively blinded or deafened.
Distracting tactile sensations can force the victim to make a Physical saving throw in order to act each round.
Blinded creatures can’t make ranged attacks and roll all melee hit rolls twice, taking the worst result.
If a creature thinks itself in mortal peril its excitement allows it to make a Mental save to throw off the spell at the end of each round.

### The Dazzling Prismatic Hemicycle Level 5

A blinding fan of impossible colors cascades over all targets in front of the caster in an area equal to a cone a hundred feet long and a hundred feet wide at the end.
Each creature within that area must make a Physical saving throw; those who fail roll 1d6.
On a 1, they are unharmed, on a 2 they collapse into a handful of dust, on a 3 they fall unconscious for an hour, on a 4 they go violently insane for the next hour and attack all around them, on a 5 they turn to stone, and on a 6 they become utterly enthralled to the caster’s commands for the next hour, as if under the effect of an Ineluctable Shackles of Volition spell.

### Decree of Ligneous Dissolution Level 1

Wood, linen, cotton, rope, and other plant- or fungus-derived matter is annihilated by a wave of entropic force that washes through an area near the caster.
The mage targets a point within one hundred feet per caster level and designates a number of contiguous 10-foot cubes within that area equal to or less than their caster level.
All non-magical plant matter within that area immediately erodes away to dust.
Enchanted objects of plant matter cannot be affected, but ordinary plant-based clothing, bows, or wooden-hafted weapons will be destroyed.
If used against plant-based monsters, the spell does 1d10 damage per caster level, with a Physical save for half.

### Decree of Lithic Dissolution Level 2

Stone, earth, sod, sand, or other largely mineral material is reduced to a faint spray of fine dust by this sorcery, though metal and enchanted objects are unaffected.
The caster nominates up to one contiguous 10-foot cube per caster level in a visible area within one hundred feet per caster level, causing all such stone or mineral material within the cubes to disappear.
Such rapid destruction may well cause larger structures to collapse as well.
If used against rock-based monsters, the spell does 1d10 damage per caster level, with a Physical save for half.

### Deluge of Hell Level 5

The caster unleashes a consuming destruction on a visible point within three thousand feet.
A torrent of eldritch ruin rains down from the heavens on everything within up to a two hundred foot radius per caster level, inflicting 1d8 damage per caster level with a Physical save for half damage, and automatically killing all targets with 4 or fewer hit dice.
This damage is sufficient to destroy any wooden or lightly-built stone structure and will seriously damage even fortifications.
The caster can tighten the radius down to a minimum of 20 feet, but cannot be selective about targets within that area, This spell cannot be cast indoors, and is extremely hazardous to the caster; they must make a Mental saving throw on casting it or suffer a quarter of the damage inflicted, rounded up.

### Disjunctive Temporal Reversion Level 4

The caster may invoke this spell as an Instant action on any single creature within one hundred feet.
Time is rolled back slightly, allowing them to replay their current round of action as if it never occurred, though all involved retain a memory of what originally happened.
This spell is only useful on targets that are currently taking their round’s actions; once they’ve finished for the round and another creature has started acting, it is too late to benefit from this spell.
If the target is unwilling to roll back their action, they may make a Mental saving throw to resist the spell.

### The Earth as Clay Level 5

The caster molds the soil and stone of the land around them, raising hills, digging trenches, or forming simple structures out of extruded bedrock.
Once cast, the mage may psychically mold the terrain within three hundred feet per caster level, shifting it slowly over the course of an hour to form whatever shape they desire out of it, provided the material used can actually maintain such a shape.
Simple buildings and walls may be created out of bedrock or available stone, and tunnels and caves may be shaped up to 50 feet down from the caster, while hills may be raised up to 200 feet above the prevailing grade.
This spell cannot work within a thousand feet of stone or earth that has been significantly worked by intelligent creatures, though the use of this spell by the same caster doesn’t count as working the earth on further castings.

### Evert the Inwardness Level 4

A single visible target is selected, whereupon whatever contents that target may possess are extracted and placed in the caster’s hands.
If used on a cabinet, backpack, pocket, or other such container it can be no larger than an armoire, and the caster can choose whether or not to receive any particular object from inside it, being instantly appraised of its contents when the spell is cast.
If used on a creature, it attempts to tear out the subject’s innards; if the subject has equal or fewer hit dice than the caster, it must make a Physical save or die instantly.
Whether or not it perishes, it suffers 1d10 damage per caster level, with a Physical save for half.

### The Excellent Transpicuous Transformation Level 1

The mage chooses up to one visible willing target per caster level provided they are within 100 feet, though afterwards the targets can separate freely.
The targets and all they wear or carry become perfectly transparent.
Missile attacks against invisible foes are largely impossible, and melee attacks against them usually suffer a -4 penalty to hit rolls.
The spell lasts for up to an hour per caster level, but it breaks if a subject performs some violent motion, such as running, attacking, or casting a spell.
Once broken for one subject, it breaks for all.

### Exhalation of Congelating Cold Level 3

The caster invokes a gust of deepest winter on a point within one hundred feet per level, freezing everything in a radius up to ten feet per caster level.
All liquids freeze solid down to a depth of two feet and all living creatures not impervious to arctic cold must suffer 1d6 damage per two levels of the caster, with a Physical save for half damage.
Those who fail their Physical saves are numbed by the cold and lose their Move action each round for the next 1d4 rounds.
The frozen area warms again at the usual rate for the surrounding environment.

### Extirpate Arcana Level 2

The caster sweeps away all unwanted magical effects and enchantments within a twenty-foot radius, centered on any visible point within one hundred feet per level.
If the magical effect was cast by a creature with equal or fewer hit dice or levels, the effect is negated automatically.
Otherwise, a contested Int/Magic or Cha/Magic skill roll must be made, with higher-leveled caster gaining a +2 bonus on their roll and the dispeller winning any ties.
This spell is not strong enough to permanently suppress standing magical effects, and requires the aforementioned contested roll to have any chance to even temporarily suppressing them.
If successful, the effect is negated for 1d6 rounds.
If cast on a very large enchantment, only the portion within the spell’s zone of effect is suppressed.

### Foresightful Apprehension Level 3

The caster probes the near currents of fate to discern the likely immediate outcome of an action.
The caster describes a particular action they or a comrade intend to presently undertake, whereupon the GM tells them what is most likely to happen in consequence within the first five minutes after the action, as the GM thinks it most probable.
This spell cannot be used more than once a week on the same general topic.

### The Glass Chimes of the Bamboo Terrace Level 3

This spell calls forth a floating set of colored glass chimes.
The caster alone may strike them, producing sounds of great subtlety and penetration.
The caster may allow anyone in the desired range to hear the music, or may make it inaudible to anyone save specific targets within range.
Such is the expressiveness of the chimes that those who hear them may instantly understand the caster’s desired message, however abstract.
If the caster strikes the chimes violently as a Main Action, they may shatter them, causing a deafening clamor that does 3d6 damage to all non-deaf targets within forty feet except for the caster.
The maximum range of the chimes is ten miles per caster level, and they persist until shattered or the scene ends.

### The Grinding Geas Level 4

A single visible living creature is struck with a grim geas, forcing it to comply with a particular command or else suffer a progressively-worse affliction.
The caster may lay one single-sentence command on the target that is neither suicidal, indefinitely imprecise, nor likely physically impossible for them to comply with.
Thus, the caster could geas a man to kill his son or never seek to harm his lord, but he could not reasonably command a peasant to become king or bind someone to forever after comply with a master’s arbitrary commands.
If the target then defies that geas or unreasonably delays its execution, they suffer an agonizing progressive wasting disease that will inevitably kill them in 1d6 weeks, applying -2 to their hit rolls and -1 to skill checks for each week that passes and halving their maximum hit points.
If they again begin complying with the geas afterwards or if the curse is lifted, the disease halts and reverses itself at the same rate it progressed.
An active target can make a Mental save to resist this spell, but they cannot if they are restrained, unconscious, or otherwise subdued.
The curse lasts until dispelled or the caster lifts it.

### The Howl of Light Level 3

A flattened disc of tremendous heat, sound, and shock is triggered at a visible point within fifty feet per caster level, erupting in a twenty-foot radius eight feet in height.
Everything within the area suffers 1d8 damage from flame, concussion, and sonic shock per caster level, with an Evasion save for half.
If used in a space less than 40 feet in width, the explosion is channeled through adjacent spaces and passages for an additional 1d6 x 10 feet.

### Imperceptible Cerebral Divulgence Level 1

The mage focuses on a visible living creature within 100 feet per caster level.
For the rest of the scene, they immediately receive an impression of the target’s surface thoughts and interests, understanding them regardless of any lack of a shared language.
The caster may ask one question of the target’s memories per caster level, but doing so risks breaking the spell; the target can make a Mental save before answering each question and the spell ends if the save is successful.
These questions can only query memories, not compel any exercise of judgment or extrapolation.
This spell is very subtle in its casting, and can be invoked without any visible gestures or audible incantations.

### Ineluctable Shackles of Volition Level 1

The mage targets a visible living creature within 100 feet.
The subject must immediately make a Mental saving throw at a penalty equal to the caster’s Magic skill or become enthralled to the caster’s will.
Such victims will obey any physical commands issued by the caster, barring ones that seem suicidal or patently meant to result in their murder.
The caster cannot order the creature to answer questions or perform acts that require independent judgment, nor to use non-physical abilities or spells, but it will fight for the caster or to defend its own life.
En chanted victims appear torpid and dazed, and will act only to defend themselves or satisfy their needs.
The spell lasts until dispelled, or until the caster dies or releases them.
Every time this spell is cast, however, all prior subjects get an immediate unmodified Mental saving throw to throw it off.
If the creature has more hit dice than the caster, it gets an unmodified saving throw to end the effect the first time each day it’s ordered to do something it finds strongly objectionable.

### The Inexorable Imputation Level 2

The caster makes a single one-sentence statement as part of casting this spell, a process which is subtle enough to appear as no more than ordinary conversation.
All who hear the caster speak within a forty-foot radius must make a Mental saving throw or come to immediately believe the statement is true unless it seems physically impossible to them or it is emotionally intolerable to believe.
The listeners must be able to understand the caster’s language, and the caster may exempt up to two targets per caster level from this delusion when casting the spell, so as to avoid beguiling their companions.
The spell’s effects linger for one hour per caster level, after which the believers will be no more persuaded of the fact than events or their own common sense would allow.

### Invocation of the Invincible Citadel Level 5

The caster hurls up a magical shield that is impervious to almost all hostile powers.
Unlike most sorceries, this spell can be cast as an Instant action, and immediately creates a transparent bubble of force around the caster with up to a twenty-foot radius.
Entities and forces outside the bubble cannot penetrate it, and the interior remains a warm, breathable, safe environment regardless of the exterior.
Those within the bubble can pass out of it, but they cannot then return within.
The bubble can be dispelled by appropriate magics, but other enchantments and attacks cannot harm it, nor can magical effects or material objects pass through it from either direction.
The spell lasts until the caster departs from the bubble.

### The Jade Palanquin of the Faceless God Level 2

This spell calls forth a floating palanquin of ornately-carved green stone.
A slim three-foot-tall statue of a faceless entity stands at the center of the platform, while four slender pillars rise at the corners, the roof and sides being draped by metallic golden cloth.
The palanquin itself is twelve feet long and eight wide, with room enough for several people to sit on it.
It floats at shoulder-height above the ground or a liquid surface below.
It does not move of its own, but may be pulled along by even one human-sized bearer.
The total burden on the palanquin cannot exceed more than two thousand pounds, or it sinks to the ground.
It lasts until dusk, dawn, or until the statue of the god is struck or insulted, whichever comes first.

### The Long Amber Moment Level 1

This spell may be cast as an On Turn spell, provided no other has been cast this round, and targets a single willing or helpless creature the caster is touching.
The subject is shifted out of the flow of conventional time, freezing and being limned in a pale sepia light.
They and all their carried possessions are rendered impervious to all non-magical harm and are frozen in temporal stasis until the spell ends or is dispelled.
Enchanted creatures are quite light, counting as only four items of Encumbrance due to their unwieldiness.
The spell ends when the mage releases it, or up to a maximum of one day per caster level.
If cast on himself, the mage cannot end it before the full duration expires.

### Mantle of Disjecting Dissection Level 2

A visible willing creature within thirty feet is surrounded by a whirl of razor-sharp energy shards.
Anyone who attempt to touch the target or make a melee attack against them must make an Evasion saving throw or suffer 1d6 damage plus the caster’s level before resolving their attack.
The field of blades is indiscriminate and will affect even allies trying to touch the target.
The blades remain as long as the target gives up a Move Action each round in order to avoid disrupting them from within, up to a maximum of one scene.

### Obnubilation of the Will Level 4

This spell can only be applied to a helpless or restrained living victim, who gets a Mental saving throw to resist it and be forever after immune to its effects.
On a failure, the target becomes hopelessly subject to the caster via a series of psychic fetters, obediently carrying out their will.
The target must be able to understand the caster’s wishes, but will obey them to the best of its intelligence, capability, and initiative.
They must make a Mental saving throw to resist performing even suicidal acts.
The spell lasts until it is dispelled or the caster releases the target.
A caster can have no more creatures under this spell than twice their level or hit dice; if this number is exceeded, the earliest thrall is freed first.
Creatures under the effect of the Obnubilation of the Will display numerous small tics and magically-compelled quirks of behavior that may provoke puzzlement in casual observers and will give away the mental influence entirely to an onlooker with at least Magic-0 skill who can observe them for at least an hour.

### The Ochre Sigil of Juxtaposition Level 4

This spell is cast in two parts, each of which must be cast separately within a mile of each other, though such paired use counts as only a single spell slot.
The first casting inscribes a palm-sized seal of ruddy brown radiance on the ground beneath the caster.
The second casting will immediately switch the caster and all creatures and objects within ten feet with all creatures and objects within ten feet of the sigil.
The caster may refrain from swapping certain targets as they wish.
Only portable objects are swapped; any object too heavy for the caster to lift or secured in place remains unmoved.
Unwilling targets get no saving throw if they have four or less hit dice; others can make a Mental save.
Unused sigils dissipate in a day.

### Open the High Road Level 5

The caster carves open a metadimensional gate between their present location and a preset target point.
Attuning the target point requires an hour of effort, after which this spell will open an opaque portal to it from a distance of up to one hundred miles per caster level.
The portal is large enough to admit a cart and wagon and will remain open for up to one minute per caster level, ending when they pass through it.
The gate is one-way only, from caster to target.
Only living creatures and the vehicles they drive and objects they carry may pass through; atmosphere and other environments remain on the far side of the gate.
Only one target point may be prepared for this spell at a time, and there is a 1-in-10 chance for every casting that the real destination is 1d100 miles in a random direction, discovered only after the portal is used.

### Phantasmal Mimesis Level 1

The mage creates a phantasmal seeming at a visible location with in 100 feet per caster level.
The illusion can occupy a number of 10-foot cubes equal to the caster’s level and can include visual, audible, olfactory, and even tactile elements.
If onlookers have no reason to believe the illusion is false, they will unconsciously move and perceive so as to conform to its apparent physical qualities, halting before walls, reeling from imagined blows, and seeing their clothing burnt by phantasmal flames.
The illusion will behave and act on its own in accordance with the caster’s intentions for it, but it cannot go more than a hundred feet from where it was conjured.
Phantasmal monsters fight with the caster’s hit bonus, do 1d8 damage, have AC 10, and vanish if struck.
There can be no more than one active illusionary attacker per two caster levels.
Foes brought to zero hit points fall unconscious and wake up ten minutes later with 1 hit point.
Creatures convinced the illusion is false can make a Mental saving throw each round to reject its psychic influences, becoming immune to its effects.
The illusion lasts until dispelled, the caster drops it, or this spell is cast again.

### Prudentially Transient Abnegation of Life Level 2

This spell may be cast as an Instant action, even if another spell has been cast the same round, and targets either the caster or a willing visible target.
It can only be used in immediate response to an injury that reduces the target to zero hit points.
When it does, the target suffers dramatic and ostentatiously obvious death, with mortal blows cleaving them asunder or gorily butchering them.
They are dead to all mundane or magical examination.
Up to two hours later however, at the target’s discretion, the various remains of the target flow back together and restore their body with two hit points per caster level and two additional System Strain points gained.
If the target’s remains are intentionally scattered widely, incinerated, or entirely eaten, however, they cannot revive.
The subject is aware of their surroundings while “dead”.

### Phobic Storm Level 3

A wave of numbing terror sweeps over all enemies of the caster within forty feet.
They must immediately make a Morale check at a -1 penalty, with failure causing flight.
Those who succeed at the check suffer one point of damage per caster level from demoralization and despair, with those brought to zero hit points instead regaining one hit point and fleeing.
This spell has no effect on creatures that feel no fear or that have a Morale of 12.

### Pierce the Pallid Gate Level 4

The caster opens a short-ranged spatial rift between two points within one hundred feet per level, provided they can see or have physically occupied both points at some prior time and provided the two points are at least twenty feet apart.
The rift forms a gateway that connects the two points, one large enough to drive a cart through, and allows subjects on either side to see through and pass through the gate.
Ambient environmental qualities such as liquids or atmospheres will not pass through the portal; only intentionally-directed creatures and objects will transfer.
Once the creature has passed through a portal, it cannot pass through again until its next turn.
The portal remains open for up to one round per caster level, but can be shut as an Instant action by the caster.
Creatures partially in a rift when it closes are spat out on the far side.

### Resounding Temporal Echo Level 2

The caster imbues one visible ally per caster level with a burst of tremendous speed as their localized time begins to flow more rapidly.
For 1d4+1 rounds, all subjects get an extra Main Action during their turn.
This Main Action may not be used to cast spells or use arts.
This spell severely taxes those who take advantage of it, adding 1 System Strain for each round in which the recipient takes the bonus action.

### Scorn the Fetters of Earth Level 3

The caster and up to one visible ally per caster level are briefly grant ed the ability to move in three dimensions, being able to walk and run upward into the air as easily as along flat ground.
For the rest of the scene, those affected can move in such a way, remaining suspended in the air after their movement for the round.
When the spell ends, either naturally or upon being dispelled, all affected targets float gently to the ground.
Optionally, the caster can instead use this spell on one visible flying creature per caster level, which must make a Physical save at a penalty equal to the caster’s Magic skill or be forced to land as they had fallen half the distance they had descended.
For the rest of the scene, such creatures remain grounded and unable to fly.

### Sigil of Aeolian Auctoritas Level 4

A fierce gust of wind may be conjured in a line a hundred feet long and thirty feet wide.
All creatures of man-size or smaller must make Physical saves or be bowled over and blown back thirty feet, losing their next round’s Main Action and suffering 1d6 damage.
Light wooden constructions or similar structures are flattened by the wind.
If used outdoors, the spell can instead control the local weather, transforming the quarter-mile around the caster into any climate found normally at that location at any point during the year.
The spell cannot summon a tornado or other extreme weather, but it can call rain and conventional storms sufficient to cause flooding in areas prone to it.
The gust of wind is instantaneous, but the weather change lasts for one hour per caster level before reverting to its natural state.

### The Torment of Tumefaction Level 3

A single visible living creature is smote with a hideous curse of torment.
Boils erupt all over their body, blood weeps from their orifices, tumors engorge their flesh, and all of their hair falls out almost instantaneously.
If they perform any vigorous physical action save movement they incur two points of damage per caster level from the effects of the curse, damage which can be suffered no more than once per round by a target.
A creature may spend its Main Action to attempt a Physical save to throw off the curse and return to a glabrous normalcy, but on a failure, the curse remains for the rest of the scene.
If the creature has fewer hit dice than the caster has levels, a single failed save means the curse lasts indefinitely, until dispelled or until the caster releases them.

### Touch of Elucidating Intangibility Level 3

The caster touches a solid non-magical barrier when casting this spell.
A 10 foot cube of the barrier then becomes perfectly transparent on the caster’s side, allowing them to see and hear whatever lies on the other side as if it were well-lit and visible.
Optionally, the caster may make the barrier insubstantial for one round per caster level, allowing anyone to pass or shoot through it from either side.
The barrier always appears solid and opaque from the other side, even while the caster is peering through.
If someone is caught within the barrier when it becomes solid again, they’re spat out on the nearest clear side and suffer 2d10 damage.
The spell ends when the caster ceases to touch the barrier or immediately after it has been made insubstantial.

### Vallation of Specified Exclusion Level 3

The caster must form some sort of line as part of this spell’s invocation, either with dropped powders or a trace drawn in the dirt or a more permanent inlay into a floor.
In extremis, the caster can make such a line as part of a Move action, tracing it out or scattering powder as they go.
The line itself may be no longer than twenty feet per caster level, and may be straight or curved as the caster wishes.
Once it has been drawn, this spell may be cast, empowering the line with the ability to ward off a particular target and prevent them or their powers from crossing or being made to cross the line, as if it were a physical wall.
The caster can nominate any kind of target to be warded that could be distinguished without need for knowing their thoughts, such as “humans” or “non-humans” or “men wearing the livery of the baron”.
The barrier extends a hundred feet upward and ten feet downward and blocks any attack or magical power used by those warded.
If the excluded creatures are attacked or targeted by powers from the other side of the barrier, however, the entire field shatters.
The barrier lasts until dispelled, the caster drops it, or until one hour has passed per caster level.

### Velocitous Imbuement Level 1

Unlike most spells, this one may be cast as a simple On Turn action, targeting up to one visible willing creature per caster level within one hundred feet.
Enchanted creatures become incredibly fleet of foot, doubling their usual ground movement rate and becoming capable of running up walls and across ceilings without falling, provided they end their round upright on a navigable surface.
They may also move away from melee opponents without needing to make a Fighting Withdrawal to avoid a parting attack, and may pass through and around armed foes who do not completely physically block their path.
The spell lasts for the remainder of the scene and adds 1 System Strain to those who take advantage of it.

### The Verdant Vallation Level 2

A vast wall of thick, heavy vines and other plant life blooms from a visible point within one hundred feet per caster level.
The wall is up to twenty feet high and three feet thick and runs for as much as twenty feet in width per caster level.
The vine wall can be shaped as the caster wishes within the spell’s area of effect, so long as it’s contiguous, and may be laid horizontally if a bridge or roof is desired.
Enemies must inflict five hit points of damage per caster level to cut a man-sized hole in the vines, and they must be using weapons or means that could actually cut through a wooden wall to do any meaningful harm.
The vines may be optionally covered in long, vicious thorns to discourage climbing, inflicting 2d6 damage on any creature who tries to climb over it.
If the vine wall is summoned from earth that could plausibly support plant life, it remains until dispelled or until it dies naturally.
If called from bare stone or other infertile soils, it withers away to dust at the end of the scene.

### Visitation of the Clement Clime Level 2

The caster and up to three visible allies per caster level are shielded from the excesses of hostile elemental energies, becoming immune to mundane extremes of heat and cold and natural acids or electrical discharges.
Against magical harm of this nature, they automatically take only half the damage they normally would, or none if they make a successful Physical saving throw.
This spell lasts for one hour per caster level.

### Wardpact Invocation Level 1

This spell may be cast in two different forms.
If it targets a creature within 200 feet, the target becomes partially immune to physical weapons; any weapon hit on them requires the attacker make a successful Physical save or the hit is negated.
This effect lasts for one round per two caster levels, rounded up, and can’t target the caster.
If it targets a visible weapon within two hundred feet, that weapon is rendered entirely harmless and unable to inflict damage for the rest of the scene, with no saving throw.
The spell may only affect a given target or weapon once per scene and natural body weapons aren’t affected.

### The Wind of the Final Repose Level 1

The mage designates a visible point within two hundred feet.
A silent, invisible burst of soporific influence erupts from that point, targeting all living creatures within a twenty-foot radius.
All such targets with 4 or fewer hit dice within that area fall unconscious instantly, and can be roused only by damage or by a Main Action used to kick them awake.
If not roused, they revive at the end of the scene.
Entities that do not sleep are immune to this spell.

## 4.5.0 Elementalist Spells

Only Full or Partial Elementalists can learn or cast these spells.

### Aqueous Harmony Level 1

The elementalist and up to a dozen allies are charmed with powers of water-breathing, a tolerance for the pressure and cold of the deeps, and the ability to see through water as if it were well-lit air.
Ensorceled beings may move freely while in the water at their usual movement rate and their attacks and projectile weapons are not hindered by the medium, nor are their possessions soaked or damaged.
The spell lasts for one hour per caster level, but will not naturally end so long as a subject is still at least partially submerged.
Only the caster or magical dispelling can stop it under those circumstances.

### Flame Scrying Level 1

The elementalist becomes aware of the approximate locations of all open flames within thirty feet per caster level.
They may choose one of those flames as a focus for the scrying, allowing them to see and hear everything around the flame as if they were present.
The spell’s duration lasts for as long as the elementalist remains motionlessly focused on it; during this duration, they may switch their focus between the various flames in range as they wish.

### Elemental Favor Level 1

The elementalist makes a direct appeal to a non-magical mass of earth, stone, water, flame, or air no larger than a ten-foot cube.
At the end of the round, the mass will move or reshape itself within that space as the elementalist requests, maintaining its new form until the end of the scene.
If its new shape is one that is stable without magical help, it can be told to remain in it after the spell is finished.

### Elemental Spy Level 1

The elementalist enchants a stone, ounce of liquid, flame no smaller than a candleflame, or a particular plume of smoke or incense.
For one day per level, so long as the charmed object is not destroyed, dispersed, or consumed, they can as a Main Action see and listen to anything around the object as if they were standing there.

### Boreal Wings Level 2

The elementalist chooses a visible ally within one hundred feet; the target becomes capable of swift and easy aerial travel, flying at twice their usual movement rate.
If the spell ends or is dispelled while aloft, the target descends gently to the earth.
This spell lasts for one scene, though casters of fifth level or more can make it last an hour, and those of eighth level or more can make it last until dawn or dusk, whichever comes next.

### The Burrower Below Level 2

A passage is carved through natural stone or earth, forming a tunnel up to twenty feet long per caster level and up to ten feet wide and tall.
The caster can cause the earth to compress and pack itself so as to stabilize the tunnel even in very sandy or burdened soil, or they can allow it to collapse naturally if burdened by some large structure or unstable surroundings.
This spell can function against worked stone, but the length of the tunnel is much shorter, being only two feet per level.
Magical stone or earth cannot be altered by this spell.
The caster has basic control over the direction and interior features of the tunnel, and can form stairs or other simple structures within it.

### Flame Without End Level 2

A sample of flame no larger than the caster is made effectively eternal.
It no longer consumes the object it burns, though it can still be used to burn or heat other things, and it resists all extinguishing save being buried or wholly immersed in water.
The elementalist can temporarily extinguish it at will.
A number of such flames can be created equal to the elementalist’s level; beyond that, special ingredients and fuels are needed that cost 500 silver pieces per flame.
If used as a weapon, it adds +2 damage to a successful hit, albeit nothing to Shock.
The flame lasts until dispelled, extinguished, or the elementalist releases it.

### Pact of Stone and Sea Level 2

The elementalist chooses earth, water, fire, or wind when casting this spell and selects a visible target to be affected.
For the rest of the scene, the target is immune to injury caused by mundane manifestations of that substance; stone weapons don’t harm them, water doesn’t drown them, fire doesn’t burn them, and wind doesn’t topple them.
This affects secondary effects of the material as well; a fire-pacted mage couldn’t be boiled in a pot, and an earth-pacted one won’t be suffocated if buried alive.

### Elemental Vallation Level 3

A wall of a chosen churning elemental force can be called up by the elementalist.
The barrier is ten feet long per character level, with a height of ten feet and a thickness of one foot.
The barrier must rest on solid ground but may be bent or shaped as desired so long as no part of it is more than two hundred feet from the caster.
Earthen walls are impervious to anything but mining-appropriate tools or rock-shattering strength, taking 20 HP of damage to knock a man sized hole in them.
Fire walls inflict 3d6 damage plus the elementalist’s level on anyone who passes through them.
Water walls spin and hurl creatures of ox-size or less who pass through them, ejecting them at a random point on the far side of the wall and doing 2d6 damage from the buffeting.
Air walls are invisible, inaudible, and twenty feet in height; those who cross them suffer 1d6 plus the elementalist’s level in electrical damage.
The walls vanish at the end of the scene.

### Like the Stones Level 3

The elementalist charges their physical shape with the qualities of a chosen element for the rest of the scene.
In all cases, they need not breathe and become immune to poisons and diseases not already present in them.
If stone, they automatically stabilize at zero hit points and ignore the first three points of damage from any source of harm.
If water, they can pass through any aperture a mouse could get through.
If air, they can fly at their usual movement rate and gain a +4 Armor Class bonus against ranged attacks.
If fire, they inflict 1d6 damage to all creatures in melee range at the start of their turn each round and become immune to heat damage.

### Wind Walking Level 3

A visible target creature and their possessions are briefly transformed into a misty, insubstantial cloud.
Only sources of harm that could conceivably disrupt a cloud of mist can harm them, and until the spell’s end they may pass freely into any area that a vapor could reach.
They may move freely in all three dimensions at their normal movement rate, though they cannot physically manipulate objects.
The spell lasts until the end of the scene or until the target or the caster choose to end it.

### Calcifying Scourge Level 4

A visible target within one hundred feet must make a Physical saving throw or be turned to stone.
Any size of living creature may be so transmuted, though inanimate objects larger than a cart cannot.
Objects being held or worn by someone else get a Physical saving throw made by their user.
The calcification remains until dispelled or the caster undoes the magic, but if the object or creature is dam aged in the meanwhile, it may end up being harmed or killed on its restoration.
If the Physical saving throw is made successfully by a creature, the target is temporarily slowed, losing its Move action for the next 1d6 rounds.

### Elemental Guardian Level 4

The elementalist imbues a human-sized mass of earth, water, fire, or air with a crude awareness and an eagerness to defend them.
Whatever the substance used, it now has 4 HD, AC 15, a Move of
40’/action, a +1 skill bonus, saves of 13+, Instinct 0, Morale 12, and a melee attack of +6/1d10 with no Shock.
If called from earth, it has 6 hit dice, albeit its other stats don’t change.
If called from fire, it does 5/- Shock damage.
If summoned from water, it has an AC of
18, and if called from air, it can fly at its usual movement rate.
It has a human degree of intelligence, can communicate with others and manipulate objects, and serves with suicidal devotion.
Only one elemental guardian can be summoned at any one time, and if destroyed, a new one cannot be called that same scene.
A guardian persists until destroyed or until the dawn after they have been summoned.

### Fury of the Elements Level 5

A combination of molten rock, searing pyroclastic winds, and super heated steam erupts forth to ravage a chosen target point within two hundred feet per caster level.
The cataclysmic ruin smites everything within thirty feet of the target point for 10d6 damage, destroying all conventional structures.
The zone of devastation then moves 1d6x10 feet in a random direction at the start of the next round, blasting everything in its path.
The zone will continue to wander in this fashion for 1d6 rounds in total before dying out.
The molten remnants of the spell remain after this duration, a hazard for whomever enters the area for the rest of the day.

### Tremors of the Depths Level 5

The elementalist calls up a deep, rolling tremor from within the earth, centering it on a visible point and affecting all structures in a radius of up to five hundred feet.
This spell’s effects build slowly, requiring five minutes to fully manifest, but they can successfully topple or destroy any structures, tunnels, or caves within the affected area unless such structures are magically reinforced.
The effects are negated if the spell is dispelled within a minute after it was cast; after that, it’s too late to stop the effect.

## 4.6.0 Necromancer Spells

Only Full and Partial Necromancers have the necessary background to learn or cast these spells.

### Command the Dead Level 1

The necromancer exerts their will over a number of hit dice worth of undead equal to twice their character level.
These undead must be visible and within one hundred feet of the caster.
Undead get a Mental saving throw to resist this binding, at a penalty equal to the caster’s Magic skill.
Creatures only partially-bewitched by the spell due to their excess hit dice merely stand dazed for a round.
Those fully within the hit die cap who are affected become suicidally loyal to the necromancer until they are released by the caster.
Regardless of how often the caster uses this spell, they may have no more than twice their level worth of hit dice bound at any one time, with the oldest-enchanted being first released.

### Query the Skull Level 1

This spell requires a corpse with largely-intact organs of communication.
The corpse cannot have been dead for more than one day per caster level.
Once sorceled, the corpse will answer up to one question per caster level, with the caster understanding the answers regardless of the creature’s natural language.
Corpses are laconic, and generally answer in no more than one or two sentences; their replies will be truthful, but tend to be literal and they have no power to hypothesize or make judgments.
This spell may not be used twice on the same corpse.

### Smite the Dead Level 1

The necromancer conjures a blast of dispelling force at a point within one hundred feet per caster level, affecting an area up to 20 feet in radius.
All hostile undead within that area immediately suffer 1d10 damage per caster level.
Undead with hit dice equal or less than the caster’s level must make a Physical save or be destroyed outright.
The necromancer may Commit Effort for the day immediately before casting this spell; if so, its casting does not count against the Necromancer’s available spell slots for the day.

### Terrible Liveliness Level 1

A necromancer can give an undead creature the semblance of a healthy, normal living being with this spell.
The target appears as it did in life, at any point in its lifespan that the necromancer so desires, and is capable of performing all normal human activities that its cognition allows it to perform.
The spell uses the creature’s nature as a template for its effects, so the disguise is tangible and physically real until dispelled or the necromancer drops the effect.
A necromancer can maintain only one such disguise per level, and it does not work on sentient unwilling targets.

### Augment Mortal Vitality Level 2

The necromancer may refine and enhance the natural flow of vitality within a willing visible target.
For the rest of the scene, all Physical saving throws they make gain a bonus equal to the necromancer’s Magic skill and they automatically stabilize when Mortally Wounded.
Once during the scene, as an Instant action, they can immediately heal from all damage inflicted by an injury that did not Mortally Wound them.
The recipient of this spell gains one System Strain.

### Enfeebling Wave Level 2

A wash of debilitating force erupts in a 20-foot radius at a visible point within one hundred feet.
All living creatures within the area must make a Physical saving throw or for the rest of the scene their movement rate is halved and they must make all attack and damage rolls twice and take the worse result.
If the save is successful, these penalties apply only to their next turn.

### Final Death Level 2

The necromancer curses one visible target per level.
For the rest of the scene, these targets cannot recover or gain hit points and will die instantly if Mortally Wounded.
After each failed instance of healing, a target can make a Physical save to throw off the spell.

### Raise Corpse Level 2

The necromancer targets a mostly-intact skeleton or corpse, imbuing it with a semblance of life.
Whatever the creature’s attributes were in life, it now has 1 HD, an AC of 13, a Move of 30’/round, a +0 skill bonus, saves of 15+, a +1/1d6 unarmed melee attack, Instinct 0, and a Morale of 12.
Such corpses may be equipped with weapons or armor.
Its decay or dissolution immediately ceases, and it becomes suicidally loyal to its creator.
The corpse has no natural volition, but will obey commands with a human degree of intelligence.
It has only vague memories of its prior life, and while it may retain human tics or habits it had in life it can answer only the simplest and most self-evident questions.
Damage to a corpse can be repaired only by casting this spell on it again, which restores it to its original “health”.
The corpse continues to exist until it is reduced to zero hit points or its creator releases it.
A necromancer cannot have more active subjects of this spell than their character level.

### Compel Flesh Level 3

A visible living creature or physically-bodied undead within 100 feet is ensorceled by this spell, their flesh and bones becoming tempo rarily enthralled to the caster’s will.
The target becomes paralyzed unless commanded by the caster as an On Turn action; once given a command, their body will dutifully carry it out on their next turn.
The user’s mind is not affected by this spell, so they cannot be made to cast spells, answer questions, or perform other intellectual tasks, but they will fight, move, and perform other non-suicidal physical acts as normal.
The target may make a Physical saving throw at the start of each of their turns to throw off the effect, but they will inevitably suffer one point of damage per caster level with each attempt as their flesh writhes and tears.
If not thrown off earlier, the spell lasts until the end of the scene.

### Festering Curse Level 3

The qualities of a corpse are forced upon a visible living target.
The subject begins to rot, fester, and decay in whatever ways the necromancer thinks appropriate.
Food tastes like ashes, water does not quench thirst, and their body is numb to all physical pleasures.
They suffer a -2 penalty to all social skill checks due to their repugnance.
This transformation does not inflict physical injury, but it makes the tar get’s life an utterly joyless misery until it is dispelled or the necromancer lifts the curse.
Creatures with more hit dice than the necromancer has levels can make a Physical save to resist the curse.

### Forgetting the Grave Level 3

A necromancer can temporarily suspend the mortality of a willing target within sight.
For one round per Necromancer level, the creature simply cannot die, no matter how drastic their injuries.
Those reduced to zero hit points lose their Move action each turn but can continue to act otherwise; if such a target is damaged yet again, they must make a Physical saving throw each time or become incapable of movement until healed.
At the spell’s end targets reduced to zero hit points are Mortally Wounded; those that have been dismembered or otherwise slaughtered beyond the hope of survival die instantly.

### Merge Souls Level 3

By molding the plasmic stuff of life force, the necromancer can create a bond between two willing or helpless targets.
These subjects must be close enough for the necromancer to touch; thereafter, for as long as the spell is in effect, the two creatures pool their hit points.
Injuries to either subtract from this pool, and neither dies until it is reduced to zero, whereupon both are Mortally Wounded.
They can communicate telepathically at will while the spell is in effect.
The power ends at daybreak; if dispelled sooner, the remaining pool is split proportionately.

### Boneshaper Level 4

The clay of flesh and bone run obediently to the will of the necromancer.
Whether living or undead, a willing or helpless victim under the effects of this spell can be reshaped into any roughly-equivalent form with an hour’s work.
Limbs can be added or removed and existing tissue can be recolored, re-textured, or rearranged to the necromancer’s wishes.
Perfect imitation of a particular creature requires a Dex/Magic skill check against difficulty 10.
This spell is limited in the physical changes it can effect; a bonus of +1 can be added to modifier of Charisma or a physical attribute at the cost of a -1 penalty to the modifier of one of the same attributes, to a maximum of +2 or -2.
This spell lasts until the necromancer lifts it or it is dispelled.

### Raise Grave Knight Level 4

The corpse used for this spell must be of a relatively powerful creature with at least four hit dice or levels.
The result is much like that of the Raise Corpse spell, but this undead servitor is much stronger, with 4 HD, AC 15, a Move of 40’/action, a +1 skill bonus, saves of 13+, Instinct 0, Morale of 12, and a melee attack of +6/1d10, assuming it doesn’t use a normal weapon.
It is fully intelligent and self-willed, albeit utterly devoted to its creator, and it remembers a significant amount about its prior life.
A grave knight regains all lost hit points at dusk each day, assuming it’s not destroyed.
A caster may have only one grave knight active at a time.
One that has been reduced to zero hit points is destroyed and can only be rebuilt with a month of painstaking repair and re-enchantment.

### Call of the Tomb Level 5

The necromancer invokes the inescapable urge for self-dissolution within the substance of all that exists, amplifying the weight of their own mortality.
All enemy creatures within forty feet are affected.
Targets can make a Physical saving throw to resist, in which case the effects last only one round, otherwise lasting for one round per caster level.
During this time all attacks made against the victims automatically hit and all damage dice they suffer are maximized.
Any special defenses they may have against mundane weapons or certain types of injury are negated while the spell is on them.

### Everlasting Level 5

The spell may be triggered as an Instant action by the caster, imparting a burst of unquenchable life force to all allied creatures within 50 feet.
For the next five rounds, no affected ally can be reduced below 1 hit point, regardless of the damage inflicted upon them.
At the end of the spell’s effect, the caster’s own life energy is exhausted, leaving them with only 1 hit point.
A creature can benefit from this spell no more than once per day.

## 4.7.0 Developing New Spells

To research a new spell, the wizard’s player first writes up the spell they want to develop.
The GM then judges its appropriateness for the campaign.
If it passes, an appropriate level is assigned to the spell and the PC can begin the research process.
Researching a spell requires a properly-equipped laboratory, raw materials, and time.

### 4.7.1 Preparing To Develop a Spell

The laboratory must be established in a secure, serviceable room or building.
A lab sufficient for researching a level 1 or 2 spell can fit into a room, one for a 3rd level spell can fit into a house, one for a 4th level spell can fit into a wizard’s tower, and a lab suitable for devising a new 5th level spell needs its own subterranean research complex or similar edifice.
The cost for the lab is given on the adjacent table; this does not include the price for the building itself.
The necessary materials for a lab can generally be acquired in any major city, provided it’s not hostile to sorcerers.

The raw materials must also be purchased in a city or gathered from ransacked lairs by adventurers.
They include occult materials, esoteric creature body parts, specialized lab equipment, and obscure monographs and grimoires.
These materials are used up in the process of research.

The time required for researching a spell varies with its complexity, as given on the table.
A wizard can halve this time by spending twice as much on raw materials, and adventuring for certain lost grimoires or special ingredients might further decrease the time required.

A wizard can adventure and perform other tasks during their research time, including the development of Workings or the construction of magical items, but they can’t simultaneously research two spells at once.
Wizards kept entirely away from their labs for weeks or months at a time might suffer a halt to the work until they can get back to it.

#### Spell Research

| Spell Level |     Lab Cost | Materials Cost | Time Needed |
|-------------|-------------:|---------------:|------------:|
| 1           |    50,000 sp |      25,000 sp |     1 month |
| 2           |   125,000 sp |      50,000 sp |    2 months |
| 3           |   250,000 sp |     100,000 sp |    4 months |
| 4           |   500,000 sp |     200,000 sp |    8 months |
| 5           | 1,000,000 sp |     400,000 sp |     2 years |

### 4.7.2 The Development Skill Check

Once the lab is established, the raw materials gathered, and the necessary time taken, the mage makes an Int/Magic skill check against a difficulty equal to 10 plus the spell level.
If they have apprentices to assist them, they can add +1 to their skill check.
Specialist mages researching spells in their own field add an additional +1 to the skill check, so an Elementalist researching an Elementalist spell would get the bonus, as would a Necromancer researching a spell about undeath.
Special resources or uniquely apposite grimoires gathered on an adventure might add an additional bonus to the roll.

If the roll is successful, the spell is perfected and added to the caster’s grimoire.
They can teach it to other wizards if they wish, or keep it to themselves.
Specialist magic can only be learned by wizards of the same tradition; if they took the spell research skill check bonus, it’s a specialist spell.
Spells devised by High Mages are almost inevitably rediscovered High Magic and can be learned by any mage capable of casting such.

If the roll is a failure, the wizard has a choice.
They can abandon their research and start over from scratch, expending new resources and time, or they can roll on the formula flaw table.
This flaw becomes part of their spell, as adjusted by the GM.
Some flaws might not be problematic at all; a spell only ever designed to affect the caster isn’t much hindered by only being usable on willing targets.
Other flaws might make the spell worthless or force the PC to start over.
If the caster decides to live with the flaw and continue research, they can spend half the required research time and make a new skill check at a cumulative +1 bonus, with no need to spend additional resources.

A determined and unlucky mage may repeat this process several times, accruing new flaws each time and increasing their bonus until they eventually come up with a functioning spell.
It may be so gnarled by flaws as to be scarcely recognizable as their original intent, but they can add it to their grimoire all the same.


| d10 | Spell Formula Flaws                                                                                                                                                                                                                  |
|----:|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | The spell can only target the caster.                                                                                                                                                                                                |
| 2   | The spell only works on willing targets.                                                                                                                                                                                             |
| 3   | Your prior work is mistaken; the formula is not changed, but you don't get the cumulative +1 bonus on the research roll for this or prior research continuations.                                                                    |
| 4   | The spell can only target people other than the caster.                                                                                                                                                                              |
| 5   | The spell is unusually slow, taking at least a Main Action to cast, or two Main Actions over the course of two consecutive rounds if it already takes a Main Action to cast.                                                         |
| 6   | The spell is very draining, exhausting two spells worth of energy for the day instead of one.                                                                                                                                        |
| 7   | The spell inflicts a severe backlash on the caster, adding 1d4 System Strain to them. If this maximizes their System Strain, they fall unconscious for ten minutes and can't cast this spell again until some System Strain is lost. |
| 8   | The spell is more difficult than it seems, being one level higher than expected. This doesn't increase the research costs or time, but if you can't cast a spell of that level, the entire project fails.                            |
| 9   | The spell is simply unreliable in its effects; whenever it's cast, roll 1d6. On a 1, the spell fizzles uselessly and the casting slot is wasted.                                                                                     |
| 10  | The whole effort was a tragic mistake. All progress and research materials are lost and everything must be done over from the start.                                                                                                 |

## 4.8.0 Building Magical Workings

In the jargon of wizards, a Working is any stationary, persistent magi cal effect or structure, such a magical ever-flowering spring, an array of heatless eternal lamps, or a persistent curse that blights all within its reach.
Unlike a conventional magical item, a Working cannot be moved from its set location, and unlike a spell it will normally persist until damage or thaumic decay finally disperses it.

Workings come in five commonly-recognized tiers: trivial, minor, major, great, and supreme.
Trivial Workings might be some minor magic like an enchanted light source, while a supreme effort might transform a whole city into a flying metropolis.
While lesser Workings are still possible for skilled and erudite mages, supreme Workings are too mighty to be accomplished by anyone short of a legendary archmage.

A Working requires a skilled mage, a great deal of resources, and a considerable amount of time.
Details can vary based on the arcane suitability of the landscape or especially powerful, useful components, but even a trivial Working is no minor labor.

### 4.8.1 Designing the Working

To create the Working, the architect must first be a spellcaster of at least 6th level, whether a full Mage or a Partial Mage.
Mage classes that do not cast spells cannot normally create Workings, as their magic is insufficiently flexible.
Less-accomplished spellcasters also lack the practical experience necessary to mold the powers.

The architect then decides what exactly the Working should do.
The player involved discusses any custom ideas with the GM, judging the magnitude of each effect desired for the Working.
A single Working may involve multiple effects, but they should be closely aligned; enchantments that provide a magical spring, hot water, enchanted lamps, and a pleasant climate might all be established as part of the same housekeeping Working, but placing a ward against devils and a magical garden at the same time might not be so plausible.

The GM then decides the total difficulty of the Working by adding up the difficulty point cost of each element of it.
The adjacent table gives common ranges for each degree of difficulty, and the GM should pick a number that sounds right; the pettiest of petty Trivial magics might be 1 point, while something that could maybe even be Minor in strength would be 4 points.
The total cost of the Working is whatever element costs most plus half the rest, rounded up.
Thus, if some 10-point major effect also had a 3-point trivial effect and 8-point minor effect bundled with it, the whole would have a difficulty of 16 points.

This difficulty is then multiplied by the area the Working will affect.
If the magic spring merely pours a small stream of water into the kitchen cellar, the area might only be that of a Room; if the stream was meant to provide a moat around a wizard’s keep, it would affect a Building, or perhaps even a Village-sized area.
If the spring was to irrigate miles of surrounding countryside, it would affect a whole Region, and would probably be a great Working to boot, if not supreme.
The difficulty total is multiplied by the given multiplier of the biggest area affected, so if the 16-point example above affected the whole wizard’s tower, its final difficulty would be 64 points.

The architect must then demonstrate that they can actually design such a Working.
A given designer multiplies their character level by their Magic skill level times two.
Thus, a 6th level High Mage with Magic-3 skill would have a total of 36.
If this total is equal or greater than the Working’s difficulty, they can establish it alone.
If it’s at least half the difficulty, they can build it if they can find other mages to help them and make up the missing points.
If it’s less than half the difficulty, the whole enterprise is too difficult for them to envision.

### 4.8.2 Building the Working

If the Working is designed properly and enough help is had, it can be constructed at a cost of 1,000 silver pieces per point of difficulty and a time cost of one month, plus one week per five points of difficulty or fraction thereof.
If the cost is doubled, the work can be done in half this time.
Note that this construction only applies to the magical components of the Working; if the mage means to enchant a wall, the wall must already be built.
A mage can generally adventure and do other things while completing a Working, but if they are taken away from the site for too long, the work may halt in their absence.

While Workings are generally very durable, intentional sabotage of critical points or the slow decay of ages can end up corrupting or destroying them, sometimes with catastrophic results.
It is for this reason that many of the Workings found in ancient dungeons or forgotten ruins are dangerous or perverse, and many nations that could at least theoretically afford the construction of Workings avoid making use of them.

#### Magical Working Costs

| Degree  | Difficulty Points | Area Affected | Difficulty Multiplier |
|---------|------------------:|---------------|----------------------:|
| Trivial |               1-4 | Room          |                    x1 |
| Minor   |               4-8 | Building      |                    x4 |
| Major   |              8-16 | Village       |                   x16 |
| Great   |             16-32 | City          |                   x64 |
| Supreme |            33-64* | Region        |                  x256 |

\* Only demi-divine wizards can make these.

## 4.9 Magic Items and Enchanted Treasures

Statting out specific magic items is left as an exercise for the reader.
Certain particular types of items do exist, however.

### 4.9.1 Scrolls

Some scrolls, tablets, or other objects can be imbued with one or more spells by wizards who craft them.
Each spell in a scroll can be used only once.
A user must grasp the item firmly and spend a Main Action, wasting the spell if they are struck or otherwise disturbed during the action.
The spell is then cast, with details of targeting and focus determined by the user.

Releasing a scroll safely requires a clear understanding of its magic.
A user must have a Magic skill no more than one less than the suspended spell’s level in order to safely trigger it.
If a wielder doesn’t have a high enough Magic skill, they can attempt to use the item anyway, albeit at considerable risk.
An Int/Magic or Cha/Magic skill check is needed against a difficulty of 8 plus the spell level.
If it fails by one or two points, the spell fizzles and is wasted.
If it fails by more than two points, it goes off, but at the wrong target or with the wrong effect at the GM’s discretion.
If the check succeeds, the spell goes off as intended.

Spellcasters can create their own scrolls for later use, creating them as single-use items.
The expense is 1,000 silver per spell level, and the difficulty is 7 plus half the spell level, rounded up.
While a scroll takes only a week to inscribe, the process is exhausting, and a wizard can do it no more than once a week per spell level inscribed.

### 4.9.2 Potions

Some magical effects can be contained in an ounce or two of liquid, oil, incense, or other consumable substance.
Using a potion requires that it be Readied and a Main Action be used to consume it.

**Apprehending the Arcane Form** can give a one-sentence description of the potion’s intended effect.
Touching a single drop of the fluid against a user’s tongue can sometimes give a hint as well, with a tiny flicker of its effect impressed on the user.
Aside from these, someone with the Magic skill can spend a full day in careful analysis of the potion, rolling Wis/Magic or Int/Magic against the potion’s creation difficulty to identify its purpose.
A given investigator can make only one attempt to identify it.

Wizards can create potions as single-use magic items, with the base cost and difficulty varying depending on the nature of the potion’s effect.

### 4.9.3 Magic Weapons and Armor

Magical weapons and armor have a bonus, usually +1, but some times as great as +3.
This bonus adds directly to the hit roll, damage roll, and Shock of weapons, while it adds to the base Armor Class of armor.
Some weapons and armor have additional special abilities.

Magical shields with special powers do exist, but shields do not get numeric bonuses as armor does.

### 4.9.4 Magical Devices

Enchanted wands, magic boots, ensorceled cloaks, and other devices of magic are rare, but do exist.
These items usually require a Main Action to trigger any active abilities they grant, while passive benefits are automatically granted to the wielder.

Most magical devices are permanent, but wands, rods, and staves often have a limited number of charges.
It is usually prohibitively difficult to recharge such devices.

## 4.10 Creating Magic Items

All permanent magical items are difficult to make.
Even if the item is nothing more than a tankard that keeps its contents perpetually chilled, making that tankard is every bit as difficult as forging an enchanted sword.
As a consequence, very few sorcerers bother to make petty items; if it’s going to be as hard to make a trifling token as a significant one, why make trifles?

Magical items are exceedingly expensive to build.
The rare components, expensive rituals, and costly processes involved eat up vast amounts of silver and great labor on the part of the mage.
These components must be bought at some major city or salvaged from the dungeons or ruins that the PCs are exploring.
They form many of the same components that are used in building Workings or researching new spells, so the same general pool of magical components can be used for any of those purposes.

Magical items are also complex and difficult to create.
A novice mage cannot fashion them and even an expert might find it difficult to get an item to come out just right.
Flaws can creep into the construction process, forcing the PC to either start over or cope with an item that isn’t exactly what was intended.

### 4.9.1 Designing the Item

To build a magic item, the player first describes what it is that the item is supposed to do.
It’s up to the GM to decide whether or not the item fits with their campaign and is an acceptable introduction.

For item designs, it’s recommended that the GM be careful not to allow magic items that simply solve whole categories of problems.
A set of magic earplugs that make it impossible to hear lies may seem clever, but it also immediately solves any challenge revolving around detecting deceit.
Boots that grant perpetual flight, apotropaic wands that banish specific types of creatures, and other items that simply remove certain problems from the party’s concerns should probably be denied.
Even if they’re charged or limit-use items, they’ll probably be available whenever the party really needs them, which means those challenges that would otherwise be the most critical become the ones they most easily bypass.

A GM should also be careful about items that simply add bonuses to the PC’s rolls, whether skill checks or combat rolls.
Magical weapons and armor do exist, and there are some items that do simply add numbers to the PC, but these should be avoided in other cases.
There’s a reason that most veteran Warriors are eager to find a magical weapon; such a weapon simply makes them better at their most important function.
If other items exist that simply increase critical numbers, then the other players will feel obligated to hunt them down.

If the item’s concept passes muster, the GM should compare it to the adjacent table to see what kind of price and minimum difficulty level should be required to make it.
Spellcasting wizards can generally make any kind of magic item, while non-casters such as Vowed or Healers are usually restricted to making items appropriate to their particular concepts, whether those are magical scriptures or healing elixirs.
A GM should always feel free to adjust prices and difficulties to reflect their own sense of what’s appropriate for their campaign.

In addition to the minimum level and money involved, permanent magic items always require at least one adventure to acquire the necessary components.
The wizard will have researched the item sufficiently to know where they need to go and what they need to fetch, but it will always be dangerous and difficult to do so.
Very capable underlings or hired adventurers might be able to fetch the required component, but it’s up to the GM whether such efforts are successful.
From a GM’s perspective, this required adventure is to ensure that a wizard who makes a permanent magic item provides at least one session worth of adventure grist in exchange for the new gear, and it also ensures that not too many permanent magic items will be made unless the party agrees to constantly be out adventuring for parts.

### 4.9.2 Creating the Item

If the creator is capable, the coin is at hand, and any adventuring components have been fetched, the mage can attempt to make the item.
They spend the time given on the adjacent table and then make an Int/Magic skill check against the appropriate difficulty.
If they’re making a batch of limited-use items, such as a batch of magic potions, they can make two doses for a +1 difficulty or four doses for a +2 difficulty.
If they have an apprentice to aid them, they can add +1 to their skill check.

If the check is a success, the item is made.
If it’s a failure, they have a choice; they can start over from the beginning, spending the money and time anew, though not needing to repeat any adventure the item might have required.
They can then make a second attempt at creating the item.
If they are reluctant to do this, the item incurs some kind of flaw or unfortunate side effect to its power, suffering that hindrance to the item’s eventual effect as adjusted by the GM.
If that flaw isn’t intolerable, they may spend half the time they originally took to make another skill check to make the item at a cumulative +1 bonus.
No additional coin need be paid.
They can repeat this process, adding a new flaw each time and paying half the original time, until they either succeed or the flaws become intolerable.

A mage can generally keep adventuring while crafting a magic item, as the work doesn’t eat up all their spare time.
Particularly massive or powerful magic items might require the use of a dedicated laboratory, forcing the wizard to remain there while the work is under way lest the process be spoiled.
Any special tools or resources such a laboratory requires are assumed to be part of the item’s creation cost.

#### Magic Item Creation Costs

| Type of Item                                                         | Creation Difficulty | Creation Cost in SP | Creation Time |
|----------------------------------------------------------------------|--------------------:|--------------------:|--------------:|
| A single-use item, such as a potion or scroll                        |                8–10 |           250–2,500 |        1 week |
| A multi-use item that still contains limited charges, such as a wand |                9–11 |        5,000–25,000 |       1 month |
| A low-powered but permanent magic item, such as a Sword +1           |                  10 |              12,500 |       1 month |
| A significant item that creates a situation-changing effect          |                  12 |              50,000 |      3 months |
| A powerful item or one with multiple significant abilities           |                  14 |             250,000 |      6 months |

# 5.0.0 Monsters and Foes

The following page includes a list of example stat lines for various kinds of people and creatures.
These are not universal truths for every being in the campaign setting, but they’re good baselines for what to expect from a given creature.
As a first step in building a creature a GM should pick the stat line that fits best the type of foe they need, and then modify it to suit their own purposes.

Note that there’s nothing stopping you from putting a guaranteed party-slaughterer in your campaign.
If the situation logically requires that such a creature be present, then it ought to be there.
If logic re quires its existence, however, you should take pains to ensure that the PCs are not forced to actually fight the thing.
They need to be able to get forewarnings of its presence, or opportunities to flee it, or some means to negotiate with it or hide from it.
It’s not unfair to populate your world with the creatures that ought to live in it, but it’s decidedly unfun to shove hapless PCs face-first into certain death.

## 5.1.0 Monster and NPC Statistics

The table of example stat lines has several columns, each one listing a particular statistic for the creature.

**Hit** dice are a measure of the creature’s general power, not unlike a level rating for PCs.
For each hit die a creature has, it rolls 1d8 for its hit points.
Most ordinary humans have only one hit die, while veterans of bloody struggle or ruthless court intrigue might have two, or three, or even more for the most heroic among them.

**AC** is for the creature’s Armor Class.
The higher this number, the harder it is to meaningfully hurt the thing.
Monsters and wild beasts have an Armor Class appropriate to their agility and the toughness of their hide; 12 or 13 for quick things with leathery skins, up to 15 for very well-armored beasts, or even up to 20 for things with supernatural hardihood.
Humans and other sentients usually have whatever Armor Class is granted by the armor they wear.
Some creatures have an “a” annotation with their AC; this just means that the creature wears armor and the AC given is what their usual armor is worth.

**Atk** is the creature’s usual total attack bonus for its hit rolls in combat.
For most creatures, this is equal to its hit dice, possibly with a bonus if it’s well-trained, exceptionally vicious, or supernaturally powerful.
Some creatures have more than one attack, indicated by an “x2” or “x3” notation.
This means the creature can attack two or three times with a single Main Action, directing them all at a single creature or splitting them up among nearby foes within reach.

**Dmg** is the damage done by a successful hit by the creature.
If the listing says “Wpn”, then it does whatever damage is usual for the weapon that it’s wielding.
A creature will never do less damage on a hit than it would do with its Shock score, if Shock would apply to the target.

**Shock** is the Shock damage inflicted by the creature and the maximum AC it affects.
Thus, “3/13” means that the creature inflicts a minimum of 3 points of Shock damage on a miss to any foe with an AC of 13 or less. “Wpn” means the usual Shock damage of the weapon being used is applied.
Exceptionally powerful or savage creatures might automatically apply Shock regardless of the AC of the foe; such creatures have a dash listed for the maximum AC, such as “3/-”.
Such damage is always applied unless the foe is immune to Shock.

**Move** is the distance the creature can move with a single Move action.
Some creatures may fly, others swim, or still stranger means of locomotion may apply depending on the beast’s nature.

**ML** is the creature’s Morale score.
Whenever a Morale check is forced by a situation, the creature must roll 2d6.
If the total is greater than its Morale score, it loses its taste for the fight and will retreat, surrender, or otherwise take whatever actions seem best to get it safely away.

**Inst** is the creature’s Instinct score.
When confused, infuriated, or goaded in combat, it runs the risk of behaving according to its instincts rather than martial prudence.

**Skill** is the creature’s total Skill bonus for any skill checks it makes that are in line with its talents and abilities.
If the creature ought to be good at something, it can add its Skill bonus to the base 2d6 skill check.
If not, it adds +0, or might even take a penalty if it seems like something it would be exceptionally bad at doing.

**Save** is the saving throw target used by the creature whenever it’s called upon to make a Physical, Mental, Evasion, or Luck saving throw.
Unlike PCs, creatures only have a single save target, usually equal to 15 minus half its hit dice, rounded down.
Thus, a foe with 3 hit dice usually rolls 14+ to succeed at any saving throw.
This score can’t be less than 2+, as a 1 on a saving throw always fails.

| Normal Humans            | HD |  AC |   Atk. |  Dmg. | Shock   | Move | ML | Inst. | Skill | Save |
|--------------------------|---:|----:|-------:|------:|--------:|-----:|---:|------:|------:|-----:|
| Peaceful Human           |  1 |  10 |     +0 |   Wpn | Wpn     |  30' |  7 |     5 |    +1 |  15+ |
| Thug or Militia          |  1 | 13a |     +1 |   Wpn | Wpn     |  30' |  8 |     4 |    +1 |  15+ |
| Barbarian Fighter        |  1 | 13a |     +2 | Wpn+1 | Wpn+1   |  30' |  8 |     5 |    +1 |  15+ |
| Veteran Soldier          |  1 | 13a |     +2 | Wpn+1 | Wpn+1   |  30' |  8 |     3 |    +1 |  15+ |
| Skilled Veteran          |  2 | 15a |     +3 | Wpn+1 | Wpn+1   |  30' |  9 |     2 |    +1 |  14+ |
| Elites or Special Guards |  3 | 18a |     +4 | Wpn+2 | Wpn+2   |  30' | 10 |     2 |    +2 |  14+ |
| Knight or Minor Hero     |  4 | 18a |     +6 | Wpn+2 | Wpn+2   |  30' | 10 |     1 |    +2 |  13+ |
| Warrior Baron            |  6 | 18a |     +8 | Wpn+3 | Wpn+3   |  30' |  9 |     1 |    +2 |  12+ |
| Barbarian Warlord        |  8 | 16a | +10 x2 | Wpn+4 | Wpn+4/- |  30' | 10 |     3 |    +2 |  11+ |
| Mighty General           |  8 | 18a |    +10 | Wpn+4 | Wpn+4/- |  30' | 10 |     1 |    +3 |  11+ |
| Major Hero               | 10 | 18a | +12 x2 | Wpn+5 | Wpn+5/- |  30' | 10 |     2 |    +3 |  10+ |
| Great Warrior King       | 12 | 18a | +14 x2 | Wpn+5 | Wpn+5/- |  30' | 10 |     1 |    +3 |   9+ |

| Spellcasters     | HD | AC | Atk. |  Dmg. | Shock | Move | ML | Inst. | Skill | Save |
|------------------|---:|---:|-----:|------:|------:|-----:|---:|------:|------:|-----:|
| Petty Mage       |  2 | 10 |   +1 |   Wpn |   Wpn |  30' |  8 |     4 |    +1 |  14+ |
| Tribal Shaman    |  4 | 10 |   +3 | Wpn+1 | Wpn+1 |  30' |  9 |     4 |    +1 |  13+ |
| Skilled Sorcerer |  5 | 10 |   +1 |   Wpn |   Wpn |  30' |  9 |     4 |    +2 |  13+ |
| Master Wizard    |  8 | 13 |   +1 |   Wpn |   Wpn |  30' |  9 |     3 |    +2 |  11+ |
| Famous Arch-Mage | 10 | 13 |   +2 |   Wpn |   Wpn |  30' |  9 |     2 |    +3 |  10+ |

Mages generally have the spellcasting and Arts of an appropriate mage tradition at a level equal to their hit dice and Effort equal to their skill bonus plus two.

| Normal animals          | HD | AC |  Atk. | Dmg. | Shock | Move | ML | Inst. | Skill | Save |
|-------------------------|----|---:|------:|-----:|------:|-----:|---:|------:|------:|-----:|
| Small Pack Predator     | 1  | 12 |    +2 |  1d4 |  1/13 |  40' |  7 |     6 |    +1 |  15+ |
| Large Solitary Predator | 5  | 13 |    +6 |  1d8 |  2/13 |  30' |  8 |     6 |    +1 |  13+ |
| Apex Predator           | 6  | 13 | +6 x2 |  1d8 |  2/13 |  40' |  8 |     6 |    +2 |  12+ |
| Herd Beast              | 2  | 11 |    +2 |  1d4 |  None |  40' |  7 |     6 |    +1 |  14+ |
| Vicious Large Herbivore | 4  | 13 |    +5 | 1d10 |  1/13 |  40' |  9 |     6 |    +1 |  13+ |
| Elephantine Grazer      | 6  | 13 |    +5 |  2d8 |  None |  40' |  7 |     6 |    +1 |  12+ |

| Unnatural Entities        | HD | AC |   Atk. |   Dmg. | Shock | Move | ML | Inst. | Skill |     |
|---------------------------|----|---:|-------:|-------:|------:|-----:|---:|------:|------:|----:|
| Save Automaton, Humanlike | 2  | 13 |     +2 |    Wpn |   Wpn |  30' | 12 |     3 |    +1 | 14+ |
| Automaton, Laborer        | 2  | 15 |     +2 |    1d6 |  1/13 |  30' | 12 |     3 |    +1 | 14+ |
| Automaton, Military       | 4  | 18 |     +5 | 1d10+2 |  4/15 |  30' | 12 |     3 |    +1 | 13+ |
| Automaton, Warbot         | 10 | 20 | +12 x3 | 1d12+5 |   7/- |  40' | 12 |     2 |    +2 | 10+ |
| Slime or ooze             | 6  | 10 |  +6 x2 |    1d8 |   1/- |  20' | 12 |     5 |    +1 | 12+ |
| Predator, Small Vicious   | 1  | 14 |     +1 |    1d4 |  1/13 |  30' |  7 |     5 |    +1 | 15+ |
| Predator, Large Vicious   | 6  | 13 |  +7 x2 |    2d6 |  2/15 |  40' |  9 |     5 |    +2 | 13+ |
| Predator, Hulking         | 10 | 15 | +12 x2 |  2d6+3 |  6/15 |  30' | 10 |     4 |    +1 | 10+ |
| Predator, Hellbeast       | 10 | 18 | +12 x4 | 1d10+5 |   6/- |  60' | 11 |     4 |    +3 | 10+ |
| Unnatural Swarm           | 4  | 10 |  +6 x3 |    1d6 |   1/- |  30' | 10 |     5 |    +1 | 13+ |
| Terrible Warbeast         | 8  | 15 | +10 x2 |  2d6+4 |  7/15 |  40' |  9 |     4 |    +2 | 11+ |
| Legendary God-Titan       | 20 | 22 | +20 x3 | 2d10+5 |  10/- |  40' | 10 |     3 |    +3 |  2+ |

### 5.1.1 Powerful Foes

These statistics are only a bare framework for most ordinary creatures. An entity of special power, such as a heroic knight or monstrous beast, should likely have at least one special ability related to their skills or nature. Potent enemies without a significant number of special defenses and powerful attack modes can often be chewed down rapidly by a PC party. Granting major enemies multiple actions per round and a good selection of special powers is generally necessary to make them a worthy opponent for a veteran party.

## 5.2.0 Reaction Rolls and Parleying

These rules do not encourage constant combat encounters. Heroes are fragile, foes are dangerous, and almost every fight runs some risk of downing at least one PC. GMs or players who arrange their games as a curated sequence of battles are going to rapidly run out of luck.

GMs need to constantly recall the fact that not every hostile en counter needs to end in a massacre. The denizens of this world are just as aware of their own mortality as the PCs are, and they will not pick chance fights that they do not expect to win decisively. Even intrinsically hostile creatures will take a moment to size up a situation.

Unless the situation is so patently destined for bloodshed that all negotiation is futile, a GM should always make a reaction roll when ever the PCs encounter another creature or group, whether friendly or hostile. This roll will indicate the general mood of the encounter, and whether the subjects are likely to be amenable to negotiations.

### 5.2.1 Making a Reaction Roll

To make a reaction roll, roll 2d6 and compare it to the adjacent table. If a PC is in a position to greet the targets, add their Charisma modifier to the roll.

The higher the reaction roll, the friendlier and more helpful the NPCs will be. This doesn’t mean that goblin raiders will invite the PCs to drink with them, but it does mean that otherwise violent groups might decide to demand a bribe instead, or back off rather than risk losing lives to the heroes. Conversely, a low roll means that the group is more hostile and unhelpful than they might be expected to be.

Once a roll is made, the GM should clearly clue the PCs into its general results. If the bandits are feeling sociable, the GM needs to let the players know that they’re looking relaxed and leaning on their spears. If the wild beast is getting ready to attack, it should be described as crouching and snarling as it begins to creep forward. This information needs to be given before the PCs choose their initial actions, or else a lot of potential parleys are going to be erased by the immediate and judicious drawing of swords.

A reaction roll applies to non-martial encounters as well. Determining the initial mood of a government clerk, the temper of a merchant, or the attitude of a noble patron can all be done by a reaction roll. This attitude will likely color the difficulty and nature of any negotiations or social skill checks the PCs might try to conduct with the target.

Reaction rolls are only the start of an encounter, not the end. Clever words, persuasive arguments, or prudent gifts can all shift the attitude of an NPC, as can insults, threats, and looking excessively tender and delicious. Provided the NPCs aren’t the sort to simply attack, the PCs always have a chance to salvage a meeting.

### 5.2.2 Peaceful Encounter Reactions

Suppose a GM’s just rolled a 12 for the PCs’ roadside encounter with a band of zealous blood cultists. The GM knows that the cultists are vicious but have no pressing reason to immediately attack the PCs, so what does “friendly” mean in such a situation? If you find yourself having to figure out plausible reactions for otherwise-hostile groups, here are some suggestions.

**They demand a bribe.** They’ll take money, gear, food, booze, praise to their dark god, or some other currency for peace.

**They back off.** If they aren’t defending their lair or carrying out some critical task, they may just decide to back away and keep their distance as the groups pass by. They may bring word of the adventurers to their comrades, but a fight under the present terms might look like a bad deal to them.

**They ask for favors.** Maybe they have an enemy they want killed, or a task they need done. They may trade offers of ignoring the PCs while they remain in the area in exchange for the help, or offer an outright bribe to the PCs to get them to cooperate. Even the most hostile raider group might be willing to let a few targets go in exchange for some profitable work being done.

**They offer tribute.** They’ve looked at the PCs and decided that their own lives are in danger. They’ll offer wealth, information, services, or other inducements to get the PCs to leave them alone. This reaction grows more possible if the PCs have been carving a bloody swath through their surroundings.

**They willingly socialize.** Maybe the bandits have been out here so long that they’re lonely for civilized conversation, and the PCs look too dangerous to engage. The goblin raiders might’ve mistaken them for allied marauders and invite them to share their camp. The vile necromancer might consider herself a perfectly respectable person who loves good dinner conversation. However it’s sliced, the NPC could be willing just to have a nice chat.

|  2d6 | NPC Reaction                                                                     |
|-----:|----------------------------------------------------------------------------------|
|   2- | As aggressively hostile as the situation allows                                  |
|  3–5 | More unfriendly and hostile than they’d be expected to be in the given situation |
|  6–8 | As predictably hostile or friendly as they’d usually be in this situation        |
| 9–11 | More friendly and benign than you’d expect them to be, given the circumstances   |
|  12+ | As friendly and helpful as their nature and the situation permits them to be     |

## 5.3.0 Morale Checks and Fleeing

Sometimes, however, combat is inevitable.
The raiders swoop down on the heroes, the savage beast pounces, or the vile necromancer runs out of small talk.
Even after blades are drawn, however, the hostile NPCs might come to think better of their choices when they fail a Morale check.

### 5.3.1 Making a Morale Check

A Morale check is made by rolling 2d6 and comparing it to the creature’s Morale score.
If the roll is greater than the score, the creature loses heart and will seek to flee or stop the battle.
PCs never make Morale checks and will fight on until they decide to flee.

A Morale check is usually made under certain circumstances, though the GM can add to these whenever they think the situation calls for one.
More than one Morale check may be needed in a fight if more than one condition occurs.

- When a non-combatant civilian is first faced with the prospect of serious physical harm.
- When the first member of a group is killed or rendered incapacitated.
- When the group starts to visibly lose the fight or see their odds of victory considerably shrink.
- When the group faces some terrifying work of magic, a horrendous slaughter, or a vastly superior foe.

The consequences of a failed Morale check will vary based on the situation and the creatures being tested.
Non-combatants and undisciplined fighters will generally flee madly, dropping shields and abandoning burdens as they try to escape their doom.

Trained and experienced warriors will usually make a fighting withdrawal, trying to pull away from their enemies and escape back to safety with as many of their comrades as they can.
Of course, in the face of truly devastating situations they may flee just as readily as their green compatriots.

If flight seems impossible or prohibitively dangerous, they might throw down their weapons and beg for mercy, if they think they have any chance of receiving it from their assailants.
If that seems hopeless, they might just collapse in terrified despair as they pray to their gods, or fight with a renewed frenzy in a desperate attempt to cut their way out of the trap.
Some may offer bribes or favors to win their lives.

In all cases, once a side has failed its Morale check, it’s not going to be in a condition to fight those foes until it’s had some time to recover its courage.

#### The Importance of Morale

It’s very important that GMs keep Morale checks in mind and use them regularly during combat.
Only truly abnormal creatures such as mindless undead, unthinking vermin, or command-bound automatons will fight relentlessly onward regardless of the situation.
If every goblin warrior is a Spartan at Thermopylae, the PCs are going to lose a lot of comrades cleaning up fights that were clearly won five rounds ago.

Aside from that, warriors are not fools, and they will not linger to fight battles that cannot be won and are not worth their lives.
Even if they never fail a Morale check, they will not stay to die to the last man if there’s no pressing reason for them to do so.
Once they see that victory is a vain hope, they’ll try to withdraw or to cut some kind of deal with their enemies.

Of course, not all beaten sides respond rationally.
The terror of battle, the confusion of melee, and a misunderstanding of the situation might leave some few warriors battling on even when all is lost, simply because they haven’t realized that all their friends are dead yet.
Green troops are notorious for sometimes achieving military goals that veterans never could, because veterans would recognize certain failure much earlier.
The same can sometimes apply to frenetic barbarians, savage mobs, or single-minded marauders.

### 5.3.2 Fleeing and Escape

Whether from a failed Morale check or the PCs deciding on a prudent retreat, sometimes a side decamps the field.
If pursued, the rules for chases and pursuit given in the rules section of this document can be used to judge the likelihood of success.

GMs should be charitable about allowing PCs to run away.
Most pursuers have little reason to be particularly relentless, and a party should be taught that running away is a viable option and not an excuse to die tired.

## 5.4.0 Instinct Checks

Very few creatures are capable of engaging in desperate, life-or death struggles without losing some amount of their rationality.
Terror, fury, excruciating pain, and situational blindness can sometimes make a combatant do something genuinely stupid, even if they would never have made such a mistake in calmer circumstances The Instinct check is the game’s way of helping a GM take this situational chaos into account.

### 5.4.1 Making an Instinct Check

Whenever an Instinct check is triggered, the GM rolls a 1d10.
If the number rolled is equal or less than a combatant’s Instinct score, measured from 1 to 10, they do something impulsive, short-sighted, instinctual, or otherwise less-than-tactically-sound.
Instinct checks are rolled separately for individual combatants, though the GM can simply decide that an appropriate percentage of large groups automatically fail the check.
If the creatures have an Instinct of 3, for example, the GM might just decide that 30% of them fail the check rather than dicing for every one.

PCs never make Instinct checks.
Even in the grip of terror or traumatic injury they remain in control of their own choices.
Heroically well-trained or tactically-expert enemies with an Instinct score of zero might likewise be immune to Instinct checks.
Even martial paragons might be susceptible to Instinct, however, if they’re so proud, blase, or contemptuous of their foes that they fail to fight them with their utmost cunning.

When an Instinct check is failed, a creature will do something thoughtless or sub-optimal that is in line with their natural instincts.
The adjacent tables offer example suggestions for various types of creatures, but a GM can simply decide the most reasonable reaction based on the situation and the combatants.
A GM should use these instances as opportunities to show off the nature of an enemy or the instincts of a bestial foe, or to set up some battlefield situation that isn’t necessarily tactically-optimal for the enemy but is still troublesome for the heroes.
Actions taken as a result of a failed Instinct check will usually only occupy one round worth of the creature’s efforts.

Actions forced by an Instinct check failure won’t necessarily be entirely useless, but they won’t be the wisest or most effective use of the creature’s abilities.
Blindly attacking sub-optimal targets, recklessly using unarmed attacks rather than the weapon in hand, or aiming spells or shots at targets of lesser importance might all be actions taken as a result of a failed Instinct check.

Instinct checks are always optional and at the discretion of the GM.
Some GMs might choose not to use them at all, judging actions strictly on what seems reasonable to them.
In all cases it’s the GM’s final call as to whether or not to roll one.

### 5.4.2 When to Make an Instinct Check

As a general guide, a GM might make an Instinct check for a creature whenever any of the situations below are applicable, or any time the GM thinks the creature might be confused or indecisive.

- The second round of combat for mobs and undisciplined fighters.
The creature could think clearly before starting the fray, but the fear and exhilaration of mortal combat might confuse it.
- The creature has just had to make a Morale check for any reason.
Terror might cloud its thoughts.
- The enemy just did something confusing or disorienting.
When the situation is strange, the creature might fall back on instinct.
- The enemy did something to enrage or directly intimidate it.
Fury or terror might force bad decisions.
- The creature is presented with something it desires, such as dropped food, hurled money, or other inducements.
It might go after the bait instead of the battle if it seems safe to do so.

Other situations might force Instinct checks as well at the GM’s discretion.
Indeed, some situations might be so compelling as to cause automatic check failure.
Depending on the situation, the GM might decide a particular response is the only reasonable one, and not bother to randomly pick it from a table.

### 5.4.3 Assigning Instinct Scores

For non-sentient beasts, a creature’s Instinct score should usually be about 5.
Such beasts act largely by instinct in a fight- often very violent instincts.

For non-combatant sentients and those unfamiliar with battle, Instinct should be 5 to 7.
They are extremely likely to become confused or useless.

Ordinary intelligent veterans should have an Instinct of 3 or 4.
They might get caught up in the confusion of battle and make some poor calls, but they’re unlikely to lose their head entirely.

Hardened, battle-tested fighters might have an Instinct of only 1 or 2, being very unlikely to forget themselves in the chaos of battle.

The coldest, calmest killers would have an Instinct of 1, and may not have to make Instinct checks at all outside of the most disorienting situations.
They’ll fight according to the plan and won’t lose track of the battle.

# 6.0.0 Factions

Factions have several statistics to define their overall qualities.
Weak or small factions tend to have low ratings even in their main focus, while kingdoms and major institutions may have a good rating even in their less important traits, simply because they have so many re sources available to them.

## 6.1.0 Faction Statistics


**Cunning** is measured from 1 to 8 and indicates the faction’s general guile, skill at subterfuge, and subtlety.
Low Cunning means the faction is straightforward or unaccustomed to dealing with trickery, while high Cunning is for Machiavellian schemers and secretive organizations.

**Force** is measured from 1 to 8 and reflects the overall military prowess and martial competence of the faction.
A faction with low Force isn’t used to using violence to get its way, or is particularly inept at it, while a high Force reflects a culture of military expertise.

**Wealth** is measured from 1 to 8 and shows the faction’s general prosperity, material resources, and facility with money.
Low Wealth means the faction is poor, disinterested in material goods, or spend thrift with what they have, while high Wealth factions are rich and familiar with using money and goods as tools for success.

**Magic** measures the amount of magical resources available to the faction. “None” is for factions that have no meaningful access to magic. “Low” is for those factions that have at best a few trained mages or small stores of magical goods. “Medium” is for a faction where there is an established source of magical power for the faction, either as a sub-group of cooperative mages, a magical academy, a tradition of sorcery in the faction, or some other institutionalized aid. “High” magic is reserved for those factions that have a strong focus on wielding magical power, most fitting for a faction that represents a magical order.

**Treasure** is counted in points, and the total reflects how much the faction owns in cash and valuable goods.
A single point of Treasure doesn’t have an established cash value; a sack of gold is worthless in itself to a faction that needs a dozen oxcarts, and a herd of cattle owned by a faction can’t necessarily be turned into a fixed sum of coin.

**Hit points** work for factions much as they do for characters; when a faction is reduced to zero hit points, it collapses.
Its individual members and sub-groups might not all be dead, but they’re so hopelessly disorganized, dispirited, or conflict-bound that the faction ceases to exist as a coherent whole.

**Assets** are important resources possessed by a faction, such as controlling a ring of Smugglers, or having a unit of Infantry.
Assets all have their own statistics and hit points, and all of them require certain scores in Force, Wealth, Cunning, and Magic to purchase.
Assets don’t cover all the resources and institutions the faction may control, but they reflect the ones that are most relevant to the faction at that moment.
A kingdom may have more military than the Infantry unit they have, but that Infantry unit is the one that’s doing something important.

## 6.2.0 The Faction Turn

Every month or so, the GM should run a faction turn.
This turn may take place more often during times of intense activity, or less often if the campaign world is quiet.
In general, a faction turn after every adventure is a good average, assuming the PCs don’t have back to-back adventures.

At the start of every faction turn, each faction rolls 1d8 for initiative, the highest rolls going first.
Ties are resolved as the GM wishes, and then each faction takes the following steps in order.

- The faction earns Treasure equal to half their Wealth plus a quarter of their combined Force and Cunning, the total being rounded up.
- The faction must pay any upkeep required by their individual Asset costs, or by the cost of having too many Assets for their attributes.
If they can’t afford this upkeep, individual Assets may have their own bad consequences, while not being able to afford excess Assets means that the excess are lost.
- The faction triggers any special abilities individual Assets may have, such as abilities that allow an Asset to move or perform some other special benefit.
- The faction takes one Faction Action as listed in the following section, resolving any Attacks or other consequences from their choice.
When an action is taken, every Asset owned by the faction may take it; thus, if Attack is chosen, then every valid Asset owned by the faction can Attack.
If Repair Asset is chosen, every Asset can be repaired if enough Treasure is spent.
- The faction checks to see if it’s accomplished its most recent goal.
If so, it collects the experience points for doing so and picks a new goal.
If not, it can abandon the old goal and pick a new one, but it will sacrifice its next turn’s Faction Action to do so and may not trigger any Asset special abilities that round, either.

The next faction in order then acts until all factions have acted for the turn.

## 6.3.0 Asset Locations and Movement

Every Asset has a location on the campaign map.
This location may not be where all the elements of the Asset are located.
It might simply be the headquarters of an organization, or the spot where the most active and important members of it are currently working.
However it’s described, it’s the center of gravity for the Asset.

This location is usually in a town or other settlement, but it could be anything that makes sense.
A reclusive Prophet might dwell deep within the wilderness, and a ring of Smugglers might currently be based out of a hidden sea cave.
A location is simply wherever the GM thinks it should be.

Assets can move locations, either with the Move Asset faction action or with a special ability possessed by the Asset itself or an allied unit.
Generally, whenever an Asset moves, it can move one turn’s worth of distance.
As a rule of thumb, for a one-month turn, this is about one hundred miles.
This is as far as an organization can shift itself in thirty days while still maintaining some degree of control and cohesion.
The GM may adjust this distance based on the situation; if the campaign is taking place in an island archipelago with fast sea travel it’s going to be easier to move long distances than if the Asset has to march through mountains to get there.

Some Assets also have special abilities that work on targets within one move of the Asset.
Again, the GM decides what this means, but generally it means that the Asset can affect targets within a hundred miles of its location.

Sometimes it doesn’t make logical sense for an Asset to be able to move to a particular location.
A unit of Infantry, for example, could hardly walk into an enemy nation’s capital so as to later Attack the Court Patronage Asset there.
In this case, the best the Infantry could do would be to move to a location near the capital, assuming the GM decides that’s plausible.
The Infantry couldn’t actually Attack the enemy faction’s Assets until they got into the city itself where those Assets were located.

Assets with the Subtle quality are not limited this way.
Subtle Assets can move to locations even where they would normally be prohibited by the ruling powers.
Dislodging them requires that they be Attacked until destroyed or moved out by their owner.

Assets with the Stealth quality are also not limited by this, and can move freely to any location within reach.
Stealthed Assets cannot be Attacked by other Assets until they lose the Stealth quality.
This happens when they are discovered by certain special Assets or when the Stealthed Asset Attacks something.

## 6.4.0 Attribute Checks

Some actions, such as Attack, require an attribute check between factions, such as Force versus Cunning, or Wealth versus Force.
Other special Asset abilities sometimes call for attribute checks as well.

To make this check, the attacker and defender both roll 1d10 and add their relevant attribute.
Thus, for a Force versus Cunning check, the attacker would roll 1d10+Force against the defender’s 1d10+-Cunning.
The attacker wins if their total is higher, and the defender wins if it’s a tie or their roll is higher.

Some special abilities or tags allow the attacker or defender to roll more than one die for a check.
In this case, the dice are rolled and the highest of them are used.

## 6.5.0 Faction Tags

These are merely some possibilities.
The GM may devise others to suit their needs.

**Antimagical:** Assets that require Medium or higher Magic to purchase roll all attribute checks twice against this faction during an Attack and take the worst roll.

**Concealed:** All Assets the faction purchases enter play with the Stealth quality.

**Imperialist:** The faction quickly expands its Bases of Influence.
Once per turn, it can use the Expand Influence action as a special ability instead of it taking a full action.

**Innovative:** The faction can purchase Assets as if their attribute ratings were two points higher than they are.
Only two such over-complex Assets may be owned at any one time.

**Machiavellian:** The faction is diabolically cunning.
It rolls an extra die for all Cunning attribute checks.
Its Cunning must always be its highest attribute.

**Martial:** The faction is profoundly devoted to war.
It rolls an extra die for all Force attribute checks.
Force must always be its highest attribute.

**Massive:** The faction is an empire, major kingdom, or other huge organizational edifice.
It automatically wins attribute checks if its attribute is more than twice as big as the opposing side’s attribute, unless the other side is also Massive.

**Mobile:** The faction is exceptionally fast or mobile.
Its faction turn movement range is twice what another faction would have in the same situation.

**Populist:** The faction has widespread popular support.
Assets that cost 5 Treasure or less to buy cost one point less, to a minimum of 1.

**Rich:** The faction is rich or possessed of mercantile skill.
It rolls an extra die for all Wealth attribute checks.
Wealth must always be its highest attribute.

## 6.6.0 Faction Turn Actions

**Attack:** The faction nominates one or more Assets to attack the enemy in their locations.
In each location, the defender chooses which of the Assets present will meet the Attack; thus, if a unit of Infantry attacks in a location where there is an enemy Base of Influence, Informers, and Idealistic Thugs, the defender could decide to use Idealistic Thugs to defend against the attack.

The attacker makes an attribute check based on the attack of the acting Asset; thus, the Infantry would roll Force versus Force.
On a success, the defending Asset takes damage equal to the attacking Asset’s attack score, or 1d8 in the case of Infantry.
On a failure, the attacking Asset takes damage equal to the defending Asset’s counterattack score, or 1d6 in the case of Idealistic Thugs.

If the damage done to an Asset reduces it to zero hit points, it is destroyed.
The same Asset may be used to defend against multiple attacking Assets, provided it can survive the onslaught.

Damage done to a Base of Influence is also done directly to the faction’s hit points.
Overflow damage is not transmitted, however.

**Move Asset:** One or more Assets are moved up to one turn’s worth of movement each.
The receiving location must not have the ability and inclination to forbid the Asset from operating there.
Subtle and Stealthed Assets ignore this limit.

If an asset loses the Subtle or Stealth qualities while in a hostile location, they must use this action to retreat to safety within one turn or they will take half their maximum hit points in damage at the start of the next turn, rounded up.

**Repair Asset:** The faction spends 1 Treasure on each Asset they wish to repair, fixing half their relevant attribute value in lost hit points, rounded up.
Thus, fixing a Force Asset would heal half the faction’s Force attribute, rounded up.
Additional healing can be applied to an Asset in this same turn, but the cost increases by 1 Treasure for each subsequent fix; thus, the second costs 2 Treasure, the third costs 3 Treasure, and so forth.

This ability can at the same time also be used to repair damage done to the faction, spending 1 Treasure to heal a total equal to the faction’s highest and lowest Force, Wealth, or Cunning attribute divided by two, rounded up.
Thus, a faction with a Force of 5, Wealth of 2, and Cunning of 4 would heal 4 points of damage.
Only one such application of healing is possible for a faction each turn.

**Expand Influence:** The faction seeks to establish a new base of operations in a location.
The faction must have at least one Asset there already to make this attempt, and must spend 1 Treasure for each hit point the new Base of Influence is to have.
Thus, to create a new Base of Influence with a maximum hit point total of 10, 10 Treasure must be spent.
Bases with high maximum hit point totals are harder to dislodge, but losing them also inflicts much more damage on the faction’s own hit points.

Once the Base of Influence is created, the owner makes a Cunning versus Cunning attribute check against every other faction that has at least one Asset in the same location.
If the other faction wins the check, they are allowed to make an immediate Attack against the new Base of Influence with whatever Assets they have present in the location.
The creating faction may attempt to block this action by defending with other Assets present.

If the Base of Influence survives this onslaught, it operates as normal and allows the faction to purchase new Assets there with the Create Asset action.

**Create Asset:** The faction buys one Asset at a location where they have a Base of Influence.
They must have the minimum attribute and Magic ratings necessary to buy the Asset and must pay the listed cost in Treasure to build it.
A faction can create only one Asset per turn.

A faction can have no more Assets of a particular attribute than their attribute score.
Thus, a faction with a Force of 3 can have only 3 Force Assets.
If this number is exceeded, the faction must pay 1 Treasure per excess Asset at the start of each turn, or else they will lose the excess.

**Hide Asset:** An action available only to factions with a Cunning score of 3 or better, this action allows the faction to give one owned Asset the Stealth quality for every 2 Treasure they spend.
Assets currently in a location with another faction’s Base of Influence can’t be hidden.
If the Asset later loses the Stealth, no refund is given.

**Sell Asset:** The faction voluntarily decommissions an Asset, salvaging it for what it’s worth.
The Asset is lost and the faction gains half its purchase cost in Treasure, rounded down.
If the Asset is damaged when it is sold, however, no Treasure is gained.

## 6.7.0 Creating Factions

A given campaign should generally not have more than six active factions at any one time, and three or four are generally more manageable.
If there are more extant factions than this, then simply run turns for the three or four most active or relevant ones.

To create a faction, first decide whether it is a small, medium, or large faction.
A small one might be a petty cult or small free city or minor magical academy.
A medium one might be a local baron’s government or province-wide faith.
A large one would be an entire kingdom or a major province of a vast empire.

It’s perfectly acceptable to break a large institution down into a smaller faction, modeling only the branch of government or part of the organization that’s actually relevant to the campaign.

All factions have a Base of Influence at their primary headquarters with a hit point total equal to the faction’s maximum.
The faction’s Magic rating is whatever the GM thinks suitable.

For a small faction, give them a 3 or 4 in their best attribute, a 2 or 3 in their second-best, and a 1 or 2 in their worst quality.

Medium factions should assign 5 or 6 to their best attribute, 4 or 5 to their second-best, and 2 or 3 to their worst.
They should have two Assets in their primary attribute and two others among the other two.

Large factions should assign 7 or 8 to their strongest attribute, 6 or 7 to their second-best attribute, and 3 or 4 to their worst quality.
They should have four Assets in their primary attribute, and four others spread among the other two.
Their Magic rating will depend on whatever you think is appropriate for their scale, but remember that it’s harder to concentrate effective magical resources when dealing with a whole province or nation than it is to enchant a single city-state or magical institution.

To determine a faction’s maximum hit points, use the adjacent table.
Thus, one with a Force of 3, a Wealth of 5, and a Cunning of 2 would have hit points equal to 4 plus 9 plus 2, or 15 total.
The Base of Influence at their primary headquarters will always have a maximum hit points equal to the faction’s maximum hit points, even if it later rises or falls due to attribute score changes.

| Attribute Rating | Faction XP Cost to Purchase | Hit Point Value |
|-----------------:|----------------------------:|----------------:|
|                1 |                           - |               1 |
|                2 |                           2 |               2 |
|                3 |                           4 |               4 |
|                4 |                           6 |               6 |
|                5 |                           9 |               9 |
|                6 |                          12 |              12 |
|                7 |                          16 |              16 |
|                8 |                          20 |              20 |

Lastly, give a faction a goal, either one from the foregoing list or one chosen by the GM.
When this goal is achieved, the faction earns experience points which it can later spend to increase its attributes.
The cost for such increases is given on the table adjacent.
Earlier levels must be purchased before later, so to raise Force from 5 to 7 will cost 9 XP to raise it to 6, then 12 more to raise it to 7.

#### Example Faction Goals

The difficulty of a faction goal is the number of experience points earned on a successful completion of it.

**Blood the Enemy:** Inflict a number of hit points of damage on enemy faction assets or bases equal to your faction’s total Force, Cunning, and Wealth ratings.
Difficulty 2.

**Destroy the Foe:** Destroy a rival faction.
Difficulty equal to 2 plus the average of the faction’s Force, Cunning, and Wealth ratings.

**Eliminate Target:** Choose an undamaged rival Asset.
If you destroy it within three turns, succeed at a Difficulty 1 goal.
If you fail, pick a new goal without suffering the usual turn of paralysis.

**Expand Influence:** Plant a Base of Influence at a new location.
Difficulty 1, +1 if a rival contests it.

**Inside Enemy Territory:** Have a number of Stealthed assets in locations where there is a rival Base of Influence equal to your Cunning score.
Units that are already Stealthed in locations when this goal is adopted don’t count.
Difficulty 2.

**Invincible Valor:** Destroy a Force asset with a minimum purchase rating higher than your faction’s Force rating.
Difficulty 2.

**Peaceable Kingdom:** Don’t take an Attack action for four turns.
Difficulty 1.

**Root Out the Enemy:** Destroy a Base of Influence of a rival faction in a specific location.
Difficulty equal to half the average of the current ruling faction’s Force, Cunning, and Wealth ratings, rounded up.

**Sphere Dominance:** Choose Wealth, Force, or Cunning.
Destroy a number of rival assets of that kind equal to your score in that attribute.
Difficulty of 1 per 2 destroyed, rounded up.

**Wealth of Kingdoms:** Spend Treasure equal to four times your faction’s Wealth rating on bribes and influence.
This money is effectively lost, but the goal is then considered accomplished.
The faction’s Wealth rating must increase before this goal can be selected again.
Difficulty 2.

## 6.8.0 Cunning Assets

**Bewitching Charmer:** When the Bewitching Charmer succeeds in an Attack, the targeted Asset is unable to leave the same location as the Bewitching Charmer until the latter Asset moves or is destroyed.
Bewitching Charmers are immune to Counterattack.

**Blackmail:** When a Blackmail asset is in a location, hostile factions can’t roll more than one die during Attacks made by or against them there, even if they have tags or Assets that usually grant bonus dice.

**Court Patronage:** Powerful nobles or officials are appointing their agents to useful posts of profit.
A Court Patronage Asset automatically grants 1 Treasure to its owning faction each turn.

**Covert Transport:** As a free action once per turn, the faction can pay 1 Treasure and move any Cunning or Wealth Asset at the same location as the Covert Transport.
The transported Asset gains the Stealth quality until it performs some action or is otherwise utilized by the faction.

**Cryptomancers:** In place of an Attack action, they can make a Cunning vs.
Cunning attack on a specific hostile Asset within one move.
On a success, the targeted Asset is unable to do anything or be used for anything on its owner’s next faction turn.
On a failure, no Counterattack damage is taken.

**Dancing Girls:** Dancing Girls or other charming distractions are immune to Attack or Counterattack damage from Force Assets, but they cannot be used to defend against Attacks from Force Assets.

**Expert Treachery:** On a successful Attack by Expert Treachery, this Asset is lost, 5 Treasure is gained by its owning faction, and the Asset that Expert Treachery targeted switches sides.
This conversion happens even if their new owners lack the attributes usually necessary to maintain their new Asset.

**Hired Friends:** As a free action, once per turn, the faction may spend 1 Treasure and grant a Wealth Asset within one turn’s movement range the Subtle quality.
This quality will remain until the Hired Friends are destroyed or they use this ability again.

**Idealistic Thugs:** Easily-manipulated hotheads are enlisted under whatever ideological or religious principle best enthuses them for violence.

**Informers:** As a free action, once per turn, the faction can spend 1 Treasure and have the Informers look for Stealthed Assets.
To do so, the Informers pick a faction and make a Cunning vs.
Cunning Attack on them.
No counterattack damage is taken if they fail, but if they succeed, all Stealthed Assets of that faction within one move of the Informers are revealed.

**Interrupted Logistics:** Non-Stealthed hostile units cannot enter the same location as the Interrupted Logistics Asset without paying 1d4 Treasure and waiting one turn to arrive there.

**Just As Planned:** Some sublimely cunning mastermind works for the faction.
Whenever the faction’s Assets make a roll involving Cunning, they may reroll a failed check at the cost of inflicting 1d6 damage on Just As Planned.
This may be done repeatedly, though it may destroy the Asset.
There is no range limit on this benefit.

**Mindbenders:** Once per turn as a free action, the Mindbenders can force a rival faction to reroll a check, Attack, or other die roll they just made and take whichever result the Mindbenders prefer.
A faction can only be affected this way once until the start of the Mindbender’s faction’s next turn.

**Occult Infiltrators:** Magically-gifted spies and assassins are enlisted to serve the faction.
Occult Infiltrator Assets always begin play with the Stealth quality.

**Omniscient Seers:** At the start of their turn, each hostile Stealthed as set within one turn’s movement of the Omniscient Seers must succeed in a Cunning vs.
Cunning check against the owning faction or lose their Stealth.
In addition, all Cunning rolls made by the faction for units or events within one turn’s movement of the seers gain an extra die.

**Organization Moles:** Sleeper agents and deep-cover spies burrow into hostile organizations, waiting to disrupt them from within when ordered.

**Petty Seers:** A cadre of skilled fortune-tellers and minor oracles have been enlisted by the faction to foresee perils and allow swift counterattacks.

**Popular Movement:** Any friendly Asset is allowed movement into the same location as the Popular Movement, even if it would normally be forbidden by its owners and lacks the Subtle quality.
If the Popular Movement later moves or is destroyed, such Assets must also leave or suffer the usual consequences for a non-Subtle Asset.

**Prophet:** Whether a religious prophet, charismatic philosopher, rebel leader, or other figure of popular appeal, the Asset is firmly under the faction’s control.

**Saboteurs:** An Asset that is Attacked by the Saboteurs can’t use any free action abilities it may have during the next turn, whether or not the Attack was successful.

**Seditionists:** In place of an Attack action, the Seditionists’ owners may spend 1d4 Treasure and attach the Asset to a hostile Asset in the same location.
Until the Seditionists are destroyed, infest another Asset, or leave the same location, the rebelling Asset cannot be used for anything and grants no benefits.

**Shapeshifters:** As a free action once per turn, the faction can spend 1 Treasure and grant the Shapeshifters the Stealth quality.

**Smugglers:** As a free action, once per faction turn, the Smugglers can move any allied Wealth or Cunning Asset in their same location to a destination within movement range, even if the destination wouldn’t normally allow an un-Subtle Asset.

**Spymaster:** A veteran operative runs a counterintelligence bureau in the area.

**Underground Roads:** A well-established network of secret transit extends far around this Asset.
As a free action, the faction may pay 1 Treasure and move any friendly Asset from a location within one round’s move of the Underground Roads to a destination within one round of the Roads.

**Useful Idiots:** Hirelings, catspaws, foolish idealists, and other disposable minions are gathered together in this Asset.
If another Asset within one turn’s move of the Useful Idiots is struck by an Attack, the faction can instead sacrifice the Useful Idiots to negate the attack.
Only one band of Useful Idiots can be sacrificed on any one turn.

**Vigilant Agents:** A constant flow of observations runs back to the faction from these watchful counterintelligence agents.
Whenever another faction moves a Stealthed asset into a location within one move’s distance from the Vigilant Agents, they may make a Cunning vs.
Cunning attack against the owning faction.
On a success, the intruding Asset loses its Stealth after it completes the move.


#### Cunning 1

| Cunning Asset | Cost | HP | Magic  | Attack            | Counter    | Qualities       |
|---------------|-----:|---:|--------|-------------------|------------|-----------------|
| Informers     | 2    | 3  | None   | C v. C/Special    | None       | Subtle, Special |
| Petty Seers   | 2    | 2  | Medium | None              | 1d6 damage | Subtle          |
| Smugglers     | 2    | 4  | None   | C v. W/1d4 damage | None       | Subtle, Action  |
| Useful Idiots | 1    | 2  | None   | None              | None       | Subtle, Special |

#### Cunning 2

| Cunning Asset | Cost | HP | Magic | Attack            | Counter | Qualities       |
|---------------|-----:|---:|-------|-------------------|---------|-----------------|
| Blackmail     |    4 |  4 | None  | C v. C/1d4 damage | None    | Subtle, Special |
| Dancing Girls |    4 |  3 | None  | C v. W/2d4 damage | None    | Subtle, Special |
| Hired Friends |    4 |  4 | None  | C v. C/1d6 damage | None    | Subtle, Special |
| Saboteurs     |    5 |  6 | None  | C v. W/2d4 damage | None    | Subtle, Special |

#### Cunning 3

| Cunning Asset       | Cost | HP |  Magic | Attack            | Counter    | Qualities       |
|---------------------|-----:|---:|-------:|-------------------|------------|-----------------|
| Bewitching Charmer  |    6 |  4 |    Low | C v. C/Special    | None       | Subtle, Special |
| Covert Transport    |    8 |  4 |   None | None              | None       | Subtle, Special |
| Occult Infiltrators |    6 |  4 | Medium | C v. C/2d6 damage | None       | Subtle, Special |
| Spymaster           |    8 |  4 |   None | C v. C/1d6 damage | 2d6 damage | Subtle          |

#### Cunning 4

| Cunning Asset    | Cost | HP | Magic | Attack            | Counter    | Qualities       |
|------------------|-----:|---:|------:|-------------------|------------|-----------------|
| Court Patronage  |    8 |  8 |  None | C v. C/1d6 damage | 1d6 damage | Subtle, Special |
| Idealistic Thugs |    8 | 12 |  None | C v. F/1d6 damage | 1d6 damage | Subtle          |
| Seditionists     |   12 |  8 |  None | Special           | None       | Subtle          |
| Vigilant Agents  |   12 |  8 |  None | None              | 1d4 damage | Subtle, Special |

#### Cunning 5

| Cunning Asset      | Cost | HP |  Magic | Attack            | Counter | Qualities       |
|--------------------|-----:|---:|-------:|-------------------|---------|-----------------|
| Cryptomancers      |   14 |  6 |    Low | C v. C/Special    | None    | Subtle          |
| Organization Moles |    8 | 10 |   None | C v. C/2d6 damage | None    | Subtle          |
| Shapeshifters      |   14 |  8 | Medium | C v. C/2d6 damage | None    | Subtle, Special |

#### Cunning 6

| Cunning Asset         | Cost | HP | Magic | Attack            | Counter    | Qualities       |
|-----------------------|-----:|---:|------:|-------------------|------------|-----------------|
| Interrupted Logistics |   20 | 10 |  None | None              | None       | Subtle, Special |
| Prophet               |   20 | 10 |  None | C v. C/2d8 damage | 1d8 damage | Subtle          |
| Underground Roads     |   18 | 15 |  None | None              | None       | Subtle, Special |

#### Cunning 7

| Cunning Asset    | Cost | HP |  Magic | Attack            | Counter    | Qualities       |
|------------------|-----:|---:|-------:|-------------------|------------|-----------------|
| Expert Treachery |   10 |  5 |   None | C v. C/Special    | None       | Subtle          |
| Mindbenders      |   20 | 10 | Medium | None              | 2d8 damage | Subtle          |
| Popular Movement |   25 | 16 |   None | C v. C/2d6 damage | 1d6 damage | Subtle, Special |

#### Cunning 8

| Cunning Asset    | Cost | HP | Magic | Attack | Counter     | Qualities       |
|------------------|-----:|---:|-------|--------|-------------|-----------------|
| Just As Planned  |   40 | 15 | None  | None   | 1d10 damage | Subtle, Special |
| Omniscient Seers |   30 | 10 | High  | None   | 1d8 damage  | Subtle, Special |

## 6.9.0 Force Assets

**Apocalypse Engine:** One of a number of hideously powerful ancient super-weapons unearthed from some lost armory, an Apocalypse Engine rains some eldritch horror down on a targeted enemy Asset.

**Brilliant General:** A leader for the ages is in service with the faction.
Whenever the Brilliant General or any allied Force Asset in the same location Attacks or is made to defend, it can roll an extra die to do so.

**Cavalry:** Mounted troops, chariots, or other mobile soldiers are in service to the faction.
While weak on defense, they can harry logistics and mount powerful charges.

**Demonic Slayer:** Powerful sorcerers have summoned or constructed an inhuman assassin-beast to hunt down and slaughter the faction’s enemies.
A Demonic Slayer enters play Stealthed.

**Enchanted Elites:** A carefully-selected group of skilled warriors are given magical armaments and arcane blessings to boost their effectiveness.

**Fearful Intimidation:** Judicious exercises of force have intimidated the locals, making them reluctant to cooperate with any group that stands opposed to the faction.

**Fortification Program:** A program of organized fortification and supply caching has been undertaken around the Asset’s location, hardening allied communities and friendly Assets.
Once per turn, when an enemy makes an Attack that targets the faction’s Force rating, the faction can use the Fortification Program to defend if the Asset is within a turn’s move from the attack.

**Guerrilla Populace:** The locals have the assistance of trained guerrilla warfare leaders who can aid them in sabotaging and attacking unwary hostiles.

**Infantry:** Common foot soldiers have been organized and armed by the faction.
While rarely particularly heroic in their capabilities, they have the advantage of numbers.

**Invincible Legion:** The faction has developed a truly irresistible military organization that can smash its way through opposition with out the aid of any support units.
During a Relocate Asset action, the Invincible Legion can relocate to locations that would otherwise not permit a formal military force to relocate there, as if it had the Subtle quality.
It is not, however, in any way subtle.

**Knights:** Elite warriors of considerable personal prowess have been trained or enlisted by the faction, either from noble sympathizers, veteran members, or amenable mercenaries.

**Local Guard:** Ordinary citizens are enlisted into night watch patrols and local guard units.
They’re most effective when defending from behind a fortified position, but they have some idea of how to use their weapons.

**Magical Logistics:** An advanced web of magical Workings, skilled sorcerers, and trained logistical experts are enlisted to streamline the faction’s maintenance and sustain damaged units.
Once per faction turn, as a free action, the Asset can repair 2 hit points of damage to an allied Force Asset.

**Military Roads:** The faction has established a network of roads with a logistical stockpile at this Asset’s location.
As a consequence, once per faction turn, the faction can move any one Asset from any location within its reach to any other location within its reach at a cost of 1 Treasure.

**Military Transport:** A branch of skilled teamsters, transport ships, road-building crews, or other logistical facilitators is in service to the faction.
As a free action once per faction turn, it can bring an allied Asset to its location, provided they’re within one turn’s movement range, or move an allied Asset from its own location to a target also within a turn’s move.
Multiple Military Transport assets can chain this movement over long distances.

**Purity Rites:** A rigorous program of regular mental inspection and counterintelligence measures has been undertaken by the faction.
This Asset can only defend against attacks that target the faction’s Cunning, but it allows the faction to roll an extra die to defend.

**Reserve Corps:** Retired military personnel and rear-line troops are spread through the area as workers or colonists, available to resist hostilities as needed.

**Scouts:** Long-range scouts and reconnaissance experts work for the faction, able to venture deep into hostile territory.

**Siege Experts:** These soldiers are trained in trenching, sapping, and razing targeted structures.
When they successfully Attack an enemy Asset, the owner loses 1d4 points of Treasure from their reserves and this faction gains it.

**Summoned Hunter:** A skilled sorcerer has summoned a magical beast or mentally bound a usefully disposable assassin into the faction’s service.

**Temple Fanatics:** Fanatical servants of a cult, ideology, or larger religion, these enthusiasts wreak havoc on enemies without a thought for their own lives.
After every time the Temple Fanatics defend or successfully attack, they take 1d4 damage.

**Thugs:** These gutter ruffians and common kneebreakers have been organized in service to the faction’s causes.

**Vanguard Unit:** This unit is specially trained to build bridges, reduce fortifications, and facilitate a lightning strike into enemy territory.
When its faction takes a Relocate Asset turn, it can move the Vanguard Unit and any allied units at the same location to any other location within range, even if the unit type would normally be prohibitive from moving there.
Thus, a Force asset could be moved into a foreign nation’s territory even against their wishes.
The unit may remain at that location afterwards even if the Vanguard Unit leaves.

**War Fleet:** While a war fleet can only Attack assets and locations within reach of the waterways, once per turn it can freely relocate itself to any coastal area within movement range.
The Asset itself must be based out of some landward location to provide for supply and refitting.

**War Machines:** Mobile war machines driven by trained beasts or magical motive power are under the faction’s control.

**Warshaped:** The faction has the use of magical creatures designed specifically for warfare, or ordinary humans that have been greatly altered to serve the faction’s needs.
Such forces are few and elusive enough to evade easy detection.

**Witch Hunters:** Certain personnel are trained in sniffing out traitors and spies in the organization, along with the presence of hostile magic or hidden spellcraft.

#### Force 1

| Force Asset          | Cost | HP | Magic  | Attack              | Counter      | Qualities |
|----------------------|-----:|---:|--------|---------------------|--------------|-----------|
| Fearful Intimidation |    2 |  4 | None   | None                | 1d4 damage   |           |
| Local Guard          |    3 |  4 | None   | F v. F/1d3+1 damage | 1d4+1 damage |           |
| Summoned Hunter      |    4 |  4 | Medium | C v. F/1d6 damage   | None         | Subtle    |
| Thugs                |    2 |  1 | None   | F v. C/1d6 damage   | None         | Subtle    |

#### Force 2

| Force Asset        | Cost | HP | Magic | Attack              | Counter      | Qualities |
|--------------------|-----:|---:|-------|---------------------|--------------|-----------|
| Guerrilla Populace |    6 |  4 | None  | F v. F/1d4+1 damage | None         |           |
| Military Transport |    4 |  6 | None  | None                | None         | Action    |
| Reserve Corps      |    4 |  4 | None  | F v. F/1d6 damage   | 1d6 damage   |           |
| Scouts             |    5 |  5 | None  | F v. F/2d4 damage   | 1d4+1 damage | Subtle    |

#### Force 3

| Force Asset      | Cost | HP | Magic  | Attack              | Counter    | Qualities |
|------------------|-----:|---:|--------|---------------------|------------|-----------|
| Enchanted Elites |    8 |  6 | Medium | F v. F/1d10 damage  | 1d6 damage | Subtle    |
| Infantry         |    6 |  6 | None   | F v. F/1d8 damage   | 1d6 damage |           |
| Temple Fanatics  |    4 |  6 | None   | F v. F/2d6 damage   | 2d6 damage | Special   |
| Witch Hunters    |    6 |  4 | Low    | C v. C/1d4+1 damage | 1d6 damage |           |

#### Force 4

| Force Asset    | Cost | HP | Magic |            Attack | Counter    | Qualities |
|----------------|------|---:|-------|------------------:|------------|-----------|
| Cavalry        | 8    | 12 | None  | F v. F/2d6 damage | 1d4 damage |           |
| Military Roads | 10   | 10 | None  |              None | None       | Action    |
| Vanguard Unit  | 10   | 10 | None  |              None | 1d6 damage | Action    |
| War Fleet      | 12   |  8 | None  | F v. F/2d6 damage | 1d8 damage | Action    |

#### Force 5

| Force Asset       | Cost | HP | Magic  | Attack              | Counter    | Qualities       |
|-------------------|-----:|---:|--------|---------------------|------------|-----------------|
| Demonic Slayer    |   12 |  4 | High   | C v. C/2d6+2 damage | None       | Subtle, Special |
| Magical Logistics |   14 |  6 | Medium | None                | None       | Special         |
| Siege Experts     |   10 |  8 | None   | F v. W/1d6 damage   | 1d6 damage |                 |

#### Force 6

| Force Asset           | Cost | HP | Magic  | Attack               | Counter     | Qualities |
|-----------------------|-----:|---:|--------|----------------------|-------------|-----------|
| Fortification Program |   20 | 18 | None   | None                 | 2d6 damage  | Action    |
| Knights               |   18 | 16 | None   | F v. F/2d8 damage    | 2d6 damage  |           |
| War Machines          |   25 | 14 | Medium | F v. F/2d10+4 damage | 1d10 damage |           |

#### Force 7

| Force Asset       | Cost | HP | Magic | Attack              | Counter      | Qualities       |
|-------------------|-----:|---:|-------|---------------------|--------------|-----------------|
| Brilliant General |   25 |  8 | None  | C v. F/1d8 damage   | None         | Subtle, Special |
| Purity Rites      |   20 | 10 | Low   | None                | 2d8+2 damage | Special         |
| Warshaped         |   30 | 16 | High  | F v. F/2d8+2 damage | 2d8 damage   | Subtle          |

#### Force 8

| Force Asset       | Cost | HP | Magic  | Attack               | Counter       | Qualities |
|-------------------|-----:|---:|--------|----------------------|---------------|-----------|
| Apocalypse Engine |   35 | 20 | Medium | F v. F/3d10+4 damage | None          |           |
| Invincible Legion |   40 | 30 | None   | F v. F/2d10+4 damage | 2d10+4 damage | Special   |


## 6.10.0 Wealth Assets

**Ancient Mechanisms:** Some useful magical mechanism from ages past has been refitted to be useful in local industry.
Whenever an Asset in the same location must roll to make a profit, such as Farmers or Manufactory, the faction may roll the die twice and take the better result.

**Ancient Workshop:** A workshop has been refitted with ancient magical tools, allowing prodigies of production, albeit not always safely.
As a free action, once per turn, the Ancient Workshop takes 1d6 damage and the owning faction gains 1d6 Treasure.

**Arcane Laboratory:** The faction’s overall Magic is counted as one step higher for the purposes of creating Assets in the same location as the laboratory.
Multiple Arcane Laboratories in the same location can increase the Magic boost by multiple steps.

**Armed Guards:** Hired caravan guards, bodyguards, or other armed minions serve the faction.

**Caravan:** As a free action, once per turn, the Caravan can spend 1 Treasure and move itself and one other Asset in the same place to a new location within one move.

**Cooperative Businesses:** If any other faction attempts to create an Asset in the same location as a Cooperative Business, the cost of doing so increases by 1 Treasure.
This penalty stacks.

**Dragomans:** Interpreters, cultural specialists, and go-betweens simplify the expansion of a faction’s influence in an area.
A faction that takes an Expand Influence action in the same location as this Asset can roll an extra die on all checks there that turn.
As a free action once per turn, this Asset can move.

**Economic Disruption:** As a free action once per turn, this Asset can move itself without cost.

**Farmers:** Farmers, hunters, and simple rural artisans are in service to the faction here.
Once per turn, as a free action, the Asset’s owner can roll 1d6; on a 5+, they gain 1 Treasure from the Farmers.

**Free Company:** Hired mercenaries and professional soldiers, this Asset can, as a free action once per turn, move itself.
At the start of each of its owner’s turn, it takes 1 Treasure in upkeep costs; if this is not paid, roll 1d6.
On a 1-3 the Asset is lost, on a 4-6 it goes rogue and will move to Attack the most profitable-looking target.
This roll is repeated each turn until back pay is paid or the Asset is lost.

**Front Merchant:** Whenever the Front Merchant successfully Attacks an enemy Asset, the target faction loses 1 Treasure, if they have any, and the Front Merchant’s owner gains it.
Such a loss can occur only once per turn.

**Golden Prosperity:** Each turn, as a free action, the faction gains 1d6 Treasure that can be used to fix damaged Assets as if by the Repair Assets action.
Any of this Treasure not spent on such purposes is lost.

**Healers:** Whenever an Asset within one move of the Healers is destroyed by an Attack that used Force against the target, the owner of the Healers may pay half its purchase price in Treasure, rounded up, to instantly restore it with 1 hit point.
This cannot be used to repair Bases of Influence.

**Hired Legion:** As a free action once per turn, the Hired Legion can move.
This faction must be paid 2 Treasure at the start of each turn as upkeep, or else they go rogue as the Free Company Asset does.
This Asset cannot be voluntarily sold or disbanded.

**Lead or Silver:** If Lead or Silver’s Attack reduces an enemy Asset to zero hit points, this Asset’s owner may immediately pay half the tar get’s purchase cost to claim it as their own, reviving it with 1 hit point.

**Mad Genius:** As a free action, once per turn, the Mad Genius may move.
As a free action, once per turn, the Mad Genius may be sacrificed to treat the Magic rating in their location as High for the purpose of buying Assets that require such resources.
This boost lasts only until the next Asset is purchased in that location.

**Manufactory:** Once per turn, as a free action, the Asset’s owner may roll 1d6; on a 1, one point of Treasure is lost, on a 2-5, one point is gained, and on a 6, two points are gained.
If Treasure is lost and none is available to pay it by the end of the turn, this Asset is lost.

**Merchant Prince:** A canny master of trade, the Merchant Prince may be triggered as a free action once per turn before buying a new Asset in the same location; the Merchant Prince takes 1d4 damage and the purchased Asset costs 1d8 Treasure less, down to a minimum of half its normal price.

**Monopoly:** Once per turn, as a free action, the Monopoly Asset can target an Asset in the same location; that Asset’s owning faction must either pay the Monopoly’s owner 1 Treasure or lose the targeted Asset.

**Occult Countermeasures:** This asset can only Attack or inflict Counterattack damage on Assets that require at least a Low Magic rating to purchase.

**Pleaders:** Whether lawyers, skalds, lawspeakers, sage elders, or other legal specialists, Pleaders can turn the local society’s laws against the enemies of the faction.
However, Pleaders can neither Attack nor inflict Counterattack damage on Force Assets.

**Smuggling Fleet:** Once per turn, as a free action, they may move themselves and any one Asset at their current location to any other water-accessible location within one move.
Any Asset they move with them gains the Subtle quality until they take some action at the destination.

**Supply Interruption:** As a free action, once per turn, the Asset can make a Cunning vs.
Wealth check against an Asset in the same location.
On a success, the owning faction must sacrifice Treasure equal to half the target Asset’s purchase cost, or else it is disabled and useless until this price is paid.

**Trade Company:** Bold traders undertake potentially lucrative- or catastrophic- new business opportunities.
As a free action, once per turn, the owner of the Asset may roll accept 1d4 damage done to the Asset in exchange for earning 1d6-1 Treasure points.

**Transport Network:** A vast array of carters, ships, smugglers, and official caravans are under the faction’s control.
As a free action the Transport Network can spend 1 Treasure to move any friendly Asset within two moves to any location within one move of either the target or the Transport Network.

**Usurers:** Moneylenders and other proto-bankers ply their trade for the faction.
For each unit of Usurers owned by a faction, the Treasure cost of buying Assets may be decreased by 2 Treasure, to a minimum of half its cost.
Each time the Usurers are used for this benefit, they suffer 1d4 damage from popular displeasure.

**Worker Mob:** The roughest, most brutal laborers in service with the faction have been quietly organized to sternly discipline the enemies of the group.

#### Wealth 1

| Wealth Asset           | Cost | HP | Magic | Attack              | Counter      | Qualities       |
|------------------------|-----:|---:|-------|---------------------|--------------|-----------------|
| Armed Guards           |    1 |  3 | None  | W v. F/1d3 damage   | 1d4 damage   |                 |
| Cooperative Businesses |    1 |  2 | None  | W v. W/1d4-1 damage | None         | Subtle, Special |
| Farmers                |    2 |  4 | None  | None                | 1d4 damage   | Action          |
| Front Merchant         |    2 |  3 | None  | W v. W/1d4 damage   | 1d4-1 damage | Subtle          |

#### Wealth 2

| Wealth Asset | Cost | HP | Magic | Attack              | Counter    | Qualities       |
|--------------|-----:|---:|-------|---------------------|------------|-----------------|
| Caravan      |    5 |  4 | None  | W v. W/1d4 damage   | None       | Action          |
| Dragomans    |    4 |  4 | None  | None                | 1d4 damage | Subtle, Special |
| Pleaders     |    6 |  4 | None  | C v. W/2d4 damage   | 1d6 damage | Special         |
| Worker Mob   |    4 |  6 | None  | W v. F/1d4+1 damage | 1d4 damage |                 |


#### Wealth 3

| Wealth Asset       | Cost | HP | Magic  | Attack              | Counter    | Qualities       |
|--------------------|-----:|---:|--------|---------------------|------------|-----------------|
| Ancient Mechanisms |    8 |  4 | Medium | None                | None       | Special         |
| Arcane Laboratory  |    6 |  4 | None   | None                | None       | Special         |
| Free Company       |    8 |  6 | None   | W v. F/2d4+2 damage | 1d6 damage | Action, Special |
| Manufactory        |    8 |  4 | None   | None                | 1d4 damage | Action          |


#### Wealth 4

| Wealth Asset           | Cost | HP | Magic | Attack             | Counter     | Qualities |
|------------------------|-----:|---:|-------|--------------------|-------------|-----------|
| Healers                |   12 |  8 | None  | None               | None        | Action    |
| Monopoly               |    8 | 12 | None  | W v. W/1d6 damage  | 1d6 damage  | Action    |
| Occult Countermeasures |   10 |  8 | Low   | W v. C/2d10 damage | 1d10 damage | Special   |
| Usurers                |   12 |  8 | None  | W v. W/1d10 damage | None        | Action    |


#### Wealth 5

| Wealth Asset        | Cost | HP | Magic | Attack            | Counter | Qualities      |
|---------------------|-----:|---:|-------|-------------------|---------|----------------|
| Mad Genius          |    6 |  2 | None  | W v. C/1d6 damage | None    | Action         |
| Smuggling Fleet     |   12 |  6 | None  | W v. F/2d6 damage | None    | Subtle, Action |
| Supply Interruption |   10 |  8 | None  | C v. W/1d6 damage | None    | Subtle, Action |


#### Wealth 6

| Wealth Asset        | Cost | HP | Magic | Attack            | Counter    | Qualities      |
|---------------------|-----:|---:|-------|-------------------|------------|----------------|
| Economic Disruption |   25 | 10 | None  | W v. W/2d6 damage | None       | Subtle, Action |
| Merchant Prince     |   20 | 10 | None  | W v. W/2d8 damage | 1d8 damage | Action         |
| Trade Company       |   15 | 10 | None  | W v. W/2d6 damage | 1d6 damage | Action         |


#### Wealth 7

| Wealth Asset      | Cost | HP | Magic  | Attack             | Counter    | Qualities |
|-------------------|-----:|---:|--------|--------------------|------------|-----------|
| Ancient Workshop  |   25 | 16 | Medium | None               | None       |           |
| Lead or Silver    |   20 | 10 | None   | W v. W/2d10 damage | 2d8 damage |           |
| Transport Network |   15 |  5 | None   | W v. W/1d12 damage | None       | Action    |


#### Wealth 8

| Wealth Asset      | Cost | HP | Magic  | Attack               | Counter     | Qualities |
|-------------------|-----:|---:|--------|----------------------|-------------|-----------|
| Golden Prosperity |   40 | 30 | Medium | None                 | 2d10 damage |           |
| Hired Legion      |   30 | 20 | None   | W v. F/2d10+4 damage | 2d10 damage | Action    |

# 7.0.0 Major Projects

In any campaign, there will likely arise some occasion when the PCs take it into their heads to accomplish some great change in the world.
Perhaps they want to abolish slavery in a country, or institute a new government in a howling wilderness, or crush the economic power of a hateful merchant cartel.
The party wants to accomplish something grand or large-scale where there is no obvious direct path to success.
No single killing or specific act of heroism will get them their aim, though the goal itself isn’t so wild as to be obviously futile.

Such ambitions are major projects, and this section will cover a simple system to help the GM adjudicate their progress and success.
This system is meant to handle sprawling, ambiguous ambitions that aren’t clearly susceptible to a simple solution.
If the party wants a dead town burgomaster, then they can simply kill him.
If they want to turn his town into a major new trading nexus, something more complicated may be required.

## 7.1.0 Renown

The basic currency of major projects is called Renown, and it’s measured in points much like experience points.
PCs gain Renown for succeeding at adventures, building ties with the world, and generally behaving in a way to attract interest and respect from those around them.
PCs then spend Renown to accomplish the changes they want to make in the world, reflecting their own background activities and the work of cooperative allies and associates.

Each individual PC has their own Renown score.
They can spend it together with the rest of the party if they agree on the mutual focus of their interests, but a PC might also spend it on other ambitions or intermediate goals that come to mind.
It’s ultimately up to the player as to what they want to put their effort into; spending Renown reflects the kind of background work and off-screen support that the hero can bring to bear.

A GM doesn’t have to track Renown unless they intend to use the this system.
If the GM prefers to do things their own way, they can completely ignore Renown awards.
If the GM changes their mind later and wants to introduce the system, they can simply give each PC a Renown score equal to their current accumulated experience points and then track things accordingly from there.

Generally, a PC will receive one point of Renown after each adventure.
Some other activities or undertakings might win them additional bonus Renown, usually those works that increase the PC’s influence and involvement with the campaign world, and some adventures might not give them much Renown at all if they left no impression on the people around the party.

## 7.2.0 Determining the Project Difficulty

To find out how much Renown is needed to achieve a project, the GM must determine its difficulty.
This total difficulty is a product of the intensity of the change, the scope it affects, and the powers that are opposed to it.

First, decide whether the change is plausible, improbable, or impossible.
If the change is something that is predictable or unsurprising, it’s a plausible change.
A town with good transport links and a couple of wealthy neighbors might quite plausibly become a trade hub.
A duke with an abandoned frontier keep and a raider problem might plausibly decide to give it to a famed warrior PC with the agreement that the PC would pledge fealty to him.
A plausible change in the campaign is simply one that no one would find particularly surprising or unlikely.

An improbable change is one that’s not physically or socially impossible, but is highly unlikely.
Transforming a random patch of steppe grasslands into a trading hub might be an improbable change, as would convincing a duke to simply hand over the frontier fort with no particular claim of allegiance.
Some things that are not particularly physically difficult might be improbable due to the social or emotional implications; a society with a relative handful of trophy slaves might find it improbable to give them up even if they serve only as status symbols for their owners.

An impossible change is just that; something that is physically or socially impossible to contemplate.
Turning a desolate glacier on the edge of the world into a trading hub might be such, or convincing the duke to simply give the PCs his duchy.
Accomplishing a feat like this might require substantial magical Workings, the involvement of ancient artifacts, or a degree of social upheaval on par with a war of conquest.
Some changes might be so drastic that they require their own heroic labors simply to prepare the groundwork for the real effort, and entire separate projects must be undertaken before the real goal even becomes possible.

## 7.3.0 Determining the Project Scope

Once the change’s probability is decided, the GM must identify how wide the scope of the change may be.
The more land and the more people the change affects, the harder it will be to bring it about.

A village-sized change is the smallest scale, affecting only a single hamlet or a village’s worth of people.
A city-sized change affects the population of a single city or several villages, while a regional one might affect a single barony or small province.
A kingdom-sized one affects a whole kingdom or a collection of feudal lordships, and a global change affects the entire world, or at least those parts known to the PCs.

When deciding the scope of the change, focus on how many people are going to be immediately affected by the project.
Turning a town into a trading hub might incidentally affect a significant part of a kingdom, but the immediate consequences are felt only by the residents of that town, and perhaps their closest trading partners.
The scope in that case would be simply that of a city, rather than a region.
Banishing slavery throughout a kingdom would require a kingdom-sized change, while getting it banned within some smaller feudal region would require a proportionately lesser scope.

If the PCs are trying to establish an educational institution, or a religious order, or some other sub-group meant to serve a chosen cause, the scope should be the largest general area the order can have influence in at any one time.
A very small order of warrior-monks might only have enough devotees to affect a village-sized community or problem.
An order with multiple monasteries and bases of operations throughout a kingdom might have enough muscle to affect events on a nation-wide scale.
In the same vein, a small academy might be enough to bring enlightened learning to a city, improving the lives of men and women there, but not have the reach to influence the greater region around it.
Individual warrior-monks or specific scholars might play major roles elsewhere in the setting, but the institution itself can’t rely on the certainty of being able to step into such roles.

In some cases, a PC might attempt to forge a Working or develop a specific bloodline of magical or cursed beings.
Assuming that they have the necessary tools and opportunities to achieve such a great feat, the scope should apply to the total number of people affected by the magic over its entire course of existence.
Thus, a village-sized change like this might apply to ten generations of a very small blood line, the enchantment lasting for a very long time but applying only to a few people at any one time.
It might be reproduced by special training, magical consecration, or a natural inherited bloodline.
Once the scope limit is reached, the magic can no longer be transmitted, as it has either been exhausted or the subtle shiftings of the magic have damaged it beyond repair.
Conversely, a very large scope for such a work might mean that many people are so affected, though a very large change like that would only last for a few generations before reaching the maximum affected population.
Because of such limits, many such empowered bloodlines or augmented magical traditions are very selective about adding new members.

Optionally, PCs who want to create such a magical working can fix it indefinitely, causing it to be heritable or transmissible for the indefinite future.
Such laborious workings are much more difficult than simply tying the effect to the natural flow of the magic, however, and so it costs four times more than it would otherwise.
Thus, imbuing a village of people with some magical quality that they will forever after transmit down to a similar number of heirs would count as a x8 multiplier instead of a x2 multiplier.

## 7.4.0 Determining the Opposition

Once you have decided on the difficulty and the scope, you now need to identify the most significant people or power bases that would be opposed to this change.
In some cases, there may be no one opposed to the alteration; turning a steppe oasis into a trading post might not have anyone to object if there are no nomads who control the land, nor terrible beasts to threaten settlers.
In most cases, however, there’s going to be at least one person, creature, or other power in the area who would prefer things not change.

If the opposition comes in the form of ordinary peasants or citizens, minor bandit rabble, normal dangerous animals, or other disorganized and low-level threats, then the difficulty is multiplied by x2.

If the opposition is organized under competent leadership, such as a local baron, rich merchant, or persuasive priest, or if the opposition is some dangerous but not especially remarkable monster, then the difficulty is multiplied by x4.

If the opposition is entrenched and powerful, such as a group of nobles, an influential bandit king, a crime boss, a major city’s mayor, or a monster impressive enough to have developed its own legendary, then the difficulty is multiplied by x8.

If the opposition involves facing down a king, a legendary monster, the primate of a major religion, or some similar monarchic power, then the difficulty is multiplied by x16.

When measuring opposition, only the greatest opponent counts.
Thus, if the king, the nobility, and the local village chief all hate an idea, the difficulty modifier is x16.
If the king is then persuaded to relent, the difficulty modifier becomes x8, until the barons are pacified, after which the village chieftain is the only opposition left, for a x2 modifier.

On top of this, if the change inspires widespread popular dis approval or unease among the populace affected by the change, multiply the modifier by an additional x2.
Such changes usually touch on delicate questions of group identity, cultural traditions, or basic values, and the people in the change’s scope are likely to resist such measures on multiple levels.

As an example, assume an idealistic band of adventurers dreamed of extirpating slavery from an entire kingdom.
The natives use slaves for work and status, but their labor isn’t crucial to the economy’s survival, so the GM decides it is merely improbable to give up slavery, for a base difficulty of 2.
The scope is kingdom-wide, so 2 is multiplied by 16, for a difficulty of 32.
As the situation stands now, the king has no desire to infuriate the wealthy magnates of his kingdom by taking away their free labor, so he would oppose it for an additional x16 multiplier, for a total difficulty of 512.
Oh, and the natives find the idea of accepting slaves as equals to be emotionally abhorrent, so that’s an additional x2 multiplier, for a final difficulty of 1,024.

It is very unlikely for the heroes to manage to scrape up the 1,024 points of Renown needed to make this change out of hand.
They’re going to have to alter the situation to quell the opposition and make specific strides toward making the ideal more plausible before they can finally bring about their dream.

| Probability of the Goal | Base Difficulty | Scope Affected | Difficulty Multiplier | Greatest Active Opposition | Difficulty Multiplier |
|-------------------------|----------------:|----------------|----------------------:|----------------------------|----------------------:|
| Plausible               |               1 | Village        |                    x2 | Minor figures              |                    x2 |
| Improbable              |               2 | City           |                    x4 | Local leaders              |                    x4 |
| Impossible              |               4 | Region         |                    x8 | Major noble or beast       |                    x8 |
|                         |                 | Kingdom        |                   x16 | King or famed monster      |                   x16 |
|                         |                 | Known World    |                   x32 |                            |                       |

Multiply opposition by x2 if the local population is emotionally or socially against it.

## 7.5.0 Decreasing Difficulty

Adventurers who have a dream bigger than their available Renown have several options for bringing it about more rapidly.
The party can use some or all of these techniques for making their ambition more feasible, and the GM might well insist on at least some of them before the PCs can succeed.

**They can spend money.** Sometimes a problem can be solved by throwing enough money at it, either by paying off troublesome opponents, constructing useful facilities or installations, or hiring enough help to push the cause through.
Money is often useful, but it eventually begets diminishing returns; once everything useful has been bought, additional coinage brings little result.

The adjacent table shows how much a point of Renown dedicated to the project costs.
The first few points come relatively cheaply, but after that the price increases rapidly.
Eventually, there comes a point where only the wealth of empires can shove a massive project through with sheer monetary force.
Small projects and modest ambitions are generally easy to accomplish with cash, but society-wide alterations and massive undertakings can defeat the richest vault.

**They can build institutions.** If the PCs want a fortified monastery loyal to them, they can either throw enough Renown at their goal until allied NPCs and local potentates think it’s a good idea to buy them off by building it for them, or they can actually go out and purchase it with their own money.
They can hire the masons, recruit the monks, and find a trustworthy abbot to act as regent for the heroes.
Such steps may not be enough to completely attain the purpose, as they’ll still have to deal with quelling any local opposition to the new monastery and any innate implausibility of establishing a monastery wherever they want to put it, but it’ll get them a long way toward success.

The GM decides a reasonable cost for the institution they want to build and the assorted recruits they’ll need to operate it, using the guidelines in this section.
Prices will vary drastically based on the situation; building a splendid stone castle in a desert with no good source of stone will cost far more than listed, while hiring skilled artisans in a major metropolis won’t be nearly as difficult as finding them in an empty tundra.

Once the cost is paid, the GM assigns a suitable amount of Renown toward attaining the goal.
For example, if the overall goal is securing the trade route between two distant cities, building a fortified caravansary with patrolling road guards might give enough Renown to solve half the problem.
The rest of it might require dealing with the opposition that’s making the hazard in the first place, such as the depredations of a bandit chief or the perils of the savage monsters that haunt the road.

**They can nullify opposition.** Either through gold, persuasion, or sharp steel, the PCs can end the opposition of those powers who stand against their ambition.
Opponents who can be bought off might be managed with nothing more than a lengthy discussion and an exchange of valuables, but other opponents might need full-fledged adventures to deal with.
Some might demand favors in exchange for withdrawing their opposition, or quests accomplished on their behalf, or enemies snuffed out by the swords of the heroes.
Others could be so unalterably opposed to the idea that they must either be killed or endured.

**They can adventure in pursuit of their goal.** This adventure might be something as simple as finding the den of a troublesome pack of monsters, or it could be something as involved as delving into an ancient dungeon to recover the lost regalia that will give them the moral authority to make demands of a troublesome prelate.
Such adventures will give the PCs their usual award of Renown, but they can also give a bonus award toward their specific goal if their efforts are particularly relevant.

This bonus is determined by the GM.
The easiest way for the GM to pick the proper amount for the award is to privately estimate how many such adventures their goal is worth and then award Renown accordingly.
Thus, if the GM thinks that three adventures like this one is as much focus and effort as the group should have to spend toward accomplishing their aim, then each adventure will decrease the goal’s difficulty by one-third.

Adventuring is by far the most efficient way to accomplish a group’s goals, assuming they can come up with adventures that are relevant.
This is intentional; a goal that gives the GM an easy supply of adventuring grist is a genuine contribution to the game.
The more adventures that a GM gets out of PC ambitions, the easier it will be to prepare for the game and ensure the players are involved in the campaign.

| Renown Bought    | Cost in Silver Pieces per Point |
|------------------|--------------------------------:|
| First 1-4 points |                   500 per point |
| Next 4 points    |                 2,000 per point |
| Next 8 points    |                 4,000 per point |
| Next 16 points   |                 8,000 per point |
| Next 32 points   |                16,000 per point |
| Next 64 points   |                32,000 per point |
| Further points   |         Prohibitively expensive |

For example, purchasing 14 points of Renown would cost 2,000 for the first four, 8,000 for the next four, and 24,000 for the next six, for 34,000 sp total.


## 7.6.0 Achieving the Goal

Once the PCs have piled up enough Renown and lowered the difficulty enough to actually make it feasible to achieve the goal, they need to take the final steps necessary to complete the work.
For a minor goal, this might be a simple matter of describing how they take care of the details, while a vast campaign of effort might culminate in several brutal, perilous adventures.

The time this change takes will rest with the GM’s judgment.
If the PCs have been working on the project for some time already this effort should be taken into account and lessen the time required.

For mundane changes or changes the GM doesn’t really want to focus on, the PCs simply declare that they’re spending their Renown and using their own good name, personal prowess, and accumulated friendships and contacts to pull off their ambition.
They might give examples of some of the ways they’re working to achieve the goal and specify what allies or resources they’re deploying.
The GM then describes the outcome of their efforts.
They may not be completely successful and events may not work out exactly as they planned, but they’ll get the substance of what they wanted.

For changes that push through opposition instead of subverting it, those that just pay the price for the opposition multiplier, the GM might make the PCs deal with consequences of that unquelled opposition.
The kingdom might outlaw slavery, but if not all the opposition was defeated there may remain small pockets where the law doesn’t reach or the populace refuses to accept the freed slaves as fellow citizens.
Solving these remnant problems might require their own projects or adventures.

For magical, impossible, or truly epic changes, the GM might oblige the PCs to face some culminating adventure or challenge before their ambition becomes real.
They might’ve marshaled enough force and enough allies to depose the wicked king, but now the day of reckoning has come and they must face the tyrant and his elite guard in a pitched battle within the capital city.
Some heroic changes might require several such adventures, with failure meaning that their efforts somehow fall short of complete success.
If the tyrant is not slain, he might escape into exile to foment further trouble, or he might flee to a province he still can control.

Once the change is successfully achieved, the GM should take a little while to consider the larger ramifications of the event.
Who in the surrounding area is going to take notice of the events, and what are they likely to do about it? What allies of the PCs might be strengthened by the change and able to push their own agendas further? What are the longer-term consequences of their actions, and how might these show up during future adventures?

The ultimate point of changes like these is not simply to make marks on the campaign map, but to create the seeds of future adventures and future events.
The actions of the characters create reactions, and the deeper they involve themselves in the campaign setting, the more that setting is going to involve itself with them.
This is ultimately a virtuous circle for the GM and the group, as it helps to generate adventures and events that matter to the players and spares the GM from confusion or uncertainty over what kind of adventuring grist to generate.

## 7.7.0 Magical Projects

Some projects are flatly impossible in nature, such as changing humans into some new humanoid species or creating a magical effect that covers an entire region.
These efforts are a step beyond ordinary impossibility, as they often require measures entirely beyond the physical capabilities of normal civilizations.

While exceedingly difficult, such projects are not out of the question for powerful mages who have the help of skilled adventurers.
They do require a few more steps than an ordinary project would require, however.

The heroes must create one or more Workings dedicated to enabling the change, using the guidelines given in the Magic chapter.
The scope of these Workings must be large enough to affect the scope of the change itself; if the alteration is to be done to an entire region, then a region-sized Working must be built.
Workings so large as to affect an entire kingdom are beyond the scope of modern magic, and only some special quest into the fathomless past could discover the keys to grand, world-spanning alterations.

The degree of the Working will depend on the degree of the change.
The devising of a race of humanoid creatures similar to humans but cosmetically different might be a Minor Working, while more substantial alterations will require great degrees of power.
The summoning of a river from the depths of the earth might be a Major change for a small stream, while something the size of the Amazon might be of Supreme difficulty.

## 7.8.0 Major Projects and Existing Factions

Players are likely to end up with goals or ambitions that directly involve them with local factions or potentially touch on Assets or other resources significant to faction powers.
This is normal, and it’s not difficult to integrate the two systems when they happen to touch.

As a general rule, major projects should be treated just as adventures would be.
When a project would plausibly damage a faction’s Assets, then the Assets will be damaged or destroyed.
When they would create an Asset useful for a faction, whether one belonging to the players or to another group, then the Asset is created.
If a faction doesn’t care for a project, it might turn into a source of opposition that must be quelled or overcome, while an allied faction might supply some portion of the Renown itself by taking an action to aid the PCs.

The help of a faction should be scaled by the GM; if an empire decides to give the PCs a castle, then it might be such a minor part of the faction’s holding that no Treasure expense or other effort is required to do so.
A small religious cult that wants to help build a monastery for the PCs might not be able to give nearly as much help, and might simply be good for a quarter of the Renown needed if they spend an action assisting the PCs.
Conversely, when a faction is opposed to some measure, the PCs will probably have to undertake an adventure to change its mind or pull the fangs that it’s using to interfere with their efforts.

If the magical change is impossible but relatively modest in scope, then one great Working will be necessary to empower it.
If the change is significant and will have major repercussions on the future area, it will take two, while a change that seems barely within the limits of possibility will need three Workings to support it, all of the appropriate degree and scope.
The construction of these Workings often require adventures in their own right to find the critical components or esoteric substances needed to erect them, to say nothing of the material cost of the work.

If these Workings are later destroyed or corrupted the change itself may be damaged as well.
Sometimes the effect is so graven on the world that it continues unsupported, but other times the change fades away into something more mundane.
In the worst cases, the magic goes rampant and terrible consequences are born from its uncontrolled fury.
As a consequence, most nations are highly averse to the construction of large-scale magical infrastructure, even when they can afford to do so.

## 7.9.0 Player-Run Factions and Major Projects

PCs who have the friendship or control of factions can leverage them to assist in their grand plans.
A faction can assist on a project only once per faction turn, and this help counts as its action for the turn.

When a faction helps, it spends one point of Treasure and decreases the difficulty of the project by the sum total of its Wealth, Force, and Cunning attributes, down to a minimum difficulty of 1 point.
The faction can’t usually complete a major project on its own; it needs the PC or some driving personality to envision and implement the plan.
A faction needs to spend Treasure and help only once on a project to decrease the difficulty.
The difficulty reduction remains until the project is complete or the faction chooses to withdraw its support for some reason.
More than one faction can contribute its help, if they can be persuaded.

If the faction is ideally suited to the project, such as a government establishing a new political order, or a religion instituting a new cultural norm, or a thieves’ guild forming a cabal of assassins, then their attribute total is doubled for purposes of calculating the new difficulty.

If the faction is willing or forced to go to extremes in helping a project, either out of desperation or the ruthless demands of its leadership, it can commit its Assets and own institutional health to the project.
Any Asset or Base of Influence in the same location as the project can accept hit point damage to lower the difficulty; each hit point they spend lowers it by one point.
This kind of commitment is difficult to calibrate safely; at the end of the spend, each Asset or Base of Influence that contributed suffers an additional 1d4 damage.
This may be enough to destroy an Asset, or even destroy the faction itself if enough damage is done to a Base of Influence.
If the Asset is destroyed in the process of helping the project, the fallout of its collapse or exhaustion may have local consequences of its own.

If the faction is of a vastly larger scale than the project, such as a prosperous kingdom helping to construct a new border village, the entire project can be resolved in a single action.
In some cases, the GM may not even require the faction to spend any Treasure, as the expense is so small relative to its resources that it’s not worth tracking.
