[Go back to index](../index.html)

# Cities Without Number SRD

This is a cleaned-up version of the WWN SRD.
You can find the original in PDF, as well as in text, here: [https://www.drivethrurpg.com/en/product/452790/cities-without-number-system-reference-document](https://www.drivethrurpg.com/en/product/452790/cities-without-number-system-reference-document).

**This document is based on version 1.0**.

The document has the same license as the SRD (as per itself written in the document) and includes adapted text, to text/Markdown, from the original SRD by Sine Nomine Publishing.
Any mistake in the document is probably mine and not Kevin's or anyone else's.

The text was adapted mostly from the PDF, with small adaptations taken from the text version where it made sense.
No changes that affect the rules (or flavour) have been found, though (e.g. the text version has some columns in tables, or tables themselves, missing, but these are just summary column/tables).

# Links

- [Markdown version](srd.md)
- [HTML (one page)](srd.html)
- [HTML (several pages)](srd/index.html)


# Corrections

Feel free to [create issues](https://gitlab.com/blulaktuko-rpg/rpg-blulaktuko-net/-/issues) (you'll need a free GitLab account), new Pull Requests, or contact me in case you find errors.
You can also contact me writing an email to `rpg at blulaktuko.net`.

The text was partially automatically transformed from the original PDF to text, and partially taken from the text version, and then cleaned-up using some automations.
It's very probable there's errors here and there.

# Resources and Support

The [CWN subreddit](https://www.reddit.com/r/cwn) is a good starting place to check into the system.

I'd encourage you to support the game and the community.
A way of doing so is buying products from the publisher, Sine Nomine Publishing:

- [Directly](https://sine-nomine-publishing.myshopify.com/)
- At [DrivethruRPG](https://www.drivethrurpg.com/browse/pub/3482/Sine-Nomine-Publishing)
- Follow them at [Kickstarter](https://www.kickstarter.com/profile/sinenomineinc), where sometimes projects are funded
